if [ "$1" = "dev" ]
then
  echo 'DEV: BUILDING PROJECT INTO WWW/'
  ionic build --configuration=development

  echo 'DEPLOYING'
  firebase deploy --only hosting:dev

elif [ "$1" = "prod" ]
then
  echo 'PROD: BUILDING PROJECT INTO WWW/'
  ionic build --configuration=production

  echo 'DEPLOYING'
  firebase deploy --only hosting:prod

else
  echo "First parameter must be dev or prod"

fi
