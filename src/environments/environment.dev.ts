export const environment = {
  apiUrl: 'https://back.dev-web-retail-vr.com',
  apiUrl2: 'https://back.dev-web-retail-vr.com/_plugin/kuzzle-app/',
  gcUrl: 'https://storage.googleapis.com/rvr_smartobjects_dev',
  production: false,
  parentUrl: 'https://dev-web-retail-vr.ew.r.appspot.com/',
  firebaseConfig: {
    apiKey: "AIzaSyANJWOvRY-BaZaSWS6U0Ffq_A6Fol4U1nM",
    authDomain: "retail360-fe8e3.firebaseapp.com",
    databaseURL: "https://retail360-fe8e3.firebaseio.com",
    projectId: "retail360-fe8e3",
    storageBucket: "retail360-fe8e3.appspot.com",
    messagingSenderId: "646714558581",
    appId: "1:646714558581:web:dd10638b1f26d4d85f903c",
    measurementId: "G-0YE1JFNGFK"
  }
};
