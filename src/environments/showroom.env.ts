import {BehaviorSubject} from 'rxjs';
import {Showroom} from "../app/models/showroom.model";

class ShowroomEnv {
    hideWishlist = false;
    sendEndExperienceWishlist = false;
    fixturesManager = false;
    private = false;
    api = undefined;
    apiToken = undefined;
    initialized = new BehaviorSubject(false);
    showroom: Showroom = undefined;

    constructor() {
    }
}

export const showroomEnv = new ShowroomEnv();
