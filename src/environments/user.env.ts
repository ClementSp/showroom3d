import {BehaviorSubject} from 'rxjs';

class UserEnv {

    jwt: string = null;
    expiresAt: number;
    callId: string = null;
    shopifyc: string = null;
    customer: string = null;
    tenant: string = null;
    showroomId: string = null;
    sessionId: string = null;
    toolbar: string = null;
    roles: Array<string> = [];
    loginType: 'platform' | 'credentials' = null;
    authorization = false;
    currentLng = 'en';
    defaultLanguage = 'fr';

    initialized = new BehaviorSubject<boolean>(false);

    constructor() {
    }

    isPersonalShopper() {
        const roles = this.roles ?? [];

        if (roles.length === 0) {
            return false;
        }

        return roles.find((r: string) => r === this.tenant + '-profile-personal-shopper');
    }
}

export const userEnv = new UserEnv();
