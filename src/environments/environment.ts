// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiUrl: 'https://back.dev-web-retail-vr.com/',
  // apiUrl2: 'https://back.dev-web-retail-vr.com/_plugin/kuzzle-app/',
  apiUrl: 'https://api.retail-vr.com/',
  apiUrl2: 'https://api.retail-vr.com/_plugin/kuzzle-app/',
  // apiUrl: 'http://localhost:7512/',
  // apiUrl2: 'http://localhost:7512/_plugin/kuzzle-app/',
  gcUrl: 'https://storage.googleapis.com/rvr_smartobjects_dev',
  parentUrl: 'http://localhost:4200',
  firebaseConfig: {
    apiKey: "AIzaSyANJWOvRY-BaZaSWS6U0Ffq_A6Fol4U1nM",
    authDomain: "retail360-fe8e3.firebaseapp.com",
    databaseURL: "https://retail360-fe8e3.firebaseio.com",
    projectId: "retail360-fe8e3",
    storageBucket: "retail360-fe8e3.appspot.com",
    messagingSenderId: "646714558581",
    appId: "1:646714558581:web:a6e5d35863a85cac5f903c",
    measurementId: "G-G8X0B3WSJ3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
