import {ShowroomRight} from '../app/services/customer.service';

class CustomerEnv {

    country = 'France';
    currency = 'EUR';
    email: string = undefined;
    name: string = undefined;
    showPrice = true;
    showroomRights = new Array<ShowroomRight>();

    constructor() {
    }
}

export const customerEnv = new CustomerEnv();
