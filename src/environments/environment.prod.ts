export const environment = {
  apiUrl: 'https://api.retail-vr.com/',
  apiUrl2: 'https://api.retail-vr.com/_plugin/kuzzle-app/',
  gcUrl: 'https://storage.googleapis.com/rvr_smartobjects',
  production: true,
  parentUrl: 'https://my.retail-vr.com',
  firebaseConfig: {
    apiKey: "AIzaSyANJWOvRY-BaZaSWS6U0Ffq_A6Fol4U1nM",
    authDomain: "retail360-fe8e3.firebaseapp.com",
    databaseURL: "https://retail360-fe8e3.firebaseio.com",
    projectId: "retail360-fe8e3",
    storageBucket: "retail360-fe8e3.appspot.com",
    messagingSenderId: "646714558581",
    appId: "1:646714558581:web:a6e5d35863a85cac5f903c",
    measurementId: "G-G8X0B3WSJ3"
  }
};
