import Values from 'values.js';

function applyIonicStyle(element: HTMLElement, fieldName: string, color: string) {
    const value = new Values(color);

    return applyColorToStyle(element, fieldName, value);
}

function applyIonicTextStyleForColor(element: HTMLElement, fieldName: string, color: string) {
    const value = new Values(color);
    const contrastIsLight = (value.getBrightness() < 50);

    return applyColorToStyle(element, fieldName, (contrastIsLight) ? new Values('#ffffff') : new Values('#000000'));
}

function applyColorToStyle(element: HTMLElement, fieldName: string, value: Values) {
    const contrastIsLight = (value.getBrightness() > 0.5);

    element.style.setProperty(`--ion-color-${fieldName}`, value.hexString());
    element.style.setProperty(`--ion-color-${fieldName}-rgb`, value.rgb);
    element.style.setProperty(`--ion-color-${fieldName}-contrast`, (contrastIsLight) ? value.tint(100).hexString() : value.shade(100).hexString());
    element.style.setProperty(`--ion-color-${fieldName}-contrast-rgb`, (contrastIsLight) ? value.tint(100).rgb : value.shade(100).rgb);
    element.style.setProperty(`--ion-color-${fieldName}-shade`, value.shade(12.5).hexString());
    element.style.setProperty(`--ion-color-${fieldName}-tint`, value.tint(10).hexString());
}

function isColorHexValid(color: string) {
    const parsedColor = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);

    return !(!parsedColor || parsedColor.length < 4);
}

function isLightColor(color: string) {
    const value = new Values(color);

    return (value.getBrightness() < 50);
}

export const StyleHelper = {
    applyIonicStyle,
    isColorHexValid,
    applyIonicTextStyleForColor,
    isLightColor
};
