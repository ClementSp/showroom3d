import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {RouterService} from '../services/router.service';

@Injectable({
  providedIn: 'root'
})
export class IsAccessedByRouterGuard implements CanActivate {

  constructor(private routerService: RouterService) {
  }

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.routerService.url === '/') {
      return this.routerService.parseUrl('/loading');
    }
    return true;
  }
}
