import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Showroom} from '../models/showroom.model';
import {RouterService} from '../services/router.service';
import {userEnv} from '../../environments/user.env';
import {ShowroomsService} from '../services/showrooms.service';
import {showroomEnv} from '../../environments/showroom.env';

@Injectable({
    providedIn: 'root'
})
export class LoginPageGuard implements CanActivate {

    constructor(private routerService: RouterService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (showroomEnv.private) {
            const authState = JSON.parse(localStorage.getItem('authState'));
            if (userEnv.authorization) {
                return this.routerService.parseUrl('/loading');
            } else if (authState && authState.jwt && (authState.expiresAt > new Date().getTime())) {
                userEnv.jwt = authState.jwt;
                userEnv.expiresAt = authState.expiresAt;
                return this.routerService.parseUrl('/loading');
            } else {
                return true;
            }
        }
        // if (showroomEnv.private) {
        //     if (userEnv.authorization) {
        //         return this.routerService.parseUrl('/loading');
        //     } else {
        //         return true;
        //     }
        // }
        return this.routerService.parseUrl('/loading');
    }
}
