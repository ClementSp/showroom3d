import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {RouterService} from '../services/router.service';
import {userEnv} from '../../environments/user.env';
import {showroomEnv} from '../../environments/showroom.env';
import {filter, map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PrivateOnlyGuard implements CanActivate {

    constructor(private routerService: RouterService) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return showroomEnv.initialized.pipe(
            filter(isInitialized => isInitialized),
            map((isInitialized => {
                const authState = JSON.parse(localStorage.getItem('authState'));
                if (!userEnv.authorization && showroomEnv.private && !(authState && authState.jwt && (authState.expiresAt > new Date().getTime()))) {
                    return this.routerService.parseUrl('/auth');
                }
                if (authState && authState.jwt && (authState.expiresAt > new Date().getTime())) {
                    userEnv.jwt = authState.jwt;
                    userEnv.expiresAt = authState.expiresAt;
                }
                return true;
            }))
        );
    }
}
