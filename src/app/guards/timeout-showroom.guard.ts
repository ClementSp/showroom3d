import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import {Observable} from 'rxjs';
import {CustomerService, ShowroomRight} from '../services/customer.service';
import {RouterService} from '../services/router.service';
import {customerEnv} from '../../environments/customer.env';
import {userEnv} from '../../environments/user.env';

@Injectable({
    providedIn: 'root'
})
export class TimeoutShowroomGuard implements CanActivate {
    constructor(private customerService: CustomerService,
                private routerService: RouterService) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (customerEnv?.showroomRights) {
            const right: ShowroomRight = customerEnv?.showroomRights.find(r => r.showroomId === userEnv.showroomId);

            if (right?.validendAt) {
                const validity = new Date(right.validendAt);

                if (this.isPastDate(validity)) {
                    return this.routerService.parseUrl('timeout');
                }
            }
            return true;
        } else {
            return true;
        }
    }

    isPastDate(validity: Date) {
        const toDay = new Date();
        return toDay.getFullYear() > validity.getFullYear() ||
            (toDay.getFullYear() === validity.getFullYear() && toDay.getMonth() > validity.getMonth()) ||
            (toDay.getFullYear() === validity.getFullYear() &&
                toDay.getMonth() === validity.getMonth() &&
                toDay.getDay() > validity.getDay());
    }
}
