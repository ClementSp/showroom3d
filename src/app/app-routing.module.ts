import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {IsAccessedByRouterGuard} from './guards/is-accessed-by-router.guard';
import {PrivateOnlyGuard} from './guards/private-only.guard';
import {LoginPageGuard} from './guards/login-page.guard';
import {TimeoutShowroomGuard} from './guards/timeout-showroom.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./views/home/home.module').then(m => m.HomePageModule),
    canActivate: [
      IsAccessedByRouterGuard,
      PrivateOnlyGuard,
      TimeoutShowroomGuard
    ]
  },
  {
    path: 'error',
    loadChildren: () => import('./views/error/error-page.module').then(m => m.NotFoundPageModule)
  },
  {
    path: 'timeout',
    loadChildren: () => import('./views/timeout/timeout-page.module').then(m => m.TimeOutPageModule)
  },
  {
    path: 'loading',
    loadChildren: () => import('./views/home/home.module').then(m => m.HomePageModule),
    canActivate: [
        PrivateOnlyGuard, TimeoutShowroomGuard
    ]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: () => import('./views/auth/auth.module').then(m => m.AuthPageModule),
    canActivate: [LoginPageGuard, TimeoutShowroomGuard],
    canDeactivate: []
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
