import {Component, OnDestroy, OnInit} from '@angular/core';
import {RouterService} from '../../services/router.service';
import {Showroom} from '../../models/showroom.model';
import {ShowroomsService} from '../../services/showrooms.service';
import {showroomEnv} from "../../../environments/showroom.env";

@Component({
  selector: 'app-timeout-page',
  templateUrl: './timeout-page.component.html',
  styleUrls: ['./timeout-page.component.scss'],
})
export class TimeoutPage implements OnInit, OnDestroy {

  backgroundImageUrl = '';

  constructor(private router: RouterService,
              private showroomService: ShowroomsService) {
  }

  ngOnInit() {
    const showroom = showroomEnv.showroom;
    if (showroom.settings.components.welcomePage.backgroundUrl?.length === 0) {
      this.backgroundImageUrl = undefined;
    } else {
      this.backgroundImageUrl = showroom.settings.components.welcomePage.backgroundUrl;
    }
  }

  ngOnDestroy(): void {
  }
}
