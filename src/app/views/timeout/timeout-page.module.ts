import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {TimeoutPageRoutingModule} from './timeout-page-routing.module';
import {TimeoutPage} from './timeout-page.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeoutPageRoutingModule,
    TranslateModule
  ],
  declarations: [TimeoutPage]
})
export class TimeOutPageModule {}
