import { Component, OnInit } from '@angular/core';
import {RouterService} from '../../services/router.service';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss'],
})
export class ErrorPage implements OnInit {

  message = 'Shop not found.';

  constructor(private router: RouterService) {
    const extras = this.router.getCurrentNavigation().extras;

    if (extras && extras.state && extras.state.message) {
      this.message = extras.state.message;
    }
  }

  ngOnInit() {
  }

}
