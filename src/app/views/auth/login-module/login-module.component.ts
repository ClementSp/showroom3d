import {Component, EventEmitter, HostListener, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RouterService} from '../../../services/router.service';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-login-module',
  templateUrl: './login-module.component.html',
  styleUrls: ['./login-module.component.scss'],
})
export class LoginModuleComponent implements OnInit, OnDestroy {

  @Output() error = new EventEmitter<string>();

  formGroup: FormGroup;
  idControl: FormControl;
  pwdControl: FormControl;

  readonly errorMessage = 'Wrong username/password combination';

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private translateService: TranslateService,
              private routerService: RouterService) {
    this.idControl = new FormControl('', Validators.required);
    this.pwdControl = new FormControl('', Validators.required);
    this.formGroup = this.formBuilder.group({
      username: this.idControl,
      password: this.pwdControl
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  login() {
    const credentials = this.formGroup.getRawValue();
    const message = this.translateService.instant(this.errorMessage);

    credentials.from = 'credentials';
    this.userService.login(credentials).then((resp) => {
          if (resp.jwt) {
            this.error.emit(null);
            this.routerService.navigateByUrl('loading');
          } else {
            this.error.emit(message);
          }
        },
        () => {
          this.error.emit(message);
        });
  }

  @HostListener('document:keyup', ['$event'])
  onKeyDown(event: any) {
    const key = event.key;

    if (key === 'Enter') {
      this.login();
    }
  }
}
