import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AuthPageRoutingModule } from './auth-routing.module';
import { AuthPage } from './auth.page';
import {LoginModuleComponent} from './login-module/login-module.component';
import {TranslateModule} from '@ngx-translate/core';
import {RegisterModuleComponent} from './register-module/register-module.component';
import {NgxFlagIconCssModule} from 'ngx-flag-icon-css';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AuthPageRoutingModule,
        TranslateModule,
        ReactiveFormsModule,
        NgxFlagIconCssModule,
    ],
    declarations: [AuthPage, LoginModuleComponent, RegisterModuleComponent]
})
export class AuthPageModule {}
