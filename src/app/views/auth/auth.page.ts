import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Showroom} from '../../models/showroom.model';
import {ShowroomsService} from '../../services/showrooms.service';
import {IonSlides} from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';
import {ModulesService} from '../../services/modules.service';
import {userEnv} from '../../../environments/user.env';
import {showroomEnv} from "../../../environments/showroom.env";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('authSlides') slides: IonSlides;

  registrationModuleName = 'Registration';
  registrationEnabled = false;

  errorMessage = new BehaviorSubject<string>(null);
  backgroundImageUrl = '';

  constructor(private showroomService: ShowroomsService,
              private moduleService: ModulesService) {
    this.getRegistrationState();
    if (showroomEnv.showroom.settings.components.welcomePage.backgroundUrl.length === 0) {
      this.backgroundImageUrl = undefined;
    } else {
      this.backgroundImageUrl = showroomEnv.showroom.settings.components.welcomePage.backgroundUrl;
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.cleanErrorMessageOnSlide();
  }

  onLoginSuccess($event: any) {
    this.slides.slideTo(0);
  }

  cleanErrorMessageOnSlide() {
    this.slides.ionSlideWillChange.subscribe(() => this.error(null));
  }

  error(msg: string) {
    this.errorMessage.next(msg);
  }

  ngOnDestroy(): void {
  }

  private getRegistrationState() {
    this.moduleService.getConfig(this.registrationModuleName).then((config) => {
      if (config) {
        this.registrationEnabled = config.enabled;
      }
    });
  }
}
