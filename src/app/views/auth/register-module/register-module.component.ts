import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../../services/user.service';
import {userEnv} from '../../../../environments/user.env';
import {ModalController} from '@ionic/angular';
import {CountryPickerComponent} from '../../../components/country-picker/country-picker.component';
import {Countries, Country} from '../../../components/country-picker/data/Countries';

@Component({
  selector: 'app-register-module',
  templateUrl: './register-module.component.html',
  styleUrls: ['./register-module.component.scss'],
})
export class RegisterModuleComponent implements OnInit, AfterViewInit {
  @Output() error = new EventEmitter<string>();
  @Output() success = new EventEmitter<void>();

  formGroup: FormGroup;

  firstnameControl: FormControl;
  emailControl: FormControl;
  countryControl: FormControl;
  zipCodeControl: FormControl;
  pwdControl: FormControl;
  cciRGPD = 'La CCI Paris Île-de-France collecte ces informations pour traiter votre demande de mise en relation. Vos coordonnées ne peuvent être transmises exclusivement qu\'aux interlocuteurs de votre choix sur ce formulaire.\n' +
      'Vos données sont conservées tant que vous ne demandez pas la suppression. Les informations ne sont pas communiquées à des tiers.\n' +
      'Conformément à la loi « Informatique et Libertés » du 6 janvier 1978, modifiée et au Règlement (UE) 2016-679 sur la protection des données, dans le cadre et les limites de ces textes, vous disposez d\'un droit d\'accès, de modification, de rectification, d\'opposition et de suppression relatif aux données vous concernant.\n' +
      'Ces droits s\'exercent auprès du webmestre ou, en cas de difficulté, auprès du délégué à la protection des données à l\'adresse cpdp@cci-paris-idf.fr. En dernier lieu, vous pouvez déposer une réclamation auprès de la CNIL, 3 Place de Fontenoy - TSA 80715 - 75334 PARIS CEDEX 07.';
  isCCI = false;

  readonly countries = Countries;
  readonly defaultCountry = this.countries.find(c => c.name === 'France');
  readonly defaultMessage = 'Fail to register your account.';
  readonly emailAlreadyExist = 'Email address already in use.';

  constructor(private formBuilder: FormBuilder,
              private modalController: ModalController,
              private userService: UserService,
              private translateService: TranslateService) {
    this.firstnameControl = new FormControl(undefined, Validators.required);
    this.emailControl = new FormControl(undefined, Validators.required);
    this.countryControl = new FormControl(this.defaultCountry, Validators.required);
    this.zipCodeControl = new FormControl(undefined, Validators.required);
    this.pwdControl = new FormControl(undefined, Validators.required);
    this.formGroup = this.formBuilder.group({
      firstname: this.firstnameControl,
      email: this.emailControl,
      country: this.countryControl,
      zipcode: this.zipCodeControl,
      password: this.pwdControl,
    });
  }

  ngOnInit() {
    if (userEnv.tenant === 'cci') {
      this.isCCI = true;
    }
  }

  ngAfterViewInit(): void {
  }

  register() {
    const data = this.getFormattedRegisterData();

    this.userService.register(data)
        .then(() => this.successfulRegistration())
        .catch((err) => this.unsuccessfulRegistration(err));
  }

  async openCountryPicker() {
    const modal = await this.modalController.create({
      component: CountryPickerComponent
    });
    modal.onDidDismiss().then((result) => {
      const country = result?.data?.country;

      if (country) {
        this.countryControl.setValue(country);
      }
    });
    return modal.present();
  }

  private successfulRegistration() {
    this.success.emit();
  }

  private unsuccessfulRegistration(err) {
    const code = err.error.error.code;
    // tslint:disable-next-line:no-bitwise
    const emailAlreadyExit = (errCode) => errCode & 0x1;
    let message = this.translateService.instant(this.defaultMessage);

    if (emailAlreadyExit(code)) {
      message = this.translateService.instant(this.emailAlreadyExist);
    }

    this.error.emit(message);
  }

  private getFormattedRegisterData() {
    return {
      showroomId: userEnv.showroomId,
      user: {
        credentials: {
          local: {
            password: this.pwdControl.value,
            username: this.emailControl.value
          }
        },
        content: {
          firstname: this.firstnameControl.value,
          localisation: {
            country: {
              name: this.countryControl.value.name,
              code: this.countryControl.value.alpha2
            },
            zipcode: this.zipCodeControl.value,
          },
          email: this.emailControl.value
        }
      }
    };
  }
}
