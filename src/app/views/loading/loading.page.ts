import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ShowroomsService} from '../../services/showrooms.service';
import {RouterService} from '../../services/router.service';
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('150ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [animate('150ms', style({ opacity: 0 }))]),
    ])]
})
export class LoadingPage implements OnInit, OnDestroy {

  @Input() percent: any;
  // showroomSubscription: Subscription;
  // interval: any;
  // value = 0;

  constructor(private showroomService: ShowroomsService,
              private router: RouterService) {
    setTimeout(() => {}, 1000);
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    // this.showroomSubscription.unsubscribe();
  }
}
