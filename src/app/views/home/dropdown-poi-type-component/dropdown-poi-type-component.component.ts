import { Component, OnInit } from '@angular/core';
import {PopoverController} from "@ionic/angular";

@Component({
  selector: 'app-dropdown-poi-type-component',
  templateUrl: './dropdown-poi-type-component.component.html',
  styleUrls: ['./dropdown-poi-type-component.component.scss'],
})
export class DropdownPoiTypeComponentComponent implements OnInit {


  constructor(private popoverController: PopoverController) { }

  ngOnInit() {}

  onSelection(selection: string) {
    return this.popoverController.dismiss(selection);
  }

}

