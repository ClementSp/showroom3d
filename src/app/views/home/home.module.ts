import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HomePage } from './home.page';
import { HomePageRoutingModule } from './home-routing.module';
import { EngineComponent } from '../../components/engine/engine.component';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component';
import { EngineModule } from '../../components/engine/engine.module';
import { ToolbarComponent } from '../../components/toolbar/toolbar.component';
import { EditMarkerComponent } from '../../components/edit-marker/edit-marker.component';
import { ClusterFormComponent } from '../../components/edit-marker/cluster-form/cluster-form.component';
import { ProductRowComponent } from '../../components/edit-marker/cluster-form/product-row/product-row.component';
import { VideoFormComponent } from '../../components/edit-marker/video-form/video-form.component';
import { DescriptorFormComponent } from '../../components/edit-marker/descriptor-form/descriptor-form.component';
import { HelpPopoverComponent } from '../../components/help-popover/help-popover.component';
import { HelpPopoverDirective } from '../../directives/help-popover.directive';
import {TranslateModule} from '@ngx-translate/core';
import {NewMenuItemFormComponent} from '../../components/side-menu/new-menu-item-form/new-menu-item-form.component';
import {VideoPresentationFormComponent} from '../../components/side-menu/video-presentation-form/video-presentation-form.component';
import {EditorViewOnlyDirective} from '../../directives/editor-view-only.directive';
import {ClientViewOnlyDirective} from '../../directives/client-view-only.directive';
import {ConfirmationModalComponent} from '../../components/confirmation-modal/confirmation-modal.component';
import {ProductFormComponent} from '../../components/edit-marker/product-form/product-form.component';
import {LogoManagerComponent} from '../../components/side-menu/logo-manager/logo-manager.component';
import {FileUploadModule} from 'ng2-file-upload';
import {InputErrorDirective} from '../../directives/input-error.directive';
import {CustomProductFormComponent} from '../../components/edit-marker/custom-product-form/custom-product-form.component';
import {ProductFinderComponent} from '../../components/product-finder/product-finder.component';
import {ClusterOfCustomProductFormComponent} from '../../components/edit-marker/cluster-of-custom-product-form/cluster-of-custom-product-form.component';
import {WishlistComponent} from '../../components/wishlist/wishlist.component';
import {ProductLineComponent} from '../../components/wishlist/product-line/product-line.component';
import {CreateProductComponent} from '../../components/edit-marker/cluster-of-custom-product-form/create-product/create-product.component';
import {CustomProductComponent} from '../../components/edit-marker/custom-product/custom-product.component';
import {CustomProductPageV2Component} from '../../components/custom-product-page-v2/custom-product-page-v2.component';
import {QuantityButtonComponent} from '../../components/custom-product-page-v2/quantity-button/quantity-button.component';
import {JitsiB2BComponent} from '../../components/modules/jitsi-b2-b/jitsi-b2-b.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {SettingsCallComponent} from '../../components/modules/jitsi-b2-b/settings-call/settings-call.component';
import {VideoCallComponent} from '../../components/modules/jitsi-b2-b/video-call/video-call.component';
import {CallOverlayComponent} from '../../components/modules/jitsi-b2-b/call-overlay/call-overlay.component';
import {ProductDetails} from '../../components/product-details/product-details';
import {PortalSettingsComponent} from '../../components/portal-settings/portal-settings.component';
import {PdfFormComponent} from '../../components/edit-marker/pdf-form/pdf-form.component';
import {RgpdModuleComponent} from '../../components/modules/rgpd-module/rgpd-module.component';
import {BarcodeScannerModalComponent} from '../../components/edit-marker/custom-product-form/barcode-scanner-modal/barcode-scanner-modal.component';
import {EditorSettingsComponent} from '../../components/editor-settings/editor-settings.component';
import {ObjectImporterModule} from '../../components/modules/objects/object-importer.module';
import {Template1Component} from '../../components/custom-product-page-v2/templates/template1/template-1.component';
import {Template2Component} from '../../components/custom-product-page-v2/templates/template2/template-2.component';
import {Template3Component} from '../../components/custom-product-page-v2/templates/template3/template-3.component';
import {VideoRowComponent} from '../../components/edit-marker/video-form/video-row/video-row.component';
import {PdfRowComponent} from '../../components/edit-marker/pdf-form/pdf-row/pdf-row.component';
import {CustomProductMediaFormComponent} from '../../components/edit-marker/custom-product-media-form/custom-product-media-form.component';
import {CustomProductMainFormComponent} from '../../components/edit-marker/custom-product-main-form/custom-product-main-form.component';
import {SharedModule} from '../../shared.module';
import {ImageViewerModalComponent} from '../../components/image-viewer-modal/image-viewer-modal.component';
import {MapComponent} from '../../components/map/map.component';
import {ItemListComponent} from '../../components/side-menu/item-list/item-list.component';
import {ProductUrlFinderComponent} from '../../components/product-url-finder/product-url-finder.component';
import {FirstStepComponent} from '../../components/custom-product-page-v2/templates/cci/first-step/first-step.component';
import {CCIComponent} from '../../components/custom-product-page-v2/templates/cci/second-step/cci.component';
import {FlexModule} from '@angular/flex-layout';
import {MiniaturesComponent} from '../../components/custom-product-page-v2/templates/cci/second-step/miniatures/miniatures.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import {CciFeatureComponent} from '../../components/custom-product-page-v2/templates/cci/first-step/features/feature-component/cci-feature.component';
import {CciFavoriteFeatureComponent} from '../../components/custom-product-page-v2/templates/cci/first-step/features/favorite-feature-component/cci-favorite-feature.component';
import {ContactFormComponent} from '../../components/custom-product-page-v2/templates/cci/contact-form/contact-form.component';
import {CountryPickerComponent} from '../../components/country-picker/country-picker.component';
import {NgxFlagIconCssModule} from 'ngx-flag-icon-css';
import {ImageFormComponent} from '../../components/edit-marker/image-form/image-form.component';
import {ImageRowPreviewComponent} from '../../components/edit-marker/image-form/image-row-preview/image-row-preview.component';
import {PortalEditionModalComponent} from '../../components/portal-edition-modal/portal-edition-modal.component';
import {ShowroomPortalComponent} from '../../components/portal-edition-modal/showroom-portal/showroom-portal.component';
import {SweepPortalComponent} from '../../components/portal-edition-modal/sweep-portal/sweep-portal.component';
import {SweepSelectorComponent} from '../../components/portal-edition-modal/sweep-selector/sweep-selector.component';
import {FirstPremiumStepComponent} from '../../components/custom-product-page-v2/templates/cci/first-premium-step/first-premium-step.component';
import {CCIPremiumComponent} from '../../components/custom-product-page-v2/templates/cci/second-premium-step/cci-premium.component';
import {LinkMenuComponent} from '../../components/link-menu/link-menu.component';
import {LinkListComponent} from '../../components/link-menu/link-list/link-list.component';
import {NewMenuLinkItemComponent} from '../../components/link-menu/new-menu-link-item-form/new-menu-link-item.component';
import {ChatFormComponent} from '../../components/edit-marker/chat-form/chat-form.component';
import {MatSliderModule} from '@angular/material/slider';
import {DropdownPoiTypeComponentComponent} from './dropdown-poi-type-component/dropdown-poi-type-component.component';
import {PoiColorPickerComponent} from '../../components/edit-marker/poi-color-picker/poi-color-picker.component';
import {PoiSpritePickerComponent} from "../../components/edit-marker/poi-color-picker/poi-sprite-picker/poi-sprite-picker.component";
import {TemplateFullPicturesComponent} from "../../components/custom-product-page-v2/templates/templateFullPicture/template-full-pictures.component";
import {ObjectConfiguratorModule} from "../../components/object-configurator/object-configurator.module";
import {LoadingPageModule} from "../loading/loading.module";
import {ZoomBarComponent} from "../../components/zoom-bar/zoom-bar.component";
import {PaymentComponent} from "../../components/wishlist/payement/payment.component";
import {ClientEditorOnlyDirective} from '../../directives/client-editor-only.directive';
import {ShareShowroomComponent} from "../../components/share-showroom/share-showroom.component";

@NgModule({
    imports: [
        DragDropModule,
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        EngineModule,
        ReactiveFormsModule,
        TranslateModule,
        FileUploadModule,
        ObjectImporterModule,
        SharedModule,
        FlexModule,
        NgxImageZoomModule,
        NgxFlagIconCssModule,
        MatSliderModule,
        ObjectConfiguratorModule,
        LoadingPageModule,
    ],
    declarations: [
        HomePage,
        EngineComponent,
        SideMenuComponent,
        DropdownPoiTypeComponentComponent,
        ToolbarComponent,
        EditMarkerComponent,
        ClusterFormComponent,
        PoiSpritePickerComponent,
        ProductRowComponent,
        VideoFormComponent,
        DescriptorFormComponent,
        VideoRowComponent,
        HelpPopoverDirective,
        EditorViewOnlyDirective,
        ClientViewOnlyDirective,
        ClientEditorOnlyDirective,
        InputErrorDirective,
        ConfirmationModalComponent,
        HelpPopoverComponent,
        LogoManagerComponent,
        NewMenuItemFormComponent,
        ProductFormComponent,
        ProductFinderComponent,
        ProductUrlFinderComponent,
        VideoPresentationFormComponent,
        CustomProductFormComponent,
        ClusterOfCustomProductFormComponent,
        WishlistComponent,
        PaymentComponent,
        ProductLineComponent,
        CreateProductComponent,
        CustomProductComponent,
        CustomProductPageV2Component,
        Template1Component,
        Template2Component,
        Template3Component,
        TemplateFullPicturesComponent,
        QuantityButtonComponent,
        ProductDetails,
        PortalSettingsComponent,
        ChatFormComponent,
        PdfFormComponent,
        PdfRowComponent,
        JitsiB2BComponent,
        SettingsCallComponent,
        VideoCallComponent,
        CallOverlayComponent,
        ProductDetails,
        RgpdModuleComponent,
        ShareShowroomComponent,
        BarcodeScannerModalComponent,
        EditorSettingsComponent,
        CustomProductMediaFormComponent,
        CustomProductMainFormComponent,
        MapComponent,
        ImageViewerModalComponent,
        ItemListComponent,
        NewMenuLinkItemComponent,
        FirstStepComponent,
        FirstPremiumStepComponent,
        CCIComponent,
        CCIPremiumComponent,
        MiniaturesComponent,
        CciFeatureComponent,
        CciFavoriteFeatureComponent,
        ContactFormComponent,
        CountryPickerComponent,
        ImageFormComponent,
        ImageRowPreviewComponent,
        PortalEditionModalComponent,
        ShowroomPortalComponent,
        SweepPortalComponent,
        SweepSelectorComponent,
        ImageRowPreviewComponent,
        LinkMenuComponent,
        LinkListComponent,
        PoiColorPickerComponent,
        ZoomBarComponent
    ],
    providers: [
    ],
    exports: [
        EditorViewOnlyDirective,
        HelpPopoverDirective
    ]
})
export class HomePageModule {
}
