import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Showroom} from '../../models/showroom.model';
import {BehaviorSubject, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import * as firebase from 'firebase/app';
import {ModalController, Platform, PopoverController} from '@ionic/angular';
import {EngineComponent} from '../../components/engine/engine.component';
import EngineEvent from '../../models/EngineEvent';
import {EditorService} from '../../services/editor.service';
import {EditorMode} from '../../models/EditorMode';
import {TranslateService} from '@ngx-translate/core';
import {WishlistHandler} from '../../DataHandlers/Wishlist.handler';
import {CustomerService} from '../../services/customer.service';
import {UserService} from '../../services/user.service';
import {EngineService} from '../../services/engine.service';
import {animate, style, transition, trigger} from '@angular/animations';
import {userEnv} from '../../../environments/user.env';
import {customerEnv} from '../../../environments/customer.env';
import {ShowroomsService} from '../../services/showrooms.service';
import {showroomEnv} from '../../../environments/showroom.env';
import {RouterService} from '../../services/router.service';
import {CurrentNodeHandler} from '../../DataHandlers/CurrentNode.handler';
import {SceneHandler} from '../../DataHandlers/scene.handler';
import {Location} from '@angular/common';
import {ShareShowroomComponent} from '../../components/share-showroom/share-showroom.component';

enum EditorModalType {
  NONE = -1,
  CAMERA,
  FIXTURE,
  SHAPE_POI,
  POI,
  PORTAL,
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
    animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(1000, style({ opacity: 1 })),
      ]),
      transition(':leave', [animate(1000, style({ opacity: 0 }))]),
    ]),
    trigger('fadeInOut', [
      transition('in => out', [
        style({opacity: 0}),
        animate(1000, style({opacity: 1}))
      ]),
      transition('out => in', [
        animate(1000, style({opacity: 0}))
      ])
    ])
  ]
})
export class HomePage implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('engine') engine: EngineComponent;
  @ViewChild('wishListMenu') wishListMenu: ElementRef;
  toggleMute$ = new Subject<void>();
  toggleGyroscope$ = new Subject<void>();
  zoom$ = new Subject<void>();

  engineIsLoading$ = new BehaviorSubject(false);
  objIsLoading$ = new BehaviorSubject(true);
  shellIsValid$ = new BehaviorSubject(true);
  closeToolbar = new EventEmitter();

  currentEditorModal = EditorModalType.NONE;
  openWishList = new EventEmitter();
  closeWishList = new EventEmitter();
  toggleBurgerMenu = new EventEmitter();

  isLoaded = false;
  openLanguageMenu = false;
  isMenuOpen = false;
  showroom: Showroom;
  isWelcomeOpen = false;
  welcomePageInfos;
  logoUrl;
  firebase = (!firebase.apps.length) ? firebase.initializeApp(environment.firebaseConfig) : firebase.app();
  objectName: string;
  isReady = false ;
  hideZoom = false;
  hideToolbar = false;
  object: any;
  objectId: string;
  editorModeChange;
  copyShowroom = false;
  hasToCloseObjectImporter = false;

  constructor(public showroomService: ShowroomsService,
              public userService: UserService,
              public customerService: CustomerService,
              public currentNodeHandler: CurrentNodeHandler,
              public wishListHandler: WishlistHandler,
              public engineService: EngineService,
              public modalController: ModalController,
              public popoverController: PopoverController,
              public editorService: EditorService,
              private ref: ChangeDetectorRef,
              public platform: Platform,
              public translateService: TranslateService,
              private router: RouterService,
              public sceneHandler: SceneHandler,
              private location: Location) {
    // this.router.navigateByUrl(routeParams);
  }

  get userEnv() {
    return userEnv;
  }

  get customerEnv() {
    return customerEnv;
  }

  get showroomEnv() {
    return showroomEnv;
  }

  ngOnInit(){
    // this.closeToolbar.subscribe(() => {
    //   this.currentEditorModal = EditorModalType.NONE;
    //   this.editorService.mode = EditorMode.NONE;
    // });
    this.showroom = showroomEnv.showroom;
    this.hideZoom = this.showroom.settings.components.zoom.hide;
    this.logoUrl = this.showroom.settings.sprites.logo;
    if (!(userEnv.loginType === 'platform') && this.showroom.settings.components.welcomePage.enabled && window.location.search.indexOf('entranceSweep') < 0) {
      this.welcomePageInfos = {
        customerId: userEnv.customer
      };
      this.isWelcomeOpen = true;
    }

    if (userEnv.toolbar === 'hidden') {
      this.hideToolbar = true;
    }

  }

  ngOnDestroy(): void {
  }

  ngAfterViewInit(): void {
    this.engineIsLoading$.subscribe(isLoading => {
      if (isLoading && !this.isLoaded) {
        this.engine.toggleMute$.subscribe((toggleMute: EngineEvent) => {
          if (toggleMute) {
            toggleMute.addTrigger(this.toggleMute$);
          }
        });
        this.engine.toggleGyroscope$.subscribe((toggleGyroscope: EngineEvent) => {
          if (toggleGyroscope) {
            toggleGyroscope.addTrigger(this.toggleGyroscope$);
          }
        });
        this.engine.zoom$.subscribe((zoom: EngineEvent) => {
          if (zoom) {
            zoom.addTrigger(this.zoom$);
          }
        });
        this.isLoaded = true;
      }
    });


    if (!this.showroom) {
      this.router.navigateToError();
    }

    let routeParams = '?';
    if (userEnv.tenant) {
      routeParams = routeParams + 'tenant=' + userEnv.tenant;
    }
    if (userEnv.showroomId) {
      routeParams = routeParams + '&name=' + userEnv.showroomId;
    }
    if (userEnv.customer) {
      routeParams = routeParams + '&customer=' + userEnv.customer;
    }
    if (userEnv.toolbar) {
      routeParams = routeParams + '&toolbar=' + userEnv.toolbar;
    }
    if (userEnv.callId) {
      routeParams = routeParams + '&callId=' + userEnv.callId;
    }
    if (userEnv.shopifyc) {
      routeParams = routeParams + '&shopifyc=' + userEnv.shopifyc;
    }
    this.location.go(routeParams);
  }

  saveProductConfigurations() {
    this.showroomService.updateShowroom(this.showroom).subscribe();
  }

  getMode() {
    if (this.platform.is('desktop')) {
      return 'ios';
    } else if (this.platform.is('android')) {
      return 'md';
    } else if (this.platform.is('ios')) {
      return 'ios';
    } else {
      return 'md';
    }
  }

  isMuted() {
    return this.sceneHandler.isSoundMuted.value;
  }

  isGyroscopeOn() {
    return this.engineService.isGyroscropeOn.value;
  }

  displayMenu(): boolean {
    if (!this.showroom || this.showroom.settings.hideMenu) {
      return false;
    }
    const menuHasAtLeastOneItem = this.showroom.menu && this.showroom.menu.length > 0;
    const menuHasVideoPresentation = this.showroom.settings.presentationVideoUrl && this.showroom.settings.presentationVideoUrl.length > 0;
    return menuHasAtLeastOneItem || menuHasVideoPresentation || userEnv.authorization;
  }

  displayLinkMenu(): boolean {
    if (!this.showroom || this.displayMenu()) {
      return false;
    }
    const menuHasAtLeastOneItem = this.showroom.menuLink && this.showroom.menuLink.length > 0;
    return menuHasAtLeastOneItem || userEnv.authorization;
  }

  switchView(event) {
    this.editorService.switchView(event.detail.value);
  }

  initMenu() {
    this.openLanguageMenu = false;
  }

  toggleMute() {
    this.engineService.toggleMute();
  }

  toggleGyroscope() {
    this.engineService.toggleGyroscope();
  }

  openMenu(event: MouseEvent): void {
    this.openLanguageMenu = !this.openLanguageMenu;
  }

  changeLanguage(value: any) {
    this.translateService.use(value);
    this.openLanguageMenu = false;
  }

  isEditingCamera() {
    return this.currentEditorModal === EditorModalType.CAMERA;
  }

  isEditing() {
    return this.editorService.isEditing();
  }

  isEditingObject() {
    if (this.editorModeChange !== this.editorService.currentMode.value) {
      this.editorModeChange = this.editorService.currentMode.value;
      return false;
    }
    return this.editorService.currentMode.value === EditorMode.ROTATE ||
        this.editorService.currentMode.value === EditorMode.TRANSLATE ||
        this.editorService.currentMode.value === EditorMode.SCALE ||
        this.editorService.currentMode.value === EditorMode.EDIT;
  }

  closeEditing() {
    this.editorService.currentMode.next(EditorMode.NONE);
    this.currentEditorModal = EditorModalType.NONE;
  }

  zoom(value: any) {
    this.engineService.zoom(value);
  }

  getZoomLevel() {
    return this.engineService.getZoomLevel();
  }

  async onEnterClicked() {
    this.isWelcomeOpen = false;
  }

  async openShareModal() {
    const popover = await this.popoverController.create({
      component: ShareShowroomComponent,
      cssClass: 'share-modal',
      componentProps: {
        editMode: this.copyShowroom,
        showroom: this.showroom
      }
    });
    popover.onWillDismiss().then((result) => {
      if (result && result.data) {
        this.copyShowroom = true;
        this.showroom =  result.data;
      }
    });

    return await popover.present();
  }

  meshLoading() {
    return !this.isWelcomeOpen && this.sceneHandler.meshIsLoading !== 1;
  }

  objectsLoading() {
    return !this.isWelcomeOpen && this.sceneHandler.objectsAreLoading !== 1;
  }
}
