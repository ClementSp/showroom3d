import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafePipe } from './pipes/safe.pipe';
import { FormsModule } from '@angular/forms';
import {MediasViewerComponent} from './components/custom-product-page-v2/medias-viewer/medias-viewer.component';
import {IonicModule} from '@ionic/angular';
import {NgxImageZoomModule} from 'ngx-image-zoom';

@NgModule({
    declarations: [SafePipe, MediasViewerComponent],
    exports: [
        SafePipe,
        MediasViewerComponent
    ],
  providers: [SafePipe],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NgxImageZoomModule
    ]
})
export class SharedModule {}
