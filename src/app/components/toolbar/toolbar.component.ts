import {AfterViewInit, Component, EventEmitter, Input, OnInit} from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import {EditorMode} from '../../models/EditorMode';
import {BehaviorSubject} from 'rxjs';
import ToolbarFeature from '../../models/ToolbarFeature';
import {EditorService} from '../../services/editor.service';
import {ToastService} from '../../services/toast.service';

@Component({
  selector: 'app-toolbar-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit, AfterViewInit {

  @Input() modeChanges = new BehaviorSubject<EditorMode>(EditorMode.NONE);
  @Input() features: Array<ToolbarFeature>;

  constructor(private navCtrl: NavController,
              private toastService: ToastService,
              private modalCtrl: ModalController,
              public editorService: EditorService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.updateModalStyle();
  }

  onClick(feature: ToolbarFeature) {
    if (feature.event) {
      feature.event.next();
    }
    this.toggleMode(feature.editorMode);
  }

  isEditorView() {
    return this.editorService.editorView.value;
  }

  isButtonActive(feature: ToolbarFeature) {
    return this.modeChanges.value === feature.editorMode && feature.editorMode !== EditorMode.NONE;
  }

  async close() {
    return await this.modalCtrl.dismiss();
  }

  private toggleMode(mode: EditorMode) {
    if (mode === EditorMode.EDIT) {
      this.editorService.openModal();
    } else if (mode === EditorMode.DELETE) {
      this.editorService.deleteObject();
    } else {
      this.modeChanges.next(
          (this.modeChanges.value === mode)
              ? EditorMode.NONE
              : mode
      );
    }
  }

  private updateModalStyle() {
    const children = (this as any).modal.children;

    for (let i = 0; i < children.length; i++) {
      const element = (children.item(i) as HTMLElement);

      if (element.classList.contains('modal-wrapper')) {
        element.classList.add('showroom-modal');
        (element as HTMLElement).style.height = '100%';
        (element as HTMLElement).style.pointerEvents = 'none';
      }
    }
  }
}
