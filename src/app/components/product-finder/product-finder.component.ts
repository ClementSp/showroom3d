import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {IBaseProduct} from '../../models/CustomProduct';
import {ShowroomsService} from '../../services/showrooms.service';
import {ProductsService} from '../../services/products.service';
import {TranslateService} from '@ngx-translate/core';

export interface IProductListItem extends IBaseProduct {
  checked: boolean;
}

@Component({
  selector: 'app-product-finder',
  templateUrl: './product-finder.component.html',
  styleUrls: ['./product-finder.component.scss'],
})
export class ProductFinderComponent implements OnInit, AfterViewInit {

  @Input() multipleSelect = false;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('searchbar') searchbar: IonSearchbar;

  filteredProducts = new BehaviorSubject<Array<IProductListItem>>([]);
  nbProducts = this.filteredProducts.pipe(map((array) => array.length));
  productListLoaded$ = new BehaviorSubject<boolean>(false);
  expandedRow = new BehaviorSubject<number>(-1);

  page = 0;

  constructor(private modalController: ModalController,
              private translateService: TranslateService,
              public productsService: ProductsService) { }

  ngOnInit() {
    this.initializeCheck();
    this.productsService.getProducts('', this.page).subscribe((products) => {
      this.filteredProducts.next(products);
      this.productListLoaded$.next(true);
    });
  }

  ngAfterViewInit(): void {
    this.initializeSearchbar();
  }

  nbProductChecked() {
    let nb = 0;

    this.filteredProducts.value.forEach((product) => {
      if (product.checked) {
        nb++;
      }
    });
    return nb;
  }

  onCheck(ean: string) {
    const productsList = [...this.filteredProducts.value];

    if (!this.multipleSelect) {
      productsList.forEach((product: IProductListItem) => {
        if (product.ean !== ean) {
          product.checked = false;
        }
      });
      this.filteredProducts.next(productsList);
    }
  }

  confirm() {
    let results = [];
    const selected = this.filteredProducts.value.filter((product) => product.checked);

    if (selected) {
      results = selected.map(item => item.product_ean);
    }
    this.modalController.dismiss(results);
  }

  getProductTitle(product: IBaseProduct) {
    let title = product.code;

    if (product.label && product.label.length > 0) {
      title += ' / ' + product.label;
    }

    if (product.product_color && product.product_color.length > 0) {
      title += ' / ' + product.product_color;
    }
    return title;
  }

  dismiss() {
    this.modalController.dismiss([]);
  }

  toggleContent(index: number) {
    if (this.expandedRow.value === index) {
      this.expandedRow.next(-1);
    } else {
      this.expandedRow.next(index);
    }
  }

  loadData($event) {
    const term = this.searchbar.value.toLowerCase();

    this.page++;
    this.productsService.getProducts(term, this.page).subscribe((products) => {
      const currentProducts = [...this.filteredProducts.value];

      products.forEach((item) => currentProducts.push(item));
      this.filteredProducts.next(currentProducts);
      $event.target.complete();
    });
  }

  private initializeSearchbar() {
    this.searchbar.ionInput.pipe(debounceTime(300), distinctUntilChanged()).subscribe((event) => {
      const term = this.searchbar.value.toLowerCase();
      this.page = 0;
      this.productsService.getProducts(term, this.page, this.translateService.currentLang).subscribe((products) => {
        this.filteredProducts.next(products);
      });
    });
  }

  private initializeCheck() {
    const productsCopy = [...this.filteredProducts.value];

    productsCopy.forEach((product) => {
      product.checked = false;
    });
    this.filteredProducts.next(productsCopy);
  }
}
