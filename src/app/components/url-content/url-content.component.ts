import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SafePipe} from '../../pipes/safe.pipe';
import {ModalController, Platform} from '@ionic/angular';
import {ProductPage} from '../product-viewer/product-viewer.component';
import {BehaviorSubject} from 'rxjs';
import {AudioHandler} from "../../DataHandlers/audio.handler";

@Component({
  selector: 'app-url-content',
  templateUrl: './url-content.component.html',
  styleUrls: ['./url-content.component.scss'],
})
export class UrlContentComponent implements OnInit, AfterViewInit, ProductPage, OnDestroy {

  @Input() data: any;

  url: string;
  ext: string;
  dismissEvent: EventEmitter<void>;
  scroll = new BehaviorSubject<boolean>(true);

  iframeLoaded = new BehaviorSubject<boolean>(false);

  constructor(private modalCtrl: ModalController,
              private safePipe: SafePipe,
              private platform: Platform,
              public audioHandler: AudioHandler) { }

  ngOnInit() {
    this.audioHandler.setVolume(0);
    this.url = this.data.url;
    this.ext = this.url.substring(this.url.lastIndexOf('.') + 1, this.url.indexOf('?'));
    this.dismissEvent = this.data.dismissEvent;
    if (this.data.scroll !== undefined) {
      this.scroll.next(this.data.scroll);
    }
  }

  ngAfterViewInit(): void {
    this.customModalWrapper();
    this.modalCtrl.getTop().then((modal) => {
      modal.swipeToClose = this.isMd();
    });
  }

  isMd() {
    return this.platform.width() < 768;
  }

  close() {
    this.dismissEvent.emit();
  }

  private customModalWrapper() {
    const children = (this as any).modal.children;

    for (let i = 0; i < children.length; i++) {
      const element = (children.item(i) as HTMLElement);

      if (element.classList.contains('modal-wrapper')) {
        element.style.background = 'transparent';
      }
    }
  }

  ngOnDestroy(): void {
    this.audioHandler.setVolume( this.audioHandler.getDefaultVolume());
  }
}
