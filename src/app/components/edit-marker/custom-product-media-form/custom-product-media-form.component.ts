import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FileItem, FileUploader} from 'ng2-file-upload';
import CustomProduct, {
  ImageMediaInfos,
  PdfMediaInfos,
  ProductMedia,
  VideoMediaInfos
} from '../../../models/CustomProduct';
import {ProductsService} from '../../../services/products.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Platform} from '@ionic/angular';

@Component({
  selector: 'app-custom-product-media-form',
  templateUrl: './custom-product-media-form.component.html',
  styleUrls: ['./custom-product-media-form.component.scss'],
})
export class CustomProductMediaFormComponent implements OnInit {
  @Input() product: CustomProduct;
  @Output() productChange = new EventEmitter<CustomProduct>();
  @Output() isFormValid = new EventEmitter<boolean>();
  @Output() hasLoaded = new EventEmitter<boolean>();

  @ViewChild('fileInputImage') fileInputImage: ElementRef;
  @ViewChild('fileInputPdf') fileInputPdf: ElementRef;
  @ViewChild('fileInputVideo') fileInputVideo: ElementRef;
  @ViewChild('fileInputLogo') fileInputLogo: ElementRef;

  imageUploaderErrorMessage = new BehaviorSubject<string>(undefined);
  videoUploaderErrorMessage = new BehaviorSubject<string>(undefined);
  videoIsLoading = new BehaviorSubject<boolean>(false);
  pdfUploaderErrorMessage = new BehaviorSubject<string>(undefined);
  pricePdfUploaderErrorMessage = new BehaviorSubject<string>(undefined);
  logoUploaderErrorMessage = new BehaviorSubject<string>(undefined);

  nbVideosMax = 1;
  nbPdfsMax = 1;
  nbPricePdfMax = 1;
  nbImagesMax = 8;
  nbLogoMax = 1;

  readonly allowedImageFileTypes = ['jpg', 'jpeg', 'png'];
  readonly allowedPdfFileTypes = ['pdf'];
  readonly allowedVideoFileTypes = ['mp4', 'mov'];
  uploader: FileUploader;

  logos = new Array<ImageMediaInfos>();
  images = new Array<ImageMediaInfos>();
  videos = new Array<VideoMediaInfos>();
  pdfs = new Array<PdfMediaInfos>();
  pricePdfs = new Array<PdfMediaInfos>();

  formGroup: FormGroup = undefined;

  constructor(private formBuilder: FormBuilder,
              private platform: Platform,
              public productsService: ProductsService) { }

  ngOnInit() {
    this.uploader = new FileUploader({
      removeAfterUpload: true
    });
    const twitterControl = new FormControl(this.product.twitterUrl);
    const facebookControl = new FormControl(this.product.facebookUrl);
    const instagramControl = new FormControl(this.product.instagramUrl);
    const contactControl = new FormControl(this.product.contact);
    const youtubeControl = new FormControl(this.product.youtubeUrl);
    const linkedinControl = new FormControl(this.product.linkedinUrl);

    this.formGroup = this.formBuilder.group({
      twitter: twitterControl,
      facebook: facebookControl,
      instagram: instagramControl,
      contact: contactControl,
      youtube: youtubeControl,
      linkedin: linkedinControl
    });
    this.getMedias(this.product);
  }

  uploadNewVideo() {
    const fileItem: FileItem = this.uploader.queue[0];
    const builtForm = this.formBuilder.group({ profile: [''] });
    const getFileExtension = (filename: string) => filename.split('.').pop();
    const fileExtension = getFileExtension(fileItem._file.name);

    this.videoIsLoading.next(true);
    this.videoUploaderErrorMessage.next(undefined);
    if (this.allowedVideoFileTypes.includes(fileExtension.toLocaleLowerCase())) {
      builtForm.get('profile').setValue(fileItem._file);
      const blob = builtForm.get('profile').value;
      const media: ProductMedia = {
        id: '',
        type: 'video',
        ext: ''
      };
      this.productsService.putMedia(this.product.product_ean, media, blob).subscribe((result) => {
        this.videos.push({blob, url: this.addVersionToUrl(result.url), name: undefined, media: result.media});
        this.product.medias.push(result.media);
        this.videoIsLoading.next(false);
        this.productChange.emit(this.product);
      });
    } else {
      this.videoUploaderErrorMessage.next('File extension must be one of : ' + this.allowedVideoFileTypes.join(', '));
      this.videoIsLoading.next(false);
    }
    this.fileInputVideo.nativeElement.value = '';
    this.uploader.clearQueue();
  }

  uploadNewLogo() {
    const fileItem: FileItem = this.uploader.queue[0];
    const builtForm = this.formBuilder.group({ profile: [''] });
    const getFileExtension = (filename: string) => filename.split('.').pop();
    const fileExtension = getFileExtension(fileItem._file.name);
    let blob;

    this.imageUploaderErrorMessage.next(undefined);
    if (this.allowedImageFileTypes.includes(fileExtension.toLowerCase())) {
      builtForm.get('profile').setValue(fileItem._file);
      blob = builtForm.get('profile').value;
      const media: ProductMedia = {
        id: '',
        type: 'logo',
        ext: ''
      };
      this.productsService.putMedia(this.product.product_ean, media, blob).subscribe((result) => {
        this.logos.push({blob, url: this.addVersionToUrl(result.url), media: result.media});
        this.product.medias.push(result.media);
        this.productChange.emit(this.product);

      });
    } else {
      this.imageUploaderErrorMessage.next('File extension must be one of : ' + this.allowedImageFileTypes.join(', '));
    }
    this.fileInputLogo.nativeElement.value = '';
    this.uploader.clearQueue();
  }

  uploadNewImage() {
    const fileItem: FileItem = this.uploader.queue[0];
    const builtForm = this.formBuilder.group({ profile: [''] });
    const getFileExtension = (filename: string) => filename.split('.').pop();
    const fileExtension = getFileExtension(fileItem._file.name);
    let blob;

    this.imageUploaderErrorMessage.next(undefined);
    if (this.allowedImageFileTypes.includes(fileExtension.toLowerCase())) {
      builtForm.get('profile').setValue(fileItem._file);
      blob = builtForm.get('profile').value;
      const media: ProductMedia = {
        id: '',
        type: 'picture',
        ext: ''
      };
      this.productsService.putMedia(this.product.product_ean, media, blob).subscribe((result) => {
        this.images.push({blob, url: this.addVersionToUrl(result.url), media: result.media});
        this.product.medias.push(result.media);
        this.productChange.emit(this.product);

      });
    } else {
      this.imageUploaderErrorMessage.next('File extension must be one of : ' + this.allowedImageFileTypes.join(', '));
    }
    this.fileInputImage.nativeElement.value = '';
    this.uploader.clearQueue();
  }

  uploadNewPdf(type: string, errorMessage: BehaviorSubject<string>, list: Array<any>) {
    const fileItem: FileItem = this.uploader.queue[0];
    const builtForm = this.formBuilder.group({ profile: [''] });
    const getFileExtension = (filename: string) => filename.split('.').pop();
    const fileExtension = getFileExtension(fileItem._file.name);
    let blob;

    errorMessage.next(undefined);
    if (this.allowedPdfFileTypes.includes(fileExtension.toLowerCase())) {
      builtForm.get('profile').setValue(fileItem._file);
      blob = builtForm.get('profile').value;
      const media: ProductMedia = {
        id: '',
        type,
        ext: ''
      };
      this.productsService.putMedia(this.product.product_ean, media, blob).subscribe((result) => {
        list.push({blob, url: this.addVersionToUrl(result.url), media: result.media});
        this.product.medias.push(result.media);
        this.productChange.emit(this.product);
      });
    } else {
      errorMessage.next('File extension must be one of : ' + this.allowedPdfFileTypes.join(', '));
    }
    this.fileInputPdf.nativeElement.value = '';
    this.uploader.clearQueue();
  }

  deleteMedia(mediaId: string) {
    const media = this.product.medias.find(m => m.id === mediaId);

    this.videoUploaderErrorMessage.next(undefined);
    this.productsService.deleteMedia(this.product.product_ean, mediaId).subscribe(
        () => {
          if (media.type === 'picture') {
            this.images = this.images.filter(img => img.media.id !== mediaId);
          } else if (media.type === 'video') {
            this.videos = this.videos.filter(vid => vid.media.id !== mediaId);
          } else if (media.type === 'pdf') {
            this.pdfs = this.pdfs.filter(pdf => pdf.media.id !== mediaId);
          } else if (media.type === 'pricePdf') {
            this.pricePdfs = this.pricePdfs.filter(pdf => pdf.media.id !== mediaId);
          } else if (media.type === 'logo') {
            this.logos = this.logos.filter(logo => logo.media.id !== mediaId);
          }
          this.product.medias = this.product.medias.filter(media => media.id !== mediaId);
          this.productChange.emit(this.product);
        },
        () => this.videoUploaderErrorMessage.next('Fail to delete')
    );
  }

  private getMedias(customProduct: CustomProduct) {
    customProduct.medias.forEach((media: ProductMedia) => {
      this.productsService.getMediaUrl(customProduct.product_ean, media.id).subscribe((url: string) => {
        if (!url) {
          return;
        }

        this.fetchToBlob(url).then((data) => {
          if (data) {
            if (media.type === 'picture') {
              this.images.push({blob: data, url: this.addVersionToUrl(url), media});
              this.images = this.sortMediaArrayFromMediaIndexes(this.images);
            } else if (media.type === 'video') {
              this.videos.push({blob: data, url: this.addVersionToUrl(url), media, name: undefined});
              this.videos = this.sortMediaArrayFromMediaIndexes(this.videos);
            } else if (media.type === 'pdf') {
              this.pdfs.push({blob: data, url: this.addVersionToUrl(url), media});
              this.pdfs = this.sortMediaArrayFromMediaIndexes(this.pdfs);
            } else if (media.type === 'pricePdf') {
              this.pricePdfs.push({blob: data, url: this.addVersionToUrl(url), media});
              this.pricePdfs = this.sortMediaArrayFromMediaIndexes(this.pricePdfs);
            } else if (media.type === 'logo') {
              this.logos.push({blob: data, url: this.addVersionToUrl(url), media});
              this.logos = this.sortMediaArrayFromMediaIndexes(this.logos);
            }
            setTimeout(() => {
              this.hasLoaded.emit();
            }, 500);
          }
        });
      });
    });
  }

  rotateMedia(media: ProductMedia, rotation: number) {
    if (media.type !== 'picture') {
      return;
    }
    this.productsService.rotateMedia(this.product.product_ean, media.id, rotation).subscribe(res => {
      const curMediaIndex = this.product.medias.findIndex(m => m.id === media.id);
      this.product.medias[curMediaIndex].id = res.media.id;
      const imageIndex = this.images.findIndex(img => img.media.id === media.id);
      this.fetchToBlob(res.url).then((blob) => {
        this.images[imageIndex].url = res.url;
        this.images[imageIndex].blob = blob;
      });
      this.images = [...this.images];

      this.productChange.emit(this.product);
    });
  }

  private sortMediaArrayFromMediaIndexes(array) {
    return array.sort((a, b) => {
      const idx1 = this.product.medias.findIndex(m => m.id === a.media.id);
      const idx2 = this.product.medias.findIndex(m => m.id === b.media.id);

      return idx1 - idx2;
    });
  }

  private fetchToBlob(url: string) {
    const builtForm = this.formBuilder.group({ profile: [''] });
    let status;

    return fetch(this.addVersionToUrl(url), {cache: 'no-cache'}).then((response) => {
      status = response.status;
      return response.blob();
    }).then((blob) => {
      if (status === 200) {
        builtForm.get('profile').setValue(blob);
        return builtForm.get('profile').value;
      }
      return undefined;
    });
  }

  private addVersionToUrl(url: string): string {
    if (url) {
      const urlObj = new URL(url);
      // urlObj.searchParams.set('v', Math.random().toString());
      return urlObj.href;
    } else {
      return undefined;
    }
  }

  isMobile() {
    return this.platform.is('mobile');
  }

  onMediaChange() {
    this.product.twitterUrl = this.formGroup.controls.twitter.value;
    this.product.facebookUrl = this.formGroup.controls.facebook.value;
    this.product.instagramUrl = this.formGroup.controls.instagram.value;
    this.product.linkedinUrl = this.formGroup.controls.linkedin.value;
    this.product.youtubeUrl = this.formGroup.controls.youtube.value;
    this.product.contact = this.formGroup.controls.contact.value;
  }
}
