import {AfterViewInit, Component, EventEmitter, Input, ViewChild} from '@angular/core';
import {Poi} from '../../../../models/showroom.model';
import {IonSlides, ModalController} from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';
import CustomProduct from '../../../../models/CustomProduct';
import {ProductsService} from '../../../../services/products.service';
import {CustomerService} from '../../../../services/customer.service';
import {ModulesService} from '../../../../services/modules.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements AfterViewInit {

  @Input() poi: Poi;
  @Input() windowTitle = 'Create product';
  @Input() updateProduct = false;

  formHasBeenConfirmedEvent = new EventEmitter();

  product: CustomProduct = new CustomProduct();
  productValid = false;
  hasLoaded = false;
  eanLoadInProgress = false;
  barcodeScannerModuleName = 'BarcodeScanner';
  personalEditorModuleName = 'personalEditor';
  isBarcodeScannerEnabled = false;
  isPersonalEditorEnabled = false;

  @ViewChild('slider') slides: IonSlides;
  sliderOpts = { simulateTouch: false, autoHeight: true };
  currentSliderPage = new BehaviorSubject(0);

  constructor(private modalController: ModalController,
              private customerService: CustomerService,
              private productsService: ProductsService,
              private translateService: TranslateService,
              private modulesService: ModulesService) { }

  ngAfterViewInit() {
    this.modulesService.getModules().then(modules => {
      modules.forEach(mod => {
        if (!this.updateProduct && mod.name === this.barcodeScannerModuleName) {
          this.isBarcodeScannerEnabled = mod.enabled;
        }
        if (mod.name === this.personalEditorModuleName) {
          this.isPersonalEditorEnabled = mod.enabled;
        }
      });
      this.getCustomProductInfosByEan();
    });

    this.slides.ionSlidesDidLoad.subscribe(() => {
      this.currentSliderPage.subscribe((value) => {
        this.slides.slideTo(value, 400)
            .catch(() => alert('error'));
      });
    });
  }

  private getCustomProductInfosByEan() {
    if (this.poi.data.ean && this.poi.data.ean.length > 0) {
      this.productsService.getProductByEan(this.poi.data.ean, undefined, this.translateService.currentLang)
          .subscribe((product: CustomProduct) => {
            this.product = product;
            this.hasLoaded = true;
          });
    } else {
      setTimeout(() => this.hasLoaded = true, 50);
    }
  }

  slideHasLoaded() {
    this.slides.update().then(() => this.slides.updateAutoHeight(400));
  }

  dismiss() {
    this.modalController.dismiss();
  }

  eanLoading(value: boolean) {
    this.eanLoadInProgress = value;
  }

  productValidityUpdate(value: boolean) {
    this.productValid = value;
  }

  next() {
    if (!this.canGoNext()) {
      return;
    }

    if (!this.updateProduct) {
      this.productsService.putProduct(this.product).subscribe(res => {
        this.product = res.result;
        this.currentSliderPage.next(this.currentSliderPage.value + 1);
      });
    } else {
      this.currentSliderPage.next(this.currentSliderPage.value + 1);
    }
  }

  back() {
    this.currentSliderPage.next(this.currentSliderPage.value - 1);
  }

  canGoNext() {
    if (this.updateProduct) {
      return true;
    }
    if (this.eanLoadInProgress || !this.productValid) {
      return false;
    }
    if (this.product.product_ean && this.product.product_ean.length > 0 &&
        (this.product.language && this.product.language.length > 0) &&
        (this.product.code && this.product.code.length > 0)) {
      return true;
    }
    return false;
  }

  async confirm() {
    this.formHasBeenConfirmedEvent.emit({createPoi: false});
    this.productsService.putProduct(this.product).subscribe();
    await this.modalController.dismiss(this.product);
  }
}
