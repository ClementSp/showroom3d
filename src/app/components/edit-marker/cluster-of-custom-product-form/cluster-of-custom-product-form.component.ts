import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Poi, ShapePoi} from '../../../models/showroom.model';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductFinderComponent} from '../../product-finder/product-finder.component';
import CustomProduct, {IBaseProduct} from '../../../models/CustomProduct';
import {ModalController, Platform} from '@ionic/angular';
import {ShowroomsService} from '../../../services/showrooms.service';
import {BehaviorSubject, Subscription} from 'rxjs';
import {CreateProductComponent} from './create-product/create-product.component';
import {ProductsService} from '../../../services/products.service';
import {CustomerService} from '../../../services/customer.service';
import {UserService} from '../../../services/user.service';
import {userEnv} from '../../../../environments/user.env';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-cluster-of-custom-product-form',
  templateUrl: './cluster-of-custom-product-form.component.html',
  styleUrls: ['./cluster-of-custom-product-form.component.scss'],
})
export class ClusterOfCustomProductFormComponent implements OnInit {

  @Input() poi: any;
  @Input() confirmForm: EventEmitter<any>;
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();

  products = new BehaviorSubject<Array<IBaseProduct>>([]);
  errorMessage = new BehaviorSubject<string>(undefined);

  formGroup: FormGroup;
  clusterNameControl: FormControl;
  productsEanControl: FormArray;
  templateControl: FormControl;

  templateList = [
    'Default', 'template 2', 'template 3', 'template 4', 'Two Steps', 'Premium', 'template full pictures'
  ];

  expandedRow = new BehaviorSubject<number>(-1);

  onUpdateValiditySubscription: Subscription;
  onConfirmSubscription: Subscription;

  constructor(private modalController: ModalController,
              private formBuilder: FormBuilder,
              public platform: Platform,
              public translateService: TranslateService,
              public userService: UserService,
              public customerService: CustomerService,
              private productsService: ProductsService,
              private showroomService: ShowroomsService) { }

  ngOnInit() {
    this.buildFormGroup();
    this.onFormConfirmation();
    this.emitFormOnValidityCheck();
    this.isFormValid.emit(this.formGroup.valid);
  }

  get userEnv() {
    return userEnv;
  }

  onTemplateChange(event) {

  }

  set templateControlValue(template: string) {
    this.templateControl.setValue(template);
    this.formChanged.emit(this.formGroup);
  }

  async openProductCreator(ean?: string, updateProduct: boolean = false) {
    const modal = await this.modalController.create({
      component: CreateProductComponent,
      cssClass: 'edit-marker-component backdrop-05',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        poi: {data: {ean}},
        windowTitle: (updateProduct) ? 'Update product' : 'Create product',
        updateProduct
      }
    });
    modal.onDidDismiss().then((response) => response.data).then((product: any) => {
      let errorMessage: string;
      const products = [...this.products.value];

      if (product) {
        const indexOfProduct = products.map((item) => item.product_ean).indexOf(product.product_ean);

        if (indexOfProduct < 0) {
          this.productsEanControl.push(new FormControl(product.product_ean));
          products.push(product);
        } else {
          this.productsEanControl.controls[indexOfProduct] = new FormControl(product.product_ean);
          products.splice(indexOfProduct, 1, product);
          errorMessage = 'The product you tried to import already exist in the carousel.';
        }
      }

      if (!updateProduct) {
        this.errorMessage.next(errorMessage);
      }
      this.isFormValid.emit(this.formGroup.valid);
      this.products.next(products);

    });
    return await modal.present();
  }

  async openProductFinder() {
    const modal = await this.modalController.create({
      component: ProductFinderComponent,
      cssClass: 'product-finder backdrop-05 fit-content-modal',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        multipleSelect: true
      }
    });
    modal.onDidDismiss().then((response) => response.data).then((eanList: Array<string>) => {
      const products = [...this.products.value];
      const promises = [];
      let errorMessage: string;
      if (eanList) {
        eanList.forEach((ean: string) => {
          promises.push(this.productsService.getProductByEan(ean, undefined, this.translateService.currentLang).toPromise());
        });
        Promise.all(promises).then((promisesResultsArray: Array<CustomProduct>) => {
          promisesResultsArray.forEach((product: CustomProduct) => {
            if (!products.find(item => item.product_ean === product.product_ean)) {
              this.productsEanControl.push(new FormControl(product.product_ean));
              products.push(product);
            } else {
              errorMessage = 'Some products you tried to import already exist in the carousel.';
            }
          });
          this.isFormValid.emit(this.formGroup.valid);
          this.products.next(products);
          this.errorMessage.next(errorMessage);
        });
      }
    });
    return await modal.present();
  }

  reorderItems(ev) {
    const productsCopy = [...this.products.value];
    const itemMove = productsCopy.splice(ev.detail.from, 1)[0];
    const formMove = this.productsEanControl.at(ev.detail.from);

    this.productsEanControl.removeAt(ev.detail.from);
    this.productsEanControl.insert(ev.detail.to, formMove);
    productsCopy.splice(ev.detail.to, 0, itemMove);
    this.products.next(productsCopy);
    ev.detail.complete();
  }

  toggleContent(index: number) {
    if (this.expandedRow.value === index) {
      this.expandedRow.next(-1);
    } else {
      this.expandedRow.next(index);
    }
  }

  changeLng($event) {
    const value = $event.detail.value;
    const products = [...this.products.value];

    this.translateService.use(value).toPromise().then(() => {
      products.forEach((product: CustomProduct) => {
        this.productsService.getProductByEan(product.product_ean, undefined, this.translateService.currentLang)
            .subscribe((item) => {
              product.code = item.code;
              product.label = item.label;
              product.language = item.language;
              product.description = item.description;
            });
      });
      this.products.next(products);
    });
  }

  getProductTitle(product: IBaseProduct) {
    let title = product.code;

    if (product.label && product.label.length > 0) {
      title += ' / ' + product.label;
    }

    if (product.product_color && product.product_color.length > 0) {
      title += ' / ' + product.product_color;
    }
    return title;
  }

  deleteProduct(index: number) {
    const products = [...this.products.value];

    products.splice(index, 1);
    this.productsEanControl.removeAt(index);
    this.isFormValid.emit(this.formGroup.valid);
    this.products.next(products);
  }

  private buildFormGroup() {
    const promises = new Array<Promise<any>>();

    this.clusterNameControl = new FormControl('');
    if (this.poi.data) {
      this.clusterNameControl = new FormControl(this.poi.data.name);
    }
    this.templateControl = new FormControl('');
    this.productsEanControl = new FormArray([]);
    if (this.poi.data && this.poi.data?.products) {
      this.poi.data?.products.forEach((ean) => {
        this.productsEanControl.push(new FormControl(ean));
        promises.push(this.productsService.getProductByEan(ean, undefined, this.translateService.currentLang).toPromise());
      });
      Promise.all(promises).then((promisesResults: Array<CustomProduct>) => {
        this.products.next(promisesResults);
      });
    }
    if (this.poi.data && this.poi.data.template) {
      this.templateControlValue = this.poi.data.template;
    } else {
      this.templateControlValue = '0';
    }
    this.formGroup = this.formBuilder.group({
      name: this.clusterNameControl,
      template: this.templateControl,
      products: this.productsEanControl
    });
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe(() => {
      this.poi.data = this.formGroup.getRawValue();
      if (this.poi.sprite && this.products.value[0] && this.products.value[0].medias[0]) {
        let mediaId;
        const product = this.products.value[0];

        if (this.poi.sprite.type === 'logo') {
          mediaId = product.medias.filter(media => media.type === 'logo')[0]?.id;
        } else if (this.poi.sprite.type === 'picture') {
          mediaId = product.medias.filter(media => media.type === 'picture')[0]?.id;
        } else {
          if (this.poi.isShapePoi){
            this.showroomService.putShapePoi(this.poi).subscribe();
          }else{
            this.showroomService.putPoi(this.poi).subscribe();
          }
        }

        this.productsService.getMediaUrl(product.product_ean, mediaId).subscribe((url) => {
          this.poi.sprite.path = url;
          if (this.poi.isShapePoi){
            this.showroomService.putShapePoi(this.poi).subscribe();
          }else{
            this.showroomService.putPoi(this.poi).subscribe();
          }
        });

      } else {
        if (this.poi.isShapePoi){
          this.showroomService.putShapePoi(this.poi).subscribe();
        }else{
          this.showroomService.putPoi(this.poi).subscribe();
        }
      }
    });
  }

  private emitFormOnValidityCheck() {
    this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
      this.formChanged.emit(this.formGroup);
    });
  }
}
