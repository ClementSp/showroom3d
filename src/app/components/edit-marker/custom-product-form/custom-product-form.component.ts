import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Poi} from '../../../models/showroom.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Subscription} from 'rxjs';
import {IonSegment, ModalController, Platform} from '@ionic/angular';
import {ProductFinderComponent} from '../../product-finder/product-finder.component';
import {ShowroomsService} from '../../../services/showrooms.service';
import CustomProduct, {ProductMedia, ImageMediaInfos, VideoMediaInfos} from '../../../models/CustomProduct';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {map} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {CustomerService} from '../../../services/customer.service';
import {ProductsService} from '../../../services/products.service';
import {BarcodeScannerModalComponent} from './barcode-scanner-modal/barcode-scanner-modal.component';
import {ModulesService} from '../../../services/modules.service';
import {uuid4} from '@capacitor/core/dist/esm/util';

@Component({
  selector: 'app-custom-product-form',
  templateUrl: './custom-product-form.component.html',
  styleUrls: ['./custom-product-form.component.scss'],
})
export class CustomProductFormComponent implements OnInit, OnDestroy {

  @Input() poi: any;
  @Input() confirmForm: EventEmitter<any>;
  @Input() displaySearchProductButton = true;
  @Input() updateProduct = false;
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();
  @Output() productFullyUpload = new EventEmitter<any>();

  @ViewChild('segment') createNewProductSegment: IonSegment;

  imageUploaderErrorMessage = new BehaviorSubject<string>(undefined);
  videoUploaderErrorMessage = new BehaviorSubject<string>(undefined);
  videoIsLoading = new BehaviorSubject<boolean>(false);
  formGroup: FormGroup;
  eanControl: FormControl;
  descriptionControl: FormControl;
  detailsControl: FormControl;
  priceControl: FormControl;
  codeControl: FormControl;
  labelControl: FormControl;
  languageControl: FormControl;
  idControl: FormControl;
  public warningEan: string;
  twitterControl: FormControl;
  facebookControl: FormControl;
  instagramControl: FormControl;

  readonly allowedImageFileTypes = ['jpg', 'jpeg', 'png'];
  readonly allowedVideoFileTypes = ['mp4', 'mov'];
  uploader: FileUploader;
  product = new CustomProduct();

  images = new Array<ImageMediaInfos>();
  videos = new Array<VideoMediaInfos>();

  onConfirmSubscription: Subscription;

  barcodeScannerModuleName = 'barcodeScanner';
  isBarcodeScannerEnabled = false;

  nbVideosMax = 1;
  nbImagesMax = 8;

  constructor(private formBuilder: FormBuilder,
              public translateService: TranslateService,
              private platform: Platform,
              public customerService: CustomerService,
              public productsService: ProductsService,
              private showroomService: ShowroomsService,
              private modalController: ModalController,
              private modulesService: ModulesService) {

    this.modulesService.getModules().then(modules => {
      modules.forEach(mod => {
        if (mod.name === this.barcodeScannerModuleName) {
          this.isBarcodeScannerEnabled = mod.enabled;
        }
      });
    });
  }

  ngOnInit() {
    this.onFormConfirmation();
    this.initializeForm(this.product);
    this.getCustomProductInfosByEan();
  }

  ngOnDestroy(): void {
    this.onConfirmSubscription.unsubscribe();
  }

  putProduct() {
    const product: any = {
      code: this.codeControl.value,
      product_ean: this.eanControl.value,
      description: this.descriptionControl.value,
      details: this.detailsControl.value,
      language: this.languageControl.value,
      sale_price: this.priceControl.value,
      label: this.labelControl.value,
      id: this.idControl.value,
      twitterUrl: this.twitterControl.value,
      facebookUrl: this.facebookControl.value,
      instagramUrl: this.instagramControl.value
    };

    return this.productsService.putProduct(product).pipe(map(resp => resp.result));
  }

  emptyId() {
    const id = this.idControl.value;
    return !id || id.length === 0;
  }

  async openProductFinder() {
    const modal = await this.modalController.create({
      component: ProductFinderComponent,
      cssClass: 'product-finder backdrop-05 fit-content-modal',
      showBackdrop: true,
      backdropDismiss: true
    });
    modal.onDidDismiss()
        .then((response) => response.data).then((eanList) => {
      if (eanList) {
        const selectedEan = eanList[0];

        this.productsService.getProductByEan(selectedEan, undefined, this.translateService.currentLang)
            .subscribe((product: CustomProduct) => {
              this.getMedias(product);
              this.eanControlValue = product.product_ean;
              this.eanControl.setValue(product.product_ean);
              this.labelControl.setValue(product.label);
              this.codeControl.setValue(product.code);
              this.priceControl.setValue(product.sale_price);
              this.languageControl.setValue(product.language);
              this.descriptionControl.setValue(product.description);
              this.detailsControl.setValue(product.details);
              this.twitterControl.setValue(product.twitterUrl);
              this.facebookControl.setValue(product.facebookUrl);
              this.instagramControl.setValue(product.instagramUrl);
            });
      }
    });
    return await modal.present();
  }

  uploadNewVideo(index: number) {
    const fileItem: FileItem = this.uploader.queue[0];
    const builtForm = this.formBuilder.group({ profile: [''] });
    const getFileExtension = (filename: string) => filename.split('.').pop();
    const fileExtension = getFileExtension(fileItem._file.name);

    this.videoIsLoading.next(true);
    this.videoUploaderErrorMessage.next(undefined);
    if (this.allowedVideoFileTypes.includes(fileExtension.toLocaleLowerCase())) {
      builtForm.get('profile').setValue(fileItem._file);
      const blob = builtForm.get('profile').value;
      if (this.idControl.value && this.idControl.value.length > 0) {
        const media: ProductMedia = {
          id: '',
          type: 'video',
          ext: ''
        };
        this.productsService.putMedia(this.eanControl.value, media, blob).subscribe((result) => {
          this.videos.push({blob, url: this.addVersionToUrl(result.url), name: undefined, media: result.media});
          this.product.medias.push(result.media);
          this.videoIsLoading.next(false);
        });
      } else {
        const media = {id: uuid4(), type: 'video', ext: 'mp4'};
        this.videos.push({blob, url: undefined, name: fileItem.file.name, media});
        this.product.medias.push(media);
        this.videoIsLoading.next(false);
      }
    } else {
      this.videoUploaderErrorMessage.next('File extension must be one of : ' + this.allowedVideoFileTypes.join(', '));
      this.videoIsLoading.next(false);
    }
    this.uploader.queue = [];
  }

  uploadNewImage() {
    const fileItem: FileItem = this.uploader.queue[0];
    const builtForm = this.formBuilder.group({ profile: [''] });
    const idControlValueIsCorrect = this.idControl.value && this.idControl.value.length > 0;
    const getFileExtension = (filename: string) => filename.split('.').pop();
    const fileExtension = getFileExtension(fileItem._file.name);
    let blob;

    this.imageUploaderErrorMessage.next(undefined);
    if (this.allowedImageFileTypes.includes(fileExtension.toLowerCase())) {
      builtForm.get('profile').setValue(fileItem._file);
      blob = builtForm.get('profile').value;
      if (idControlValueIsCorrect) {
        const media: ProductMedia = {
          id: '',
          type: 'picture',
          ext: ''
        };
        this.productsService.putMedia(this.eanControl.value, media, blob).subscribe((result) => {
          this.images.push({blob, url: this.addVersionToUrl(result.url), media: result.media});
          this.product.medias.push(result.media);
        });
      } else {
        const media = {id: uuid4(), type: 'picture', ext: 'jpg'};
        this.images.push({blob, url: URL.createObjectURL(fileItem._file as Blob), media});
        this.product.medias.push(media);
      }
    } else {
      this.imageUploaderErrorMessage.next('File extension must be one of : ' + this.allowedImageFileTypes.join(', '));
    }
    this.uploader.queue = [];
  }

  deleteMedia(mediaId: string) {
    const media = this.product.medias.find(m => {
      return m.id === mediaId;
    });
    this.videoUploaderErrorMessage.next(undefined);
    if (this.idControl.value.length > 0) {
      this.productsService.deleteMedia(this.product.product_ean, mediaId).subscribe(
          () => {
            if (media.type === 'picture') {
              this.images = this.images.filter(img => img.media.id !== mediaId);
            } else if (media.type === 'video') {
              this.videos = this.videos.filter(vid => vid.media.id !== mediaId);
            }
          },
          () => this.videoUploaderErrorMessage.next('Fail to delete')
      );
    }
  }

  onEanChange($event) {
    this.productsService.getProductByEan($event.detail.value).subscribe((product) => {
      if (product) {
        this.isFormValid.emit(false);
        this.warningEan = 'L\'ean est déjà utilisé';
      } else {
        this.isFormValid.emit(this.formGroup.valid);
        this.warningEan = undefined;
      }
    });
  }

  onCodeChange($event) {
    if (!this.warningEan) {
      this.isFormValid.emit(this.formGroup.valid);
    }
  }

  onLanguageChange($event) {
    const language = $event.detail.value;

    this.translateService.use(language).toPromise().then(() => {
      this.productsService.getProductByEan(this.product.product_ean, undefined, this.translateService.currentLang)
          .subscribe((product: CustomProduct) => {
            this.codeControl.setValue(product.code);
            this.labelControl.setValue(product.label);
            this.descriptionControl.setValue(product.description);
            this.detailsControl.setValue(product.details);
          });
      this.isFormValid.emit(this.formGroup.valid);
    });
  }

  isMobile() {
    return this.platform.is('mobile');
  }

  updateControlsValidity() {
    Object.keys(this.formGroup.controls).forEach((e) => {
      this.formGroup.controls[e].updateValueAndValidity();
    });
  }

  async scanProduct() {
    const modal = await this.modalController.create({
      component: BarcodeScannerModalComponent,
      cssClass: 'fit-content-modal barcode-scanner-modal',
      showBackdrop: true,
      backdropDismiss: true
    });
    modal.onDidDismiss().then((response) => response.data)
        .then((ean) => {
          if (ean) {
            this.updateProductFormFromEan(ean);
          }
        });
    return await modal.present();
  }

  async updateProductFormFromEan(ean: string) {
    this.eanControlValue = ean;
    this.productsService.getProductByEan(ean).subscribe(res => {
      if (res) {
        this.updateProduct = true;
        this.setValuesFromCustomProduct(res);
      }
    });
  }

  async eanChanged() {
    this.productsService.getProductByEan(this.eanControl.value).subscribe(res => {
      if (res) {
        this.eanControl.setErrors({incorrect: true});
        this.warningEan = 'This Code/EAN is already linked to an existing product.';
      } else {
        this.eanControl.setErrors({incorrect: null});
        this.eanControl.updateValueAndValidity();
        this.warningEan = '';
      }
    });
  }

  private initializeForm(customProduct: CustomProduct) {
    this.uploader = new FileUploader({});
    this.languageControl = new FormControl(this.translateService.currentLang, Validators.required);
    this.idControl = new FormControl(customProduct.id, Validators.pattern('^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$'));
    this.eanControl = new FormControl(customProduct.product_ean, Validators.required);
    this.priceControl = new FormControl(customProduct.sale_price, [Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]);
    this.codeControl = new FormControl(customProduct.code, Validators.required);
    this.labelControl = new FormControl(customProduct.label);
    this.descriptionControl = new FormControl(customProduct.description);
    this.detailsControl = new FormControl(customProduct.details);
    this.twitterControl = new FormControl(customProduct.twitterUrl);
    this.facebookControl = new FormControl(customProduct.facebookUrl);
    this.instagramControl = new FormControl(customProduct.instagramUrl);

    this.formGroup = this.formBuilder.group({
      ean: this.eanControl,
      price: this.priceControl,
      code: this.codeControl,
      description: this.descriptionControl,
      details: this.detailsControl,
      language: this.languageControl,
      label: this.labelControl,
      twitterUrl: this.twitterControl,
      facebookUrl: this.facebookControl,
      instagramUrl: this.instagramControl
    });
    this.isFormValid.emit(this.formGroup.valid);
  }

  private setValuesFromCustomProduct(customProduct: CustomProduct) {
    this.languageControl.setValue(customProduct.language.toLowerCase());
    this.idControl.setValue(customProduct.id);
    this.eanControlValue = customProduct.product_ean;
    this.eanControl .setValue(customProduct.product_ean);
    this.priceControl.setValue(customProduct.sale_price);
    this.labelControl.setValue(customProduct.label);
    this.codeControl.setValue(customProduct.code);
    this.descriptionControl.setValue(customProduct.description);
    this.detailsControl.setValue(customProduct.details);
    this.twitterControl.setValue(customProduct.twitterUrl);
    this.facebookControl.setValue(customProduct.facebookUrl);
    this.instagramControl.setValue(customProduct.instagramUrl);
    this.getMedias(customProduct);
    this.updateControlsValidity();
    this.isFormValid.emit(this.formGroup.valid);
  }

  private getMedias(customProduct: CustomProduct) {
    customProduct.medias.forEach((media: ProductMedia) => {
      this.productsService.getMediaUrl(customProduct.product_ean, media.id).subscribe((url: string) => {
        this.fetchToBlob(url).then((data) => {
          if (data) {
            if (media.type === 'picture') {
              this.images.push({blob: data, url: this.addVersionToUrl(url), media});
              this.images = this.sortMediaArrayFromMediaIndexes(this.images);
            } else if (media.type === 'video') {
              this.videos.push({blob: data, url: this.addVersionToUrl(url), media, name: undefined});
              this.videos = this.sortMediaArrayFromMediaIndexes(this.videos);
            }
          }
        });
      });
    });
  }

  rotateMedia(media: ProductMedia, rotation: number) {
    if (media.type !== 'picture') {
      return;
    }
    this.productsService.rotateMedia(this.product.product_ean, media.id, rotation).subscribe(res => {
      const curMediaIndex = this.product.medias.findIndex(m => m.id === media.id);
      this.product.medias[curMediaIndex].id = res.media.id;
      const imageIndex = this.images.findIndex(img => img.media.id === media.id);
      this.fetchToBlob(res.url).then((blob) => {
        this.images[imageIndex].url = res.url;
        this.images[imageIndex].blob = blob;
      });
      this.images = [...this.images];
    });
  }

  private sortMediaArrayFromMediaIndexes(array) {
    return array.sort((a, b) => {
      const idx1 = this.product.medias.findIndex(m => m.id === a.media.id);
      const idx2 = this.product.medias.findIndex(m => m.id === b.media.id);

      return idx1 - idx2;
    });
  }

  private getCustomProductInfosByEan() {
    if (this.poi.data.ean && this.poi.data.ean.length > 0) {
      this.productsService.getProductByEan(this.poi.data.ean, undefined, this.translateService.currentLang)
          .subscribe((product: CustomProduct) => {
            this.product = product;
            this.setValuesFromCustomProduct(product);
          });
    }
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe((data) => {
      const createPoi = data.createPoi;

      this.putProduct().subscribe(
          (newProduct) => {
            this.poi.data = { ean: this.eanControl.value };
            if (createPoi) {
              if (this.poi.isShapePoi){
                this.showroomService.putShapePoi(this.poi).subscribe();
              }else{
                this.showroomService.putPoi(this.poi).subscribe();
              }
            }
            this.productFullyUpload.emit(newProduct);
          }
      );
    });
  }

  private addVersionToUrl(url: string): string {
    if (url) {
      const urlObj = new URL(url);
      // urlObj.searchParams.set('v', Math.random().toString());
      return urlObj.href;
    } else {
      return undefined;
    }
  }

  private fetchToBlob(url: string) {
    const builtForm = this.formBuilder.group({ profile: [''] });
    let status;

    return fetch(this.addVersionToUrl(url), {cache: 'no-cache'}).then((response) => {
      status = response.status;
      return response.blob();
    }).then((blob) => {
      if (status === 200) {
        builtForm.get('profile').setValue(blob);
        return builtForm.get('profile').value;
      }
      return undefined;
    });
  }

  set eanControlValue(ean: string) {
    this.eanControl.setValue(ean);
    this.formChanged.emit(this.formBuilder.group({
      ean: this.eanControl
    }));
  }
}
