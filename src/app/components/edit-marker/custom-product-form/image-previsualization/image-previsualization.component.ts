import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ImageMediaInfos} from '../../../../models/CustomProduct';

@Component({
  selector: 'app-image-previsualization',
  templateUrl: './image-previsualization.component.html',
  styleUrls: ['./image-previsualization.component.scss'],
})
export class ImagePrevisualizationComponent {
  @Input() image: ImageMediaInfos;
  @Input() index: number;
  @Output() deleteEvent = new EventEmitter();
  @Output() rotateEvent = new EventEmitter();

  constructor() { }

  deleteImage() {
    this.deleteEvent.emit(this.image.media.id);
  }

  rotateImage(deg: number) {
    this.rotateEvent.emit(deg);
  }

}
