import {Component, Input, Output, EventEmitter} from '@angular/core';
import {PdfMediaInfos} from '../../../../models/CustomProduct';

@Component({
    selector: 'app-pdf-previsualization',
    templateUrl: './pdf-previsualization.component.html',
    styleUrls: ['./pdf-previsualization.component.scss'],
})
export class PdfPrevisualizationComponent {
    @Input() pdf: PdfMediaInfos;
    @Input() index: number;
    @Input() isMobile: boolean;
    @Output() deleteEvent = new EventEmitter();

    constructor() { }

    deletePdf() {
        this.deleteEvent.emit(this.pdf.media.id);
    }

}
