import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {VideoMediaInfos} from '../../../../models/CustomProduct';

@Component({
  selector: 'app-video-previsualization',
  templateUrl: './video-previsualization.component.html',
  styleUrls: ['./video-previsualization.component.scss'],
})
export class VideoPrevisualizationComponent implements OnInit {
  @Input() video: VideoMediaInfos;
  @Input() isMobile: boolean;
  @Input() index: number;
  @Output() deleteEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  deleteVideo() {
    this.deleteEvent.emit(this.video.media.id);
  }

}
