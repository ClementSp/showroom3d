import {Component, ViewChild} from '@angular/core';
import {BarcodeFormat} from '@zxing/library';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-barcode-scanner-modal',
  templateUrl: './barcode-scanner-modal.component.html',
  styleUrls: ['./barcode-scanner-modal.component.scss'],
})
export class BarcodeScannerModalComponent {

  @ViewChild('scanner', { static: false })
  // scanner: ZXingScannerComponent;

  formats = [BarcodeFormat.EAN_8, BarcodeFormat.EAN_13, BarcodeFormat.CODE_128];
  isTorchCompatible = false;
  torchLid = false;
  currentDevice = null;
  camerasList = [];
  loading = true;
  error = null;

  constructor(private modalController: ModalController) {}

  scanSuccessHandler(result) {
    this.modalController.dismiss(result);
  }

  scanErrorHandler(result) {
    this.modalController.dismiss(null);
  }

  changeCamera(cameraId) {
    if (cameraId === this.currentDevice.deviceId) {
      return;
    }

    const cam = this.camerasList.find(c => c.deviceId === cameraId);

    if (!cam) {
      return;
    }

    this.currentDevice = cam;
  }

  torchCompatibleFound(event) {
    this.isTorchCompatible = event;
  }

  camerasFound(cameras) {
    this.camerasList = cameras;
    this.loading = false;
  }

  camerasNotFound() {
    this.loading = false;
    this.error = 'No camera found';
  }

  toggleTorch() {
    this.torchLid = !this.torchLid;
  }

  closeModal() {
    this.modalController.dismiss(null);
  }
}
