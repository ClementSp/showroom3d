import {AfterViewInit, Component, ComponentFactoryResolver, EventEmitter, Input, OnInit, Type, ViewChild} from '@angular/core';
import {Poi, ShapePoi} from '../../models/showroom.model';
import {IonSlides, ModalController} from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ShowroomsService} from '../../services/showrooms.service';
import {UserService} from '../../services/user.service';
import {KeyValue} from '@angular/common';
import {ClusterFormComponent} from './cluster-form/cluster-form.component';
import {VideoFormComponent} from './video-form/video-form.component';
import {ClusterOfCustomProductFormComponent} from './cluster-of-custom-product-form/cluster-of-custom-product-form.component';
import {PdfFormComponent} from './pdf-form/pdf-form.component';
import {DynamicFormDirective} from '../../directives/dynamic-form.directive';
import {DescriptorFormComponent} from './descriptor-form/descriptor-form.component';
import {userEnv} from '../../../environments/user.env';
import {EditorService} from '../../services/editor.service';
import {PoiAction} from '../../common/PoiAction';
import {ImageFormComponent} from './image-form/image-form.component';
import {ChatFormComponent} from './chat-form/chat-form.component';

interface ActionMarkerDataForm {
  poi: Poi|ShapePoi;
  confirmForm: EventEmitter<any>;
  formChanged: EventEmitter<FormGroup>;
  isFormValid: EventEmitter<boolean>;
}

@Component({
  selector: 'app-edit-marker',
  templateUrl: './edit-marker.component.html',
  styleUrls: ['./edit-marker.component.scss'],
})
export class EditMarkerComponent implements OnInit, AfterViewInit {

  @Input() poi: Poi | ShapePoi;
  @ViewChild('slider') slides: IonSlides;
  @ViewChild(DynamicFormDirective, {static: true}) formHost: DynamicFormDirective;

  poiSize = undefined;

  isDataFormValid = new BehaviorSubject<boolean>(false);
  formHasBeenConfirmedEvent = new EventEmitter();

  currentSliderPage = new BehaviorSubject(0);
  selectedType: BehaviorSubject<any>;
  sliderOpts = { simulateTouch: false, autoHeight: false };
  slideSpeed = undefined;
  actionMarkerTypes = new Map([
    ['product', 'Products'],
    ['productUrl', 'Products as URLs'],
    ['image', 'Images'],
    ['video', 'Video URL'],
    ['pdf', 'Pdf'],
    ['descriptor', 'Descriptor'],
    ['chat', 'Chat']
  ]);

  poiImages = new Map([
    ['default', 'Default'],
    ['picture', 'First product image'],
    ['logo', 'Product logo'],
  ]);

  formByAttributeTypeMap = new Map<string, Type<any>>([
    ['product', ClusterOfCustomProductFormComponent],
    ['productUrl', ClusterFormComponent],
    ['video', VideoFormComponent],
    ['image', ImageFormComponent],
    ['pdf', PdfFormComponent],
    ['descriptor', DescriptorFormComponent],
    ['chat', ChatFormComponent]
  ]);

  form: FormGroup;
  idControl: FormControl;
  tipControl: FormControl;
  typeControl: FormControl;
  poiImageControl: FormControl;
  originalOrder = (a: KeyValue<string, string>, b: KeyValue<string, string>): number => 0;

  constructor(private modalCtrl: ModalController,
              public userService: UserService,
              private showroomService: ShowroomsService,
              private editorService: EditorService,
              private componentFactoryResolver: ComponentFactoryResolver,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.selectedType = new BehaviorSubject<any>(this.poi.type);
    this.idControl = new FormControl(this.poi.id);
    this.tipControl = new FormControl(this.poi.tip);
    this.typeControl = new FormControl(this.poi.type, Validators.required);
    this.poiImageControl = new FormControl(this.poi.sprite?.type);
    this.poiSize = this.poi.size ? this.poi.size * 100 : 0;
    this.form = this.formBuilder.group({
      id: this.idControl,
      tip: this.tipControl,
      type: this.typeControl,
      poiImage: this.poiImageControl,
      data: undefined
    });
    if (userEnv.isPersonalShopper()) {
      this.slideSpeed = 0;
      this.currentSliderPage.next(1);
    }
    this.loadForm();
  }

  get userEnv() {
    return userEnv;
  }

  loadForm() {
    const type = this.selectedType.value;
    const formItem = this.formByAttributeTypeMap.get(type);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(formItem);

    const viewContainerRef = this.formHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<ActionMarkerDataForm>(componentFactory);
    componentRef.instance.isFormValid.subscribe(($event) => this.setDataFormValidity($event));
    componentRef.instance.formChanged.subscribe(($event) => this.dataFormChanged($event));
    componentRef.instance.poi = this.poi;
    componentRef.instance.confirmForm = this.formHasBeenConfirmedEvent;

    const element: HTMLElement = componentRef.location.nativeElement as HTMLElement;
    element.style.width = '100%';
  }

  dataFormChanged(form: FormGroup) {
    this.form.controls.data = form;
  }

  setDataFormValidity(value: boolean) {
    this.isDataFormValid.next(value);
  }

  ngAfterViewInit(): void {
    this.slides.ionSlidesDidLoad.subscribe(() => {
      this.currentSliderPage.subscribe((value) => {
        this.slides.slideTo(value, this.slideSpeed)
            .then(() => {
              this.slides.update();
            })
            .catch(() => alert('error'));
      });
    });
  }

  async confirm() {
    let poi: Poi | ShapePoi ;
    this.formHasBeenConfirmedEvent.emit({createPoi: true});
    poi = Object.assign(this.poi, { ...this.form.getRawValue() });
    await this.modalCtrl.dismiss(poi);
  }

  back() {
    this.currentSliderPage.next(this.currentSliderPage.value - 1);
  }

  formIsValid() {
    return this.form.controls.tip.valid
        && this.form.controls.id.valid
        && this.form.controls.type.valid
        && this.isDataFormValid.value;
  }

  next() {
    this.poi = Object.assign(this.poi, { ...this.form.getRawValue() });
    if (this.poi) {
      if (this.isShapePoi(this.poi)){
        this.showroomService.putShapePoi(this.poi).subscribe();
      }else{
        this.showroomService.putPoi(this.poi).subscribe();
      }
    }

    this.currentSliderPage.next(this.currentSliderPage.value + 1);
  }

  typeChanged(value: string) {
    this.selectedType.next(value);
    this.loadForm();
  }

  poiImageChanged(value: string) {
    if (value !== 'default') {
      if (!this.poi.sprite) {
        this.poi.sprite = {
          type: '',
          path: ''
        };
      }
      this.poi.sprite.type = value;
    } else {
      this.poi.sprite = undefined;
    }
  }

  public assignColorToSprite(color: string) {
    if (!this.poi.sprite) {
      this.poi.sprite = {
        path: '',
        type: '',
        data: {}
      };
    }
    if (!this.poi.sprite.data) {
      this.poi.sprite.data = {};
    }
    this.poi.sprite.data.color = color;
  }

  poiSizeChanged(value) {
    this.poi.size = value / 100;
  }

  movePoi() {
    return this.modalCtrl.dismiss({action: PoiAction.MOVE, poi: this.poi});
  }

  removePoi() {
    return this.modalCtrl.dismiss({action: PoiAction.REMOVE, poi: this.poi});
  }

  async cancel() {
    await this.modalCtrl.dismiss({action: PoiAction.EDIT, poi: this.poi});
  }

   isShapePoi(poi: any) {
     return poi?.isShapePoi;
  }
}
