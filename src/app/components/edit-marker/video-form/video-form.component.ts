import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Poi, ShapePoi} from '../../../models/showroom.model';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import Utils from '../../../models/Utils';
import {ShowroomsService} from '../../../services/showrooms.service';
import {BehaviorSubject, Subscription} from 'rxjs';
import {CustomerService} from '../../../services/customer.service';
import {TranslateService} from '@ngx-translate/core';

interface IVideo {
  url: string;
  language: string;
  openInNewTab: boolean;
}

@Component({
  selector: 'app-video-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.scss'],
})
export class VideoFormComponent implements OnInit, OnDestroy {

  @Input() poi: any;
  @Input() confirmForm: EventEmitter<any>;
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();

  videosFormArray: FormArray;
  formGroup: FormGroup;

  onConfirmSubscription: Subscription;
  onUpdateValiditySubscription: Subscription;

  availableLanguages = new BehaviorSubject<Array<string>>([]);

  constructor(private formBuilder: FormBuilder,
              private showroomService: ShowroomsService,
              private translateService: TranslateService,
              public customerService: CustomerService) {
    this.availableLanguages.next(translateService.langs);
  }

  ngOnInit() {
    this.buildFormGroup();
    this.onFormConfirmation();
    this.emitFormOnValidityCheck();
    this.initAvailableLanguages();
    this.isFormValid.emit(this.formGroup.valid);
  }

  ngOnDestroy(): void {
    this.onConfirmSubscription.unsubscribe();
    this.onUpdateValiditySubscription.unsubscribe();
  }

  addVideo() {
    let language: string;

    if (this.availableLanguages.value.length > 0) {
      language = this.availableLanguages.value[0];
      this.videosFormArray.push(new FormGroup({
        url: new FormControl('', [Utils.urlValidator, Validators.required]),
        language: new FormControl(language, [Validators.required]),
        openInNewTab: new FormControl(false),
      }));
      this.removeAvailableLanguage(language);
      this.isFormValid.emit(this.formGroup.valid);
    }
  }

  addAvailableLanguage(language: string) {
    const availableLanguages = [...this.availableLanguages.value];

    availableLanguages.push(language);
    this.availableLanguages.next(availableLanguages);
  }

  removeAvailableLanguage(language: string) {
    const availableLngs = [...this.availableLanguages.value];
    const indexOfLng = availableLngs.indexOf(language);

    if (indexOfLng >= 0) {
      availableLngs.splice(indexOfLng, 1);
      this.availableLanguages.next(availableLngs);
    }
  }

  deleteItem(index: number) {
    const language = this.videosFormArray.controls[index].value.language;

    this.videosFormArray.removeAt(index);
    this.addAvailableLanguage(language);
    this.updateControlsValidity();
  }

  updateControlsValidity() {
    Object.keys(this.formGroup.controls).forEach((e) => {
      this.formGroup.controls[e].updateValueAndValidity();
    });
    this.isFormValid.emit(this.formGroup.valid);
  }

  onLanguageChanged(index: number, toAdd: any): void {
    const toRemove = '' + this.videosFormArray.controls[index].value.language;

    this.removeAvailableLanguage(toRemove);
    this.addAvailableLanguage(toAdd);
  }

  getAvailableLanguages(index: number): Array<string> {
    const availableLanguages = new Array<string>(...this.availableLanguages.value);
    const usedLanguages = this.videosFormArray.controls
        .map((control: FormGroup) => control.value)
        .map((lng: IVideo) => lng.language);

    availableLanguages.push(usedLanguages[index]);
    return availableLanguages;
  }

  private initAvailableLanguages() {
    let availableLanguages = new Array<string>(...this.availableLanguages.value);
    const alreadyUsedLanguages = this.videosFormArray.controls
        .map((control: FormGroup) => control.value)
        .map((lng: IVideo) => lng.language);

    alreadyUsedLanguages.forEach((alreadyUsedLng: string) => {
      availableLanguages = availableLanguages.filter(language => language !== alreadyUsedLng);
    });
    this.availableLanguages.next(availableLanguages);
  }

  private emitFormOnValidityCheck() {
    this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
      this.formChanged.emit(this.formGroup);
    });
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe(() => {
      this.poi.data = this.formGroup.getRawValue();
      if (this.poi.isShapePoi){
        this.showroomService.putShapePoi(this.poi).subscribe();
      }else{
        this.showroomService.putPoi(this.poi).subscribe();
      }
    });
  }

  private buildFormGroup() {
    this.videosFormArray = new FormArray([]);

    if (this.poi?.data?.videos) {
      const videos: Array<IVideo> = this.poi.data.videos;

      videos.forEach((video) => {
        this.videosFormArray.controls.push(new FormGroup({
          url: new FormControl(video.url, [Utils.urlValidator, Validators.required]),
          language: new FormControl(video.language),
          openInNewTab: new FormControl(video.openInNewTab),
        }));
        this.removeAvailableLanguage(video.language);
      });
    }

    this.formGroup = this.formBuilder.group({
      videos: this.videosFormArray
    });
  }
}
