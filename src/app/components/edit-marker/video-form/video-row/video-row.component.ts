import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Platform} from '@ionic/angular';
import {FormGroup} from '@angular/forms';
import Utils from '../../../../models/Utils';

@Component({
    selector: 'app-video-row',
    templateUrl: './video-row.component.html',
    styleUrls: ['./video-row.component.scss'],
})
export class VideoRowComponent implements OnInit {

    @Input() videoFormGroup: FormGroup;
    @Input() localLanguageList: Array<string>;

    @Output() deleteEvent = new EventEmitter<void>();
    @Output() fieldUpdated = new EventEmitter<string>();
    @Output() languageChanged = new EventEmitter<string>();

    savedValue: string;

    constructor(public platform: Platform) {
    }

    ngOnInit() {
        this.savedValue = this.videoFormGroup.controls.language.value;
    }

    onLanguageChange($event) {
        let oldValue = this.savedValue;
        const value = $event.detail.value;

        this.videoFormGroup.controls.language.setValue(value);
        this.languageChanged.emit(oldValue);
        this.fieldUpdated.emit();
        this.savedValue = value;
    }

    onUrlChanged(resp: any) {
        let url = resp.detail.value;

        url = Utils.transformToYoutubeEmbedUrl(url);
        this.videoFormGroup.controls.url.setValue(url);
        this.fieldUpdated.emit('url');
    }

    delete() {
        this.deleteEvent.emit();
    }
}
