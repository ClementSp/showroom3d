import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Poi} from '../../../models/showroom.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ShowroomsService} from '../../../services/showrooms.service';

@Component({
  selector: 'app-descriptor-form',
  templateUrl: './descriptor-form.component.html',
  styleUrls: ['./descriptor-form.component.scss'],
})
export class DescriptorFormComponent implements OnInit, OnDestroy {

  @Input() poi: any;
  @Input() confirmForm: EventEmitter<any>;
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();

  formGroup: FormGroup;

  onConfirmSubscription: Subscription;
  onUpdateValiditySubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private showroomService: ShowroomsService) { }

  ngOnInit() {
    this.buildFormGroup();
    this.onFormConfirmation();
    this.emitFormOnValidityCheck();
    this.isFormValid.emit(this.formGroup.valid);
  }

  ngOnDestroy(): void {
    this.onConfirmSubscription.unsubscribe();
    this.onUpdateValiditySubscription.unsubscribe();
  }

  private emitFormOnValidityCheck() {
    this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
      this.formChanged.emit(this.formGroup);
    });
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe(() => {
      this.poi.data = this.formGroup.getRawValue();
      if (this.poi.isShapePoi){
        this.showroomService.putShapePoi(this.poi).subscribe();
      }else{
        this.showroomService.putPoi(this.poi).subscribe();
      }
    });
  }

  private buildFormGroup() {
    this.formGroup = this.formBuilder.group({});
  }
}
