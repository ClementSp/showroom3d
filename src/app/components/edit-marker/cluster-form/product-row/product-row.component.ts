import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IProduct} from '../../../../models/ClusterData';
import {BehaviorSubject} from 'rxjs';
import {IonItemSliding, Platform} from '@ionic/angular';

@Component({
  selector: 'app-product-row',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.scss'],
})
export class ProductRowComponent implements OnInit {

  @Input() product: IProduct;
  @Input() index: number;
  @Input() expandedRow: BehaviorSubject<number>;
  @Input() formGroup: any;

  @Output() deleteEvent = new EventEmitter<number>();
  @Output() fieldUpdated = new EventEmitter<string>();

  constructor(public platform: Platform) {
  }

  ngOnInit() {}

  toggleContent() {
    if (this.expandedRow.value === this.index) {
      this.expandedRow.next(-1);
    } else {
      this.expandedRow.next(this.index);
    }
  }

  updateValueAndValidity(key: string) {
    this.fieldUpdated.emit(key);
  }

  delete() {
    this.deleteEvent.emit(this.index);
  }
}
