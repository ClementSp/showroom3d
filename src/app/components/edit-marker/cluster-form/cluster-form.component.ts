import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Poi} from '../../../models/showroom.model';
import ClusterData from '../../../models/ClusterData';
import {BehaviorSubject, Subscription} from 'rxjs';
import {ModalController, Platform} from '@ionic/angular';
import Utils from '../../../models/Utils';
import {ShowroomsService} from '../../../services/showrooms.service';
import {ProductUrlFinderComponent} from '../../product-url-finder/product-url-finder.component';
import {showroomEnv} from '../../../../environments/showroom.env';

@Component({
  selector: 'app-cluster-form',
  templateUrl: './cluster-form.component.html',
  styleUrls: ['./cluster-form.component.scss'],
})
export class ClusterFormComponent implements OnInit, OnDestroy {

  @Input() poi: any;
  @Input() parentForm: FormGroup;
  @Input() confirmForm: EventEmitter<any>;
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();

  formGroup: FormGroup;
  clusterNameControl: FormControl;
  productsFormArray: FormArray;
  expandedRow = new BehaviorSubject(-1);
  clusterData: ClusterData;
  onConfirmSubscription: Subscription;
  onUpdateValiditySubscription: Subscription;

  isApi = false;

  constructor(private modalController: ModalController,
              private formBuilder: FormBuilder,
              private showroomService: ShowroomsService,
              public platform: Platform) {
  }

  ngOnInit() {
    this.buildFormGroup();
    this.onFormConfirmation();
    this.emitFormOnValidityCheck();
    this.isFormValid.emit(this.formGroup.valid);
    this.isApi = showroomEnv.api && showroomEnv.api !== '';
  }

  ngOnDestroy(): void {
    this.onConfirmSubscription.unsubscribe();
    this.onUpdateValiditySubscription.unsubscribe();
  }

  addProduct() {
    const product = this.clusterData.pushProduct( '', '', '', '' , undefined, false);
    this.productsFormArray.push(new FormGroup({
      name: new FormControl(product.name),
      ean: new FormControl(product.ean),
      imgUrl1: new FormControl(product.imgUrl1),
      imgUrl2: new FormControl(product.imgUrl2),
      productUrl: new FormControl(product.productUrl, [Utils.urlValidator, Validators.required]),
      openInNewTab: new FormControl(product.openInNewTab)
    }));
    this.isFormValid.emit(this.formGroup.valid);
  }

  reorderItems(ev) {
    const itemMove = this.clusterData.products.splice(ev.detail.from, 1)[0];
    const formMove = this.productsFormArray.at(ev.detail.from);

    this.productsFormArray.removeAt(ev.detail.from);
    this.productsFormArray.insert(ev.detail.to, formMove);
    this.clusterData.products.splice(ev.detail.to, 0, itemMove);
    ev.detail.complete();
  }

  deleteItem(index: number) {
    this.clusterData.products.splice(index, 1);
    this.productsFormArray.removeAt(index);
  }

  updateControlsValidity(key: string) {
    Object.keys(this.formGroup.controls).forEach((e) => {
      this.formGroup.controls[e].updateValueAndValidity();
    });
    this.isFormValid.emit(this.formGroup.valid);
  }

  private emitFormOnValidityCheck() {
    this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
      this.formChanged.emit(this.formGroup);
    });
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe(() => {
      this.poi.data = this.formGroup.getRawValue();
      if (this.poi.isShapePoi){
        this.showroomService.putShapePoi(this.poi).subscribe();
      }else{
        this.showroomService.putPoi(this.poi).subscribe();
      }
    });
  }

  private buildFormGroup() {
    this.clusterData = new ClusterData(this.poi);
    this.clusterNameControl = new FormControl(this.clusterData.name, Validators.required);
    this.productsFormArray = new FormArray([]);
    this.clusterData.products.forEach((product) => {
      this.productsFormArray.controls.push(new FormGroup({
        name: new FormControl(product.name),
        ean: new FormControl(product.ean, Validators.required),
        imgUrl1: new FormControl(product.imgUrl1),
        imgUrl2: new FormControl(product.imgUrl2),
        productUrl: new FormControl(product.productUrl, Validators.required),
        openInNewTab: new FormControl(product.openInNewTab)
      }));
    });
    this.formGroup = this.formBuilder.group({
      name: this.clusterNameControl,
      products: this.productsFormArray
    });
  }

  async openProductFinder(sortBy) {
    const modal = await this.modalController.create({
      component: ProductUrlFinderComponent,
      cssClass: 'product-finder backdrop-05 fit-content-modal',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        multipleSelect: true,
        sort: sortBy
      }
    });
    modal.onDidDismiss().then((response) => response.data).then(products => {
      products.forEach(product => {
        this.productsFormArray.push(new FormGroup({
          name: new FormControl(product.name),
          ean: new FormControl(product.ean),
          imgUrl1: new FormControl(product.imgUrl1),
          imgUrl2: new FormControl(product.imgUrl2),
          productUrl: new FormControl(product.productUrl, [Utils.urlValidator, Validators.required]),
          openInNewTab: new FormControl(product.openInNewTab)
        }));
        this.clusterData.pushProduct(product.productUrl, product.imgUrl1, product.imgUrl2, product.name , product.ean, product.openInNewTab);
        this.isFormValid.emit(this.formGroup.valid);
      });
    });
    return await modal.present();
  }

}
