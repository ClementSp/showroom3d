import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {defaultSprites} from '../../../common/PoiDefaultSprites';

@Component({
  selector: 'app-poi-color-picker',
  templateUrl: './poi-color-picker.component.html',
  styleUrls: ['./poi-color-picker.component.scss'],
})
export class PoiColorPickerComponent implements OnInit {
  @Input() current: string;
  @Output() colorSelected = new EventEmitter();
  public currentSprite = null;
  public sprites = defaultSprites;

  constructor() { }

  ngOnInit() {
    return this.getSprite();
  }

  private async getSprite() {
    if (!this.current) {
      this.current = 'blue';
    }

    this.currentSprite = this.sprites.get(this.current) ?? this.sprites.get('blue');
  }

  selectSprite(key) {
    this.colorSelected.emit(key);
    this.getSprite();
  }
}
