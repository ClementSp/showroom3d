import { Component, OnInit } from '@angular/core';
import {defaultSprites} from '../../../../common/PoiDefaultSprites';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-poi-sprite-picker',
  templateUrl: './poi-sprite-picker.component.html',
  styleUrls: ['./poi-sprite-picker.component.scss'],
})
export class PoiSpritePickerComponent implements OnInit {

  public sprites = defaultSprites;

  constructor(private popoverController: PopoverController) { }

  ngOnInit() {}

  public async selectSprite(sprite: string) {
    return this.popoverController.dismiss(sprite);
  }

}
