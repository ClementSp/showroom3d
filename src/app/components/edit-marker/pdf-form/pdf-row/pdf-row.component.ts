import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Platform} from '@ionic/angular';
import {CustomerService} from '../../../../services/customer.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FileUploader, FileItem} from 'ng2-file-upload';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {MediasService} from '../../../../services/medias.service';
import {BehaviorSubject} from 'rxjs';

@Component({
    selector: 'app-pdf-row',
    templateUrl: './pdf-row.component.html',
    styleUrls: ['./pdf-row.component.scss'],
})
export class PdfRowComponent implements OnInit {

    @Input() pdfFormGroup: FormGroup;
    @Input() localLanguageList: Array<string>;
    @Input() poiId: string;
    @Input() index: number;

    @Output() deleteEvent = new EventEmitter<void>();
    @Output() fieldUpdated = new EventEmitter<string>();
    @Output() languageChanged = new EventEmitter<string>();

    @ViewChild('fileInputVideo') fileInputVideo: any;

    pdfName: string;
    savedValue: string;
    uploader: FileUploader;
    isLoading = new BehaviorSubject<boolean>(false);

    allowedFileExt = ['pdf'];

    constructor(public platform: Platform,
                private formBuilder: FormBuilder,
                public customerService: CustomerService,
                private showroomService: ShowroomsService,
                private mediasService: MediasService) {
        this.uploader = new FileUploader({queueLimit: 1});
        this.uploader.onAfterAddingFile = (file: FileItem) => {
            file.withCredentials = false;
            file.method = 'POST';
        };
    }

    ngOnInit(): void {
        this.savedValue = this.pdfFormGroup.controls.language.value;
    }

    onLanguageChange($event) {
        let oldValue = this.savedValue;
        const value = $event.detail.value;

        this.pdfFormGroup.controls.language.setValue(value);
        this.languageChanged.emit(oldValue);
        this.fieldUpdated.emit();
        this.savedValue = value;
    }


    // TODO: change mediasService.updatePoiPdf to mediasService.postMedia
    uploadPdf() {
        const getFileExtension = (filename: string) => filename.split('.').pop();
        const file: FileItem = this.uploader.queue[0];
        const fileExtension = getFileExtension(file._file.name);

        this.isLoading.next(true);
        if (this.allowedFileExt.includes(fileExtension.toLocaleLowerCase())) {
            this.mediasService.updatePoiPdf(this.poiId, file._file, this.pdfFormGroup.controls.language.value).subscribe(resp => {
                const url = resp.url;
                const filename = resp.filename;

                this.pdfFormGroup.controls.url.setValue(url);
                this.pdfFormGroup.controls.filename.setValue(filename);
                this.fieldUpdated.emit();
                this.isLoading.next(false);
            });
        }
        this.fileInputVideo.nativeElement.value = "";
        this.uploader.clearQueue();
    }

    download() {
        const url = this.pdfFormGroup.controls.url.value;

        window.open(url);
    }

    delete() {
        const language = this.pdfFormGroup.controls.language.value;

        this.deleteEvent.emit(language);
    }
}
