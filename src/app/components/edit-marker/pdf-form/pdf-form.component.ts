import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Poi} from '../../../models/showroom.model';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {ShowroomsService} from '../../../services/showrooms.service';
import Utils from '../../../models/Utils';
import {MediasService} from '../../../services/medias.service';
import {UserService} from '../../../services/user.service';

export interface IPdf {
    url: string;
    language: string;
    filename: string;
}

@Component({
    selector: 'app-pdf-form',
    templateUrl: './pdf-form.component.html',
    styleUrls: ['./pdf-form.component.scss'],
})
export class PdfFormComponent implements OnInit, OnDestroy {

    @Input() poi: any;
    @Input() confirmForm: EventEmitter<any>;
    @Output() formChanged = new EventEmitter<FormGroup>();
    @Output() isFormValid = new EventEmitter<boolean>();

    pdfsFormArray: FormArray;
    formGroup: FormGroup;
    onConfirmSubscription: Subscription;
    onUpdateValiditySubscription: Subscription;

    availableLanguages = new BehaviorSubject<Array<string>>([]);

    /*
    *   Push into this array when pdf is delete from list
    *   and only subscribe on form confirmation (to delete)
    */
    mediaToDeleteObsArray = new Array<Observable<any>>();

    readonly defaultLanguage = 'fr';

    constructor(private formBuilder: FormBuilder,
                private mediasService: MediasService,
                private showroomService: ShowroomsService,
                private userService: UserService) {
        this.availableLanguages.next(userService.handleLng);
    }

    ngOnInit() {
        this.buildFormGroup();
        this.onFormConfirmation();
        this.emitFormOnValidityCheck();
        this.isFormValid.emit(this.formGroup.valid);
    }

    ngOnDestroy(): void {
        this.onConfirmSubscription.unsubscribe();
        this.onUpdateValiditySubscription.unsubscribe();
    }

    getAvailableLanguages(index: number): Array<string> {
        const availableLanguages = new Array<string>(...this.availableLanguages.value);
        const usedLanguages = this.pdfsFormArray.controls
            .map((control: FormGroup) => control.value)
            .map((lng: IPdf) => lng.language);

        availableLanguages.push(usedLanguages[index]);
        return availableLanguages;
    }

    onLanguageChanged(index: number, toAdd: any): void {
        const toRemove = '' + this.pdfsFormArray.controls[index].value.language;

        this.removeAvailableLanguage(toRemove);
        this.addAvailableLanguage(toAdd);
    }

    addAvailableLanguage(language: string) {
        const availableLanguages = [...this.availableLanguages.value];

        availableLanguages.push(language);
        this.availableLanguages.next(availableLanguages);
    }

    removeAvailableLanguage(language: string) {
        const availableLngs = [...this.availableLanguages.value];
        const indexOfLng = availableLngs.indexOf(language);

        if (indexOfLng >= 0) {
            availableLngs.splice(indexOfLng, 1);
            this.availableLanguages.next(availableLngs);
        }
    }

    private emitFormOnValidityCheck() {
        this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
            this.formChanged.emit(this.formGroup);
        });
    }

    private onFormConfirmation() {
        this.onConfirmSubscription = this.confirmForm.subscribe(() => {
            this.poi.data.pdfs = this.pdfsFormArray.getRawValue();
            if (this.poi.isShapePoi){
                this.showroomService.putShapePoi(this.poi).subscribe();
            }else{
                this.showroomService.putPoi(this.poi).subscribe();
            }
            this.mediaToDeleteObsArray.forEach((deleteMedia$: Observable<any>) => {
                deleteMedia$.subscribe();
            });
        });
    }

    private buildFormGroup() {
        this.pdfsFormArray = new FormArray([]);

        if (this.poi?.data?.pdfs) {
            const pdfs: Array<IPdf> = this.poi.data.pdfs;

            pdfs.forEach((pdf) => {
                this.pdfsFormArray.controls.push(new FormGroup({
                    url: new FormControl(pdf.url, [Utils.urlValidator, Validators.required]),
                    language: new FormControl(pdf.language),
                    filename: new FormControl(pdf.filename, [Validators.required])
                }));
                this.removeAvailableLanguage(pdf.language);
            });
        }

        this.formGroup = this.formBuilder.group({
            pdfs: this.pdfsFormArray
        });
    }

    addPdf() {
        let language: string;

        if (this.availableLanguages.value.length > 0) {
            language = this.availableLanguages.value[0];
            this.pdfsFormArray.push(new FormGroup({
                url: new FormControl('', [Utils.urlValidator, Validators.required]),
                language: new FormControl(language, [Validators.required]),
                filename: new FormControl('', [Validators.required])
            }));
            this.removeAvailableLanguage(language);
            this.isFormValid.emit(this.formGroup.valid);
        }
    }

    deleteItem(index: number, language: any) {
        this.pdfsFormArray.removeAt(index);
        this.addAvailableLanguage(language);
        this.mediaToDeleteObsArray.push(this.mediasService.deletePoiPdf(this.poi.id, language));
        this.updateControlsValidity();
    }

    updateControlsValidity() {
        Object.keys(this.formGroup.controls).forEach((e) => {
            this.formGroup.controls[e].updateValueAndValidity();
        });
        this.isFormValid.emit(this.formGroup.valid);
    }
}
