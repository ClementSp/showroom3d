import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject, Subscription} from 'rxjs';
import {ModalController} from '@ionic/angular';
import {ProductFinderComponent} from '../../product-finder/product-finder.component';
import CustomProduct from '../../../models/CustomProduct';
import {Poi} from '../../../models/showroom.model';
import {ShowroomsService} from '../../../services/showrooms.service';
import {CreateProductComponent} from '../cluster-of-custom-product-form/create-product/create-product.component';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ProductsService} from '../../../services/products.service';
import {CustomerService} from '../../../services/customer.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-custom-product',
  templateUrl: './custom-product.component.html',
  styleUrls: ['./custom-product.component.scss'],
})
export class CustomProductComponent implements OnInit {

  @Input() poi: any;
  @Input() confirmForm: EventEmitter<any>;
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();

  errorMessage = new BehaviorSubject<string>(undefined);
  product = new BehaviorSubject<CustomProduct>(undefined);
  isExpanded = new BehaviorSubject<boolean>(false);

  formGroup: FormGroup;
  productEanControl: FormControl;
  templateControl: FormControl;

  onUpdateValiditySubscription: Subscription;
  onConfirmSubscription: Subscription;

  templateList = [
    'Default', 'template 2', 'template 3', 'template 4', 'Two Steps', 'Premium'
  ];

  constructor(private modalController: ModalController,
              private formBuilder: FormBuilder,
              private translateService: TranslateService,
              private customerService: CustomerService,
              private productsService: ProductsService,
              private showroomService: ShowroomsService) {
  }

  ngOnInit() {
    this.buildFormGroup();
    this.onFormConfirmation();
    this.emitFormOnValidityCheck();
    this.isFormValid.emit(this.formGroup.valid);
  }

  async openProductFinder() {
    const modal = await this.modalController.create({
      component: ProductFinderComponent,
      cssClass: 'product-finder backdrop-05 fit-content-modal',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        multipleSelect: false
      }
    });
    modal.onDidDismiss().then((response) => response.data).then((eanList: Array<string>) => {
      const ean = eanList[0];

      if (ean) {
        this.productsService.getProductByEan(ean, undefined, this.translateService.currentLang).subscribe((product: any) => {
          this.product.next(product);
          this.productEanControlValue = ean;
        });
      }
    });
    return await modal.present();
  }

  async openProductCreator(ean?: string, updateProduct: boolean = false) {
    const modal = await this.modalController.create({
      component: CreateProductComponent,
      cssClass: 'edit-marker-component backdrop-05',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        poi: {data: {ean}},
        updateProduct
      }
    });
    modal.onDidDismiss().then((response) => response.data).then((product: any) => {
      if (product) {
        this.product.next(product);
        this.productEanControlValue = product.product_ean;
      }
    });
    return await modal.present();
  }

  deleteProduct() {
    this.product.next(undefined);
    this.productEanControlValue = undefined;
  }

  toggleContent() {
    this.isExpanded.next(!this.isExpanded.value);
  }

  set productEanControlValue(ean: string) {
    this.productEanControl.setValue(ean);
    this.formChanged.emit(this.formGroup);
  }

  set templateControlValue(template: string) {
    this.templateControl.setValue(template);
    this.formChanged.emit(this.formGroup);
  }

  get productEan() {
    let ean: string;

    if (this.product.value) {
      ean = this.product.value.product_ean;
    }
    return ean;
  }

  onTemplateChange(event) {
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe(() => {
      this.poi.data = this.formGroup.getRawValue();
      if (this.poi.isShapePoi){
        this.showroomService.putShapePoi(this.poi).subscribe();
      }else{
        this.showroomService.putPoi(this.poi).subscribe();
      }
    });
  }

  private emitFormOnValidityCheck() {
    this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
      this.formChanged.emit(this.formGroup);
    });
  }

  private buildFormGroup() {
    let ean;

    this.productEanControl = new FormControl('');
    this.templateControl = new FormControl('');
    if (this.poi && this.poi.data && this.poi.data.ean) {
      ean = this.poi.data.ean;
      this.productEanControlValue = ean;
      this.productsService.getProductByEan(ean, undefined, this.translateService.currentLang).subscribe((product: CustomProduct) => {
        this.product.next(product);
      });
    }
    if (this.poi && this.poi.data && this.poi.data.template) {
      this.templateControlValue = this.poi.data.template;
    } else {
      this.templateControlValue = '0';
    }
    this.formGroup = this.formBuilder.group({
      ean: this.productEanControl,
      template: this.templateControl
    });
  }
}
