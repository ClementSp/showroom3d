import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {ProductMedia} from '../../../../models/CustomProduct';
import {CustomMedia} from '../image-form.component';
import {MediasService} from '../../../../services/medias.service';

@Component({
  selector: 'app-image-row-preview',
  templateUrl: './image-row-preview.component.html',
  styleUrls: ['./image-row-preview.component.scss'],
})
export class ImageRowPreviewComponent implements OnInit {
  @Input() image: ProductMedia;
  @Output() deleteEvent = new EventEmitter();

  public isReady = false;
  public isMouseOver = false;
  public isDeleting = false;
  public media: CustomMedia = null;

  constructor(private mediaService: MediasService) { }

  ngOnInit() {
    this.mediaService.getMediasById(this.image.id).toPromise()
        .then((media: CustomMedia) => {
          this.media = media;
        }).finally(() => {
      this.isReady = true;
    });
  }

  public async deleteImage() {
    this.isDeleting = true;
    return this.deleteEvent.emit();
  }
}
