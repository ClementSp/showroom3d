import {Component, Input, OnInit, Output, EventEmitter, OnDestroy} from '@angular/core';
import {Poi} from '../../../models/showroom.model';
import {FileUploader} from 'ng2-file-upload';
import {ShowroomsService} from '../../../services/showrooms.service';
import {MediasService, MediaType} from '../../../services/medias.service';
import {ProductMedia} from '../../../models/CustomProduct';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';

export interface CustomMedia extends ProductMedia {
  url: string;
  fileName: string;
}

@Component({
  selector: 'app-image-form',
  templateUrl: './image-form.component.html',
  styleUrls: ['./image-form.component.scss'],
})
export class ImageFormComponent implements OnInit, OnDestroy {
  @Input() poi: any;
  @Input() confirmForm = new EventEmitter();
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();

  public uploader = new FileUploader({});
  public maxNbImages = 10;
  public formGroup: FormGroup;
  public isReady = false;

  private onConfirmSubscription: Subscription;
  private onUpdateValiditySubscription: Subscription;

  constructor(private mediaService: MediasService,
              private formBuilder: FormBuilder,
              private showroomService: ShowroomsService) { }

  ngOnInit() {
    this.buildFormGroup();
    this.onFormConfirmation();
    this.emitFormOnValidityCheck();
    this.isFormValid.emit(this.formGroup.valid);
    this.isReady = true;
  }

  ngOnDestroy() {
    this.onConfirmSubscription.unsubscribe();
    this.onUpdateValiditySubscription.unsubscribe();
  }

  public async addImage(media: CustomMedia) {
    if (!this.poi.data?.images) {
      this.poi.data = {images: []};
    }

    const image = {...media};
    delete image.url;
    delete image.fileName;

    this.poi.data.images.push(image);
    return this.savePoi();
  }

  public async deleteImage(index: number) {
    if (this.poi.data?.images[index]) {
      this.mediaService.deleteMediaById(this.poi.data?.images[index].id).toPromise()
          .then(() => {
            this.poi.data?.images.splice(index, 1);
            return this.savePoi();
          });
    }
  }

  public uploadMedia() {
    const file = this.uploader.queue[0]._file;

    if (!file) {
      return;
    }

    return this.mediaService.postMedia(MediaType.PICTURE, file, file.name).toPromise()
        .then((res) => {
          this.uploader.clearQueue();
          return this.addImage(res);
        });
  }

  public hasImages() {
    return this.images && this.images.length > 0;
  }

  public canUpload() {
    return this.images.length < this.maxNbImages;
  }

  private emitFormOnValidityCheck() {
    this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
      this.formChanged.emit(this.formGroup);
    });
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe(() => {
      this.poi.data = this.formGroup.getRawValue();
      if (this.poi.isShapePoi){
        this.showroomService.putShapePoi(this.poi).subscribe();
      }else{
        this.showroomService.putPoi(this.poi).subscribe();
      }
    });
  }

  private buildFormGroup() {
    const images = (this.poi.data?.images) ? [...this.poi.data?.images] : [];
    this.formGroup = this.formBuilder.group({
      images: new FormControl(images)
    });
  }

  get images() {
    return this.formGroup.controls.images.value;
  }


  private savePoi() {
    this.formChanged.emit(this.formGroup);
  }
}
