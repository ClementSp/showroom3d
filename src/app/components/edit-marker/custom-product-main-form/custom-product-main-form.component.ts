import {Component, Input, OnInit, Output, EventEmitter, AfterViewInit} from '@angular/core';
import CustomProduct from '../../../models/CustomProduct';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {CustomerService} from '../../../services/customer.service';
import {ProductsService} from '../../../services/products.service';
import {BarcodeScannerModalComponent} from '../custom-product-form/barcode-scanner-modal/barcode-scanner-modal.component';
import {ModalController} from '@ionic/angular';
import {customerEnv} from '../../../../environments/customer.env';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-custom-product-main-form',
  templateUrl: './custom-product-main-form.component.html',
  styleUrls: ['./custom-product-main-form.component.scss'],
})
export class CustomProductMainFormComponent implements OnInit {
  @Input() product: CustomProduct;
  @Input() isUpdating = false;
  @Input() hasScanner = false;
  @Input() hasPersonalEditor = false;
  @Output() isUpdatingChange = new EventEmitter<boolean>();
  @Output() productChange = new EventEmitter<CustomProduct>();
  @Output() loadingInProgress = new EventEmitter<boolean>();
  @Output() productValidityChange = new EventEmitter<boolean>();

  formGroup: FormGroup = undefined;
  warningEan = null;
  loadingLang = false;
  users = [];
  owner;

  constructor(public translateService: TranslateService,
              private formBuilder: FormBuilder,
              public customerService: CustomerService,
              private modalController: ModalController,
              public productsService: ProductsService,
              private userService: UserService) { }

  ngOnInit() {
    if (this.hasPersonalEditor) {
      this.userService.getUsers().subscribe(users => {
        this.users = users;
        this.owner = this.users.find(user => user.id === this.product.ownerId);
        this.initializeForm(this.product);
      });
    } else {
      this.initializeForm(this.product);
    }
  }

  private initializeForm(customProduct: CustomProduct) {
    const languageControl = new FormControl(this.translateService.currentLang, Validators.required);
    const eanControl = new FormControl({value: customProduct.product_ean, disabled: this.isUpdating}, Validators.required);
    const currencyControl = new FormControl(customProduct.currency, Validators.required);
    const userControl = new FormControl(this.owner);
    const priceControl = new FormControl(customProduct.sale_price, [Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]);
    const codeControl = new FormControl(customProduct.code, Validators.required);
    const labelControl = new FormControl(customProduct.label);
    const descriptionControl = new FormControl(customProduct.description);
    const detailsControl = new FormControl(customProduct.details);

    this.formGroup = this.formBuilder.group({
      ean: eanControl,
      price: priceControl,
      code: codeControl,
      description: descriptionControl,
      details: detailsControl,
      language: languageControl,
      currency: currencyControl,
      user: userControl,
      label: labelControl,
    });
    this.warningEan = null;
  }

  onLanguageChange($event) {
    const language = $event.detail.value;

    this.loadingLang = true;
    this.productsService.getProductByEan(this.product.product_ean, undefined, language).toPromise()
        .then((product: CustomProduct) => {
          this.formGroup.controls.label.setValue(product.label);
          this.formGroup.controls.description.setValue(product.description);
        }).finally(() => {
      this.loadingLang = false;
      this.translateService.use(language);
    });
    this.onProductChange();
  }

  onCurrencyChange($event) {
    customerEnv.currency = $event.detail.value;
    this.onProductChange();
  }

  onOwnerChange(event) {
    const id = event.detail.value;
    const owner = this.users.find(user => user.id === id);
    if (owner) {
      this.product.ownerId = id;
      this.onProductChange();
    }
  }

  async onEanChanged() {
    this.loadingInProgress.emit(true);
    this.productsService.getProductByEan(this.formGroup.controls.ean.value).subscribe(res => {
      if (res) {
        this.formGroup.controls.ean.setErrors({incorrect: true});
        this.warningEan = 'This Code/EAN is already linked to an existing product.';
      } else {
        this.formGroup.controls.ean.setErrors({incorrect: null});
        this.warningEan = null;
        this.formGroup.controls.ean.updateValueAndValidity();
      }
      this.loadingInProgress.emit(false);
      this.onProductChange();
    });
  }

  async scanProduct() {
    const modal = await this.modalController.create({
      component: BarcodeScannerModalComponent,
      cssClass: 'fit-content-modal barcode-scanner-modal',
      showBackdrop: true,
      backdropDismiss: true
    });
    modal.onDidDismiss().then((response) => response.data)
        .then((ean) => {
          if (ean) {
            this.updateProductFormFromEan(ean);
          }
        });
    return await modal.present();
  }

  async updateProductFormFromEan(ean: string) {
    this.productsService.getProductByEan(ean).toPromise()
        .then(res => {
          if (res) {
            this.isUpdating = true;
            this.isUpdatingChange.emit(true);
            this.initializeForm(res);
          }
        }).finally(() => {
          this.formGroup.controls.ean.setValue(ean);
    });
  }

  onProductChange() {
    this.product.product_ean = (this.warningEan) ? '' : this.formGroup.controls.ean.value;
    this.product.language = this.formGroup.controls.language.value;
    this.product.sale_price = this.formGroup.controls.price.value;
    this.product.currency = this.formGroup.controls.currency.value;
    this.product.code = this.formGroup.controls.code.value;
    this.product.description = this.formGroup.controls.description.value;
    this.product.details = this.formGroup.controls.details.value;
    this.product.label = this.formGroup.controls.label.value;
    this.productChange.emit(this.product);
    this.productValidityChange.emit(this.formGroup.valid);
  }
}
