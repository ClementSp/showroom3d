import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Poi} from '../../../models/showroom.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SafePipe} from '../../../pipes/safe.pipe';
import Utils from '../../../models/Utils';
import {ShowroomsService} from '../../../services/showrooms.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent implements OnInit, OnDestroy {

  @Input() poi: any;
  @Input() confirmForm: EventEmitter<any>;
  @Output() formChanged = new EventEmitter<FormGroup>();
  @Output() isFormValid = new EventEmitter<boolean>();

  formGroup: FormGroup;
  urlControl: FormControl;
  openInNewTabControl: FormControl;
  eanControl: FormControl;

  onConfirmSubscription: Subscription;
  onUpdateValiditySubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private showroomService: ShowroomsService,
              private safePipe: SafePipe) { }

  ngOnInit() {
    this.buildFormGroup();
    this.onFormConfirmation();
    this.emitFormOnValidityCheck();
    this.isFormValid.emit(this.formGroup.valid);
  }

  ngOnDestroy(): void {
    this.onConfirmSubscription.unsubscribe();
    this.onUpdateValiditySubscription.unsubscribe();
  }

  private emitFormOnValidityCheck() {
    this.onUpdateValiditySubscription = this.isFormValid.subscribe(() => {
      this.formChanged.emit(this.formGroup);
    });
  }

  private onFormConfirmation() {
    this.onConfirmSubscription = this.confirmForm.subscribe(() => {
      this.poi.data = this.formGroup.getRawValue();
      if (this.poi.isShapePoi){
        this.showroomService.putShapePoi(this.poi).subscribe();
      }else{
        this.showroomService.putPoi(this.poi).subscribe();
      }
    });
  }

  private buildFormGroup() {
    let url = '';
    let ean = '';
    let newTab = false;

    if (this.poi.data) {
      url = (this.poi.data.url) ? this.poi.data.url : '';
      ean = (this.poi.data.ean) ? this.poi.data.ean : '';
      newTab = (this.poi.data.openInNewTab) ? this.poi.data.openInNewTab : false;
    }
    this.urlControl = new FormControl(url, [ Utils.urlValidator, Validators.required ]);
    this.openInNewTabControl = new FormControl(newTab);
    this.eanControl = new FormControl(ean);
    this.formGroup = this.formBuilder.group({
      url: this.urlControl,
      openInNewTab: this.openInNewTabControl,
      ean: this.eanControl
    });
  }
}
