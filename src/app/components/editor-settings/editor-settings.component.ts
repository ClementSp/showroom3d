import {Component, Input, OnInit} from '@angular/core';
import {EditorService} from '../../services/editor.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-editor-settings',
  templateUrl: './editor-settings.component.html',
  styleUrls: ['./editor-settings.component.scss'],
})
export class EditorSettingsComponent implements OnInit {

  @Input() shellIsValid$ = new BehaviorSubject<boolean>(false);

  constructor(public editorService: EditorService) { }

  ngOnInit() {
  }

  get displayShellValue() {
    return this.editorService.settings.value.displayShell;
  }

  displayShellChanged(value: boolean) {
    const settings = this.editorService.settings.value;

    settings.displayShell = value;
    this.editorService.settings.next(settings);
  }
}
