import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import CustomProduct from '../../models/CustomProduct';
import {IProduct} from '../../models/product.model';

@Component({
  selector: 'app-object-configurator',
  templateUrl: './object-configurator.component.html',
  styleUrls: ['./object-configurator.component.scss'],
})

export class ObjectConfiguratorComponent implements OnInit, OnChanges {
  @Input() product: CustomProduct;
  @Input() currentConfig: any;
  @Output() configToSave = new EventEmitter<any>();
  selections: any;
  currentIProduct: IProduct;

  constructor() { }

  ngOnInit() {
    if (this.product.selections?.length) {
      this.selections = this.product.selections;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  getSelections() {
    return this.product?.selections;
  }

  saveConfiguration(configToSave: any) {
    this.configToSave.emit(configToSave);
  }

  getCurrentSelection(name) {
    if (this.currentConfig) {
      return this.currentConfig.get(name);
    }
   }

}
