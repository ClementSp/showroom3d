import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Configuration, ProductAdjustment, Selection, TextureConfig} from '../../../../models/CustomProduct';
import {Option} from '../../../../models/CustomProduct';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss'],
})
export class SelectionComponent implements  OnInit, OnChanges {
  @Input() selectionItem: Selection;
  @Input() currentConfig: any;
  @Output() configToSave = new EventEmitter<any>();
  option: Option;
  configuration: Configuration;
  config: TextureConfig;
  productAdjustment: ProductAdjustment;


  constructor() {}

  ngOnInit() {
    this.productAdjustment = new ProductAdjustment();
  }


  configurationChange() {
    this.productAdjustment.config = this.configuration;
    this.configToSave.emit(this.productAdjustment);
  }

  optionChange() {
    this.productAdjustment.name = this.selectionItem.name;
    this.productAdjustment.meshes = this.option.components;
    this.productAdjustment.option = this.option;
    this.productAdjustment.config = undefined;
    this.configuration = undefined;
    this.configToSave.emit(this.productAdjustment);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentConfig && changes.currentConfig.currentValue !== undefined) {
      const currentMotifs = changes.currentConfig.currentValue.get(this.selectionItem.name);

      if (currentMotifs) {
        if (!this.option) {
          this.option = this.selectionItem.options.find(opt => opt.name === currentMotifs?.option.name);
        }
        if (!this.configuration && this.option) {
          this.configuration =  this.option.configurations.find( conf => conf?.name === currentMotifs.config?.name);
        }
      }
    }
  }
}
