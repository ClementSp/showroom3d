import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IonicModule} from '@ionic/angular';

import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '../../shared.module';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {ObjectConfiguratorComponent} from './object-configurator.component';
import {SelectionComponent} from './components/selection/selection.component';
import {FlexModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [ ObjectConfiguratorComponent, SelectionComponent],
    providers: [],
    exports: [
        ObjectConfiguratorComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        TranslateModule,
        SharedModule,
        PdfViewerModule,
        FlexModule,
        FormsModule,
    ]
})
export class ObjectConfiguratorModule { }
