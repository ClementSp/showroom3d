import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {IonMenu, MenuController, ModalController, } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import {NewMenuItemFormComponent} from './new-menu-item-form/new-menu-item-form.component';
import {CurrentNodeHandler} from '../../DataHandlers/CurrentNode.handler';
import {ConfirmationModalComponent} from '../confirmation-modal/confirmation-modal.component';
import {EditorService} from '../../services/editor.service';
import {VideoPresentationFormComponent} from './video-presentation-form/video-presentation-form.component';
import {FileUploader} from 'ng2-file-upload';
import {UserService} from '../../services/user.service';
import {userEnv} from '../../../environments/user.env';
import {MenuLink, Showroom} from '../../models/showroom.model';
import {ShowroomHandler} from '../../DataHandlers/Showroom.handler';
import {EngineService} from '../../services/engine.service';
import {ExtendedMenuLink} from '../../models/Menu.model';
import {uuid4} from '@capacitor/core/dist/esm/util';
import {ShowroomsService} from '../../services/showrooms.service';
import {ToastService, ToastType} from '../../services/toast.service';
import {TranslateService} from '@ngx-translate/core';
import {SceneHandler} from '../../DataHandlers/scene.handler';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit, OnChanges {

  @ViewChild('menu') menu: IonMenu;

  @Input() logoUrl: string;
  @Input() title: string;
  @Input() presentationVideoUrl: string;
  @Input() presentationVideoAutoStart: boolean;
  @Input() itemList: Array<ExtendedMenuLink>;
  @Input() firebase: any;
  @Input() showroom: Showroom;
  @Input() toggleMenu: EventEmitter<any>;
  @Output() menuToggleEvent = new EventEmitter<any>();

  swipeGesture = false;
  uploader: FileUploader;

  constructor(private modalController: ModalController,
              private menuController: MenuController,
              public currentNode: CurrentNodeHandler,
              public engineService: EngineService,
              public editorService: EditorService,
              public userService: UserService,
              public platform: Platform,
              public showroomService: ShowroomsService,
              public translateService: TranslateService,
              public toastService: ToastService,
              public sceneHandler: SceneHandler) {
  }

  get userEnv() {
    return userEnv;
  }

  ngOnInit() {
    this.setExpanded();
    this.toggleMenu.subscribe(() => {
      this.menu.isOpen().then((isOpen) => {
        if (isOpen) {
          this.menu.close();
        } else {
          this.menu.open();
        }
      });
      this.menu.open(true);
    });
    this.uploader = new FileUploader({});
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown(event: any){
    if (event.target.tagName === 'ION-MENU') {
      this.menuController.close();
    }
  }

  setExpanded() {
    this.itemList.forEach(item => {
      if (item.level2) {
        item.hidden = true;
      } else {
        item.expanded = false;
      }
    });
  }

  updateExpanded() {
    let lastLvl1Expanded = false;
    this.itemList.forEach(item => {
      if (item.level2) {
        (lastLvl1Expanded ? item.hidden = false : item.hidden = true);
      } else {
        if (this.itemList.indexOf(item) === this.itemList.length - 1 || !this.itemList[this.itemList.indexOf(item) + 1].level2) {
          item.expanded = true;
        }
        lastLvl1Expanded = item.expanded;
      }
    });
  }

  @HostListener('mouseup', ['$event'])
  onMouseUp(event: any){
    if (event.target.tagName === 'ION-MENU') {
      // this.clickOutsideEvent.emit(event);
    }
  }

  onEntranceClicked() {
    const rotation = this.showroom.settings.entrance.rotation;
    const position = this.showroom.settings.entrance.position;

    this.sceneHandler.moveCamera(position);
    this.sceneHandler.rotateCamera(rotation);

    this.toggleMenu.emit();
  }

  onMenuItemClicked(item: any) {
    this.sceneHandler.moveCamera(item.cameraPosition);
    this.sceneHandler.rotateCamera(item.cameraRotation);
    this.toggleMenu.emit();
  }

  onMenuDidOpen() {
    this.menuToggleEvent.emit(null);
    this.swipeGesture = this.isMobile();
  }

  onMenuDidClose() {
    this.menuToggleEvent.emit(null);
    this.swipeGesture = false;
  }

  isMobile(): boolean {
    return this.platform.is('mobile');
  }

  updateTitle(data: any) {
    this.showroom.settings.menuTitle = data.detail.value;
    this.showroomService.updateSettings(this.showroom.settings).subscribe();
  }

  async deleteItem(id: string) {
    const modal = await this.modalController.create({
      component: ConfirmationModalComponent,
      cssClass: 'fit-content-modal',
      componentProps: {
        message: `Do you want to delete item from menu ?`
      }
    });
    modal.onDidDismiss().then((resp) => {
      if (resp.data) {
        this.showroomService.deleteMenuItem(id).subscribe(() => {
          this.showroom.menu = this.showroom.menu.filter(item => item.id !== id);
        });
        this.itemList = this.itemList.filter(item => item.id !== id);
      }
      this.updateExpanded();
    });
    return modal.present();
  }

  updateList(itemList) {
    this.showroomService.updateMenu(itemList);
  }

  deleteVideoPresentation() {
    this.showroom.settings.presentationVideoUrl = '';
    this.showroomService.updateSettings(this.showroom.settings).subscribe();
  }

  goToEntrance() {
    if (this.showroom.settings.entrance !== undefined) {
      this.onEntranceClicked();
    }
  }

  currentNodeIsEntrance() {
      return false;
  }

  setAsEntrance() {
    const settingsCopy = Object.assign({}, this.showroom.settings);
    settingsCopy.entrance = {
      position: {
        x: this.sceneHandler.camera.position.x,
        y: this.sceneHandler.camera.position.y,
        z: this.sceneHandler.camera.position.z
      },
      rotation: {
        x: this.sceneHandler.camera.rotation.x,
        y: this.sceneHandler.camera.rotation.y,
        z: this.sceneHandler.camera.rotation.z
      }
    };
    this.showroom.settings.entrance = settingsCopy.entrance;
    this.showroomService.updateSettings(settingsCopy).subscribe(() => {
    });
  }

  removeLogo() {
    this.showroom.settings.logoUrl = '';
    this.showroomService.updateSettings(this.showroom.settings).subscribe();
  }

  async addLogo() {
    const path = `shops/${userEnv.showroomId}/ui/logo/`;
    const uiStorage = this.firebase.storage().ref(path);
    const file = this.uploader.queue[0];
    const deletedPromises = [];
    const getFileExtension = (filename: string) => filename.split('.').pop();

    file.file.name = 'logo.' + getFileExtension(file._file.name);

    uiStorage.listAll().then((result) => {
      result.items.forEach((fileRef) => {
        if (fileRef.name.startsWith('logo.')) {
          deletedPromises.push(fileRef.delete());
        }
      });
      Promise.all(deletedPromises).then(() => {
        const logoRef = this.firebase.storage().ref(path + file.file.name);
        logoRef.put(file._file).then(() => {
          logoRef.getDownloadURL().then((url) => {
            this.showroom.settings.logoUrl = url;
            this.showroomService.updateSettings(this.showroom.settings).subscribe();
          });
        });
        this.uploader.queue = [];
      });
    });
  }

  async openVideoPresentationForm() {
    const modal = await this.modalController.create({
      component: VideoPresentationFormComponent,
      cssClass: 'fit-content-modal',
      componentProps: {
        presentationVideoUrl: this.presentationVideoUrl,
        presentationVideoAutoStart: this.presentationVideoAutoStart
      }
    });
    modal.onDidDismiss().then((response) => {
      if (response.data) {
        Object.assign(this.showroom.settings, response.data);
        this.showroomService.updateSettings(this.showroom.settings).subscribe();
      }
    });
    return await modal.present();
  }

  isValidUrl() {
    let validUrl = false;

    try {
      if (this.presentationVideoUrl.startsWith('http://') || this.presentationVideoUrl.startsWith('https://')) {
        validUrl = true;
      }
    } catch {
      validUrl = false;
    }
    return validUrl;
  }

  updateCurrentCamera(menuItem: MenuLink) {
    menuItem.cameraPosition = {
      x: this.sceneHandler.camera.position.x,
      y: this.sceneHandler.camera.position.y,
      z: this.sceneHandler.camera.position.z
    };
    menuItem.cameraRotation = {
      x: this.sceneHandler.camera.rotation.x,
      y: this.sceneHandler.camera.rotation.y,
      z: this.sceneHandler.camera.rotation.z
    };

    const menus = this.showroom.menu;
    const menuToUpdate = this.showroom.menu.find(menu => menu.id === menuItem.id);
    if (menuToUpdate) {
      menuToUpdate.cameraPosition = menuItem.cameraPosition;
      menuToUpdate.cameraRotation = menuItem.cameraRotation;
    } else {
      menus.push(menuItem);
    }

    this.showroomService.updateMenu(menus).subscribe(
      () => {
        const translate = this.translateService.instant('Target saved for');
        this.toastService.presentToast(ToastType.NORMAL, `${translate} ${menuItem.title}`);
      }, () => this.toastService.presentToast(ToastType.ERROR, 'Error')
    );
  }

  async openMenuItemCreationModal(menuItem?: MenuLink) {
    if (!menuItem) {
      menuItem = {
        id: '',
        title: '',
        cameraPosition: this.sceneHandler.camera.position,
        cameraRotation: this.sceneHandler.camera.rotation,
        level2: false
      };
    }
    const modal = await this.modalController.create({
      component: NewMenuItemFormComponent,
      cssClass: 'fit-content-modal',
      componentProps: {
        menuItem
      }
    });
    modal.onDidDismiss().then((value) => {
      if (value && value.data) {

        const item = value.data;
        if (menuItem && menuItem.id) {
          item.id = menuItem.id;
        } else {
          item.id = uuid4();
        }

        const indexInList = this.itemList.map(i => i.id).indexOf(item.id);

        if (indexInList >= 0) {
          this.itemList[indexInList] = item;
          this.itemList = this.itemList.slice();
        } else {
          this.itemList = this.itemList.concat(item);
        }

        const menuLink: MenuLink = { title: item.title, id: item.id, cameraPosition: item.cameraPosition, cameraRotation: item.cameraRotation, level2: (item.level === '2') };

        this.showroomService.putMenuItem(menuLink).subscribe(
            () => {
              const foundIndex = this.showroom.menu.findIndex( item => item.id === menuLink.id);

              if (foundIndex >= 0) {
                this.showroom.menu.splice(foundIndex, 1, JSON.parse(JSON.stringify(menuLink)));
              } else {
                this.showroom.menu.push(menuLink);
              }
            },
            () => { }
        );
        this.updateExpanded();
      }
    });
    const res = await modal.present();
    this.updateExpanded();
    return res;
  }

  toggleLevel2(link: ExtendedMenuLink) {
    link.expanded = !link.expanded;
    for (let i = (this.itemList.indexOf(link) + 1); i < this.itemList.length && this.itemList[i].level2; i++) {
      this.itemList[i].hidden = !link.expanded;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateExpanded();
  }
}
