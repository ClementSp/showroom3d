import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {ExtendedMenuLink} from '../../../models/Menu.model';
import {userEnv} from '../../../../environments/user.env';
import {EditorService} from '../../../services/editor.service';
import {CurrentNodeHandler} from '../../../DataHandlers/CurrentNode.handler';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemListComponent implements OnInit {

  @Input() itemList: Array<ExtendedMenuLink>;

  @Output() addItem = new EventEmitter<void>();
  @Output() deleteItem = new EventEmitter<string>();
  @Output() updateCamera = new EventEmitter<any>();
  @Output() itemClicked = new EventEmitter<any>();
  @Output() editItem = new EventEmitter<any>();
  @Output() toggleItem = new EventEmitter<ExtendedMenuLink>();
  @Output() updateListEvent = new EventEmitter<any>();

  constructor(private editorService: EditorService,
              public currentNode: CurrentNodeHandler) { }

  ngOnInit() {
      this.currentNodeExistInMenu().subscribe((value) => {
        setInterval(() => {
        }, 500);
      });
  }

  get userEnv() {
    return userEnv;
  }

  isEditorActivated() {
    return this.editorService.isEditorActivated;
  }

  onUpdateCamera($event: Event, item) {
    $event.stopPropagation();

    this.updateCamera.emit(item);
  }

  onItemClicked(item: any) {
    this.itemClicked.emit(item);
  }

  onDeleteItem(id: string, $event: Event) {
    $event.stopPropagation();

    this.deleteItem.emit(id);
  }

  openMenuItemCreationModal(item, $event: Event) {
    $event.stopPropagation();

    this.editItem.emit(item);
  }

  currentNodeExistInMenu() {
    return this.currentNode.source$.pipe(
        map(v => this.itemList.find(item => v && item.id === v.id))
    );
  }

  toggleLevel2(link: ExtendedMenuLink, $event: Event) {
    $event.stopPropagation();

    this.toggleItem.emit(link);
  }

  reorderItems(ev) {
    this.updateExpanded();
    const itemMove = this.itemList.splice(ev.detail.from, 1)[0];

    this.itemList.splice(ev.detail.to, 0, itemMove);
    this.updateListEvent.emit(this.itemList);
    this.updateExpanded();
    ev.detail.complete();
  }

  updateExpanded() {
    let lastLvl1Expanded = false;
    this.itemList.forEach(item => {
      if (item.level2) {
        (lastLvl1Expanded ? item.hidden = false : item.hidden = true);
      } else {
        if (this.itemList.indexOf(item) === this.itemList.length - 1 || !this.itemList[this.itemList.indexOf(item) + 1].level2) {
          item.expanded = true;
        }
        lastLvl1Expanded = item.expanded;
      }
    });
  }

  trackByTitle(index: number, item: ExtendedMenuLink) {
    return item.title;
  }
}
