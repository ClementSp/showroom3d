import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-logo-manager',
  templateUrl: './logo-manager.component.html',
  styleUrls: ['./logo-manager.component.scss'],
})
export class LogoManagerComponent implements OnInit {

  @Input() logoUrl: string;

  constructor() { }

  ngOnInit() {}

}
