import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SafePipe} from '../../../pipes/safe.pipe';
import {ModalController} from '@ionic/angular';
import Utils from '../../../models/Utils';
import {MediasService} from '../../../services/medias.service';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-video-presentation-form',
  templateUrl: './video-presentation-form.component.html',
  styleUrls: ['./video-presentation-form.component.scss'],
})
export class VideoPresentationFormComponent implements OnInit {

  constructor(private modalController: ModalController,
              private formBuilder: FormBuilder,
              private mediasService: MediasService,
              private safePipe: SafePipe) {
    this.uploader = new FileUploader({});
  }

  @ViewChild('iframe') iframe: HTMLElement;

  @Input() presentationVideoUrl = '';
  @Input() presentationVideoAutoStart = false;

  form: FormGroup;

  isUploading = new BehaviorSubject<boolean>(false);
  isLoading = new BehaviorSubject<boolean>(true);
  uploader: FileUploader;
  videos: Array<any> = [];

  ngOnInit() {
    this.form = this.formBuilder.group({
      presentationVideoUrl: new FormControl(this.presentationVideoUrl, [
        Validators.required,
        Utils.urlValidator
      ]),
      presentationVideoAutoStart: new FormControl(this.presentationVideoAutoStart, [
        Validators.required
      ])
    });
  }

  validForm() {
    this.modalController.dismiss(this.form.getRawValue());
  }

  dismiss() {
    this.modalController.dismiss();
  }

  onChangePresentationVideoUrl() {
    const url = this.form.controls.presentationVideoUrl.value;

    this.form.controls.presentationVideoUrl.setValue(Utils.transformToYoutubeEmbedUrl(url));
  }

  upload() {
    const fileItem: FileItem = this.uploader.queue[0];

    this.isUploading.next(true);
    this.mediasService.postMedia('video', fileItem._file, fileItem.file.name).subscribe((media) => {
      this.videos.push(media);
      this.uploader.queue = [];
      this.isUploading.next(false);
      this.form.controls.presentationVideoUrl.setValue(Utils.transformToYoutubeEmbedUrl(media.url));
    });
  }
}
