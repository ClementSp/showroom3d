import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalController} from '@ionic/angular';
import {MenuLink} from '../../../models/showroom.model';
import {CurrentNodeHandler} from '../../../DataHandlers/CurrentNode.handler';

@Component({
  selector: 'app-new-menu-item-form',
  templateUrl: './new-menu-item-form.component.html',
  styleUrls: ['./new-menu-item-form.component.scss'],
})
export class NewMenuItemFormComponent implements OnInit {
  @Input() menuItem: MenuLink;

  form: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private currentNode: CurrentNodeHandler,
              private modalController: ModalController) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      title: new FormControl(this.title, [
        Validators.required,
        Validators.minLength(1)
      ]),
      level: new FormControl(this.level)
    });
  }

  get title() {
    return this.menuItem?.title;
  }

  get level() {
    return this.menuItem ? (this.menuItem.level2 ? '2' : '1') : '1';
  }

  validate() {
    this.modalController.dismiss(this.form.getRawValue());
  }

  dismiss() {
    this.modalController.dismiss();
  }
}
