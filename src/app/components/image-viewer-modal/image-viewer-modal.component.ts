import {Component, Input} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer-modal.component.html',
  styleUrls: ['./image-viewer-modal.component.scss'],
})
export class ImageViewerModalComponent {
  @Input() title: string;
  @Input() src: string;

  constructor(private modalCtrl: ModalController) { }

  public async close() {
    return this.modalCtrl.dismiss(null);
  }
}
