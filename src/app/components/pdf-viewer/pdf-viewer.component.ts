import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ModalController, Platform} from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-pdf-viewer',
    templateUrl: './pdf-viewer.component.html',
    styleUrls: ['./pdf-viewer.component.scss'],
})
export class PdfViewerComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() data: any;

    public pdfSrc;
    public isReady = false;
    public hasFailed = false;

    constructor(private platform: Platform,
                private modalController: ModalController,
                private sanitizer: DomSanitizer) {
    }

    ngAfterViewInit(): void {
    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
        this.pdfSrc = this.data.pdf;
    }

    downloadPdf() {
        window.open(this.pdfSrc, '_blank');
    }

    close() {
        this.modalController.dismiss();
    }

    isMd() {
        return this.platform.is('android');
    }

    public async hasLoaded() {
        this.isReady = true;
    }

    public async onError() {
        this.hasFailed = true;
    }
}
