import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IBaseProduct} from '../../models/CustomProduct';
import {BehaviorSubject, Subscription} from 'rxjs';
import {ProductsService} from '../../services/products.service';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.html',
    styleUrls: ['./product-details.scss'],
})
export class ProductDetails implements OnInit, AfterViewInit, OnDestroy {

    @Input() product;
    @ViewChild('canvas') canvas: ElementRef;

    photo = 'assets/ui/nophoto.jpg';
    getMediaSubscription: Subscription;
    isProductUrl = false;

    constructor(public productsService: ProductsService) { }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        if (!this.product || !(this.product.medias || this.product.imgUrl1)) {
            return;
        }

        if (this.product.medias) {
            this.initProduct();
        } else {
            this.isProductUrl = true;
        }
    }

    initProduct() {
        const firstImage = this.product.medias.find(m => m.type === 'picture');

        if (!firstImage) {
            return;
        }

        this.getMediaSubscription = this.productsService.getMediaUrl(this.product.product_ean, firstImage.id).subscribe(url => {
            this.setPicture(url);
        });
    }

    ngOnDestroy(): void {
        if (this.getMediaSubscription) {
            this.getMediaSubscription.unsubscribe();
        }
    }

    setPicture(url: string) {
        return this.fetchStatus(url)
            .then((status) => {
                if (status === 200) {
                    this.photo = url;
                }
            });
    }

    private fetchStatus(URL: string) {
        return fetch(URL).then((response) => response.status);
    }
}
