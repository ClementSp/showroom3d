import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IonSlides, ModalController, Platform} from '@ionic/angular';
import {slideOpts} from './data/SlideOpts';
import {LogType, ShowroomsService} from '../../services/showrooms.service';
import {Poi} from '../../models/showroom.model';
import {UrlContentComponent} from '../url-content/url-content.component';
import {BehaviorSubject, combineLatest, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {CustomProductPageV2Component} from '../custom-product-page-v2/custom-product-page-v2.component';
import {Template1Component} from '../custom-product-page-v2/templates/template1/template-1.component';
import {Template2Component} from '../custom-product-page-v2/templates/template2/template-2.component';
import {Template3Component} from '../custom-product-page-v2/templates/template3/template-3.component';
import {UserService} from '../../services/user.service';
import {userEnv} from '../../../environments/user.env';
import {AnalyticService} from '../../services/analytic.service';
import {FirstStepComponent} from '../custom-product-page-v2/templates/cci/first-step/first-step.component';
import {FirstPremiumStepComponent} from '../custom-product-page-v2/templates/cci/first-premium-step/first-premium-step.component';
import {TemplateFullPicturesComponent} from "../custom-product-page-v2/templates/templateFullPicture/template-full-pictures.component";

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() poi: Poi;
  @ViewChild('slides') slides: IonSlides;

  items = [];
  isPicture = false;
  carouselOpts = slideOpts;
  currentPageIndex = 0;
  isEnd = false;
  isBegin = true;
  currentViewOnClick: any;
  productsLoadingArray = new Array<BehaviorSubject<boolean>>();
  productsFullyLoaded = new BehaviorSubject<boolean>(false);
  viewsOnClickMap = new Map<string, any>([
    ['productUrl', UrlContentComponent],
    ['product', CustomProductPageV2Component],
    ['CCI', FirstStepComponent],
    ['PremiumCCI', FirstPremiumStepComponent],
    ['template-1', Template1Component],
    ['template-2', Template2Component],
    ['template-3', Template3Component],
    ['template-full-pictures', TemplateFullPicturesComponent]
  ]);

  productsFullyLoadedSubscription: Subscription;

  constructor(private modalController: ModalController,
              private showroomService: ShowroomsService,
              private analyticService: AnalyticService,
              private userService: UserService,
              private platform: Platform) {
    this.modalController.getTop().then((modal) => {
      modal.swipeToClose = this.platform.is('mobile');
    });
  }

  ngOnInit() {
    if (this.poi.type.indexOf('image') === -1) {
      this.items = this.poi.data?.products;
    } else {
      this.isPicture = true;
      this.items = this.poi.data?.images;
    }

    const reducer = (accumulator, currentValue) => accumulator && currentValue;

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.items.length; i++) {
      // display carousel only when first displayed elements are fully loaded
      this.productsLoadingArray.push(new BehaviorSubject(false));
    }

    this.productsFullyLoadedSubscription =
        combineLatest([...this.productsLoadingArray])
        .pipe(map((values: Array<boolean>) => values.reduce(reducer)))
        .subscribe((value: boolean) => {
          this.productsFullyLoaded.next(value);
        });

    if (this.poi.type === 'product' && this.poi.data.template && this.poi.data.template !== '0') {
      if (this.poi.data.template === 4) {
        this.currentViewOnClick = this.viewsOnClickMap.get('CCI');
      } else if (this.poi.data.template === 5) {
        this.currentViewOnClick = this.viewsOnClickMap.get('PremiumCCI');
      } else {
        this.currentViewOnClick = this.viewsOnClickMap.get('template-' + this.poi.data.template);
      }
    } else {
      this.currentViewOnClick = this.viewsOnClickMap.get(this.poi.type);
    }
  }

  ngOnDestroy(): void {
    if (this.productsFullyLoadedSubscription) {
      this.productsFullyLoadedSubscription.unsubscribe();
    }
  }

  close() {
    this.modalController.dismiss();
  }

  ngAfterViewInit(): void {
    this.customModalWrapper();
    this.slides.ionSlideDrag.subscribe(() => {
      this.slides.getSwiper().then((swiper) => {
        this.isEnd = swiper.isEnd;
      });
      this.slides.getActiveIndex().then((newPage) => {
        if (this.isBegin && newPage > 0) {
          this.isBegin = false;
        }
        this.currentPageIndex = newPage;
      });
    });
    this.slides.ionSlideReachStart.subscribe(() => this.isBegin = true);
    this.slides.ionSlideReachEnd.subscribe(() => this.isEnd = true);
  }

  next() {
    this.isBegin = false;
    if (!this.isEnd) {
      this.slides.slideNext().then(() => {
        if (!userEnv.authorization) {
          const productDisplayed = this.items[this.getDisplayedSlidesNumber() + this.currentPageIndex];
          this.analyticService.putLog(LogType.CLUSTER_SLIDE_NEXT, productDisplayed?.ean.toString(), undefined, this.poi.id);
        }
      });
    }
  }

  prev() {
    this.isEnd = false;
    if (!this.isBegin) {
      this.slides.slidePrev().then(() => {
        if (!userEnv.authorization) {
          let productDisplayed;
          this.slides.getActiveIndex().then((activeIndex) => {
            productDisplayed = this.items[activeIndex];
            this.analyticService.putLog(LogType.CLUSTER_SLIDE_PREV, productDisplayed?.ean.toString(), undefined, this.poi.id);
          });
        }
      });
    }
  }

  isMobile() {
    return this.platform.is('mobile');
  }

  private getDisplayedSlidesNumber() {
    let slidesPerView;
    const screenWidth = window.innerWidth;
    const breakpoints = this.slides.options.breakpoints;
    const keys: Array<number> = Object.keys(breakpoints).map((key) => parseInt(key, 10));
    let closest = keys.findIndex((key) => key > screenWidth);

    if (closest > 0) {
      closest--;
    }
    // @ts-ignore
    slidesPerView = Object.values(breakpoints).map(item => item)[closest].slidesPerView;
    return (this.items.length < slidesPerView) ? this.items.length : slidesPerView;
  }

  productIsLoaded(index: number, $event: boolean, error?: any) {
    if (error) {
      console.error('Error loading product.');
    }
    if (index < this.productsLoadingArray.length && this.productsLoadingArray[index]) {
      this.productsLoadingArray[index].next($event);
    }
  }

  private customModalWrapper() {
    const children = (this as any).modal.children;

    for (let i = 0; i < children.length; i++) {
      const element = (children.item(i) as HTMLElement);

      if (element.classList.contains('modal-wrapper')) {
        element.classList.add('showroom-modal');
      }
    }
  }
}
