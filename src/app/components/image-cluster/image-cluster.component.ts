import {Component, Input, OnInit} from '@angular/core';
import {ProductMedia} from '../../models/CustomProduct';
import {ModalController} from '@ionic/angular';
import {MediasService} from '../../services/medias.service';
import {ImageViewerModalComponent} from '../image-viewer-modal/image-viewer-modal.component';

@Component({
  selector: 'app-image-cluster',
  templateUrl: './image-cluster.component.html',
  styleUrls: ['./image-cluster.component.scss'],
})
export class ImageClusterComponent implements OnInit {
  @Input() image: ProductMedia;

  public url = '';
  public isReady = false;

  constructor(private mediaService: MediasService,
              private modalController: ModalController) { }

  ngOnInit() {
    this.mediaService.getMediasById(this.image.id).subscribe((media) => {
        this.url = media.url;
        this.isReady = true;
      });
  }

  public async openImage() {
    const modal = await this.modalController.create({
      component: ImageViewerModalComponent,
      cssClass: 'edit-marker-component backdrop-05',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        src: this.url,
        title: 'Preview'
      }
    });

    return await modal.present();
  }
}
