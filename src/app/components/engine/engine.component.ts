import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {EngineService} from '../../services/engine.service';
import {BehaviorSubject, Subject} from 'rxjs';
import {Poi, Showroom} from '../../models/showroom.model';
import * as THREE from 'three';
import EngineEvent from '../../models/EngineEvent';
import {EditorService} from '../../services/editor.service';
import {EditorMode} from '../../models/EditorMode';
import MovementMarker from '../../models/MovementMarker';
import {CurrentNodeHandler} from '../../DataHandlers/CurrentNode.handler';
import {userEnv} from '../../../environments/user.env';
import ActionMarker from '../../models/ActionMarker';
import PortalMarker from '../../models/PortalMarker';
import {AudioHandler} from '../../DataHandlers/audio.handler';
import {SceneHandler} from '../../DataHandlers/scene.handler';
import {Vector3} from 'three';

@Component({
    selector: 'app-engine',
    templateUrl: './engine.component.html',
    styleUrls: ['./engine.component.scss'],
})
export class EngineComponent implements OnInit {

    @Input() showroom: Showroom;
    @Output() isLoading = new EventEmitter<boolean>(false);
    @Output() isObjLoading = new EventEmitter<boolean>(false);
    @Output() shellIsValid = new BehaviorSubject<boolean>(false);

    lastPointerDown: number;
    moveToShowroomEvent$ = new Subject<EngineEvent>();
    toggleMute$ = new Subject<EngineEvent>();
    toggleGyroscope$ = new Subject<EngineEvent>();
    zoom$ = new Subject<EngineEvent>();
    updateAttributesOfFixture$ = new Subject<EngineEvent>();
    lastId = null;
    clickPosition;
    selectChild = 0;
    positionX;
    positionY;
    audioHandler = globalThis.injector.get(AudioHandler);

    public constructor(private currentNode: CurrentNodeHandler,
                       public engineService: EngineService,
                       public editorService: EditorService,
                       public sceneHandler: SceneHandler) {}

    ngOnInit(): void {
        this.initializeViewer();
        this.renderOnRequestAnimationFrame();
    }

    private initializeViewer() {
        const entranceFromUrl = this.getEntranceFromUrl();
        const viewerContainer = document.querySelector('#viewer');
        this.engineService.init(viewerContainer as HTMLElement, entranceFromUrl);
    }

    private getEntranceFromUrl() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        urlParams.forEach((value, key) => {});

        return urlParams.get('entranceSweep');
    }

    renderOnRequestAnimationFrame(): void {
        const updateOnEachScreenFrame = () => {
            requestAnimationFrame(updateOnEachScreenFrame);
        };
        requestAnimationFrame(updateOnEachScreenFrame);
    }

    @HostListener('window.resize', ['$event'])
    onResize(event: any) {
        const windowWidth = event.target.innerWidth;
        const windowHeight = event.target.innerHeight;

        //this.engineService.resizeWindow(windowWidth, windowHeight);
    }

    @HostListener('mousedown', ['$event'])
    async onClickDownEvent(event: any) {
        this.clickPosition = {
            x: event.x,
            y: event.y
        };
    }

    @HostListener('touchend', ['$event'])
    @HostListener('click', ['$event'])
    async onClickEvent(event: any) {
        event.preventDefault();

        this.engineService.annotation = undefined;
        if (!this.isClickHeld(event)) {
            let intersects: Array<THREE.Intersection>;
            const clientX = (window.TouchEvent && event instanceof TouchEvent) ? event.changedTouches[0].clientX : event.clientX;
            const clientY = (window.TouchEvent && event instanceof TouchEvent) ? event.changedTouches[0].clientY : event.clientY;
            const pageX = (window.TouchEvent && event instanceof TouchEvent) ? event.changedTouches[0].pageX : event.pageX;
            const pageY = (window.TouchEvent && event instanceof TouchEvent) ? event.changedTouches[0].pageY : event.pageY;
            const x = ( clientX / window.innerWidth ) * 2 - 1;
            const y = - ( clientY / window.innerHeight ) * 2 + 1;

            this.engineService.raycastFromCamera(x, y);
            intersects = this.engineService.getIntersectObjects( true);

            if (userEnv.loginType === 'platform' && this.editorService.editorView.value) {
                if (userEnv.isPersonalShopper()) {
                    if (this.intersectsContainObjectType(intersects, MovementMarker)) {
                        await this.engineService.triggerObjectOnClick(intersects, true);
                    }
                } else  {
                    if (this.editorService.isEditing() || this.intersectsContainAnObject(intersects)) {
                        console.log('isIntersectsContainAnObject');
                        this.handleClickByEditor(intersects);
                    } else {
                        await this.engineService.triggerObjectOnClick(intersects, false);
                    }
                }
            } else if (this.clickPosition && (this.clickPosition.x === pageX) && (this.clickPosition.y === pageY)) {
                if (this.editorService.mode === EditorMode.PLACING_PRODUCT ||
                    this.editorService.mode === EditorMode.PLACING_SMART_OBJECT ||
                    this.editorService.mode === EditorMode.PLACING_FIXTURE ||
                    this.editorService.mode === EditorMode.ADD_SPOT_LIGHT ||
                    this.editorService.mode === EditorMode.ADD_POINT_LIGHT){
                    this.handleClickByEditor(intersects);
                    this.editorService.mode = EditorMode.NONE;
                } else{
                    if ( this.showroom.settings.editable && userEnv.jwt){
                        this.handleClickByEditor(intersects);
                    }else {
                        await this.engineService.triggerObjectOnClick(intersects, false);
                    }
                }
            } else {
                await this.engineService.triggerObjectOnClick(intersects, true);
            }
            this.clickPosition = undefined;
        }
    }

    @HostListener('mousewheel', ['$event'])
    async onMouseWheel(event: any) {
        this.sceneHandler.zoom(event.wheelDelta);
    }

    @HostListener('mousemove', ['$event'])
    async onMouseMove(event: any) {

        let intersects: Array<THREE.Intersection>;
        event.preventDefault();

        const x = (event.clientX / window.innerWidth) * 2 - 1;
        const y = -(event.clientY / window.innerHeight) * 2 + 1;

        this.engineService.raycastFromCamera(x, y);
        switch (this.editorService.mode) {
             case  EditorMode.PLACING_FIXTURE:
                 let positionFixture;
                 intersects = this.engineService.getIntersectObjects( true);
                 const intersectShellComponentsFixture = intersects.filter(o => o.object.name === 'shellComponent');
                 if (intersectShellComponentsFixture && intersectShellComponentsFixture.length > 0) {
                     const normal = intersectShellComponentsFixture[0].face.normal;
                     const origin = this.sceneHandler.raycaster.ray.origin;
                     positionFixture = intersectShellComponentsFixture[0].point;

                     if (normal.x * (origin.x - positionFixture.x) < 0) {
                         normal.x = -normal.x;
                     }
                     if (normal.y * (origin.y - positionFixture.y) < 0) {
                         normal.y = -normal.y;
                     }
                     if (normal.z * (origin.z - positionFixture.z) < 0) {
                         normal.z = -normal.z;
                     }

                     this.editorService.currentlyPlacingFixture.position.set(
                         positionFixture.x + normal.x / 50,
                         positionFixture.y + normal.y / 50,
                         positionFixture.z + normal.z / 50
                     );

                     const rotation = new Vector3(positionFixture.x + normal.x, positionFixture.y + 2 * normal.y, positionFixture.z + normal.z);
                     this.editorService.currentlyPlacingFixture.lookAt(rotation);
                 }
                 break;
            case EditorMode.PLACING_PRODUCT:
                let positionProduct;
                intersects = this.engineService.getIntersectObjects( true);
                const intersectShellComponentsProduct = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
                if (intersectShellComponentsProduct && intersectShellComponentsProduct.length > 0) {
                    positionProduct = intersectShellComponentsProduct[0];
                    this.editorService.currentlyPlacingProduct.position.set(
                        positionProduct.x,
                        positionProduct.y,
                        positionProduct.z
                    );
                }
                break;
            case  EditorMode.PLACING_SMART_OBJECT:
                let positionSmartObject;
                intersects = this.engineService.getIntersectObjects( true);
                const intersectShellComponentsSmartObject = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
                if (intersectShellComponentsSmartObject && intersectShellComponentsSmartObject.length > 0) {
                    positionSmartObject = intersectShellComponentsSmartObject[0];
                    this.editorService.currentlyPlacingSmartObject.position.set(
                        positionSmartObject.x,
                        positionSmartObject.y,
                        positionSmartObject.z
                    );
                }
                break;
            case  EditorMode.ADD_SHAPE_POI_MARKERS:
                let positionShapePoi;
                intersects = this.engineService.getIntersectObjects( true);
                const intersectShellComponentsShapePio = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
                if (intersectShellComponentsShapePio && intersectShellComponentsShapePio.length > 0) {
                    positionShapePoi = intersectShellComponentsShapePio[0];
                    this.editorService.currentlyPlacingShapePoi.position.set(
                        positionShapePoi.x,
                        positionShapePoi.y,
                        positionShapePoi.z
                    );
                }
                break;
            case  EditorMode.ADD_POI_MARKERS:
                let positionPoi;
                intersects = this.engineService.getIntersectObjects( true);
                const intersectShellComponentsPoi = intersects.filter(o => o.object.name === 'shellComponent');
                if (intersectShellComponentsPoi && intersectShellComponentsPoi.length > 0) {
                    const normal = intersectShellComponentsPoi[0].face.normal;
                    const origin = this.sceneHandler.raycaster.ray.origin;
                    positionPoi = intersectShellComponentsPoi[0].point;

                    if (normal.x * (origin.x - positionPoi.x) < 0) {
                        normal.x = -normal.x;
                    }
                    if (normal.y * (origin.y - positionPoi.y) < 0) {
                        normal.y = -normal.y;
                    }
                    if (normal.z * (origin.z - positionPoi.z) < 0) {
                        normal.z = -normal.z;
                    }

                    this.editorService.currentlyPlacingPoi.position.set(
                        positionPoi.x + normal.x / 50,
                        positionPoi.y + normal.y / 50,
                        positionPoi.z + normal.z / 50
                    );

                    const rotation = new Vector3(positionPoi.x + normal.x, positionPoi.y + 2 * normal.y, positionPoi.z + normal.z);
                    this.editorService.currentlyPlacingPoi.lookAt(rotation);
                }
                break;
            case  EditorMode.ADD_PORTAL_MARKERS:
                let positionPortal;
                intersects = this.engineService.getIntersectObjects( true);
                const intersectShellComponentsPortal = intersects.filter(o => o.object.name === 'shellComponent');
                if (intersectShellComponentsPortal && intersectShellComponentsPortal.length > 0) {
                    const normal = intersectShellComponentsPortal[0].face.normal;
                    const origin = this.sceneHandler.raycaster.ray.origin;
                    positionPortal = intersectShellComponentsPortal[0].point;

                    if (normal.x * (origin.x - positionPortal.x) < 0) {
                        normal.x = -normal.x;
                    }
                    if (normal.y * (origin.y - positionPortal.y) < 0) {
                        normal.y = -normal.y;
                    }
                    if (normal.z * (origin.z - positionPortal.z) < 0) {
                        normal.z = -normal.z;
                    }

                    this.editorService.currentlyPlacingPortal.position.set(
                        positionPortal.x + normal.x / 50,
                        positionPortal.y + normal.y / 50,
                        positionPortal.z + normal.z / 50
                    );

                    const rotation = new Vector3(positionPortal.x + normal.x, positionPortal.y + 2 * normal.y, positionPortal.z + normal.z);
                    this.editorService.currentlyPlacingPortal.lookAt(rotation);
                }
                break;
            case EditorMode.ADD_POINT_LIGHT:
                let positionPointLight;
                intersects = this.engineService.getIntersectObjects( true);
                const intersectShellComponentsPointLight = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
                if (intersectShellComponentsPointLight && intersectShellComponentsPointLight.length > 0) {
                    positionPointLight = intersectShellComponentsPointLight[0];
                    this.editorService.currentlyPlacingPointLight.position.set(
                        positionPointLight.x,
                        positionPointLight.y,
                        positionPointLight.z
                    );
                }
                break;
            case EditorMode.ADD_SPOT_LIGHT:
                let positionSpotLight;
                intersects = this.engineService.getIntersectObjects( true);
                const intersectShellComponentsSpotLight = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
                if (intersectShellComponentsSpotLight && intersectShellComponentsSpotLight.length > 0) {
                    positionSpotLight = intersectShellComponentsSpotLight[0];
                    this.editorService.currentlyPlacingSpotLight.position.set(
                        positionSpotLight.x,
                        positionSpotLight.y,
                        positionSpotLight.z
                    );
                }
                break;
            default:
                this.positionX = (event.clientX - 30) + 'px';
                this.positionY = (event.clientY - 30) + 'px';
                intersects = this.engineService.getIntersectObjects();
                await this.engineService.triggerObjectHover(intersects);
                break;
         }
    }

    delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    intersectsContainObjectType(intersects: Array<THREE.Intersection>, type: any) {
        let containMovementMarker = false;

        intersects.forEach((intersect) => {
            if (intersect.object instanceof type) {
                containMovementMarker = true;
            }
        });
        return containMovementMarker;
    }

    intersectsContainAnObject(intersects: Array<any>) {
        let contain = false;
        const names = ['product', 'smartObject', 'fixture', 'poi', 'shapePoi', 'pointLight', 'spotLight'];

        intersects.forEach((intersect) => {
            names.forEach(name => {
                if (intersect.object.name.indexOf(name) !== -1 ||
                    this.parentObjectMatchName(intersect.object.parent, name)) {
                    contain = true;
                }
            });
        });
        return contain;
    }

    parentObjectMatchName(parent, name) {
        if (parent) {
            if (parent.name.indexOf(name) !== -1) {
                return true;
            } else {
                return this.parentObjectMatchName(parent.parent, name);
            }
        }
    }

    handleClickByEditor(intersects: Array<THREE.Intersection>) {
        this.editorService.handleClick(intersects, this.currentNode.value);
    }

    @HostListener('touchstart', ['$event'])
    @HostListener('pointerdown')
    onPointerDown(event: any) {
        this.lastPointerDown = this.getCurrentTimestamp();

        if (window.TouchEvent && event instanceof TouchEvent) {
            this.clickPosition = {
                x: event.changedTouches[0].pageX,
                y: event.changedTouches[0].pageY
            };
        }
    }

    isEditorView() {
        return this.editorService.editorView.value;
    }

    private isClickHeld(event: any) {
        if (event.target.tagName !== 'CANVAS') {
            return false;
        }
        return this.getCurrentTimestamp() - this.lastPointerDown > 250;
    }

    private getCurrentTimestamp() {
        const date = new Date();

        return date.getTime();
    }

    public getSpritePath(poi: Poi) {
        return this.engineService.getSpritePath(this.showroom, poi);
    }
}
