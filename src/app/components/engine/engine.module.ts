import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CarouselComponent} from '../carousel/carousel.component';
import {IonicModule} from '@ionic/angular';
import {UrlContentComponent} from '../url-content/url-content.component';
import {ProductViewerComponent} from '../product-viewer/product-viewer.component';
import {ComponentHolderDirective} from '../../directives/component-holder.directive';
import {PdfViewerComponent} from '../pdf-viewer/pdf-viewer.component';
import {IntroductionPopUpComponent} from '../introduction-pop-up/introduction-pop-up.component';
import {TranslateModule} from '@ngx-translate/core';
import {ImagePrevisualizationComponent} from '../edit-marker/custom-product-form/image-previsualization/image-previsualization.component';
import {VideoPrevisualizationComponent} from '../edit-marker/custom-product-form/video-previsualization/video-previsualization.component';
import {PdfPrevisualizationComponent} from '../edit-marker/custom-product-form/pdf-previsualization/pdf-previsualization.component';
import {SharedModule} from '../../shared.module';
import {ImageClusterComponent} from '../image-cluster/image-cluster.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {ChatViewerComponent} from '../chat-viewer/chat-viewer.component';
import {ObjectPrecisionComponent} from '../object-precision/object-precision.component';
import {FormsModule} from '@angular/forms';
import {NgxMatColorPickerModule} from '@angular-material-components/color-picker';
import {MAT_COLOR_FORMATS, NGX_MAT_COLOR_FORMATS} from '@angular-material-components/color-picker';

@NgModule({
    declarations: [CarouselComponent,
        UrlContentComponent,
        ComponentHolderDirective,
        ProductViewerComponent,
        PdfViewerComponent,
        ObjectPrecisionComponent,
        ChatViewerComponent,
        IntroductionPopUpComponent,
        ImagePrevisualizationComponent,
        VideoPrevisualizationComponent,
        PdfPrevisualizationComponent,
        ImageClusterComponent],
    providers: [
        { provide: MAT_COLOR_FORMATS, useValue: NGX_MAT_COLOR_FORMATS }],
    exports: [ComponentHolderDirective, ProductViewerComponent, ImagePrevisualizationComponent, VideoPrevisualizationComponent, PdfPrevisualizationComponent, ObjectPrecisionComponent, IntroductionPopUpComponent, ObjectPrecisionComponent],
    imports: [
        CommonModule,
        IonicModule,
        TranslateModule,
        SharedModule,
        PdfViewerModule,
        FormsModule,
        NgxMatColorPickerModule,
    ]
})
export class EngineModule { }
