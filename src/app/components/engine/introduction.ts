const template = (
    actionMarkerSpriteUrl: string,
    actionText: string,
    movementMarkerSpriteUrl: string,
    movementText: string
) => `
<ion-grid>
     <ion-row>
        <ion-col class="logo">
            <img src="${actionMarkerSpriteUrl}" alt="cluster-logo"/>
        </ion-col>
        <ion-col class="text-box">
            <ion-text>${actionText}</ion-text>
        </ion-col>
    </ion-row>
    <ion-row>
        <ion-col class="logo">
            <img src="${movementMarkerSpriteUrl}" alt="cluster-logo"/>
        </ion-col>
        <ion-col class="text-box">
            <ion-text>${movementText}</ion-text>
        </ion-col>
    </ion-row>
</ion-grid>
`;

export default template;
