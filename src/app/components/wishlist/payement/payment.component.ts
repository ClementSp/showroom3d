import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ModalController, Platform} from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() data: any;

    public paymentSrc;
    public isReady = false;
    public hasFailed = false;

    constructor(private platform: Platform,
                private modalController: ModalController,
                private sanitizer: DomSanitizer) {}

    ngAfterViewInit(): void {
    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
        // this.paymentSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.data.paymentUrl);
        this.paymentSrc = this.data.paymentUrl;
    }

    close() {
        this.modalController.dismiss();
    }

    isMd() {
        return this.platform.is('android');
    }

    public async hasLoaded() {
        this.isReady = true;
    }

    public async onError() {
        this.hasFailed = true;
    }
}
