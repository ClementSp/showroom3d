import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {IWishListProduct, WishlistHandler} from '../../../DataHandlers/Wishlist.handler';
import {map, tap} from 'rxjs/operators';
import {ShowroomsService} from '../../../services/showrooms.service';
import {BehaviorSubject, Observable} from 'rxjs';
import CustomProduct from '../../../models/CustomProduct';
import {CustomerService} from '../../../services/customer.service';
import {ProductsService} from '../../../services/products.service';
import {UserService} from '../../../services/user.service';
import {userEnv} from '../../../../environments/user.env';
import {TranslateService} from '@ngx-translate/core';
import {customerEnv} from '../../../../environments/customer.env';

@Component({
  selector: 'app-product-line',
  templateUrl: './product-line.component.html',
  styleUrls: ['./product-line.component.scss'],
})
export class ProductLineComponent implements OnInit, AfterViewInit {

  @Input() product: IWishListProduct;
  @ViewChild('canvas') canvas: ElementRef;

  product$: Observable<CustomProduct>;
  photoUrl = new BehaviorSubject<string>('');
  photoIsVideoThumbnail = new BehaviorSubject<boolean>(false);
  noMedia = new BehaviorSubject<boolean>(false);
  currency = customerEnv.currency;

  constructor(public showroomService: ShowroomsService,
              public userService: UserService,
              private translateService: TranslateService,
              public productsService: ProductsService,
              public wishListHandler: WishlistHandler) {
  }

  ngOnInit() {
  }

  get customerEnv() {
    return customerEnv;
  }

  fetchStatus(URL: string) {
    return fetch(URL).then((response) => response.status);
  }

  ngAfterViewInit(): void {
    this.product$ = this.productsService.getProductByEan(this.product.ean, userEnv.customer, this.translateService.currentLang)
        .pipe(
            map((product: CustomProduct) => {
              if (!product || !product.medias) {
                this.noMedia.next(true);
              } else {
                const firstImageMedia = product.medias.find(m => m.type === 'picture');

                if (firstImageMedia) {
                  this.productsService.getMediaUrl(product.product_ean, firstImageMedia.id).subscribe((url) => {
                    this.photoUrl.next(url);
                  });
                } else {
                  this.noMedia.next(true);
                }
              }
              return product;
            })
        );
  }

  getPicture(url: string): Promise<void> {
    return this.fetchStatus(url).then((status) => {
      if (status !== 200) {
        throw new Error();
      } else {
        this.photoUrl.next(url);
      }
    });
  }

  removeProduct() {
    this.wishListHandler.removeProduct(this.product.ean);
  }

  incrementProductQuantity() {
    this.wishListHandler.incrementProductQuantity(this.product.ean);
  }

  decrementProductQuantity() {
    this.wishListHandler.decrementProductQuantity(this.product.ean);
  }
}
