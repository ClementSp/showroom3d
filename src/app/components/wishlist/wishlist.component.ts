import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {IWishListProduct, WishlistHandler} from '../../DataHandlers/Wishlist.handler';
import {AlertController, IonMenu, ModalController, Platform} from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';
import {CustomerService} from '../../services/customer.service';
import {TranslateService} from '@ngx-translate/core';
import * as FAIL from './fail-template';
import {customerEnv} from '../../../environments/customer.env';
import {showroomEnv} from '../../../environments/showroom.env';
import {CustomProductPageV2Component} from "../custom-product-page-v2/custom-product-page-v2.component";
import {userEnv} from "../../../environments/user.env";
import {PaymentComponent} from "./payement/payment.component";

export enum WishlistResponseTypes {
  EXTERNAL_CHECKOUT_URL = 'externalCheckoutUrl'
}

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss'],
})
export class WishlistComponent implements OnInit {

  @Input() openEvent: EventEmitter<any>;
  @Input() closeEvent: EventEmitter<any>;
  @Output() menuToggleEvent = new EventEmitter<any>();
  @ViewChild('menu') menu: IonMenu;

  products = new BehaviorSubject<Array<IWishListProduct>>([]);
  success = false;
  swipeGesture = false;
  loading = false;

  constructor(public wishListHandler: WishlistHandler,
              private translateService: TranslateService,
              private alertController: AlertController,
              private platform: Platform,
              public customerService: CustomerService,
              public modalController: ModalController) {
  }

  ngOnInit() {
    this.openEvent.subscribe(() => {
      this.menu.open(true);
      this.success = false;
    });
    this.closeEvent.subscribe(() => {
      this.menu.close(true);
      this.success = false;
    });
    this.wishListHandler.products$.subscribe((products: Array<IWishListProduct>) => {
      this.products.next(products);
    });
  }

  get customerEnv() {
    return customerEnv;
  }

  get showroomEnv() {
    return showroomEnv;
  }

  sendWishList() {
    let message;
    this.loading = true;
    this.wishListHandler.sendWishList().then(response => {
      if (response && response.type === WishlistResponseTypes.EXTERNAL_CHECKOUT_URL) {
        // this.openPaymentModal(response.url);
        window.open(response.url, '_blank');
      } else {
        message = this.translateService.instant('Successfully sent');
        this.wishListHandler.resetWishList();
      }
      this.success = true;
      this.loading = false;
    }).catch(() => {
      message = this.translateService.instant('Failed to send wishlist');
      return this.presentAlertConfirm(FAIL.default(message));
    });
  }

  async openPaymentModal(url) {
    const modal = await this.modalController.create({
      component: PaymentComponent,
      cssClass: 'fullscreen-modal',
      showBackdrop: false,
      backdropDismiss: true,
      componentProps: {
        data : {
          paymentUrl: url
        }
      }
    });
    return await modal.present().then(() => { });
  }

  async presentAlertConfirm(message: string) {
    const alert = await this.alertController.create({
      cssClass: 'alert',
      message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }


  closeMenu() {
    this.menu.close(true);
  }

  onMenuDidOpen() {
    this.menuToggleEvent.emit();
    this.swipeGesture = this.isMobile();
  }

  onMenuDidClose() {
    this.menuToggleEvent.emit();
    this.swipeGesture = false;
  }

  isMobile() {
    return this.platform.is('mobile');
  }
}
