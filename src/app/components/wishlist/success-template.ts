const template = (message: string) => `
<ion-grid>
    <ion-row>
        <ion-col>
            <ion-icon name="checkmark-circle-outline" class="success-icon"></ion-icon>
        </ion-col>
    </ion-row>
    <ion-row>
        <ion-col>
            <strong>${message}</strong>
        </ion-col>
    </ion-row>
</ion-grid>
`;

export default template;
