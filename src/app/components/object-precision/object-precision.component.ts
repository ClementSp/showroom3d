import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {EditorService} from '../../services/editor.service';
import {EditorMode} from '../../models/EditorMode';
import {NgxMatColorPickerComponent} from '@angular-material-components/color-picker';
import {Color} from 'three';

@Component({
  selector: 'app-object-precision',
  templateUrl: './object-precision.component.html',
  styleUrls: ['./object-precision.component.scss'],
})
export class ObjectPrecisionComponent implements OnInit {

  @ViewChild('picker') picker: NgxMatColorPickerComponent;

  mode;
  space;
  values;
  color = 0xffffff;

  private timeout = null;
  private debounceTime = 300;

  constructor(private editorService: EditorService) {
    this.space = 'world';
    this.mode = EditorMode[this.editorService.mode].toLowerCase();
    switch (this.mode) {
      case 'translate':
        this.values = this.editorService.target.position;
        break;
      case 'rotate':
        this.values = this.editorService.target.rotation;
        break;
      case 'scale':
        this.values = this.editorService.target.scale;
        break;
      case 'edit':
        this.values = this.editorService.target.children[0];
        break;
    }
  }

  ngOnInit() {
    console.log(this.values.color);
    if (this.values.color) {
      this.color = new Color(this.values.color).getHex();
    }
  }

  public getTitle() {
    switch (this.mode) {
      case 'translate': return 'position';
      case 'rotate': return 'rotation';
      case 'scale': return 'scale';
      case 'edit': return 'edit';
    }
  }

  public resetToDefault() {
    switch (this.mode) {
      case 'translate': return;
      case 'rotate': {
        this.values.x = 0;
        this.values.y = 0;
        this.values.z = 0;
        break;
      }
      case 'scale': {
        this.values.x = 1;
        this.values.y = 1;
        this.values.z = 1;
        break;
      }
    }
  }

  public async openPicker(event: Event) {
    if (this.picker && !this.picker.disabled) {
      this.picker.open();
      event.stopPropagation();
    }
  }

  public valueChanged() {
    if (this.values.intensity < 0) {
      this.values.intensity = 0;
    }

    if (this.color) {
      this.values.color = this.color;
    }
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }

    this.timeout = setTimeout(() => {
      this.editorService.updateValueFromPrecision();
    }, this.debounceTime);
  }

  public spaceChange(space) {
    // this.spaceSwitch.emit(space);
  }

  public getValue(value: number) {
    if (this.mode === 'rotate') {
      // radians to deg
      return value * (180 / Math.PI);
    }
    return value;
  }
}
