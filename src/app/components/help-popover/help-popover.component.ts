import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-help-popover',
  templateUrl: './help-popover.component.html',
  styleUrls: ['./help-popover.component.scss'],
})
export class HelpPopoverComponent implements OnInit {

  @Input() helpMessage: string;

  constructor(public translateService: TranslateService) { }

  ngOnInit() {
  }
}
