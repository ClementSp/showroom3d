import {
  Component,
  ElementRef,
  EventEmitter, HostListener,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import ClusterItem from '../../models/ClusterItem';
import {BehaviorSubject, of} from 'rxjs';
import {ModalController, Platform} from '@ionic/angular';
import {combineLatest} from 'rxjs';
import {ShowroomsService} from '../../services/showrooms.service';
import {map} from 'rxjs/operators';
import CustomProduct from '../../models/CustomProduct';
import {FormBuilder} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {CustomerService} from '../../services/customer.service';
import {ProductsService} from '../../services/products.service';
import {Template1Component} from '../custom-product-page-v2/templates/template1/template-1.component';
import {Template2Component} from '../custom-product-page-v2/templates/template2/template-2.component';
import {Template3Component} from '../custom-product-page-v2/templates/template3/template-3.component';
import {UserService} from '../../services/user.service';
import {userEnv} from '../../../environments/user.env';
import {FirstStepComponent} from '../custom-product-page-v2/templates/cci/first-step/first-step.component';
import {FirstPremiumStepComponent} from '../custom-product-page-v2/templates/cci/first-premium-step/first-premium-step.component';
import {TemplateFullPicturesComponent} from "../custom-product-page-v2/templates/templateFullPicture/template-full-pictures.component";

export interface ProductPage {
  data: any;
}

@Component({
  selector: 'app-product-viewer',
  templateUrl: './product-viewer.component.html',
  styleUrls: ['./product-viewer.component.scss'],
})
export class ProductViewerComponent implements OnInit {

  @ViewChild('noImgCanvas') noImgCanvas: ElementRef;
  @ViewChild('img2') img2: ElementRef;
  @ViewChild('img1') img1: ElementRef;

  @Input() indexInCluster: number;
  @Input() data: any;
  @Input() viewOnClick: any;
  @Input() clusterType: string;

  @Output() productFail = new EventEmitter();

  dataGettersMap = new Map([
    ['productUrl', this.getClusterItemData.bind(this)],
    ['product', this.getCustomClusterItemData.bind(this)]
  ]);

  randomVersion: string;

  productData = new BehaviorSubject<ClusterItem>(undefined);
  picture2Valid = new BehaviorSubject<boolean>(false);
  picture1Valid = new BehaviorSubject<boolean>(false);
  videoValid = new BehaviorSubject<boolean>(false);
  img1Loaded = new BehaviorSubject<boolean>(false);
  img2Loaded = new BehaviorSubject<boolean>(false);
  isProductAsUrls = new BehaviorSubject<boolean>(false);
  @Output() fullyLoaded$ = combineLatest([this.img1Loaded, this.img2Loaded, this.productData], (a, b, c) => a && b && c !== undefined);
  dismissProductModal = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder,
              public customerService: CustomerService,
              private platform: Platform,
              private translateService: TranslateService,
              private productsService: ProductsService,
              private userService: UserService,
              private showroomService: ShowroomsService,
              private modalController: ModalController) {
  }

  getCssForClass(data: any) {
    switch (this.viewOnClick) {
      case Template1Component:
        return 'backdrop-05 template1';
      case Template2Component:
        return 'backdrop-05 template2';
      case Template3Component:
        return 'backdrop-05 template3';
      case TemplateFullPicturesComponent:
        return 'fullPictures backdrop-05';
      case FirstStepComponent:
        return 'two-steps-template backdrop-05';
      case FirstPremiumStepComponent:
        return 'two-steps-premium-template backdrop-05';
      default:
        return 'backdrop-05 template1';
    }
  }

  ngOnInit() {
    this.randomVersion = Math.random().toString();

    if (this.clusterType === 'cluster') {
      this.isProductAsUrls.next(true);
    }

    const productData = new ClusterItem();
    this.dataGettersMap.get(this.clusterType)().subscribe((promise) => {
      promise.then((data) => {
        const picture1 = data.product.picture1 || data.product.imgUrl1;
        const picture2 = data.product.picture2 || data.product.imgUrl2;

        productData.cssClass = data.cssClass;
        productData.viewData = data.viewData;
        productData.name = data.product.name || data.product.productName;
        productData.ean = data.product.product_ean;
        if (picture1) {
          productData.imagesUrls.push(picture1);
        }
        if (picture2) {
          productData.imagesUrls.push(picture2);
        }
        this.productData.next(productData);
      });
    }, () => this.productFail.emit('Fail to get product'));

    this.dismissProductModal.subscribe(() => {
      this.modalController.getTop().then((top: HTMLIonModalElement) => {
        if (top) {
          this.modalController.dismiss();
        }
      });
    });
  }

  @HostListener('mouseover', ['$event'])
  displayPhoto2($event) {
    if (this.picture2Valid.value) {
      if (!this.picture1Valid.value) {
        return;
      } else {
        this.img1.nativeElement.classList.add('hidden');
      }
    } else if (this.picture1Valid.value) {
      return;
    }
  }

  @HostListener('mouseleave', ['$event'])
  hidePhoto2($event) {
    if (this.picture2Valid.value) {
      if (!this.picture1Valid.value) {
        return;
      } else {
        this.img1.nativeElement.classList.remove('hidden');
      }
    } else if (this.picture1Valid.value) {
      return;
    }
  }

  async openProductUrl() {
    const modal = await this.modalController.create({
      component: this.viewOnClick,
      cssClass: this.getCssForClass(this.productData),
      componentProps: {
        data: this.productData.value.viewData
      }
    });
    return modal.present();
  }

  private getClusterItemData() {
    let cssClass = 'product-modal backdrop-05 ';

    if (!this.platform.is('mobile')) {
      cssClass += ' lg-product-page';
    }
    const data: any = {
      cssClass,
      product: this.data,
      viewData: {
        url: this.data.productUrl,
        dismissEvent: this.dismissProductModal,
        scroll: false
      }
    };
    if (!this.data.imgUrl1 || this.data.imgUrl1.length === 0) {
      this.picture1Valid.next(false);
      this.img1Loaded.next(true);
    } else {
      this.picture1Valid.next(true);
    }
    if (!this.data.imgUrl2 || this.data.imgUrl2.length === 0) {
      this.picture2Valid.next(false);
      this.img2Loaded.next(true);
    } else {
      this.picture1Valid.next(true);
    }
    this.isMultiplePictures(this.data);
    return of(new Promise((res) => res(data)));
  }

  private getMediasLinks(product: CustomProduct) {
    const images = product.medias.filter(m => m.type === 'picture');
    const videos = product.medias.filter(m => m.type === 'video');
    const promises = [];

    if (images[0]) {
      promises.push(this.productsService.getMediaUrl(product.product_ean, images[0].id).toPromise()
          .then(imageLink => {
            return {url: imageLink, type: 'image1'};
          }).catch(err => {
            return {url: undefined, type: 'image1'};
          }));
    }
    if (images[1]) {
      promises.push(this.productsService.getMediaUrl(product.product_ean, images[1].id).toPromise()
          .then(imageLink => {
            return {url: imageLink, type: 'image2'};
          }).catch(err => {
            return {url: undefined, type: 'image2'};
          }));
    }
    if (videos[0]) {
      promises.push(this.productsService.getMediaUrl(product.product_ean, videos[0].id).toPromise()
          .then(videoLink => {
            return {url: videoLink, type: 'video'};
          }).catch(err => {
            return {url: undefined, type: 'video'};
          }));
    }

    return Promise.all(promises).then((res) => {
      return res;
    }).finally(() => {
      this.img1Loaded.next(true);
      this.img2Loaded.next(true);
    });
  }

  private getCustomClusterItemData() {
    const cssClass = 'backdrop-05 custom-product-page';

    return this.productsService.getProductByEan(this.data, userEnv.customer, this.translateService.currentLang).pipe(
        map((response: CustomProduct) => {
          let noImg1 = false;
          let noImg2 = false;

          return this.getMediasLinks(response).then((mediasLinks) => {
            const img1 = mediasLinks.find(ml => ml.type === 'image1');
            const img2 = mediasLinks.find(ml => ml.type === 'image2');
            const video = mediasLinks.find(ml => ml.type === 'video');

            if (img1 && img1.url) {
              this.picture1Valid.next(true);
            } else {
              this.picture1Valid.next(false);
              noImg1 = true;
            }
            if (img2 && img2.url) {
              this.picture2Valid.next(true);
            } else {
              this.picture2Valid.next(false);
              noImg2 = true;
            }

            const data =  {
              cssClass,
              product : {
                ean: response.product_ean,
                picture1: (img1 && img1.url) ? img1.url : undefined,
                picture2: (img2 && img2.url) ? img2.url : undefined,
                name: response.label || response.name,
                video: (video && video.url) ? video.url : undefined,
              },
              viewData: {
                ean: response.product_ean
              }
            };

            if (noImg1 && noImg2) {
              this.videoValid.next(false);
            }
            this.isMultiplePictures(data.product);
            return data;
          });
        })
    );
  }

  getImgByIndexWithVersion(index) {
    const url = this.productData.value.imagesUrls[index];
    const urlObj = new URL(url);

    // urlObj.searchParams.set('v', this.randomVersion);
    return urlObj.href;
  }

  private fetchStatus(url: string) {
    const fn = (URL: string) => fetch(URL).then((response) => response.status);

    return fn(this.addVersionToUrl(url)).then((status) => {
      if (status === 200) {
        return status;
      }
    });
  }

  private addVersionToUrl(url: string): string {
    const urlObj = new URL(url);
    // urlObj.searchParams.set('v', Math.random().toString());
    return urlObj.href;
  }

  private isMultiplePictures(product: any) {
    const urls = new Array<string>(2);

    urls[0] = product.picture1 || product.imgUrl1;
    urls[1] = product.picture2 || product.imgUrl2;

    if (urls.length <= 1 || !urls[1] || urls[1].length === 0) {
      return;
    }
    this.picture2Valid.next(true);
  }
}
