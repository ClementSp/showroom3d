import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ShowroomSettings} from '../../models/ShowroomSettings.model';
import {ImageViewerModalComponent} from '../image-viewer-modal/image-viewer-modal.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  @Input() showroomSettings: ShowroomSettings;

  public shouldBeDisplayed = false;
  public isOpen = false;

  constructor(private modalController: ModalController) { }

  ngOnInit() {
    if (this.showroomSettings.map?.url) {
      this.shouldBeDisplayed = true;
    }
  }

  public async openMap() {
    const modal = await this.modalController.create({
      component: ImageViewerModalComponent,
      cssClass: 'edit-marker-component backdrop-05',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        src: this.showroomSettings.map.url,
        title: 'Map'
      }
    });

    modal.onWillDismiss()
        .then(() => {
          this.isOpen = false;
        });

    this.isOpen = true;
    return await modal.present();
  }
}
