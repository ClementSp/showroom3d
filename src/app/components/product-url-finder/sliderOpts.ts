export const slidesOptions = {
    slidesPerView: 1,
    observer: true,
    simulateTouch: false,
    observeParents: true,
    pagination: {},
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
}
