import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {IonInfiniteScroll, IonSearchbar, IonSlides, ModalController, Platform} from '@ionic/angular';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {IBaseProduct} from '../../models/CustomProduct';
import {ApisExtService} from '../../services/apisExt.service';
import {slidesOptions} from './sliderOpts';

export interface IProductListItem extends IBaseProduct {
  checked: boolean;
}

@Component({
  selector: 'app-url-product-finder',
  templateUrl: './product-url-finder.component.html',
  styleUrls: ['./product-url-finder.component.scss'],
})
export class ProductUrlFinderComponent implements OnInit, AfterViewInit {

  @Input() multipleSelect = false;
  @Input() sort;
  @ViewChild('slider') slides: IonSlides;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('searchbar') searchbar: IonSearchbar;
  slidesOptions = slidesOptions;
  collections = [];
  collectionSelected;
  salesCatalogs = [];
  salesCatalogSelected;
  productsSelected = [];
  filteredProducts = [];
  products = [];
  nbProducts;
  productListLoaded$ = new BehaviorSubject<boolean>(false);
  expandedRow = new BehaviorSubject<number>(-1);
  currentSliderPage = new BehaviorSubject(0);

  page = 0;

  constructor(private platform: Platform,
              private modalController: ModalController,
              public apiExtService: ApisExtService) {
    if (this.platform.is('mobile')) {
      this.slidesOptions.simulateTouch = true;
    }
    if (this.isMd()) {
      this.slidesOptions.pagination = {
        el: '.swiper-pagination',
        type: 'bullets',
      };
    }
  }

  ngOnInit() {
    this.initializeCheck();
    if (this.sort === 'collection') {
      this.apiExtService.getApiCollections().subscribe(collections => {
        this.collections = collections;
      });
    } else if (this.sort === 'salesCatalog') {
      this.apiExtService.getApiSalesCatalogs().subscribe(salesCatalogs => {
        this.salesCatalogs = salesCatalogs;
      });
    } else {
      this.currentSliderPage.next(this.currentSliderPage.value + 1);
      this.loadProducts();
    }
  }

  loadProducts() {
    if (this.collectionSelected) {
      this.apiExtService.getApiCollectionProducts(this.collectionSelected.code).subscribe((products) => {
        if (products) {
          this.products = products;
          this.filteredProducts = products;
          this.nbProducts = products.length;
          this.productListLoaded$.next(true);
        }
      });
    }
    else if (this.salesCatalogSelected) {
      this.apiExtService.getApiSalesCatalogProducts(this.salesCatalogSelected.code).subscribe((products) => {
        if (products) {
          this.products = products;
          this.filteredProducts = products;
          this.nbProducts = products.length;
          this.productListLoaded$.next(true);
        }
      });
    } else {
      this.apiExtService.getApiProducts().subscribe((products) => {
        this.products = products;
        this.filteredProducts = products;
        this.nbProducts = products.length;
        this.productListLoaded$.next(true);
      });
    }
  }

  selectCollection(collection) {
    this.collectionSelected = collection;
    this.currentSliderPage.next(this.currentSliderPage.value + 1);
    this.loadProducts();
  }

  selectSalesCatalog(salesCatalog) {
    this.salesCatalogSelected = salesCatalog;
    this.currentSliderPage.next(this.currentSliderPage.value + 1);
    this.loadProducts();
  }

  ngAfterViewInit(): void {
    this.initializeSearchbar();

    this.slides.ionSlidesDidLoad.subscribe(() => {
      this.currentSliderPage.subscribe((value) => {
        this.slides.slideTo(value, 400)
            .catch(() => alert('error'));
      });
    });
  }

  onCheck(productChecked) {
    if (this.productsSelected.find(product => product === productChecked)) {
      this.productsSelected = this.productsSelected.filter(product => product !== productChecked);
    } else {
      this.productsSelected.push(productChecked);
    }
  }

  back() {
    this.currentSliderPage.next(this.currentSliderPage.value -1);
  }

  confirm() {
    this.modalController.dismiss(this.productsSelected);
  }

  getProductTitle(product) {
    let title = product.code;

    if (product.label && product.label.length > 0) {
      title += ' / ' + product.label;
    }

    if (product.product_color && product.product_color.length > 0) {
      title += ' / ' + product.product_color;
    }
    return title;
  }

  getCollectionTitle(collection) {
    return collection.code + ' / ' + collection.name;
  }

  getCatalogTitle(catalog) {
    return catalog.code + ' / ' + catalog.name;
  }

  toggleContent(index: number) {
    if (this.expandedRow.value === index) {
      this.expandedRow.next(-1);
    } else {
      this.expandedRow.next(index);
    }
  }

  dismiss() {
    this.modalController.dismiss([]);
  }

  private initializeSearchbar() {
    this.searchbar.ionInput.pipe(debounceTime(300), distinctUntilChanged()).subscribe((event) => {
      const term = this.searchbar.value.toLowerCase();
      this.page = 0;
      if (term === '') {
        this.filteredProducts = this.products;
      } else {
        this.filteredProducts = this.products.filter(product =>
            product.code.toLowerCase().indexOf(term) !== -1 || product.ean.toLowerCase().indexOf(term) !== -1);
      }
    });
  }

  private initializeCheck() {
    const productsCopy = this.filteredProducts;

    productsCopy.forEach((product) => {
      product.checked = false;
    });
    this.filteredProducts = productsCopy;
  }

  isMd() {
    return this.platform.width() < 768;
  }

}
