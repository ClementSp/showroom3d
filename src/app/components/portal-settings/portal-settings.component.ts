import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ShowroomPortal, SweepPortal} from '../../models/PortalMarker';
import {MenuLink} from '../../models/showroom.model';
import {PoiAction} from '../../common/PoiAction';
import {PortalAction} from '../../common/PortalAction';

@Component({
  selector: 'app-portal-settings',
  templateUrl: './portal-settings.component.html',
  styleUrls: ['./portal-settings.component.scss'],
})
export class PortalSettingsComponent implements OnInit {
  @Input() menu: Array<MenuLink>;
  @Input() entranceId: string;
  @Input() portal: SweepPortal;
  @Output() portalUpdate = new EventEmitter();


  sweepsList = [];

  constructor(private modalCtrl: ModalController) {
  }

  ngOnInit() {
      this.sweepsList = this.sweepsList.concat(this.menu);
      this.sweepsList.push({id: this.entranceId, title: 'Entrance'});
      this.sweepsList = this.sweepsList.sort((a, b) => a.title.localeCompare(b.title));
  }

  changeSweep(sweepId) {
    this.portal.sweepId = sweepId;
  }

  rotationChange(rot) {
    this.portal.rotation = rot;
  }

  async cancel() {
    await this.modalCtrl.dismiss(null);
  }

  movePortal() {
    return this.modalCtrl.dismiss({action: PortalAction.MOVE, portal: this.portal});
  }

  removePortal() {
    return this.modalCtrl.dismiss({action: PortalAction.REMOVE, portal: this.portal});
  }

  async confirm() {
    await this.modalCtrl.dismiss({action: PoiAction.EDIT, portal: this.portal});
  }
}
