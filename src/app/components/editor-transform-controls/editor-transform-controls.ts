import {TransformControls} from 'three/examples/jsm/controls/TransformControls';
import * as THREE from 'three';
import {BehaviorSubject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {FixturesManagerService} from "../../services/modules/fixtures-manager.service";
import {SmartObjectManagerService} from "../../services/modules/smart-object-manager.service";
import {ProductsManagerService} from "../../services/modules/products-manager.service";
import {ShowroomsService} from "../../services/showrooms.service";
import {showroomEnv} from "../../../environments/showroom.env";
import {SceneHandler} from '../../DataHandlers/scene.handler';

export interface HistoryItem {
    target: THREE.Object3D;
    time: number;
    transformAction: string;
    previousValue: any;
    newValue: any;
}

export default class EditorTransformControls extends TransformControls {
    readonly defaultMode = 'translate';
    readonly defaultSpace = 'world';
    readonly controlToSnap = 'Shift';
    readonly rotateSnap = 0.8;

    readonly defaultScaleSnap = .05;
    readonly scaleSnap = 0.15;

    readonly translateSnap = 0.1;

    private history = new Array<HistoryItem>();
    private currentAction: HistoryItem;
    private undoFunctionsByActionMap = new Map([
        ['translate', this.undoTranslate.bind(this)],
        ['scale', this.undoScale.bind(this)],
        ['rotate', this.undoRotation.bind(this)]
    ]);

    private fixturesManagerService: FixturesManagerService;
    private productsManagerService: ProductsManagerService;
    private smartObjectsManagerService: SmartObjectManagerService;
    private showroomService: ShowroomsService;

    attachedObject = new BehaviorSubject<any>(undefined);


    preventEvent = (event) => event.preventDefault();

    constructor(fixturesManagerService: FixturesManagerService,
                productsManagerService: ProductsManagerService,
                smartObjectsManagerService: SmartObjectManagerService,
                showroomService: ShowroomsService,
                object: THREE.Camera,
                domElement?: HTMLElement,
                public sceneHandler?: SceneHandler) {
        super(object, domElement);

        this.fixturesManagerService = fixturesManagerService;
        this.productsManagerService = productsManagerService;
        this.smartObjectsManagerService = smartObjectsManagerService;
        this.showroomService = showroomService;

        this.lockCameraOnTransform();
        this.unlockCameraOnTransformEnd();
        this.setTranslationSnapOnHoldingShift();
        if (this.defaultSpace === 'world') {
            this.setSpace('world');
        } else {
            this.setSpace('local');
        }
        this.setMode(this.defaultMode);
        this.onNewObjectAttached();
        this.fillHistoryOnTransform();
        this.resetCurrentAction();
        this.bindCtrlZUndoHistory();
        this.setScaleSnap(this.defaultScaleSnap);
    }

    isAttachedToObject() {
        return this.object !== undefined;
    }

    attachTo(obj: any) {
        this.attachedObject.next(obj);
        this.attach(obj);
    }

    private bindCtrlZUndoHistory() {
        document.addEventListener('keyup', (key) => {

            if (key.ctrlKey && key.key === 'z' && this.history.length > 0) {
                const toUndo = this.history.pop();

                if (this.undoFunctionsByActionMap.has(toUndo.transformAction)) {
                    this.undoFunctionsByActionMap.get(toUndo.transformAction)(toUndo);
                }
            }
        });
    }

    private fillHistoryOnTransform() {
        this.addEventListener('mouseDown', () => {
            const currentMode = this.getMode();

            this.currentAction.transformAction = currentMode;
            this.currentAction.target = this.attachedObject.value;
            switch (currentMode) {
                case 'translate':
                    this.currentAction.previousValue = this.attachedObject.value.position.clone();
                    this.sceneHandler.setTransformObject(true);
                    break;
                case 'scale':
                    this.currentAction.previousValue = this.attachedObject.value.scale.clone();
                    this.sceneHandler.setTransformObject(true);
                    break;
                case 'rotate':
                    this.currentAction.previousValue = this.attachedObject.value.rotation.clone();
                    this.sceneHandler.setTransformObject(true);
                    break;
            }
        });
        this.addEventListener('mouseUp', () => {
            const currentMode = this.getMode();
            this.sceneHandler.setTransformObject(false);

            this.currentAction.time = Date.now();
            switch (currentMode) {
                case 'translate':
                    this.currentAction.newValue = this.attachedObject.value.position.clone();
                    this.onTranslate({...this.currentAction});
                    break;
                case 'scale':
                    this.currentAction.newValue = this.attachedObject.value.scale.clone();
                    this.onScale(this.currentAction);
                    break;
                case 'rotate':
                    this.currentAction.newValue = this.attachedObject.value.rotation.clone();
                    this.onRotate(this.currentAction);
                    break;
            }
            this.history.push(this.currentAction);
            this.sortHistoryByTime();
            this.resetCurrentAction();
        });
    }

    private onTranslate(item: HistoryItem) {
        let diff;
        switch (item.target.userData.type) {
            case 'SMART_OBJECT':
                diff = new THREE.Vector3().subVectors(item.newValue, item.previousValue);
                this.smartObjectsManagerService.translateSmartObject(item.target.userData.id, diff).subscribe();
                break;
            case'PRODUCT':
                diff = new THREE.Vector3().subVectors(item.newValue, item.previousValue);
                this.productsManagerService.translateProduct(item.target.userData.id, diff).subscribe();
                break;
            case 'FIXTURE':
                diff = new THREE.Vector3().subVectors(item.newValue, item.previousValue);
                this.fixturesManagerService.translateFixture(item.target.userData.id, diff).subscribe();
                break;
            case 'SHAPE_POI':
                const shapePoi = showroomEnv.showroom.shapePois.find(shapeP => shapeP.id === item.target.userData.id);
                shapePoi.position = item.newValue;
                this.showroomService.putShapePoi(shapePoi).subscribe();
                break;
            case 'POI':
                const poi = showroomEnv.showroom.pois.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                poi.position = item.newValue;
                this.showroomService.putPoi(poi).subscribe();
                break;
            case 'PORTAL':
                const portal = showroomEnv.showroom.portals.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                portal.position = item.newValue;
                this.showroomService.putPortal(portal).subscribe();
                break;
            case 'POINT_LIGHT':
            case 'SPOT_LIGHT':
                const light = showroomEnv.showroom.lights.find(showroomLight => showroomLight.id === item.target.userData.id);
                light.position = item.newValue;
                this.showroomService.putLight(light).subscribe();
                break;
        }
    }

    private onScale(item: HistoryItem) {
        switch (item.target.userData.type) {
            case 'SMART_OBJECT':
                this.smartObjectsManagerService.scaleSmartObject(item.target.userData.id, item.newValue).subscribe();
                break;
            case'PRODUCT':
                this.productsManagerService.scaleProduct(item.target.userData.id, item.newValue).subscribe();
                break;
            case 'FIXTURE':
                this.fixturesManagerService.scaleFixture(item.target.userData.id, item.newValue).subscribe();
                break;
            case 'SHAPE_POI':
                const shapePoi = showroomEnv.showroom.shapePois.find(shapeP => shapeP.id === item.target.userData.id);
                shapePoi.scale = item.newValue;
                this.showroomService.putShapePoi(shapePoi).subscribe();
                break;
            case 'POI':
                const poi = showroomEnv.showroom.pois.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                poi.scale = item.newValue;
                this.showroomService.putPoi(poi).subscribe();
                break;
            case 'PORTAL':
                const portal = showroomEnv.showroom.portals.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                portal.scale = item.newValue;
                this.showroomService.putPortal(portal).subscribe();
                break;
            case 'POINT_LIGHT':
            case 'SPOT_LIGHT':
                const light = showroomEnv.showroom.lights.find(showroomLight => showroomLight.id === item.target.userData.id);
                light.scale = item.newValue;
                this.showroomService.putLight(light).subscribe();
                break;
        }
    }

    private onRotate(item: HistoryItem) {
        switch (item.target.userData.type) {
            case 'SMART_OBJECT':
                this.smartObjectsManagerService.rotateSmartObject(item.target.userData.id, item.newValue).subscribe();
                break;
            case'PRODUCT':
                this.productsManagerService.rotateProduct(item.target.userData.id, item.newValue).subscribe();
                break;
            case 'FIXTURE':
                this.fixturesManagerService.rotateFixture(item.target.userData.id, item.newValue).subscribe();
                break;
            case 'SHAPE_POI':
                const shapePoi = showroomEnv.showroom.shapePois.find(shapeP => shapeP.id === item.target.userData.id);
                shapePoi.rotation = item.newValue;
                this.showroomService.putShapePoi(shapePoi).subscribe();
                break;
            case 'POI':
                const poi = showroomEnv.showroom.pois.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                poi.rotation = item.newValue;
                this.showroomService.putPoi(poi).subscribe();
                break;
            case 'PORTAL':
                const portal = showroomEnv.showroom.portals.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                portal.rotation = item.newValue;
                this.showroomService.putPortal(portal).subscribe();
                break;
            case 'POINT_LIGHT':
            case 'SPOT_LIGHT':
                const light = showroomEnv.showroom.lights.find(showroomLight => showroomLight.id === item.target.userData.id);
                light.rotation = item.newValue;
                this.showroomService.putLight(light).subscribe();
                break;
        }
    }


    private undoTranslate(item: HistoryItem) {
        const diff = new THREE.Vector3().subVectors(item.newValue, item.previousValue);

        item.target.position.set(
            item.target.position.x - diff.x,
            item.target.position.y - diff.y,
            item.target.position.z - diff.z
        );

        switch (item.target.userData.type) {
            case 'SMART_OBJECT':
                this.smartObjectsManagerService.translateSmartObject(item.target.userData.id, diff).subscribe();
                break;
            case'PRODUCT':
                this.productsManagerService.translateProduct(item.target.userData.id, diff).subscribe();
                break;
            case 'FIXTURE':
                this.fixturesManagerService.translateFixture(item.target.userData.id, diff).subscribe();
                break;
            case 'SHAPE_POI':
                const shapePoi = showroomEnv.showroom.shapePois.find(shapeP => shapeP.id === item.target.userData.id);
                shapePoi.position = item.target.position;
                this.showroomService.putShapePoi(shapePoi).subscribe();
                break;
            case 'POI':
                const poi = showroomEnv.showroom.pois.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                poi.position = item.target.position;
                this.showroomService.putPoi(poi).subscribe();
                break;
            case 'POINT_LIGHT':
            case 'SPOT_LIGHT':
                const light = showroomEnv.showroom.lights.find(showroomLight => showroomLight.id === item.target.userData.id);
                light.position = item.target.position;
                this.showroomService.putLight(light).subscribe();
                break;
        }
    }

    private undoScale(item: HistoryItem) {
        item.target.scale.set(
            item.previousValue.x,
            item.previousValue.y,
            item.previousValue.z
        );
        switch (item.target.userData.type) {
            case 'SMART_OBJECT':
                this.smartObjectsManagerService.scaleSmartObject(item.target.userData.id, item.target.scale).subscribe();
                break;
            case'PRODUCT':
                this.productsManagerService.scaleProduct(item.target.userData.id, item.target.scale).subscribe();
                break;
            case 'FIXTURE':
                this.fixturesManagerService.scaleFixture(item.target.userData.id, item.target.scale).subscribe();
                break;
            case 'SHAPE_POI':
                const shapePoi = showroomEnv.showroom.shapePois.find(shapeP => shapeP.id === item.target.userData.id);
                shapePoi.scale = item.target.scale;
                this.showroomService.putShapePoi(shapePoi).subscribe();
                break;
            case 'POI':
                const poi = showroomEnv.showroom.pois.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                poi.scale = item.target.scale;
                this.showroomService.putPoi(poi).subscribe();
                break;
            case 'POINT_LIGHT':
            case 'SPOT_LIGHT':
                const light = showroomEnv.showroom.lights.find(showroomLight => showroomLight.id === item.target.userData.id);
                light.scale = item.target.position;
                this.showroomService.putLight(light).subscribe();
                break;
        }
    }

    private undoRotation(item: HistoryItem) {
        item.target.rotation.set(
            item.previousValue.x,
            item.previousValue.y,
            item.previousValue.z
        );
        switch (item.target.userData.type) {
            case 'SMART_OBJECT':
                this.smartObjectsManagerService.rotateSmartObject(item.target.userData.id, item.previousValue).subscribe();
                break;
            case'PRODUCT':
                this.productsManagerService.rotateProduct(item.target.userData.id, item.previousValue).subscribe();
                break;
            case 'FIXTURE':
                this.fixturesManagerService.rotateFixture(item.target.userData.id, item.previousValue).subscribe();
                break;
            case 'SHAPE_POI':
                const shapePoi = showroomEnv.showroom.shapePois.find(shapeP => shapeP.id === item.target.userData.id);
                shapePoi.rotation = item.previousValue;
                this.showroomService.putShapePoi(shapePoi).subscribe();
                break;
            case 'POI':
                const poi = showroomEnv.showroom.pois.find(showroomPoi => showroomPoi.id === item.target.userData.id);
                poi.rotation = item.previousValue;
                this.showroomService.putPoi(poi).subscribe();
                break;
            case 'POINT_LIGHT':
            case 'SPOT_LIGHT':
                const light = showroomEnv.showroom.lights.find(showroomLight => showroomLight.id === item.target.userData.id);
                light.rotation = item.previousValue;
                this.showroomService.putLight(light).subscribe();
                break;
        }
    }

    private resetCurrentAction() {
        this.currentAction = {
            transformAction: '',
            newValue: undefined,
            previousValue: undefined,
            target: undefined,
            time: 0
        };
    }

    private lockCameraOnTransform() {
        this.addEventListener('mouseDown', () => {
            //psv.on('before-rotate', this.preventEvent);
        });
    }

    private unlockCameraOnTransformEnd() {
        this.addEventListener('mouseUp', () => {
            //psv.off('before-rotate', this.preventEvent);
        });
    }

    private onNewObjectAttached() {
        this.attachedObject
            .pipe( filter(obj => obj !== undefined) )
            .subscribe((attachedObject: THREE.Object3D) => {
            });
    }

    private sortHistoryByTime() {
        this.history.sort((item1, item2) => {
            if (item1.time < item2.time) {
                return -1;
            } else if (item1.time === item2.time) {
                return 0;
            } else {
                return 1;
            }
        });
    }

    private setTranslationSnapOnHoldingShift() {
        document.addEventListener('keydown', (key) => {
            if (this.isAttachedToObject()) {
                if (key.key === this.controlToSnap) {
                    this.setRotationSnap(this.rotateSnap);
                    this.setTranslationSnap(this.translateSnap);
                }
            }
        });
        document.addEventListener('keyup', (key) => {
            if (this.isAttachedToObject()) {
                if (key.key === this.controlToSnap) {
                    this.setRotationSnap(null);
                    this.setTranslationSnap(null);
                }
            }
        });
    }
}
