import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {Countries, Country} from './data/Countries';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';
import {distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-country-picker',
  templateUrl: './country-picker.component.html',
  styleUrls: ['./country-picker.component.scss'],
})
export class CountryPickerComponent implements OnInit, AfterViewInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild('searchbar') searchbar: IonSearchbar;

  @Input() selected: string;

  readonly countries: Array<Country> = Countries;
  filteredCountries = new BehaviorSubject<Array<Country>>([]);

  readonly  pageSize = 20;
  page = 0;

  constructor(private modalController: ModalController) { }

  ngOnInit() {
    this.filteredCountries.next(this.countries.slice(this.page, this.pageSize));
  }

  ngAfterViewInit(): void {
    this.initializeSearchbar();
  }

  close() {
    return this.modalController.dismiss();
  }

  confirm(selectedCountry) {
    return this.modalController.dismiss({country: selectedCountry});
  }

  loadData($event) {
    let foundCountries;
    const term = this.searchbar.value.toLowerCase();
    const currentProducts = [...this.filteredCountries.value];

    foundCountries = this.countries.slice(this.page * this.pageSize, this.page * this.pageSize + this.pageSize);
    this.page++;
    foundCountries.forEach((item) => currentProducts.push(item));
    this.filteredCountries.next(currentProducts);
    $event.target.complete();
  }

  private initializeSearchbar() {
    this.searchbar.ionInput.pipe(distinctUntilChanged()).subscribe(() => {
      const term = this.searchbar.value.toLowerCase();
      let foundCountries;

      this.page = 0;
      foundCountries = this.countries.filter(country => country.name.toLowerCase().startsWith(term.toLowerCase()));
      this.filteredCountries.next(foundCountries);
    });
  }
}
