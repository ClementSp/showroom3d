import {AfterViewInit, Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {IonSlides} from '@ionic/angular';
import {IMedia} from '../custom-product-page-v2.component';
import {BehaviorSubject} from 'rxjs';
import {AudioHandler} from "../../../DataHandlers/audio.handler";

@Component({
  selector: 'app-medias-viewer',
  templateUrl: './medias-viewer.component.html',
  styleUrls: ['./medias-viewer.component.scss'],
})
export class MediasViewerComponent implements AfterViewInit, OnDestroy {
  @ViewChild('slider', {static: true}) slider: IonSlides;

  @Input() slides: Array<IMedia>;
  @Input() currentIndex = new BehaviorSubject<number>(0);

  public slideOpts = {
    initialSlide: 0,
    speed: 400,
    navigation: true
  };

  constructor(public audioHandler: AudioHandler) {


  }

  ngAfterViewInit(): void {
    this.onIndexUpdated();
    this.onSlideChanged();
  }

  public slideChanged() {
    if (this.slider) {
      this.slider.getActiveIndex().then(index => {
        if (this.slides[index].media.type === 'video') {
          this.audioHandler.setVolume(0);
        } else {
          this.audioHandler.setVolume( this.audioHandler.getDefaultVolume());
        }
      });
    }
  }

  private onIndexUpdated() {
    return this.currentIndex.subscribe((value: number) => {
      return this.slider.slideTo(value);
    });
  }

  private onSlideChanged() {
    this.slider.ionSlideDidChange.subscribe(() => {
       this.slider.getActiveIndex()
           .then((activeIndex: number) => this.currentIndex.next(activeIndex));
    });
  }

  ngOnDestroy(): void {
    this.audioHandler.setVolume( this.audioHandler.getDefaultVolume());
  }
}
