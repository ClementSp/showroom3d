import {Component, Input, OnInit} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Platform} from '@ionic/angular';
import {IWishListProduct, WishlistHandler} from '../../../DataHandlers/Wishlist.handler';
import {TranslateService} from '@ngx-translate/core';
import {ModulesService} from "../../../services/modules.service";
import {showroomEnv} from "../../../../environments/showroom.env";

@Component({
  selector: 'app-quantity-button',
  templateUrl: './quantity-button.component.html',
  styleUrls: ['./quantity-button.component.scss'],
})
export class QuantityButtonComponent implements OnInit {

  @Input() ean: string;
  @Input() inventoryQuantity: string;
  @Input() price = 0;

  quantity = new BehaviorSubject(1);
  inWishList = new BehaviorSubject<boolean>(false);
  isInventoryEnabled = false;

  constructor(private platform: Platform,
              private translateService: TranslateService,
              private wishlistHandler: WishlistHandler) { }

  ngOnInit() {
    this.wishlistHandler.getProductByEan(this.ean).subscribe((product: IWishListProduct) => {
      if (product) {
        this.quantity.next(product.quantity);
        this.inWishList.next(true);
      }
    });

    this.isInventoryEnabled = showroomEnv.showroom.settings.components.inventory.enabled;
  }

  add() {
    if (this.isInventoryEnabled && this.inventoryQuantity && !this.hasQuantity()) {
      return;
    }
    this.quantity.next(this.quantity.value + 1);
    this.wishlistHandler.incrementProductQuantity(this.ean);
  }

  hasQuantity() {
    return this.quantity ? this.quantity.value + 1 <= Number(this.inventoryQuantity) : true;
  }

  hasLowQuantity() {
    return showroomEnv.showroom.settings.components.inventory.nbItemForWarningMessage >= Number(this.inventoryQuantity);
  }

  isBreakpoints() {
    return (this.platform.width() >= 768 && this.platform.width() <= 914) || this.platform.width() <= 418;
  }

  remove() {
    if (this.quantity.value > 0){
      this.quantity.next(this.quantity.value - 1);
      this.wishlistHandler.decrementProductQuantity(this.ean);
      if (this.quantity.value === 0) {
        this.inWishList.next(false);
        this.wishlistHandler.removeProduct(this.ean);
      }
    }
  }

  addToWishlist() {
    this.wishlistHandler.addProduct({
      quantity: this.quantity.value,
      ean: this.ean,
      price: this.price
    });
    this.inWishList.next(true);
  }

  removeFromWishList() {
    this.wishlistHandler.removeProduct(this.ean);
    this.quantity.next(1);
    this.inWishList.next(false);
  }

  onClickWishlistButton() {
    if (this.inWishList.value) {
      this.removeFromWishList();
    } else {
      this.addToWishlist();
    }
  }
}
