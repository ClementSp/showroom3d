import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit, Output,
  ViewChild
} from '@angular/core';
import {IonSlides, ModalController, Platform} from '@ionic/angular';
import {BehaviorSubject, combineLatest, Subscription} from 'rxjs';
import {SafePipe} from '../../pipes/safe.pipe';
import CustomProduct, {ProductMedia} from '../../models/CustomProduct';
import {map} from 'rxjs/operators';
import {CustomerService} from '../../services/customer.service';
import {ProductsService} from '../../services/products.service';
import {slidesOptions} from './sliderOpts';
import {UserService} from '../../services/user.service';
import {userEnv} from '../../../environments/user.env';
import {TranslateService} from '@ngx-translate/core';
import {customerEnv} from '../../../environments/customer.env';
import {showroomEnv} from '../../../environments/showroom.env';
import {VisualizerComponent} from "../modules/objects/visualizer/visualizer.component";
import {SceneHandler} from "../../DataHandlers/scene.handler";
import {ShowroomsService} from "../../services/showrooms.service";

export interface IMedia {
  src: string;
  media: ProductMedia;
}

@Component({
  selector: 'app-custom-product-page-v2',
  templateUrl: './custom-product-page-v2.component.html',
  styleUrls: ['./custom-product-page-v2.component.scss'],
})
export class CustomProductPageV2Component implements OnInit, AfterViewInit, OnDestroy {

  @Input() data: any;
  @Output() configurationToSend  = new EventEmitter<any>();
  @ViewChild('prevButton') prevButton: ElementRef;
  @ViewChild('nextButton') nextButton: ElementRef;
  @ViewChild('slider') slider: IonSlides;
  @ViewChild('visualizer') visualizer: VisualizerComponent;
  currentMedia = new BehaviorSubject<number>(0);
  title: string;
  subtitle: string;
  description = '';
  price = 0;
  ean = '';
  id = '';
  currency = '€';
  twitterUrl = '';
  facebookUrl = '';
  instagramUrl = '';
  pdfUrl = '';
  nbMedia = 0;
  slides = new Array<IMedia>();
  videos = new Array<any>();
  slidesOptions = slidesOptions;
  productsFullyLoadedSubscription: Subscription;
  allMediasLoaded = new BehaviorSubject<boolean>(false);
  productPhotosLoadings = new Array<BehaviorSubject<boolean>>();
  productId: string;
  glbUrl: string;
  isReady = false;
  currentIndex = 0;
  product: CustomProduct;
  code: any;
  textureUrl: any;
  configurations: any;
  objectId: any;
  isEditor = false;
  template = '';
  templateList = [
    'Default', 'template 2', 'template 3', 'template 4', 'Two Steps', 'Premium', 'template full pictures'
  ];

  constructor(private platform: Platform,
              public customerService: CustomerService,
              private modalController: ModalController,
              private userService: UserService,
              private translateService: TranslateService,
              private productsService: ProductsService,
              private showroomService: ShowroomsService,
              public safePipe: SafePipe,
              public sceneHandler: SceneHandler) {
    if (this.platform.is('mobile')) {
      this.slidesOptions.simulateTouch = true;
    }
    if (this.isMd()) {
      this.slidesOptions.pagination = {
        el: '.swiper-pagination',
        type: 'bullets',
      };
    }
  }

  ngOnInit(): void {
    this.ean = this.data.ean;
    this.productId = this.data.productId;
    this.objectId = this.data.object?.userData?.id;
    this.isEditor = this.data.isEditor;
    this.template = this.data.object?.userData?.template;
    this.allMediasLoaded.next(true);
    if (this.ean) {
      this.productsService.getProductByEan(this.ean, userEnv.customer, this.translateService.currentLang)
          .subscribe((product: CustomProduct) => {
            this.initParameters(product);
          });
    } else {
      if (this.productId){
        this.productsService.getProductById(this.productId)
            .then((product: CustomProduct) => {
              this.initParameters(product);
            });
      }
    }
  }

  private initParameters(product: CustomProduct) {
    this.product = product;
    this.title = product.label;
    this.subtitle = product.code;
    this.price = product.sale_price;
    this.ean = product.product_ean;
    this.id = product.id;
    this.description = product.description;
    this.twitterUrl = product.twitterUrl;
    this.glbUrl = product.product_glbModelUrl;
    this.facebookUrl = product.facebookUrl;
    this.instagramUrl = product.instagramUrl;

    if (product.medias) {
      this.nbMedia = product.medias.length;
    }

    this.getMedias(product).then(() => {

      this.initProductFullyLoaded();
      this.getObject3D();
      if (this.slides.length === 0) {
        this.slides.push({
          media: {
            type: 'picture',
            ext: '',
            id: ''},
          src: 'assets/ui/nophoto.jpg'});
      }
    });

  }

  ngAfterViewInit(): void {
    this.initSliderEvents();
  }

  get customerEnv() {
    return customerEnv;
  }

  get showroomEnv() {
    return showroomEnv;
  }

  ngOnDestroy(): void {
    if (this.productsFullyLoadedSubscription) {
      this.productsFullyLoadedSubscription.unsubscribe();
    }
  }

  initProductFullyLoaded() {
    const reducer = (accumulator, currentValue) => accumulator && currentValue;

    this.productsFullyLoadedSubscription =
        combineLatest([...this.productPhotosLoadings])
            .pipe(map((values: Array<boolean>) => values.reduce(reducer)))
            .subscribe((value: boolean) => {
              this.allMediasLoaded.next(value);
            });
  }

  initSliderEvents(): void {
    if (this.slides.length > 0) {
      this.slider.ionSlideDidChange.subscribe(() => {
        this.slider.getActiveIndex().then((rest) => {
          this.currentMedia.next(rest);
        });
      });
    }
  }

  isMd() {
    return this.platform.width() < 768;
  }

  isMobile() {
    return this.platform.is('mobile');
  }

  slideTo(rank: number) {
    this.slider.slideTo(rank).then(() => {
      this.muteVideos();
    });
  }

  nextMedia() {
    this.next();
    this.muteVideos();
  }

  prevMedia() {
    this.prev();
    this.muteVideos();
  }

  muteVideos() {
    this.videos.forEach((video) => {
      video.muted = true;
    });
  }

  mediaLoaded(index: number, video?: any) {
    if (video) {
      this.videos.push(video);
    }
    if (index < this.productPhotosLoadings.length && this.productPhotosLoadings[index]) {
      this.productPhotosLoadings[index].next(true);
    }
  }

  close() {
    this.modalController.dismiss();
  }

  next() {
    this.slider.slideNext();
  }

  prev() {
    this.slider.slidePrev();
  }

  private getMedias(product: CustomProduct) {
    if (!product.medias) {
      product.medias = [];
    }
    if (product.medias) {
      const promises = [];
      product.medias.forEach(media => {
        promises.push(this.productsService.getMediaUrl(product.product_ean, media.id).toPromise()
            .then((url: string) => {
              this.loadMedia(url, media);
            })
            .catch(() => this.allMediasLoaded.next(true)));
      });
      return Promise.all(promises).then(() => {
        let images = this.slides.filter(s => s.media.type === 'picture');
        let videos = this.slides.filter(s => s.media.type === 'video');
        const pdfs = this.slides.filter(s => s.media.type === 'pdf') || [];
        if (pdfs.length > 0) {
          this.pdfUrl = pdfs[0].src;
        }
        images = images.sort((a, b) => {
          const idx1 = product.medias.findIndex(m => m.id === a.media.id);
          const idx2 = product.medias.findIndex(m => m.id === b.media.id);
          return idx1 - idx2;
        });

        videos = videos.sort((a, b) => {
          const idx1 = product.medias.findIndex(m => m.id === a.media.id);
          const idx2 = product.medias.findIndex(m => m.id === b.media.id);
          return idx1 - idx2;
        });

        this.slides = images.concat(videos);

      });
    }

  }

  private getObject3D() {
    const object = [];
    if (this.glbUrl || this.data.object){
      object.push({
        media: {
          type: 'object',
          ext: '',
          id: ''},
        src: this.glbUrl ? this.glbUrl : ''}) ;
      this.slides = this.slides.concat(object);
    }
  }

  private loadMedia(url: string, media: ProductMedia) {
    const fetchStatus = (URL: string) => fetch(URL).then((response) => response.status);

    return fetchStatus(this.addVersionToUrl(url)).then((status) => {
      if (status === 200) {
        this.slides.push({src: url, media});
        if (media.type !== 'picture') {
          this.productPhotosLoadings.push(new BehaviorSubject(false));
        } else {
          this.productPhotosLoadings.push(new BehaviorSubject(true));
        }
      } else {
        throw new Error();
      }
    });
  }

  private addVersionToUrl(url: string): string {
    if (url) {
      const urlObj = new URL(url);
      // urlObj.searchParams.set('v', Math.random().toString());
      return urlObj.href;
    } else {
      return undefined;
    }
  }

  public openUrl(url: string) {
    if (!url.startsWith('http')) {
      url = 'http://' + url;
    }
    window.open(url, '_blank');
  }

  initSlider(){
    const index =  !this.slides.findIndex(media => media.media.type === 'object') ? 0
        : this.slides.findIndex(media => media.media.type === 'object');
    this.currentIndex = index;
    this.slider.slideTo(this.currentIndex).then();
  }

  setIsReady() {
    this.isReady = true;
  }

  async changeSlide(event: any) {
    this.currentIndex = await this.slider.getActiveIndex();

  }

  setCode(code: any) {
    this.code = code;
  }

  setTextureUrl(textureUrl: any) {
    this.textureUrl = textureUrl;
  }

  saveConfiguration(configurations: any) {
    this.visualizer.runChanges(configurations);
    const productObject = showroomEnv.showroom.products.find(product => product.id === this.objectId);
    if (productObject) {
      if (!productObject.saveConfigurations) {
        productObject.saveConfigurations = [];
      }
      const lastConf = productObject.saveConfigurations.find(conf => conf.id === configurations.id)
      if (lastConf) {
        productObject.saveConfigurations.splice(productObject.saveConfigurations.indexOf(lastConf), 1);
      }
      productObject.saveConfigurations.push(configurations);
    }
  }

  onTemplateChange(event) {
    this.data.object.userData.template = event.detail.value;
    const product = showroomEnv.showroom.products.find(product => product.id === this.data.object.userData.id);
    if (product) {
      product.template = event.detail.value;
      this.showroomService.updateShowroom(showroomEnv.showroom).subscribe();
    }
  }
}
