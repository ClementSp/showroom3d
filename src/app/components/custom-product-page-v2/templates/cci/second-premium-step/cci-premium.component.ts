import {Component, Input, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Subscription} from 'rxjs';
import {IMedia} from '../../../custom-product-page-v2.component';
import {TranslateService} from '@ngx-translate/core';
import {ProductsService} from '../../../../../services/products.service';
import {WishlistHandler} from '../../../../../DataHandlers/Wishlist.handler';
import {userEnv} from '../../../../../../environments/user.env';
import CustomProduct, {ProductMedia} from '../../../../../models/CustomProduct';
import {map} from 'rxjs/operators';
import {slidesOptions} from '../../../sliderOpts';
import {ModalController, Platform} from '@ionic/angular';
import {ContactFormComponent} from '../contact-form/contact-form.component';

@Component({
  selector: 'app-cci-premium',
  templateUrl: './cci-premium.component.html',
  styleUrls: ['./cci-premium.component.scss'],
})
export class CCIPremiumComponent implements OnInit {

  @Input() data: any;

  title: string;
  subtitle: string;
  description = '';
  details = '';
  price = 0;
  ean = '';
  id = '';
  currency = '€';
  twitterUrl = '';
  facebookUrl = '';
  instagramUrl = '';
  linkedinUrl = '';
  youtubeUrl = '';
  contactEmail: string;
  pdfUrl = '';
  logoUrl = null;
  presentation = null;
  pricePdfUrl = '';
  nbMedia = 0;
  nbVideo = new BehaviorSubject<number>(0);

  slides = new Array<IMedia>();
  currentMediaIndex = new BehaviorSubject<number>(0);
  videos = new Array<any>();
  slidesOptions = slidesOptions;
  productsFullyLoadedSubscription: Subscription;
  allMediasLoaded = new BehaviorSubject<boolean>(false);
  productPhotosLoadings = new Array<BehaviorSubject<boolean>>();

  inWishList = new BehaviorSubject<boolean>(false);

  constructor(
      private modalController: ModalController,
      private translateService: TranslateService,
      private productsService: ProductsService,
      public platform: Platform,
      private wishlistHandler: WishlistHandler
  ) { }

  ngOnInit() {
    this.ean = this.data.ean + '-premium';
    if (this.ean) {
      this.isInWishlist();
      this.productsService.getProductByEan(this.ean, userEnv.customer, this.translateService.currentLang)
          .subscribe((product: CustomProduct) => {
            this.title = product.label;
            this.subtitle = product.code;
            this.price = product.sale_price;
            this.ean = product.product_ean;
            this.id = product.id;
            this.details = product.details;
            this.description = product.description;
            this.twitterUrl = product.twitterUrl;
            this.facebookUrl = product.facebookUrl;
            this.contactEmail = product.contact;
            this.instagramUrl = product.instagramUrl;
            this.linkedinUrl = product.linkedinUrl;
            this.youtubeUrl = product.youtubeUrl;
            this.getMedias(product).then(() => {
              this.initProductFullyLoaded();
              this.allMediasLoaded.next(true);
            });
          });
    }
  }

  close() {
    return this.modalController.dismiss();
  }

  async contact() {
    const modal = await this.modalController.create({
      component: ContactFormComponent,
      cssClass: 'backdrop-05',
      componentProps: {
        recipient: this.contactEmail
      }
    });
    return modal.present();
  }

  initProductFullyLoaded() {
    const reducer = (accumulator, currentValue) => accumulator && currentValue;

    this.productsFullyLoadedSubscription =
        combineLatest([...this.productPhotosLoadings])
            .pipe(map((values: Array<boolean>) => values.reduce(reducer)))
            .subscribe((value: boolean) => {
              this.allMediasLoaded.next(value);
            });
  }

  removeFromWishList() {
    this.wishlistHandler.removeProduct(this.ean);
    this.inWishList.next(false);
  }

  addToWishlist() {
    this.wishlistHandler.addProduct({
      quantity: 1,
      ean: this.ean,
      price: this.price
    });
    this.inWishList.next(true);
  }

  onClickWishlistButton() {
    if (this.inWishList.value) {
      this.removeFromWishList();
    } else {
      this.addToWishlist();
    }
  }

  reachUrlInNewTab(url: string) {
    window.open(url, '_blank');
  }

  screenWidthLowerThanMedium() {
    return this.platform.width() < 960;
  }

  private getMedias(product: CustomProduct) {
    const promises = [];
    product.medias.forEach(media => {
      promises.push(this.productsService.getMediaUrl(product.product_ean, media.id).toPromise()
          .then((url: string) => this.loadMedia(url, media))
          .catch(() => this.allMediasLoaded.next(true)));
    });
    return Promise.all(promises).then(() => {
      let images = this.slides.filter(s => s.media.type === 'picture');
      let videos = this.slides.filter(s => s.media.type === 'video');
      const pdfs = this.slides.filter(s => s.media.type === 'pdf') || [];
      const pricePdfs = this.slides.filter(s => s.media.type === 'pricePdf') || [];
      const logos = this.slides.filter(s => s.media.type === 'logo') || [];

      if (pdfs.length > 0) {
        this.pdfUrl = pdfs[0].src;
      }
      if (pricePdfs.length > 0) {
        this.pricePdfUrl = pricePdfs[0].src;
      }
      if (logos.length > 0) {
        this.logoUrl = logos[0].src;
      }
      images = images.sort((a, b) => {
        const idx1 = product.medias.findIndex(m => m.id === a.media.id);
        const idx2 = product.medias.findIndex(m => m.id === b.media.id);
        return idx1 - idx2;
      });

      videos = videos.sort((a, b) => {
        const idx1 = product.medias.findIndex(m => m.id === a.media.id);
        const idx2 = product.medias.findIndex(m => m.id === b.media.id);
        return idx1 - idx2;
      });
      this.slides = images.concat(videos);
      this.nbMedia = this.slides.length;
      this.nbVideo.next(videos.length);
    });
  }

  private loadMedia(url: string, media: ProductMedia) {
    const fetchStatus = (URL: string) => fetch(URL).then((response) => response.status);

    return fetchStatus(url).then((status) => {
      if (status === 200) {
        this.slides.push({src: url, media});
        if (media.type !== 'picture') {
          this.productPhotosLoadings.push(new BehaviorSubject(false));
        } else {
          this.productPhotosLoadings.push(new BehaviorSubject(true));
        }
      } else {
        throw new Error();
      }
    });
  }

  private isInWishlist() {
    return this.wishlistHandler.getProductByEan(this.ean)
        .subscribe((product) => {
          if (product) {
            this.inWishList.next(true);
          }
        });
  }
}
