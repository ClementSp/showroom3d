import {FeatureItem} from '../first-premium-step.component';

export enum Feature {
    VIDEO,
    PRODUCT_PAGE,
    PREMIUM,
    FAVORITES,
    CONTACT,
}

export const FeaturesStyleMap = new Map<Feature, FeatureItem>([
    [
        Feature.PRODUCT_PAGE,
        {
            title: 'product page',
            mainIcon: 'dvr',
            subIcon1: 'info',
            subIcon2: 'local_offer',
            subtitle: 'more information about the solution',
            color: '#4d71e1',
            subtitleColor: 'black',
            backgroundColor: 'white',
        },
    ],
    [
        Feature.CONTACT,
        {
            title: 'contact',
            mainIcon: 'question_answer',
            subIcon1: 'call',
            subIcon2: 'email',
            subtitle: 'request an estimate or more information',
            color: '#5ac85f',
            subtitleColor: 'black',
            backgroundColor: 'white',
        }
    ],
    [
        Feature.VIDEO,
        {
            title: 'video',
            mainIcon: 'ondemand_video',
            subIcon1: 'fullscreen',
            subIcon2: 'volume_up',
            subtitle: 'watch the presentation video',
            color: '#be0908',
            subtitleColor: 'black',
            backgroundColor: 'white',
        }
    ],
    [
        Feature.PREMIUM,
        {
            title: 'premium',
            mainIcon: 'workspace_premium',
            subIcon1: 'people',
            subIcon2: 'explore',
            subtitle: 'Premium partners',
            color: 'rgb(255 228 32)',
            subtitleColor: 'white',
            backgroundColor: 'rgb(31 31 33)',
        },
    ],
    [
        Feature.FAVORITES,
        {
            title: 'favorites',
            mainIcon: 'favorite_border',
            subIcon1: 'favorite_border',
            subIcon2: 'favorite_border',
            subtitle: 'add to favorites',
            color: '#717171',
            subtitleColor: 'black',
            backgroundColor: 'white',
        },
    ],
]);
