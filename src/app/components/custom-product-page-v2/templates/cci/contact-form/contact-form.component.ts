import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../../services/user.service';
import {IonSlides, ModalController} from '@ionic/angular';
import {CountryPickerComponent} from '../../../../country-picker/country-picker.component';
import {Countries} from '../../../../country-picker/data/Countries';
import {BehaviorSubject} from 'rxjs';
import {EmailService} from '../../../../../services/email.service';

interface ContactFormInput {
  type: 'text' | 'localisation' | 'phone';
  title: string;
  key: string;
  control: FormControl;
}

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent implements OnInit, AfterViewInit {
  @ViewChild('slides') slider: IonSlides;
  @Input() recipient: string;

  userFormGroup: FormGroup;
  mailFormGroup: FormGroup;

  firstnameControl: FormControl;
  lastnameControl: FormControl;
  emailControl: FormControl;
  companyNameControl: FormControl;
  countryControl: FormControl;
  zipcodeControl: FormControl;
  addressControl: FormControl;
  phoneNumberControl: FormControl;

  emailObjectControl: FormControl;
  emailContentControl: FormControl;

  readonly countries = Countries;

  user: any;
  noUserForm = false;
  userControls = new Array<ContactFormInput>();
  currentIndex = 0;

  cciRGPD = 'La CCI Paris Île-de-France collecte ces informations pour traiter votre demande de mise en relation. Vos coordonnées ne peuvent être transmises exclusivement qu\'aux interlocuteurs de votre choix sur ce formulaire.\n' +
      'Vos données sont conservées tant que vous ne demandez pas la suppression. Les informations ne sont pas communiquées à des tiers.\n' +
      'Conformément à la loi « Informatique et Libertés » du 6 janvier 1978, modifiée et au Règlement (UE) 2016-679 sur la protection des données, dans le cadre et les limites de ces textes, vous disposez d\'un droit d\'accès, de modification, de rectification, d\'opposition et de suppression relatif aux données vous concernant.\n' +
      'Ces droits s\'exercent auprès du webmestre ou, en cas de difficulté, auprès du délégué à la protection des données à l\'adresse cpdp@cci-paris-idf.fr. En dernier lieu, vous pouvez déposer une réclamation auprès de la CNIL, 3 Place de Fontenoy - TSA 80715 - 75334 PARIS CEDEX 07.';

  constructor(private formBuilder: FormBuilder,
              private modalController: ModalController,
              private emailService: EmailService,
              private userService: UserService) {
    this.userFormGroup = this.formBuilder.group({});
    this.generateEmailFormGroup();
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.userService.getMe().then((user) => {
      this.user = user;
      this.buildControlList();
      this.buildFormGroup();
    });
  }

  close() {
    return this.modalController.dismiss();
  }

  goToMailForm() {
    const userData = this.userFormGroup.getRawValue();
    this.user.lastname = userData.lastname;
    this.user.localisation.address = this.addressControl.value;
    this.user.phoneNumber = userData.phoneNumber;
    this.user.companyName = userData.companyName;
    return this.userService.updateUser(this.user).subscribe(() => {
      this.noUserForm = true;
      return this.slideNext();
    });
  }

  confirm() {
    const userFirstname = this.user.firstname ?? this.firstnameControl?.value;
    const userLastname = this.user.lastname ?? this.lastnameControl?.value;
    const userEmail = this.user.email ?? this.emailControl?.value;
    const userPhoneNumber = this.user.phoneNumber ?? this.phoneNumberControl?.value;
    const companyName = this.user.companyName ?? this.companyNameControl?.value;

    return this.emailService.showroomContact({
      recipient: this.recipient,
      mailObject: this.emailObjectControl?.value,
      mailContent: this.emailContentControl?.value,
      firstname: userFirstname,
      lastname: userLastname,
      email: userEmail,
      phoneNumber: userPhoneNumber,
      companyName
    }).then(() => this.close());
  }

  slideNext(speed?: number) {
    this.slider.slideTo(1).then(() => {
      this.currentIndex = 1;
    });
  }

  slidePrev(speed?: number) {
    this.slider.slideTo(0).then(() => {
      this.currentIndex = 0;
    });
  }

  async openCountryPicker() {
    const modal = await this.modalController.create({
      component: CountryPickerComponent,
      componentProps: {
        selected: this.countryControl?.value?.alpha2
      }
    });
    modal.onDidDismiss().then((result) => {
      const country = result?.data?.country;

      if (country) {
        this.countryControl.setValue(country);
      }
    });
    return modal.present();
  }

  private buildFormGroup() {
    if (this.user.lastname) {
      this.noUserForm = true;
    }
    this.userControls.forEach((control: ContactFormInput) => {
      this.userFormGroup.addControl(control.key, control.control);
    });
  }

  private buildControlList() {
    this.askForPrimaryInformation();
    this.askForLocalisation();
    this.askForCompanyName();
    this.askForPhoneNumber();
  }

  private askForPrimaryInformation() {
    this.firstnameControl = new FormControl(this.user.firstname, Validators.required);
    this.lastnameControl = new FormControl(this.user.lastname, Validators.required);
    this.emailControl = new FormControl(this.user.email, Validators.required);
    this.userControls.push({
      title: 'firstname',
      key: 'firstname',
      control: this.firstnameControl,
      type: 'text',
    });
    this.userControls.push({
      title: 'lastname',
      key: 'lastname',
      control: this.lastnameControl,
      type: 'text'
    });
    this.userControls.push({
      title: 'email',
      key: 'email',
      control: this.emailControl,
      type: 'text'
    });
  }

  private askForLocalisation() {
    this.countryControl = new FormControl(this.user.localisation.country, Validators.required);
    this.addressControl = new FormControl(this.user.localisation.address, Validators.required);
    this.zipcodeControl = new FormControl(this.user.localisation.zipcode, Validators.required);
    this.userControls.push({
      title: 'country',
      key: 'country',
      control: this.countryControl,
      type: 'localisation'
    });
    this.userControls.push({
      title: 'address',
      key: 'address',
      control: this.addressControl,
      type: 'text'
    });
    this.userControls.push({
      title: 'zipcode',
      key: 'zipcode',
      control: this.zipcodeControl,
      type: 'text'
    });
  }

  private askForCompanyName() {
    this.companyNameControl = new FormControl(this.user.companyName, Validators.required);
    this.userControls.push({
      title: 'company name',
      key: 'companyName',
      control: this.companyNameControl,
      type: 'text'
    });
  }

  private askForPhoneNumber() {
    this.phoneNumberControl = new FormControl(this.user.phoneNumber, Validators.required);
    this.userControls.push({
      title: 'phone number',
      key: 'phoneNumber',
      control: this.phoneNumberControl,
      type: 'text'
    });
  }

  private generateEmailFormGroup() {
    this.emailObjectControl = new FormControl('', Validators.required);
    this.emailContentControl = new FormControl('', Validators.required);
    this.mailFormGroup = this.formBuilder.group({
      object: this.emailObjectControl,
      content: this.emailContentControl
    });
  }
}
