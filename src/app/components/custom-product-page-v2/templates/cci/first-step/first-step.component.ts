import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {userEnv} from '../../../../../../environments/user.env';
import CustomProduct, {ProductMedia} from '../../../../../models/CustomProduct';
import {ProductsService} from '../../../../../services/products.service';
import {TranslateService} from '@ngx-translate/core';
import {BehaviorSubject} from 'rxjs';
import {WishlistHandler} from '../../../../../DataHandlers/Wishlist.handler';
import {ModalController, Platform} from '@ionic/angular';
import {CCIComponent} from '../second-step/cci.component';
import {EngineService} from '../../../../../services/engine.service';
import {Feature, FeaturesStyleMap} from './data/FeaturesStyle';
import {ContactFormComponent} from '../contact-form/contact-form.component';

export interface FeatureItem {
  title: string;
  subtitle: string;
  mainIcon: string;
  subIcon1: string;
  subIcon2: string;
  color: string;
  subtitleColor: string;
  backgroundColor: string;
}

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.scss'],
})
export class FirstStepComponent implements OnInit, AfterViewInit {

  @ViewChildren('features') featureQueryList: QueryList<ElementRef>;
  @ViewChild('mainContainer') mainContainer: ElementRef;

  @Input() data: any;

  price: number;
  ean: string;
  contactEmail: string;

  featuresStyleMap = FeaturesStyleMap;
  features: Array<Feature> = [
  //  Feature.PREMIUM,
    Feature.FAVORITES,
    Feature.CONTACT,
    Feature.PRODUCT_PAGE
  ];
  // tslint:disable-next-line:ban-types
  featuresFnMap = new Map<Feature, Function>([
    [Feature.PREMIUM, () => this.premium()],
    [Feature.FAVORITES, () => this.favorites()],
    [Feature.VIDEO, () => this.video()],
    [Feature.CONTACT, () => this.contact()],
    [Feature.PRODUCT_PAGE, () => this.productPage()]
  ]);

  public hadScroll = false;
  public lines = [];
  public videos = [];
  public allMediasLoaded = new BehaviorSubject<boolean>(false);
  public inWishList = new BehaviorSubject<boolean>(false);

  constructor(
      private modalController: ModalController,
      private platform: Platform,
      private translateService: TranslateService,
      private productsService: ProductsService,
      private wishlistHandler: WishlistHandler,
      private engineService: EngineService
  ) { }

  ngOnInit() {
    this.ean = this.data.ean;
    const productId = this.data.productId;
    if (this.ean) {
      this.isInWishlist();
      this.productsService.getProductByEan(this.ean, userEnv.customer, this.translateService.currentLang)
          .subscribe((product: CustomProduct) => {
            this.price = product.sale_price;
            this.ean = product.product_ean;
            this.contactEmail = product.contact;
            this.getVideo(product).then(() => {
              this.allMediasLoaded.next(true);
            });
          });
    } else {
      if (productId){
        this.productsService.getProductById(productId).then((product: CustomProduct) => {
          this.price = product.sale_price;
          this.ean = product.product_ean;
          this.data.ean = this.ean;
          this.isInWishlist();
          this.contactEmail = product.contact;
          this.getVideo(product).then(() => {
            this.allMediasLoaded.next(true);
          });
        });
      }
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.initLines();
    this.scrollMainContainerToTop();
  }

  /*
  *
  * Must scroll to the top when all features had been initialized
  * This should be avoided with justify-content: end-flex
  * but this break the scrollbar
  *
  */
  ngAfterViewInit(): void {
    this.featureQueryList.changes.subscribe(() => {
      setInterval(() => {
        if (!this.hadScroll) {
          this.scrollMainContainerToTop();
          this.hadScroll = true;
          clearInterval();
        }
      }, 50);
    });
  }

  isFeatureWishlist(feature: Feature) {
    return feature === Feature.FAVORITES;
  }

  onClickWishlistButton() {
    if (this.inWishList.value) {
      this.removeFromWishList();
    } else {
      this.addToWishlist();
    }
  }

  initLines() {
    let arr = [];

    if (this.features && this.features.length > 0) {
      if (this.platform.width() <= 600) {
        arr = new Array(this.features.length);
      } else {
        arr = new Array(Math.floor(this.features.length / 2 + this.features.length % 2));
      }
    }
    this.lines = arr;
  }

  scrollMainContainerToTop() {
    if (this.mainContainer) {
      const element = this.mainContainer.nativeElement;
      const maxScrollTop = element.scrollHeight - element.clientHeight;

      element.scrollTop  = -maxScrollTop;
    }
  }

  getFeatureItem(feature: Feature) {
    return this.featuresStyleMap.get(feature);
  }

  getFeaturesByLine(index: number) {
    const arr = [];
    let start = index * 2;
    const max = start + 2;

    if (this.platform.width() < 600) {
      arr.push(this.features[index]);
    } else {
      for (; start < max && start < this.features.length; start++) {
        arr.push(this.features[start]);
      }
    }
    return arr;
  }

  getFlexSize(nbElementOnLine: number): string {
    return (Math.floor(100 / nbElementOnLine)).toString();
  }

  close() {
    return this.modalController.dismiss();
  }

  premium() {
    alert('Premium');
  }

  video() {
    if (this.videos && this.videos.length > 0) {
      return this.engineService.displayVideoModal(this.videos[0].src);
    }
  }

  async productPage() {
    const modal = await this.modalController.create({
      component: CCIComponent,
      cssClass: 'two-steps-template-product backdrop-05',
      showBackdrop: true,
      componentProps: {
        data: this.data
      }
    });
    return await modal.present();
  }

  addToWishlist() {
    this.wishlistHandler.addProduct({
      quantity: 1,
      ean: this.ean,
      price: this.price
    });
    this.inWishList.next(true);
  }

  removeFromWishList() {
    this.wishlistHandler.removeProduct(this.ean);
    this.inWishList.next(false);
  }

  favorites() {
    if (this.inWishList.value) {
      this.removeFromWishList();
    } else {
      this.addToWishlist();
    }
  }

  async contact() {
    const modal = await this.modalController.create({
      component: ContactFormComponent,
      cssClass: 'backdrop-05',
      componentProps: {
        recipient: this.contactEmail
      }
    });
    return modal.present();
  }

  private getVideo(product: CustomProduct) {
    const promises = [];
    const videosMedia = product?.medias?.filter(m => m.type === 'video') ?? [];

    if (videosMedia.length > 0) {
      videosMedia.forEach(video => {
        promises.push(this.productsService.getMediaUrl(product.product_ean, video.id).toPromise()
            .then((url: string) => this.loadMedia(url, video))
            .catch(() => this.allMediasLoaded.next(true)));
      });
      return Promise.all(promises).then(() => {
        if (this.videos.length > 0) {
          this.features.push(Feature.VIDEO);
        }
        this.initLines();
        this.features.sort((a, b) => a - b).reverse();
      });
    } else {
      this.allMediasLoaded.next(true);
    }

  }

  private loadMedia(url: string, media: ProductMedia) {
    const fetchStatus = (URL: string) => fetch(URL).then((response) => response.status);

    return fetchStatus(url).then((status) => {
      if (status === 200 && media.type === 'video') {
        this.videos.push({src: url, media});
      } else {
        throw new Error();
      }
    });
  }

  private isInWishlist() {
    return this.wishlistHandler.getProductByEan(this.ean)
        .subscribe((product) => {
          if (product) {
            this.inWishList.next(true);
          }
        });
  }
}
