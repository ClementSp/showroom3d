import {FeatureItem} from '../first-step.component';
import {EventEmitter} from '@angular/core';

export interface IFeatureComponent {
    //  inputs
    item: FeatureItem;

    // outputs
    itemClicked: EventEmitter<void>;
    loaded: EventEmitter<void>;
}
