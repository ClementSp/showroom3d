import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FeatureItem} from '../../first-step.component';
import {IFeatureComponent} from '../IFeatureComponent';

@Component({
  selector: 'app-cci-feature',
  templateUrl: './cci-feature.component.html',
  styleUrls: ['./cci-feature.component.scss'],
})
export class CciFeatureComponent implements IFeatureComponent, OnInit, AfterViewInit {
  @Input() item: FeatureItem;

  @Output() loaded = new EventEmitter<void>();
  @Output() itemClicked = new EventEmitter<void>();

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.loaded.emit();
  }
}

