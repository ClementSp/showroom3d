import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FeatureItem} from '../../first-step.component';
import {IFeatureComponent} from '../IFeatureComponent';
import {IWishListProduct, WishlistHandler} from '../../../../../../../DataHandlers/Wishlist.handler';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-cci-favorite-feature',
  templateUrl: './cci-favorite-feature.component.html',
  styleUrls: ['./cci-favorite-feature.component.scss'],
})
export class CciFavoriteFeatureComponent implements IFeatureComponent, OnInit, AfterViewInit, OnDestroy {
  @Input() ean: string;

  @Output() loaded = new EventEmitter<void>();
  @Output() itemClicked = new EventEmitter<void>();

  notInWishlistSubtitle = 'add to favorites';
  inWishlistSubtitle = 'remove from favorites';

  notInWishlistIcon = 'favorite_border';
  inWishlistIcon = 'favorite';

  initSubscription: Subscription;
  item: FeatureItem = {
    title: 'favorites',
    subtitle: this.notInWishlistSubtitle,
    mainIcon: this.notInWishlistIcon,
    subIcon1: this.notInWishlistIcon,
    subIcon2: this.notInWishlistIcon,
    color: '#717171',
    subtitleColor: 'black',
    backgroundColor: 'white'
  };

  constructor(private wishlistHandler: WishlistHandler) {
  }

  ngOnInit() {
    this.setIcons();
  }

  ngAfterViewInit(): void {
    this.loaded.emit();
  }

  ngOnDestroy(): void {
    if (this.initSubscription) {
      this.initSubscription.unsubscribe();
    }
  }

  private setIcons() {
    this.initSubscription = this.wishlistHandler.products$.subscribe((products: Array<IWishListProduct>) => {
      const inWishlist = products?.find(item => item.ean === this.ean);

      if (inWishlist) {
        this.setAsInWishlist();
      } else {
        this.setAsNotInWishlist();
      }
    });
  }

  private setAsInWishlist() {
    this.item.mainIcon = this.inWishlistIcon;
    this.item.subIcon1 = this.inWishlistIcon;
    this.item.subIcon2 = this.inWishlistIcon;
    this.item.subtitle = this.inWishlistSubtitle;
  }

  private setAsNotInWishlist() {
    this.item.mainIcon = this.notInWishlistIcon;
    this.item.subIcon1 = this.notInWishlistIcon;
    this.item.subIcon2 = this.notInWishlistIcon;
    this.item.subtitle = this.notInWishlistSubtitle;
  }
}

