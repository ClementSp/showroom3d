import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {IMedia} from '../../../../custom-product-page-v2.component';
import {miniaturesSlidesOpts} from './miniaturesSlidesOpts';
import {IonSlides} from '@ionic/angular';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-miniatures',
  templateUrl: './miniatures.component.html',
  styleUrls: ['./miniatures.component.scss'],
})
export class MiniaturesComponent implements OnInit, AfterViewInit {
  @ViewChild('slider') slider: IonSlides;

  @Input() slides = new Array<IMedia>();
  @Input() currentIndex = new BehaviorSubject<number>(0);

  readonly miniaturesSlidesOpts = miniaturesSlidesOpts;

  constructor() { }

  ngOnInit() {}

  ngAfterViewInit(): void {
    this.onIndexUpdated();
    this.onSlideChanged();
  }

  slideNext() {
    return this.slider.slideNext()
        .then(() => this.slider.getActiveIndex())
        .then((activeIndex: number) => this.currentIndex.next(activeIndex));
  }

  slidePrev() {
    return this.slider.slidePrev()
        .then(() => this.slider.getActiveIndex())
        .then((activeIndex: number) => this.currentIndex.next(activeIndex));
  }

  slideTo(index: number) {
    return this.slider.slideTo(index);
  }

  private onIndexUpdated() {
    return this.currentIndex.subscribe((value: number) => {
      return this.slider.slideTo(value);
    });
  }

  private onSlideChanged() {
    this.slider.ionSlideDidChange.subscribe(() => {
      this.slider.getActiveIndex()
          .then((activeIndex: number) => this.currentIndex.next(activeIndex));
    });
  }
}
