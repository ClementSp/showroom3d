export const miniaturesSlidesOpts = {
    slidesPerView: 4,
    spaceBetween: 10,
    centeredSlides: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
};

