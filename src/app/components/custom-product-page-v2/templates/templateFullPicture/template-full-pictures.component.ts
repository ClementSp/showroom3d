import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IonSlides, ModalController, Platform} from '@ionic/angular';
import {BehaviorSubject, combineLatest, Subscription} from 'rxjs';
import {CustomerService} from '../../../../services/customer.service';
import {ProductsService} from '../../../../services/products.service';
import {SafePipe} from '../../../../pipes/safe.pipe';
import CustomProduct, {ProductMedia} from '../../../../models/CustomProduct';
import {map} from 'rxjs/operators';
import {slidesOptions} from '../../sliderOpts';
import {UserService} from '../../../../services/user.service';
import {userEnv} from '../../../../../environments/user.env';
import {customerEnv} from '../../../../../environments/customer.env';
import {TranslateService} from '@ngx-translate/core';
import {showroomEnv} from '../../../../../environments/showroom.env';

interface IMedia {
    src: string;
    media: ProductMedia;
}

@Component({
    selector: 'app-template-full-pictures',
    templateUrl: './template-full-pictures.component.html',
    styleUrls: ['./template-full-pictures.component.scss'],
})
export class TemplateFullPicturesComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() data: any;

    @ViewChild('prevButton') prevButton: ElementRef;
    @ViewChild('nextButton') nextButton: ElementRef;
    @ViewChild('slider') slider: IonSlides;

    currentMedia = new BehaviorSubject<number>(0);

    slides = new Array<IMedia>();
    slidesOptions = slidesOptions;
    productsFullyLoadedSubscription: Subscription;
    allImagesLoaded = new BehaviorSubject<boolean>(false);
    productPhotosLoadings = new Array<BehaviorSubject<boolean>>();
    backgroundImage = '';

    constructor(private platform: Platform,
                public customerService: CustomerService,
                private modalController: ModalController,
                private translateService: TranslateService,
                private userService: UserService,
                private productsService: ProductsService,
                public safePipe: SafePipe) {
        if (this.platform.is('mobile')) {
            this.slidesOptions.simulateTouch = true;
        }
        if (this.isMd()) {
            this.slidesOptions.pagination = {
                el: '.swiper-pagination',
                type: 'bullets',
            };
        }
    }

    ngOnInit(): void {
        this.productsService.getProductByEan(this.data.ean, userEnv.customer, this.translateService.currentLang)
            .subscribe((product: CustomProduct) => {
                this.getMedias(product).then(() => {
                    this.initProductFullyLoaded();
                }).catch(() => this.allImagesLoaded.next(true));
            });
    }

    get customerEnv() {
        return customerEnv;
    }

    get showroomEnv() {
        return showroomEnv;
    }

    ngOnDestroy(): void {
        if (this.productsFullyLoadedSubscription) {
            this.productsFullyLoadedSubscription.unsubscribe();
        }
    }

    initProductFullyLoaded() {
        const reducer = (accumulator, currentValue) => accumulator && currentValue;

        this.productsFullyLoadedSubscription =
            combineLatest([...this.productPhotosLoadings])
                .pipe(map((values: Array<boolean>) => values.reduce(reducer)))
                .subscribe((value: boolean) => {
                    this.allImagesLoaded.next(value);
                });
    }

    ngAfterViewInit(): void {
        this.initSliderEvents();
    }

    initSliderEvents(): void {
        if (this.slides.length > 0) {
            this.slider.ionSlideDidChange.subscribe(() => {
                this.slider.getActiveIndex().then((rest) => {
                    this.currentMedia.next(rest);
                });
            });
        }
    }

    isMd() {
        return this.platform.width() < 768;
    }

    isMobile() {
        return this.platform.is('mobile');
    }

    slideTo(rank: number) {
        this.slider.slideTo(rank).then(() => {});
        this.slider.getActiveIndex().then((rest) => {
            this.currentMedia.next(rest);
        });
    }

    nextMedia() {
        this.next();
    }

    prevMedia() {
        this.prev();
    }

    mediaLoaded(index: number) {
        if (index < this.productPhotosLoadings.length && this.productPhotosLoadings[index]) {
            this.productPhotosLoadings[index].next(true);
        }
    }

    close() {
        this.modalController.dismiss();
    }

    next() {
        this.slider.slideNext();
        this.slider.getActiveIndex().then((rest) => {
            this.currentMedia.next(rest);
        });
    }

    prev() {
        this.slider.slidePrev();
        this.slider.getActiveIndex().then((rest) => {
            this.currentMedia.next(rest);
        });
    }

    private getMedias(product: CustomProduct) {
        const promises = [];

        product.medias.forEach(media => {
            promises.push(this.productsService.getMediaUrl(product.product_ean, media.id).toPromise()
                .then((url: string) => {
                    return this.loadMedia(url, media);
                }).catch(() => this.allImagesLoaded.next(true)));
        });
        return Promise.all(promises).then(() => {
            let images = this.slides.filter(s => s.media.type === 'picture');

            images = images.sort((a, b) => {
                const idx1 = product.medias.findIndex(m => m.id === a.media.id);
                const idx2 = product.medias.findIndex(m => m.id === b.media.id);

                return idx1 - idx2;
            });

            this.slides = images;
        });
    }

    private loadMedia(url: string, media: ProductMedia) {
        const fetchStatus = (URL: string) => fetch(URL).then((response) => response.status);

        return fetchStatus(this.addVersionToUrl(url)).then((status) => {
            if (status === 200) {
                this.slides.push({src: url, media});
                this.productPhotosLoadings.push(new BehaviorSubject(true));
            } else {
                throw new Error();
            }
        });
    }

    private addVersionToUrl(url: string): string {
        const urlObj = new URL(url);
        return urlObj.href;
    }

    public openUrl(url: string) {
        if (!url.startsWith('http')) {
            url = 'http://' + url;
        }
        window.open(url, '_blank');
    }
}
