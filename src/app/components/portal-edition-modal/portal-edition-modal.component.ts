import {Component, ComponentFactoryResolver, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IonSlides, ModalController} from '@ionic/angular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ComponentHolderDirective} from '../../directives/component-holder.directive';
import {Portal, PortalType} from '../../models/PortalMarker';
import {ShowroomPortalComponent} from './showroom-portal/showroom-portal.component';
import {SweepPortalComponent} from './sweep-portal/sweep-portal.component';
import {PortalAction} from '../../common/PortalAction';
import {PoiAction} from '../../common/PoiAction';

@Component({
  selector: 'app-portal-edition-modal',
  templateUrl: './portal-edition-modal.component.html',
  styleUrls: ['./portal-edition-modal.component.scss'],
})
export class PortalEditionModalComponent implements OnInit, OnDestroy {
  @ViewChild(ComponentHolderDirective, {static: false}) dynComp: ComponentHolderDirective;
  @ViewChild('slider', {static: false}) slider: IonSlides;

  @Input() portal: Portal;
  @Input() isUpdating: boolean;

  public isReady = false;
  public formGroup: FormGroup = null;
  public currentHandler = null;
  public currentIndex = 0;
  public currentComponentRef = null;
  public isChildValid = true;
  public savedData = {};
  public portalHandlers = [
    {type: PortalType.SWEEP_LINK, title: 'Sweep', component: SweepPortalComponent},
    {type: PortalType.SHOWROOM_LINK, title: 'Showroom', component: ShowroomPortalComponent},
  ];
  public slidesOptions = {
    observer: true,
    simulateTouch: false,
    allowTouchMove: false,
  };
  private sub = null;

  constructor(private formBuilder: FormBuilder,
              private modalCtrl: ModalController,
              private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.initPortalForm();
    this.setCurrentHandlerFromType(this.portal.type);
    this.isReady = true;
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  initPortalForm() {
    const idControl = new FormControl(this.portal.id);
    const tipControl = new FormControl(this.portal.tip);
    const typeControl = new FormControl(this.portal.type, Validators.required);

    this.formGroup = this.formBuilder.group({
      id: idControl,
      tip: tipControl,
      type: typeControl,
    });
  }

  next() {
    if (this.slider && this.formGroup.valid && this.initSliderNextComponent()) {
      this.currentComponentRef.instance.portal = this.portal;
      this.slider.slideNext().then(() => {
        this.currentIndex += 1;
      });
    }
  }

  back() {
    if (this.slider) {
      this.slider.slidePrev().then(() => {
        this.currentIndex -= 1;
      });
    }
  }

  async cancel() {
    await this.modalCtrl.dismiss(null);
  }

  movePortal() {
    return this.modalCtrl.dismiss({action: PortalAction.MOVE, portal: this.portal});
  }

  removePortal() {
    return this.modalCtrl.dismiss({action: PortalAction.REMOVE, portal: this.portal});
  }

  async confirm() {
    if (this.formGroup.valid) {
      const portal: any = this.portal;

      if (portal.type === PortalType.SWEEP_LINK) {
        if (portal.link) {
          delete portal.link;
        }
      } else if (portal.type === PortalType.SHOWROOM_LINK) {
        if (portal.sweepId) {
          delete portal.sweepId;
        }
      }
      return this.modalCtrl.dismiss({action: PoiAction.EDIT, portal});
    }
  }


  setCurrentHandlerFromType(type: PortalType) {
    const handler = this.portalHandlers.find(h => h.type === type);

    if (!handler) {
      this.currentHandler = undefined;
    } else {
      this.currentHandler = handler;
      this.portal.type = type;
      if (this.dynComp) {
        this.initSliderNextComponent();
      }
    }
  }

  private initSliderNextComponent() {
    const component = this.currentHandler?.component ?? undefined;

    if (!component) {
      console.warn('[WARNING]: No component matches type:', this.currentHandler?.type);
      return false;
    }

    if (this.sub) {
      this.sub.unsubscribe();
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const viewContainerRef = this.dynComp.viewContainerRef;
    viewContainerRef.clear();

    this.currentComponentRef = viewContainerRef.createComponent<any>(componentFactory);
    const element: HTMLElement = this.currentComponentRef.location.nativeElement as HTMLElement;

    element.style.width = '100%';

    this.currentComponentRef.instance.portal = this.portal;
    this.sub = this.currentComponentRef.instance.portalUpdate.subscribe(portal => {
      this.portal = portal;
      this.currentComponentRef.instance.portal = portal;
    });
    return true;
  }
}
