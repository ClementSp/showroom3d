import {Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Portal, ShowroomPortal} from '../../../models/PortalMarker';
import {ShowroomsService} from '../../../services/showrooms.service';
import {MenuLink} from '../../../models/showroom.model';
import {showroomEnv} from '../../../../environments/showroom.env';

@Component({
  selector: 'app-sweep-selector',
  templateUrl: './sweep-selector.component.html',
  styleUrls: ['./sweep-selector.component.scss'],
})
export class SweepSelectorComponent implements OnChanges, OnDestroy, OnInit {
  @Input() showroomId: string;
  @Input() portal: Portal | ShowroomPortal;
  @Output() sweepSelected = new EventEmitter();

  public showroomSweeps = [];
  public isReady = false;
  public selected = new BehaviorSubject(null);

  private sub = null;

  constructor(private showroomService: ShowroomsService) {}

  ngOnInit() {
    this.sub = this.selected.subscribe((value) => {
      if (value) {
        this.changeSweep(value);
      }
    });

    return this.getShowroomSweeps()
        .then(() => {
          this.initSweepId();
        });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.showroomId && !changes.showroomId?.firstChange) {
      return this.getShowroomSweeps();
    }
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  public initSweepId() {
    // @ts-ignore
    const res = this.showroomSweeps.find(m => m.id === (this.portal.link.sweepId || this.portal.link.target));

    if (res?.id) {
      return this.selected.next(res.id);
    }

    if (this.showroomSweeps[0]?.id) {
      return this.selected.next(this.showroomSweeps[0].id);
    }

    return this.selected.next(null);
  }

  public changeSweep(sweepId: string) {
    const sweep = this.showroomSweeps.find(ss => ss.id === sweepId);

    if (sweep) {
      if (sweepId === this.selected.value) {
        return;
      }
      this.sweepSelected.emit(sweep);
    }
  }

  private async getShowroomSweeps() {
    this.isReady = false;
    this.showroomSweeps = [];
    const showroom = showroomEnv.showroom;
    if (!showroom?.menu) {
      return;
    }
    this.populateSweepsArray(showroom.menu);
    // if (showroom.settings.entrance?.sweepId) {
    //   this.showroomSweeps.unshift({...showroom.settings.entrance, id: -1, title: 'Entrance'});
    // }
    this.isReady = true;
  }

  private populateSweepsArray(menu: Array<MenuLink>) {
    menu.forEach((m) => {
      this.showroomSweeps.push({...m, depth: (m.level2) ? 1 : 0});

    });
  }

  public generateArray(length: number) {
    return new Array(length);
  }

}
