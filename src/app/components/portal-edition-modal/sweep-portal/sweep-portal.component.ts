import {Component, Input, EventEmitter, Output, OnInit} from '@angular/core';
import {SweepPortal} from '../../../models/PortalMarker';
import {userEnv} from '../../../../environments/user.env';
import {MenuLink} from '../../../models/showroom.model';

@Component({
  selector: 'app-sweep-portal',
  templateUrl: './sweep-portal.component.html',
  styleUrls: ['./sweep-portal.component.scss'],
})
export class SweepPortalComponent {
  @Input() portal: SweepPortal;
  @Output() portalUpdate = new EventEmitter();

  public menuItems = [];
  public isReady = false;
  public currentShowroom = userEnv.showroomId;

  constructor() {}

  public async changeSweep(menuLink: MenuLink) {
    const portal: SweepPortal = JSON.parse(JSON.stringify(this.portal));
    portal.sweepId = menuLink.id;

    return this.portalUpdate.emit(portal);
  }
}
