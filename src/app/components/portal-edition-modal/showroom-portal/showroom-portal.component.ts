import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ShowroomPortal} from '../../../models/PortalMarker';
import {userEnv} from '../../../../environments/user.env';
import {TenantService} from '../../../services/tenant.service';
import {MenuLink, Showroom} from '../../../models/showroom.model';

@Component({
  selector: 'app-showroom-portal',
  templateUrl: './showroom-portal.component.html',
  styleUrls: ['./showroom-portal.component.scss'],
})
export class ShowroomPortalComponent implements OnInit {
  @Input() portal: ShowroomPortal;
  @Output() portalUpdate = new EventEmitter();

  public showrooms = [];
  public showroomSweeps = [];
  public isReady = false;
  public selectedShowroomId = null;

  constructor(private tenantService: TenantService) {
    const current = userEnv.showroomId;
    this.tenantService.getTenantShowroomsList()
        .then((showrooms) => {
          this.showrooms = showrooms.filter(s => !s.isMatterport);
        });
  }

  public async changeShowroom(showroomId: string) {
    if (showroomId !== this.selectedShowroomId) {
      this.portal.link.sweepId = null;
    }
    this.selectedShowroomId = showroomId;
    const portal: ShowroomPortal = JSON.parse(JSON.stringify(this.portal));

    portal.link.target = this.selectedShowroomId;
    return this.portalUpdate.emit(portal);
  }

  public async changeSweep(sweep: MenuLink) {
    const portal = JSON.parse(JSON.stringify(this.portal));

    portal.link.target = this.selectedShowroomId;
    portal.link.sweepId = sweep.id ?? null;
    return this.portalUpdate.emit(portal);
  }

  public getSweepId() {
    const res = this.showroomSweeps.find(m => m.sweepId === this.portal.link.sweepId);

    return res?.id ?? null;
  }

  ngOnInit() {
    if (this.portal?.link?.target) {
      this.selectedShowroomId = this.portal.link.target;
      this.isReady = true;
    } else {
      this.portal.link = {target: null, sweepId: null};
      this.isReady = true;
    }
  }

}
