import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {Showroom} from '../../models/showroom.model';
import {uuid4} from '@capacitor/core/dist/esm/util';
import {ShowroomsService} from '../../services/showrooms.service';
import {userEnv} from '../../../environments/user.env';
import {showroomEnv} from '../../../environments/showroom.env';
import {coerceStringArray} from '@angular/cdk/coercion';
import {User} from '../../models/user.model';
import { ToastController} from '@ionic/angular';
@Component({
  selector: 'app-share-showroom',
  templateUrl: './share-showroom.component.html',
  styleUrls: ['./share-showroom.component.scss'],
})
export class ShareShowroomComponent implements OnInit {
  @Input() showroom: Showroom;
  @Input() editMode: boolean;
  clientName: string;
  showroomName: string;
  clientEmail: string;
  categoryName: string;
  showroomUrl: string;

  constructor(private popoverController: PopoverController,
              private showroomService: ShowroomsService,
              private toastController: ToastController
  ) {
  }

  ngOnInit() {
    if (this.editMode){
      // clientName = ;
      this.showroomName = this.showroom.name;
    }else{
      this.showroomService.me().subscribe((user: User ) => {
        this.categoryName = user.firstname + '_' + user.lastname;
      });
    }
  }


  async DismissPopover() {
    await this.popoverController.dismiss();
  }

  saveShowroom(){
    this.showroomService.createShowroomCopy(this.showroom.id, this.showroomName, true, false).subscribe(async showRoomOnEdit => {
      userEnv.showroomId = showRoomOnEdit.id;
      showroomEnv.showroom = showRoomOnEdit;
      this.showroomService.saveShowroomCategory({ name: this.categoryName, idsToAdd: userEnv.showroomId}).subscribe(async res => {
        await this.popoverController.dismiss(showRoomOnEdit);
      });
    });
  }
  async updateShowroom() {
    this.showroom.name = this.showroomName;
    this.showroomService.updateWithTenant(this.showroom).subscribe();
    await this.popoverController.dismiss(this.showroom);
  }

  async copyLink() {
    const urlBase = location.protocol + '//' + location.host
    this.showroomUrl = urlBase + '/?name=' + userEnv.showroomId + '&tenant=' + userEnv.tenant;
    this.cpToClipboard(this.showroomUrl);
    await this.presentToast('The link has been copied to your clipboard !');

  }
  cpToClipboard(value: string) {
    const selBox = window.document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = value;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    await toast.present();
  }
}
