import {AfterViewInit, Component, Input, OnDestroy, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {IonButton, ModalController} from '@ionic/angular';
import {animate, style, trigger, transition} from '@angular/animations';
import {Showroom} from '../../models/showroom.model';
import {TranslateService} from '@ngx-translate/core';
import {customerEnv} from '../../../environments/customer.env';
import {UserService} from '../../services/user.service';

@Component({
    selector: 'app-introduction-pop-up-viewer',
    templateUrl: './introduction-pop-up.component.html',
    styleUrls: ['./introduction-pop-up.component.scss'],
    animations: [
        trigger('fade', [
            transition(':enter', [
                style({ opacity: 0 }),
                animate('150ms', style({ opacity: 1 })),
            ]),
            transition(':leave', [animate('150ms', style({ opacity: 0 }))]),
        ])]
})

export class IntroductionPopUpComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('nextBtn', {static: false}) nextBtn: IonButton;
    @Output() enterClicked = new EventEmitter();
    @Input() data: any;
    @Input() showroom: Showroom;
    title;
    customClusterImg;
    gotoImg;
    logoImg;
    homePageImg;
    step = 1;
    showroomName;
    currentLanguage;
    languagesList;
    language = '';

    constructor(private modalController: ModalController,
                private translateService: TranslateService,
                private userService: UserService) {
    }

    ngAfterViewInit(): void {
    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
        this.currentLanguage = this.translateService.currentLang;
        this.languagesList = this.userService.handleLng;
        if (this.showroom) {

            const customCluster = (this.showroom.settings.sprites.productPoi)
                ? this.showroom.settings.sprites.productPoi
                : '/assets/ui/info2.png';

            const gotoCluster = (this.showroom.settings.sprites.sweep)
                ? this.showroom.settings.sprites.sweep
                : '/assets/ui/goto.png';

            this.title = customerEnv.name;
            this.customClusterImg = customCluster;
            this.gotoImg = gotoCluster;
            this.logoImg = this.showroom.settings.sprites.logo;
            if (this.showroom.settings.components.welcomePage.backgroundUrl.length === 0) {
                this.homePageImg = undefined;
            } else {
                this.homePageImg = this.showroom.settings.components.welcomePage.backgroundUrl;
            }
            this.showroomName = this.showroom.name;
        }
    }

    nextStep() {
        if (this.step === 1) {
            this.step ++;
        } else {
            this.enter();
        }
    }

    goToFirstStep() {
        this.step = 1;
    }

    goToSecondStep() {
        this.step = 2;
    }

    close() {
        this.modalController.dismiss();
    }

    changeLanguage(value: any) {
        this.language = value.substring(0, 2).toLowerCase();
        this.translateService.use(this.language);
    }

    public async enter() {
        this.enterClicked.emit();
    }
}
