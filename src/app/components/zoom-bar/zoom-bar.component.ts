import {Component, ElementRef, NgZone, OnDestroy, ViewChild} from '@angular/core';
import {EngineService} from "../../services/engine.service";

@Component({
  selector: 'app-zoom-bar',
  templateUrl: './zoom-bar.component.html',
  styleUrls: ['./zoom-bar.component.scss'],
})
export class ZoomBarComponent implements OnDestroy {
  @ViewChild('zoomBar', {static: true}) zoomBar: ElementRef;

  readonly sub = null;
  public zoomLevel = 1;
  public maxValue = 3;
  public minValue = 0.7;

  private zoomTimeout = null;
  private zoomTimeoutMs = 1000;
  private zoomFactor = 0.3;

  constructor(private zone: NgZone,
              private engineService: EngineService) {
    this.zone.run(() => {
      /*if (this.zoomLevel.toFixed(3) !== this.getZoomLevel()?.toFixed(3)) {
        this.zoomLevel = this.getZoomLevel();
        return this.applyHoverEffect();
      }*/
    });
  }

  zoom(value: any) {
    this.engineService.zoom(value);
  }

  getZoomLevel() {
    return this.engineService.getZoomLevel();
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.cancel();
    }
  }

  public async setZoomLevel(value: number) {
    return this.zoom(value);
  }

  public async increaseZoomLevel() {
    if (this.zoomLevel + this.zoomFactor >= this.maxValue) {
      return this.setZoomLevel(this.maxValue);
    }
    return this.setZoomLevel(this.zoomLevel + 0.3);
  }

  public async decreaseZoomLevel() {
    if (this.zoomLevel - this.zoomFactor <= this.minValue) {
      return this.setZoomLevel(this.minValue);
    }
    return this.setZoomLevel(this.zoomLevel - 0.3);
  }

  private async applyHoverEffect() {
    const nativeElement = this.zoomBar?.nativeElement;

    if (nativeElement) {
      if (!nativeElement.classList.contains('active')) {
        nativeElement.classList.add('active');
      }

      if (this.zoomTimeout) {
        clearTimeout(this.zoomTimeout);
      }
      this.zoomTimeout = setTimeout(() => {
        nativeElement.classList.remove('active');
      }, this.zoomTimeoutMs);
    }
  }
}
