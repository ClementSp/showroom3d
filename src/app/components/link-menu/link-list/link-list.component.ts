import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {ExtendedMenuLink} from '../../../models/Menu.model';
import {userEnv} from '../../../../environments/user.env';
import {EditorService} from '../../../services/editor.service';
import {MenuJumpLink} from '../../../models/showroom.model';
import {showroomEnv} from '../../../../environments/showroom.env';

@Component({
  selector: 'app-link-list',
  templateUrl: './link-list.component.html',
  styleUrls: ['./link-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinkListComponent implements OnInit {

  @Input() itemList: Array<MenuJumpLink>;
  @Input() itemListPicture: Map<string, any>;
  @Output() addItem = new EventEmitter<void>();
  @Output() deleteItem = new EventEmitter<string>();
  @Output() updateCamera = new EventEmitter<any>();
  @Output() itemClicked = new EventEmitter<any>();
  @Output() editItem = new EventEmitter<void>();
  @Output() toggleItem = new EventEmitter<ExtendedMenuLink>();
  @Output() updateListEvent = new EventEmitter<any>();
  @Output() logOutEvent = new EventEmitter<any>();

  constructor(private editorService: EditorService) { }

  ngOnInit() {
  }

  get userEnv() {
    return userEnv;
  }

  isEditorActivated() {
    return this.editorService.isEditorActivated;
  }

  onItemClicked(item: any) {
    this.itemClicked.emit(item);
  }

  onDeleteItem(item: any, $event: Event) {
    $event.stopPropagation();
    this.deleteItem.emit(item);
  }

  openMenuItemCreationModal($event: Event, item) {
    $event.stopPropagation();
    this.editItem.emit(item);
  }

  reorderItems(ev) {
    const itemMove = this.itemList.splice(ev.detail.from, 1)[0];
    this.itemList.splice(ev.detail.to, 0, itemMove);
    this.updateListEvent.emit(this.itemList);
    ev.detail.complete();
  }

  logOut() {
    this.logOutEvent.emit();
  }

  isPrivateShowroom() {
    return showroomEnv.private;
  }
}
