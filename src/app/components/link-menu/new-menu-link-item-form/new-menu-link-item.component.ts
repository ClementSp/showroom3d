import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PortalType, ShowroomPortal} from '../../../models/PortalMarker';
import {TenantService} from '../../../services/tenant.service';
import {MenuJumpLink, MenuLink} from '../../../models/showroom.model';
import {ModalController} from '@ionic/angular';
import {uuid4} from '@capacitor/core/dist/esm/util';
import {Euler, Vector3} from './../../../models/Utils';

@Component({
  selector: 'app-new-menu-link-item',
  templateUrl: './new-menu-link-item.component.html',
  styleUrls: ['./new-menu-link-item.component.scss'],
})
export class NewMenuLinkItemComponent implements OnInit {
  @Input() menuJumpLink: MenuJumpLink;
  @Output() portalUpdate = new EventEmitter();

  public showrooms = [];
  public showroomSweeps = [];
  public isReady = false;
  public selectedShowroomId = null;

  constructor(private tenantService: TenantService,
              private modalController: ModalController) {
    this.tenantService.getTenantShowroomsList()
        .then((showrooms) => {
          this.showrooms = showrooms.filter(showroom => !showroom.isMatterport);
        });
  }

  public async changeShowroom(showroomId: string) {
    if (showroomId !== this.selectedShowroomId) {
      this.menuJumpLink.showroomPortal.link.sweepId = null;
    }
    this.selectedShowroomId = showroomId;
    this.menuJumpLink.showroomPortal.link.target = this.selectedShowroomId;
  }

  public async changeSweep(sweep: MenuJumpLink) {
    this.menuJumpLink.showroomPortal.link.target = this.selectedShowroomId;
    this.menuJumpLink.showroomPortal.link.sweepId = sweep.id ?? null;
  }

  public save() {
    const menuJumpLink: MenuJumpLink = JSON.parse(JSON.stringify(this.menuJumpLink));
    this.modalController.dismiss(menuJumpLink);
  }

  public getSweepId() {
    const res = this.showroomSweeps.find(m => m.sweepId === this.menuJumpLink.showroomPortal.link.sweepId);

    return res?.id ?? null;
  }

  ngOnInit() {
    if (!this.menuJumpLink) {
      this.menuJumpLink = {
        name: '',
        id: uuid4(),
        showroomPortal: {
          link: {
            target: '',
            sweepId: '',
          },
          type: PortalType.SHOWROOM_LINK,
          position: {x: 0, y: 0, z: 0},
          tip: '',
          nodeId: '',
          rotation: {_x: 0, _y: 0, _z: 0},
          scale: {x: 1, y: 1, z: 1},
          id: uuid4(),
        }
      };
    }

    if (this.menuJumpLink?.showroomPortal?.link?.target) {
      this.selectedShowroomId = this.menuJumpLink.showroomPortal.link.target;
      this.isReady = true;
    } else {
      this.menuJumpLink.showroomPortal.link = {target: null, sweepId: null};
      this.isReady = true;
    }
  }

}
