import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import {ModalController} from '@ionic/angular';
import { Platform } from '@ionic/angular';
import {CurrentNodeHandler} from '../../DataHandlers/CurrentNode.handler';
import {ConfirmationModalComponent} from '../confirmation-modal/confirmation-modal.component';
import {EditorService} from '../../services/editor.service';
import {UserService} from '../../services/user.service';
import {userEnv} from '../../../environments/user.env';
import {MenuJumpLink, Showroom} from '../../models/showroom.model';
import {EngineService} from '../../services/engine.service';
import {NewMenuLinkItemComponent} from './new-menu-link-item-form/new-menu-link-item.component';
import {ShowroomsService} from '../../services/showrooms.service';
import {RouterService} from '../../services/router.service';

@Component({
  selector: 'app-link-menu',
  templateUrl: './link-menu.component.html',
  styleUrls: ['./link-menu.component.scss'],
})
export class LinkMenuComponent implements OnInit, OnChanges {

  public menu: boolean;
  public itemListPicture = new Map<string, any>();
  @Input() itemList: Array<MenuJumpLink>;
  @Input() firebase: any;
  @Input() showroom: Showroom;
  @Input() toggleMenu: EventEmitter<any>;

  swipeGesture = false;
  showroomImageLoaded = false;

  constructor(private modalController: ModalController,
              public currentNode: CurrentNodeHandler,
              public engineService: EngineService,
              public editorService: EditorService,
              public showroomService: ShowroomsService,
              public userService: UserService,
              public platform: Platform,
              private routerService: RouterService) {
  }

  get userEnv() {
    return userEnv;
  }

  ngOnInit() {
    this.toggleMenu.subscribe(() => {
      this.menu = !this.menu;
    });

    const promises = [];
    this.itemList.forEach(item => {
      promises.push(this.showroomService.getShowroomImageWithResolution(item.showroomPortal.link.target)
          .then((url) => {
            this.itemListPicture.set(item.id, url);
          }));
    });
    Promise.all(promises).then(() => {
      this.showroomImageLoaded = true;
    });
  }

  itemClicked(item: any) {
    this.engineService.handlePortalClick(item.showroomPortal);
    this.toggleMenu.emit();
  }

  isMobile(): boolean {
    return this.platform.is('mobile');
  }

  async deleteItem(menuLink) {
    const modal = await this.modalController.create({
      component: ConfirmationModalComponent,
      cssClass: 'fit-content-modal',
      componentProps: {
        message: `Do you want to delete item from menu ?`
      }
    });
    modal.onDidDismiss().then((resp) => {
      if (resp) {
        this.deleteMenuLink(menuLink);
        this.itemList = this.itemList.filter(item => item.id !== menuLink.id);
      }
    });
    return modal.present();
  }

  async openMenuItemCreationModal(menuLink) {
    const modal = await this.modalController.create({
      component: NewMenuLinkItemComponent,
      cssClass: 'fit-content-modal',
      componentProps: {
        menuJumpLink: menuLink
      }
    });
    modal.onDidDismiss().then((value: any) => {
      if (value && value.data) {
        const item = value.data;
        const indexInList = this.itemList.map(i => i.id).indexOf(item.id);

        if (indexInList >= 0) {
          this.itemList[indexInList] = item;
          this.itemList = this.itemList.slice();
        } else {
          this.itemList = this.itemList.concat(item);
        }
        this.saveMenuLink(item);
      }
    });
    return await modal.present();
  }

  saveMenuLink(item) {
    const menuLink: MenuJumpLink = { id: item.id, showroomPortal: item.showroomPortal, name: item.name };
    this.showroomService.putMenuLink(menuLink).subscribe(
        () => {
          const foundIndex = this.showroom.menuLink.findIndex(item => item.id === item.id);

          if (foundIndex >= 0) {
            this.showroom.menuLink.splice(foundIndex, 1, JSON.parse(JSON.stringify(menuLink)));
          } else {
            this.showroom.menuLink.push(menuLink);
          }
        },
        () => { }
    );
  }

  deleteMenuLink(item) {
    this.showroomService.deleteLinkMenuItem(item.id).subscribe(() => {
      this.showroom.menuLink = this.showroom.menuLink.filter(item => item.id !== item.id);
    });
  }

  menuLinkUpdate(item) {
    this.showroomService.updateLinkMenu(item).subscribe(() => {
      this.showroom.menuLink = item;
    });
  }

  logOut() {
    localStorage.removeItem('authState');
    userEnv.authorization = false;
    this.menu = !this.menu;
    this.routerService.navigateByUrl('auth');
  }

  ngOnChanges(changes: SimpleChanges): void {
  }
}
