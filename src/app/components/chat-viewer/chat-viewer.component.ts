import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalController, Platform} from '@ionic/angular';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    selector: 'app-chat-viewer',
    templateUrl: './chat-viewer.component.html',
    styleUrls: ['./chat-viewer.component.scss'],
})
export class ChatViewerComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() data: any;
    @ViewChild('content') div: ElementRef;
    public isReady = false;
    public hasFailed = false;

    public script = `<html>
                        <script>
                            window._sarbacaneSettings = {
                                key: "spk-38c672d3-5ff5-4d84-8012-480b8c0c1a87",
                            };
                        </script>
                        <script src="https://chat.sarbacane.com/chat_loader.js" async="true"></script>
                      </html>`;

    constructor(private platform: Platform,
                private modalController: ModalController) {
    }

    ngAfterViewInit(): void {
    }

    private loadScript() {
        let firstScript = document.createElement("script");
        firstScript.type = "text/javascript";
        firstScript.id = "firstScript";
        firstScript.text = `window._sarbacaneSettings = {key: "spk-38c672d3-5ff5-4d84-8012-480b8c0c1a87",};`;
        document.body.appendChild(firstScript);

        let secondScript = document.createElement("script");
        secondScript.type = "text/javascript";
        secondScript.id = "secondScript";
        secondScript.async = true;
        secondScript.src = "https://chat.sarbacane.com/chat_loader.js";
        document.body.appendChild(secondScript);
        this.openChat();
    }

    openChat() {
        setTimeout(() => {
            if (!document.getElementById("slaask-button")) {
                this.openChat();
            } else {
                document.getElementById("slaask-button-cross").click();
            }
        }, 100);
    }

    ngOnDestroy(): void {
        document.getElementById("firstScript").remove();
        document.getElementById("secondScript").remove();
        document.getElementById("slaask-button").remove();
    }

    ngOnInit(): void {
        this.loadScript();
    }

    close() {
        this.modalController.dismiss();
    }

    isMd() {
        return this.platform.is('android');
    }

    public async hasLoaded() {
        this.isReady = true;
    }

    public async onError() {
        this.hasFailed = true;
    }
}
