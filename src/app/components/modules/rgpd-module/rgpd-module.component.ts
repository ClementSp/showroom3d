import { Component } from '@angular/core';
import {ModulesService} from '../../../services/modules.service';
import {CookieService} from 'ngx-cookie';

@Component({
  selector: 'app-rgpd-module',
  templateUrl: './rgpd-module.component.html',
  styleUrls: ['./rgpd-module.component.scss'],
})
export class RgpdModuleComponent {
  moduleName = 'RGPD';
  cookieConsent = 'rvr_collect';
  enabled = false;
  opened = false;

  constructor(private modulesService: ModulesService,
              private cookieService: CookieService) {
    this.modulesService.getModules().then(modules => {
      modules.forEach(mod => {
        if (mod.name === this.moduleName) {
          this.enabled = mod.enabled;
          this.opened = this.enabled;
          if (this.cookieService.get(this.cookieConsent)) {
            this.opened = false;
          }
        }
      });
    });
  }

  closePopup() {
    this.cookieService.put(this.cookieConsent, 'true');
    this.opened = false;
  }
}
