import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Poi, Showroom} from '../../../../models/showroom.model';
import {Subscription} from 'rxjs';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {EditorMode} from '../../../../models/EditorMode';
import * as THREE from 'three';
import {ModalController} from '@ionic/angular';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {showroomEnv} from '../../../../../environments/showroom.env';

@Component({
  selector: 'app-poi',
  templateUrl: './poi.component.html',
  styleUrls: ['./poi.component.scss'],
})
export class PoiComponent implements OnInit {
  @Input() object: any ;
  @Input() objectId: string;
  @Input() showroom: Showroom ;
  @Output() onExitOrPlacement = new EventEmitter<void>();
  onPoiPlaced: Subscription;

  constructor( private editorService: EditorService,
               private toastService: ToastService,
               private engineService: EngineService,
               private currentNode: CurrentNodeHandler,
               private modalController: ModalController,
               private showroomService: ShowroomsService,
               public sceneHandler: SceneHandler) { }
  ngOnInit() {
    this.initPoiPlacement(this.object);
    this.onPoiPlacement(this.objectId);
  }

  private initPoiPlacement(poi: any) {
    this.editorService.mode = EditorMode.ADD_POI_MARKERS;
    this.editorService.currentlyPlacingPoi = poi;
    // this.displayPoiPlacementToast(poi).then();
  }

  onPoiPlacement(id: string) {
    if (this.onPoiPlaced) {
      this.onPoiPlaced.unsubscribe();
    }
    this.onPoiPlaced = this.editorService.onPlacedPoi.subscribe(() => {
      this.toastService.dismiss();
      this.pushPoi(id).subscribe(poi => {
        showroomEnv.showroom.pois.push(poi);
        this.onPoiPlaced.unsubscribe();
        this.editorService.currentlyPlacingPoi.userData.id = poi.id;
        this.editorService.currentlyEditingPoi  = poi;
        this.editorService.currentlyPlacingPoi.traverse((child) => {
          child.userData.parentId = poi.id;
          child.userData.id = poi.id;
        });
        this.onExitOrPlacement.emit();
      });
    });
  }

  displayPoiPlacementToast(shapePoi: THREE.Group) {
      return this.toastService.presentToast(ToastType.NORMAL, 'Click anywhere to add the  Poi',
          0, true, () => this.onCancelPoiPlacement(shapePoi));
  }

  private onCancelPoiPlacement(shapePoi: THREE.Group) {
    this.object = undefined;
    this.objectId = '';
    this.editorService.target = undefined;
    this.editorService.currentlyPlacingFixture = undefined;
    this.sceneHandler.removeObjFromScene(shapePoi);
    this.editorService.mode = EditorMode.NONE;
    this.onExitOrPlacement.emit();
  }

  private pushPoi(id) {
    return this.showroomService.putPoi({
      data: {},
      position: this.editorService.currentlyPlacingPoi.position,
      scale: this.editorService.currentlyPlacingPoi.scale,
      rotation: {
        _x: this.editorService.currentlyPlacingPoi.rotation.x,
        _y: this.editorService.currentlyPlacingPoi.rotation.y,
        _z: this.editorService.currentlyPlacingPoi.rotation.z
      },
      size: 0,
      spriteColor: '',
      tip: '',
      type: '',
      id
    });
  }


close() {
    this.modalController.dismiss().then();
  }
}
