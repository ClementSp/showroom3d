import { Component, OnInit, ViewChild } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {IonSearchbar, ModalController} from '@ionic/angular';
import {ProductsService} from '../../../../services/products.service';
import {IProductAssetData} from '../../../../models/product.model';
import {SceneHandler} from "../../../../DataHandlers/scene.handler";
import {CategoriesService} from "../../../../services/categories.service";
import _ from 'lodash';
import {ACustomProduct} from "../../../../models/CustomProduct";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.scss'],
})
export class ProductModalComponent implements OnInit {

  // public products = new BehaviorSubject<Array<IProductAssetData>>([]);
  @ViewChild('searchbar') searchbar: IonSearchbar;
  public items = [];
  public currentItems = [];
  public filter = '';
  public isLoading: boolean;
  public currentFolder: any;
  public path = '';

  constructor(private productService: ProductsService,
              private categoriesService: CategoriesService,
              public translateService: TranslateService,
              private modalController: ModalController,
              private sceneHandler: SceneHandler) {
    /*this.productService.getAllProducts().subscribe(products => {
      this.isLoading = true ;
      const productsWithGlb = products.filter(product => product.product_glbModelUrl !== undefined);

      this.products.next([...productsWithGlb]);
      this.isLoading = false ;
    });*/
  }

  ngOnInit() {
    this.loadProducts();
  }

  ngAfterViewInit(): void {
    this.initializeSearchbar();
  }

  private initializeSearchbar() {
    this.searchbar.ionInput.pipe(debounceTime(300), distinctUntilChanged()).subscribe((event) => {
      this.filter = this.searchbar.value.toLowerCase();
      this.updateDataSource(this.items);
    });
  }

  public loadProducts(folderId?: string) {
    this.isLoading = true;
    folderId = folderId ? folderId : '';

    this.categoriesService.getProductCategories(folderId).toPromise().then(
        items => {
          if (items) {
            this.items = items;
            this.updateDataSource(items);
          } else {
            this.isLoading = false;
          }
        });
  }

  private updateDataSource(items) {
    this.currentItems = [];
    if (items && items.length > 0) {

      items = this.applyFilter();
      //this.size = items.length;

      let folderItems = items.filter(item => !item.product_ean);
      folderItems = _.orderBy(folderItems, 'createdAt', 'desc');
      let productItems = items.filter(item => item.product_ean && item.product_glbModelUrl);
      productItems = _.orderBy(productItems, 'createdAt', 'desc');

      items = folderItems.concat(productItems);

      items.forEach(item => {
        if (item.product_ean) {
          this.getFirstPicture(item);
        }
        this.currentItems.push(item);
      })

      /*for (let loop = 0 ; loop < Number(this.paginator.pageSize) ; loop ++ ) {
        const item = items[Number(this.paginator.pageIndex) * Number(this.paginator.pageSize) + loop];
        if (item) {
          if (item.ean) {
            this.getFirstPicture(item);
          }
          this.currentItems.push(item);
        }
      }*/
    }
    this.isLoading = false;
  }

  getFirstPicture(product: ACustomProduct) {
    const pictureMedia = product.medias?.find(media => media.type === 'picture');
    if (pictureMedia) {
      product.picture = pictureMedia.url;
    } else if (product.product_thumbnailUrl) {
      product.picture = product.product_thumbnailUrl;
    }
  }

  applyFilter() {
    let newFilter = this.filter;
    newFilter = newFilter ? newFilter.trim().toLowerCase() : '';
    if (this.items && this.items.length > 0) {
      return this.items.filter(item =>
          (item.name ? item.name.toLowerCase().indexOf(newFilter) !== -1 : false) ||
          (item.code ? item.code.toLowerCase().indexOf(newFilter) !== -1 : false) ||
          (item.product_ean ? item.product_ean.toLowerCase().indexOf(newFilter) !== -1 : false));
    } else {
      return;
    }
  }

  close() {
    this.modalController.dismiss();
  }

  productPlacement(productData: IProductAssetData) {
    if (this.sceneHandler.loadedGltfMap.get(productData.product_glbModelUrl)) {
      this.sceneHandler.loadSyncGltf(productData.product_glbModelUrl).then(() => {
      })
    }
    this.modalController.dismiss(productData);
  }

  openFolder(folder) {
    this.filter = '';
    this.currentFolder = {
      name: folder.name,
      id: folder.id,
      parent: this.currentFolder ? this.currentFolder : undefined
    };
    this.path = this.path + '/' + folder.name;
    this.loadProducts(folder.id);
  }

  returnFolder() {
    this.filter = '';
    this.path = this.path.substring(0, this.path.indexOf('/' + this.currentFolder.name));
    if (this.currentFolder.parent) {
      this.currentFolder = this.currentFolder.parent;
      this.loadProducts(this.currentFolder.id);
    } else {
      this.currentFolder = undefined;
      this.loadProducts();
    }
  }
}
