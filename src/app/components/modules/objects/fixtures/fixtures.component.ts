import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Showroom} from '../../../../models/showroom.model';
import {Subscription} from 'rxjs';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {FixturesManagerService} from '../../../../services/modules/fixtures-manager.service';
import {EditorMode} from '../../../../models/EditorMode';
import {IFixture, IFixtureAssetData} from '../../../../models/fixture.model';
import * as THREE from 'three';
import {ToolbarComponent} from '../../../toolbar/toolbar.component';
import {ModalController} from '@ionic/angular';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {showroomEnv} from '../../../../../environments/showroom.env';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.scss'],
})
export class FixturesComponent implements OnInit {
  @Input() object: any ;
  @Input() objectId: string;
  @Input() objectName: string;
  @Input() data: IFixtureAssetData ;
  @Input() showroom: Showroom ;
  @Output() onExitOrPlacement = new EventEmitter<void>();
  onFixturePlaced: Subscription;

  constructor( private editorService: EditorService,
               private toastService: ToastService,
               private engineService: EngineService,
               private currentNode: CurrentNodeHandler,
               private modalController: ModalController,
               private fixturesManagerService: FixturesManagerService,
               public sceneHandler: SceneHandler) { }

  ngOnInit() {
    this.initFixturePlacement(this.object);
    this.onFixturePlacement(this.objectId, this.data);
  }

  private async initFixturePlacement(fixture: any) {
    this.editorService.mode = EditorMode.PLACING_FIXTURE;
    this.editorService.currentlyPlacingFixture = fixture;
    // this.displayFixturePlacementToast(fixture).then();
  }

  private onFixturePlacement(id: string, fixtureData: IFixtureAssetData) {
    const position = this.editorService.currentlyPlacingFixture.position;

    if (this.onFixturePlaced) {
      this.onFixturePlaced.unsubscribe();
    }
    this.onFixturePlaced = this.editorService.onPlacedFixture.subscribe(() => {
      this.toastService.dismiss();
      this.pushFixture(id, position, fixtureData).subscribe(((fixture: IFixture) => {
        showroomEnv.showroom.fixtures.push(fixture);
        this.onFixturePlaced.unsubscribe();
        this.editorService.currentlyPlacingFixture.userData.fixtureId = fixture.fixtureId;
        this.editorService.currentlyPlacingFixture.userData.id = fixture.id;
        this.editorService.currentlyPlacingFixture.traverse((child) => {
          child.userData.parentId = fixture.id;
        });
        this.onExitOrPlacement.emit();
      }));
    });
  }

  private displayFixturePlacementToast(fixture: THREE.Group) {
    return this.toastService.presentToast(ToastType.NORMAL, 'Click anywhere to add the fixture',
        0, true, () => this.onCancelProductPlacement(fixture));
  }

  private onCancelProductPlacement(fixture: THREE.Group) {
    this.object = undefined;
    this.objectId = '';
    this.objectName = '';
    this.data = undefined;
    this.editorService.target = undefined;
    this.editorService.currentlyPlacingFixture = undefined;
    this.sceneHandler.removeObjFromScene(fixture);
    this.editorService.mode = EditorMode.NONE;
    this.onExitOrPlacement.emit();
  }

  private pushFixture(fixtureId, position, fixtureData: IFixtureAssetData) {
    return this.fixturesManagerService.addFixture({
      id: fixtureId,
      position: {x: position.x, y: position.y, z: position.z},
      scale: {x: 1, y: 1, z: 1},
      attributes: [],
      rotation: {x: 0, y: 0, z: 0},
      fixtureId: fixtureData.id,
      gltf: fixtureData.gltf
    });
  }

  async openToolbar(fixture) {
    const modal = await this.modalController.create({
      component: ToolbarComponent,
      cssClass: 'tools-modal',
      backdropDismiss: false,
      componentProps: {
        modeChanges: this.editorService.currentMode,
      },
      showBackdrop: false
    });
    modal.onDidDismiss().then(() => {
      this.onCancelProductPlacement(fixture);
    });
    await modal.present();
  }

  close() {

  }
}
