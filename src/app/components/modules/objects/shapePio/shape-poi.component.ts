import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ShapePoi, Showroom} from '../../../../models/showroom.model';
import {Subscription} from 'rxjs';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {EditorMode} from '../../../../models/EditorMode';
import * as THREE from 'three';
import {ModalController} from '@ionic/angular';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {showroomEnv} from '../../../../../environments/showroom.env';

@Component({
  selector: 'app-shape-poi',
  templateUrl: './shape-poi.component.html',
  styleUrls: ['./shape-poi.component.scss'],
})
export class ShapePoiComponent implements OnInit {
  @Input() object: any ;
  @Input() objectId: string;
  @Input() showroom: Showroom ;
  @Output() onExitOrPlacement = new EventEmitter<void>();
  onShapePoiPlaced: Subscription;

  constructor( private editorService: EditorService,
               private toastService: ToastService,
               private engineService: EngineService,
               private currentNode: CurrentNodeHandler,
               private modalController: ModalController,
               private showroomService: ShowroomsService,
               public sceneHandler: SceneHandler) { }
  ngOnInit() {
    this.initShapePoiPlacement(this.object);
    this.onShapePoiPlacement(this.objectId);
  }

  private initShapePoiPlacement(shapePoi: any) {
    this.editorService.mode = EditorMode.ADD_SHAPE_POI_MARKERS;
    this.editorService.currentlyPlacingShapePoi = shapePoi;
    this.displayShapePoiPlacementToast(shapePoi).then();
  }

  onShapePoiPlacement(id: string) {

    // const position = this.editorService.currentlyPlacingShapePoi.position;

    if (this.onShapePoiPlaced) {
      this.onShapePoiPlaced.unsubscribe();
    }
    this.onShapePoiPlaced = this.editorService.onPlacedShapePoi.subscribe(() => {
      this.toastService.dismiss();

      // const pos = this.getShapePoiPositionRelativeToShowroomEntrance(position);
      this.pushShapePoi(id).subscribe(shapePoi => {
        showroomEnv.showroom.shapePois.push(shapePoi);
        this.onShapePoiPlaced.unsubscribe();
        this.editorService.currentlyPlacingShapePoi.userData.id = id;
        this.editorService.currentlyEditingShapePoi = shapePoi;
        const shapeP = this.sceneHandler.scene.children.filter(i => i.userData.id === id);
        shapeP[0].userData.poi = shapePoi;

        this.editorService.currentlyPlacingShapePoi.traverse((child) => {
          child.userData.parentId = shapePoi.id;
          child.userData.id = shapePoi.id;
        });
        this.onExitOrPlacement.emit();
      });
    });
  }

  displayShapePoiPlacementToast(shapePoi: THREE.Group) {
      return this.toastService.presentToast(ToastType.NORMAL, 'Click anywhere to add the Shape Poi',
          0, true, () => this.onCancelShapePoiPlacement(shapePoi));
  }

  private onCancelShapePoiPlacement(shapePoi: THREE.Group) {
    this.object = undefined;
    this.objectId = '';
    this.editorService.target = undefined;
    this.editorService.currentlyPlacingFixture = undefined;
    this.sceneHandler.removeObjFromScene(shapePoi);
    this.editorService.mode = EditorMode.NONE;
    this.onExitOrPlacement.emit();
  }

  private pushShapePoi(id) {
    return this.showroomService.putShapePoi({
      data: {},
      position: this.editorService.currentlyPlacingShapePoi.position,
      scale: this.editorService.currentlyPlacingShapePoi.scale,
      rotation: {
        _x: this.editorService.currentlyPlacingShapePoi.rotation.x,
        _y: this.editorService.currentlyPlacingShapePoi.rotation.y,
        _z: this.editorService.currentlyPlacingShapePoi.rotation.z
      },
      size: 0,
      spriteColor: '',
      tip: '',
      type: '',
      isShapePoi: true,
      id
    });
  }


close() {
    this.modalController.dismiss().then();
  }
}
