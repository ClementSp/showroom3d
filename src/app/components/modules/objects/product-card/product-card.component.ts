import {Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {IProductAssetData} from '../../../../models/product.model';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {

  @Input() product: IProductAssetData;

  @ViewChild('card') card: ElementRef;

  @Output() downloadProductInShowroom = new EventEmitter<IProductAssetData>();

  constructor() { }

  ngOnInit() {
  }
}
