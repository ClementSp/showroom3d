import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Showroom} from '../../../../models/showroom.model';
import {Subscription} from 'rxjs';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {ModalController} from '@ionic/angular';
import {EditorMode} from '../../../../models/EditorMode';
import * as THREE from 'three';
import {ToolbarComponent} from '../../../toolbar/toolbar.component';
import {ISmartObject, ISmartObjectAssetData} from '../../../../models/smartObject.model';
import {SmartObjectManagerService} from '../../../../services/modules/smart-object-manager.service';
import {SceneHandler} from "../../../../DataHandlers/scene.handler";
import {showroomEnv} from "../../../../../environments/showroom.env";

@Component({
  selector: 'app-smart-objects',
  templateUrl: './smart-objects.component.html',
  styleUrls: ['./smart-objects.component.scss'],
})
export class SmartObjectsComponent implements OnInit {

  @Input() object: any ;
  @Input() objectId: string;
  @Input() objectName: string;
  @Input() data: ISmartObjectAssetData ;
  @Input() showroom: Showroom ;
  @Output() onExitOrPlacement = new EventEmitter<void>();
  onSmartObjectPlaced: Subscription;

  constructor( private editorService: EditorService,
               private toastService: ToastService,
               private engineService: EngineService,
               private currentNode: CurrentNodeHandler,
               private modalController: ModalController,
               private smartObjectManagerService: SmartObjectManagerService,
               public sceneHandler: SceneHandler) { }

  ngOnInit() {
    this.initSmartObjectPlacement(this.object);
    this.onSmartObjectPlacement(this.objectId, this.data);
  }

  private async initSmartObjectPlacement(smartObject: any) {
    this.editorService.mode = EditorMode.PLACING_SMART_OBJECT;
    this.editorService.currentlyPlacingSmartObject = smartObject;
    // this.displayFixturePlacementToast(smartObject).then();
  }

  private onSmartObjectPlacement(id: string, smartObjectData: ISmartObjectAssetData) {
    const position = this.editorService.currentlyPlacingSmartObject.position;

    if (this.onSmartObjectPlaced) {
      this.onSmartObjectPlaced.unsubscribe();
    }
    this.onSmartObjectPlaced = this.editorService.onPlacedSmartObject.subscribe(() => {
      this.toastService.dismiss();

      // const pos = this.getSmartObjectPositionRelativeToShowroomEntrance(position);
      this.pushSmartObject(id, position, smartObjectData).subscribe(((smartObject: ISmartObject) => {
          showroomEnv.showroom.smartObjects.push(smartObject);
          this.onSmartObjectPlaced.unsubscribe();
          this.editorService.currentlyPlacingSmartObject.userData.smartObjectId = smartObject.smartObjectId;
          this.editorService.currentlyPlacingSmartObject.userData.id = smartObject.id;
          this.editorService.currentlyPlacingSmartObject.traverse((child) => {
            child.userData.parentId = smartObject.id;
          });
          this.onExitOrPlacement.emit();
        })
      );
    });
  }

  private displayFixturePlacementToast(fixture: THREE.Group) {
    return this.toastService.presentToast(ToastType.NORMAL, 'Click anywhere to add the smartObject',
        0, true, () => this.onCancelSmartObjectPlacement(fixture));
  }

  private onCancelSmartObjectPlacement(fixture: THREE.Group) {
    this.object = undefined;
    this.objectId = '';
    this.objectName = '';
    this.data = undefined;
    this.editorService.target = undefined;
    this.editorService.currentlyPlacingFixture = undefined;
    this.sceneHandler.removeObjFromScene(fixture);
    this.editorService.mode = EditorMode.NONE;
    this.onExitOrPlacement.emit();
  }

  private pushSmartObject(smartObjectId, position, smartObjectData: ISmartObjectAssetData) {
    return this.smartObjectManagerService.addSmartObject({
      id: smartObjectId,
      position: {x: position.x, y: position.y, z: position.z},
      scale: {x: 1, y: 1, z: 1},
      attributes: [],
      rotation: {x: 0, y: 0, z: 0},
      smartObjectId: smartObjectData.id,
      smart_objects_glbModelUrl: smartObjectData.smart_objects_glbModelUrl
    });
  }

  async openToolbar(fixture) {
    const modal = await this.modalController.create({
      component: ToolbarComponent,
      cssClass: 'tools-modal',
      backdropDismiss: false,
      componentProps: {
        modeChanges: this.editorService.currentMode,
      },
      showBackdrop: false
    });
    modal.onDidDismiss().then(() => {
      this.onCancelSmartObjectPlacement(fixture);
    });
    await modal.present();
  }

  close() {

  }

}
