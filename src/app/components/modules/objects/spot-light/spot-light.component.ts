import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {EditorMode} from '../../../../models/EditorMode';
import * as THREE from 'three';
import {ModalController} from '@ionic/angular';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {showroomEnv} from '../../../../../environments/showroom.env';
import {Showroom} from '../../../../models/showroom.model';

@Component({
  selector: 'app-spot-light',
  templateUrl: './spot-light.component.html',
  styleUrls: ['./spot-light.component.scss'],
})
export class SpotLightComponent implements OnInit {
  @Input() object: any ;
  @Input() objectId: string;
  @Input() showroom: Showroom ;
  @Output() onExitOrPlacement = new EventEmitter<void>();
  onSpotLightPlaced: Subscription;

  constructor( private editorService: EditorService,
               private toastService: ToastService,
               private engineService: EngineService,
               private currentNode: CurrentNodeHandler,
               private modalController: ModalController,
               private showroomService: ShowroomsService,
               public sceneHandler: SceneHandler) { }
  ngOnInit() {
    this.initSpotLightPlacement(this.object);
    this.onSpotLightPlacement(this.objectId);
  }

  private initSpotLightPlacement(pointLight: any) {
    this.editorService.mode = EditorMode.ADD_SPOT_LIGHT;
    this.editorService.currentlyPlacingSpotLight = pointLight;
  }

  onSpotLightPlacement(id: string) {
    if (this.onSpotLightPlaced) {
      this.onSpotLightPlaced.unsubscribe();
    }
    this.onSpotLightPlaced = this.editorService.onPlacedSpotLight.subscribe(() => {
      this.toastService.dismiss();
      this.pushSpotLight(id).subscribe(pointLight => {
        showroomEnv.showroom.lights.push(this.editorService.currentlyPlacingSpotLight);
        this.onSpotLightPlaced.unsubscribe();
        this.onExitOrPlacement.emit();
      });
    });
  }

  private pushSpotLight(id) {
    return this.showroomService.putLight({
      position: this.editorService.currentlyPlacingSpotLight.position,
      rotation: {
        _x: this.editorService.currentlyPlacingSpotLight.children[0].rotation.x,
        _y: this.editorService.currentlyPlacingSpotLight.children[0].rotation.y,
        _z: this.editorService.currentlyPlacingSpotLight.children[0].rotation.z
      },
      tip: '',
      nodeId: undefined,
      type: 'SPOT_LIGHT',
      id
    });
  }


close() {
    this.modalController.dismiss().then();
  }
}
