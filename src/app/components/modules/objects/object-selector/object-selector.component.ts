import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-object-selector',
  templateUrl: './object-selector.component.html',
  styleUrls: ['./object-selector.component.scss'],
})
export class ObjectSelectorComponent implements OnInit {

  constructor(private modalCtrl: PopoverController) { }

  ngOnInit() {}

  selectObjectType(objectType: string) {
    return this.modalCtrl.dismiss(objectType);
  }
}
