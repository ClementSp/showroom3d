import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-light-selector',
  templateUrl: './light-selector.component.html',
  styleUrls: ['./light-selector.component.scss'],
})
export class LightSelectorComponent implements OnInit {

  constructor(private modalCtrl: PopoverController) { }

  ngOnInit() {}

  selectObjectType(objectType: string) {
    return this.modalCtrl.dismiss(objectType);
  }
}
