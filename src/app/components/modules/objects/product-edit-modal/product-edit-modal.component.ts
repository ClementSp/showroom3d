import {Component, OnDestroy, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {EditorService} from '../../../../services/editor.service';
import * as THREE from 'three';
import {ModulesService} from '../../../../services/modules.service';
import {TranslateService} from '@ngx-translate/core';
import {MediasService} from '../../../../services/medias.service';
import {FormBuilder} from '@angular/forms';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {ProductAction} from '../../../../common/ProductAction';
import {ProductsService} from '../../../../services/products.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit-modal.component.html',
  styleUrls: ['./product-edit-modal.component.scss'],
})
export class ProductEditModalComponent implements OnInit, OnDestroy {

  products = [];
  product: THREE.Group;
  url: string;
  type = 'PRODUCT';
  isReady: boolean;

  constructor(private modalController: ModalController,
              private formBuilder: FormBuilder,
              private showroomService: ShowroomsService,
              public translateService: TranslateService,
              public productService: ProductsService,
              private mediasService: MediasService,
              private modulesService: ModulesService,
              private editorService: EditorService) {}

  ngOnInit() {
    this.product = (this.editorService.attachedObject as THREE.Group);
    this.productService.getProductById(this.product.userData.productId).then(product => {
      this.url = product.product_glbModelUrl;
    });
  }
T
  close() {
    this.modalController.dismiss();
  }

  ngOnDestroy(): void {
  }

  moveProduct() {
    return this.modalController.dismiss({action: ProductAction.MOVE, portal: this.product});
  }

  removeProduct() {
    return this.modalController.dismiss({action: ProductAction.REMOVE, portal: this.product});
  }
}
