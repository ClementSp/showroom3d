import {Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {IFixtureAssetData} from '../../../../models/fixture.model';

@Component({
  selector: 'app-fixture-card',
  templateUrl: './fixture-card.component.html',
  styleUrls: ['./fixture-card.component.scss'],
})
export class FixtureCardComponent implements OnInit {

  @Input() fixture: IFixtureAssetData;

  @ViewChild('card') card: ElementRef;

  @Output() downloadFixtureInShowroom = new EventEmitter<IFixtureAssetData>();

  constructor() { }

  ngOnInit() {}
}
