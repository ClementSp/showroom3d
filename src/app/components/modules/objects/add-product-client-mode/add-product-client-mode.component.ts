import {Component, Input, OnInit} from '@angular/core';
import {ProductModalComponent} from '../product-modal/product-modal.component';
import {IProduct, IProductAssetData} from '../../../../models/product.model';
import {uuid4} from '@capacitor/core/dist/esm/util';
import {ModalController} from '@ionic/angular';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {Showroom} from '../../../../models/showroom.model';

@Component({
  selector: 'app-add-product-client-mode',
  templateUrl: './add-product-client-mode.component.html',
  styleUrls: ['./add-product-client-mode.component.scss'],
})
export class AddProductClientModeComponent implements OnInit {

  @Input() showroom: Showroom;
  isReady = false;
  object: any;
  objectId: string;
  data: IProductAssetData;

  constructor(private modalController: ModalController,
              public sceneHandler: SceneHandler) { }

  private static getDefaultProduct(productData: IProductAssetData): IProduct {
    return {
      template: "",
      ean: productData.product_ean,
      product_glbModelUrl: productData.product_glbModelUrl,
      poiId: '',
      id: '',
      position: {x: 0, y: 0, z: 0},
      scale: {x: 1, y: 1, z: 1},
      rotation: {x: 0, y: 0, z: 0},
      productId: productData.id,
      components: productData.components
    };
  }

  ngOnInit() {}
  async openProductModal() {
    const modal = await this.modalController.create({
      component: ProductModalComponent,
      showBackdrop: true,
      cssClass: 'fullscreen-modal',
      backdropDismiss: true,
      componentProps: {
        multipleSelect: false
      }
    });
    modal.onDidDismiss().then(async (response) => {
      if (!response.data) {
        this.isReady = false;
        return;
      }
      const productData: IProductAssetData = response.data;
      const product: IProduct = AddProductClientModeComponent.getDefaultProduct(productData);
      if (productData && product) {
        const id = uuid4();
        this.object = await this.sceneHandler.placeGltfInScene(id, productData.product_glbModelUrl, product);
        this.objectId = id;
        this.data = productData;
        this.isReady = true;
      }
    });
    return await modal.present().then();
  }

}
