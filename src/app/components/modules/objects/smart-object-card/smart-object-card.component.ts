import {Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {ISmartObjectAssetData} from '../../../../models/smartObject.model';

@Component({
  selector: 'app-smart-object-card',
  templateUrl: './smart-object-card.component.html',
  styleUrls: ['./smart-object-card.component.scss'],
})
export class SmartObjectCardComponent implements OnInit {

  @Input() smartObject: ISmartObjectAssetData;

  @ViewChild('card') card: ElementRef;

  @Output() downloadFixtureInShowroom = new EventEmitter<ISmartObjectAssetData>();

  constructor() { }

  ngOnInit() {}
}
