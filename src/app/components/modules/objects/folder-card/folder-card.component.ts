import {Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {IProductAssetData} from '../../../../models/product.model';

@Component({
  selector: 'app-folder-card',
  templateUrl: './folder-card.component.html',
  styleUrls: ['./folder-card.component.scss'],
})
export class FolderCardComponent implements OnInit {

  @Input() folder: any;

  @ViewChild('card') card: ElementRef;

  @Output() openFolder = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }
}
