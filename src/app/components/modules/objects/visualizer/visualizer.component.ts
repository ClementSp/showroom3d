import {
  AmbientLight,
  AxesHelper,
  Box3,
  Cache,
  DirectionalLight,
  GridHelper,
  HemisphereLight,
  LinearEncoding,
  LoadingManager,
  PMREMGenerator,
  PerspectiveCamera,
  REVISION,
  Scene,
  SkeletonHelper,
  Vector3,
  WebGLRenderer,
  sRGBEncoding,
  CubeTextureLoader, MeshBasicMaterialParameters, MeshBasicMaterial, TextureLoader, RepeatWrapping, Mesh,
} from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { KTX2Loader } from 'three/examples/jsm/loaders/KTX2Loader.js';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';
import { MeshoptDecoder } from 'three/examples/jsm/libs/meshopt_decoder.module.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {Configuration, ProductAdjustment} from "../../../../models/CustomProduct";
import {Texture} from "../../../../models/texture.model";


const DEFAULT_CAMERA = '[default]';

const MANAGER = new LoadingManager();
const THREE_PATH = `https://unpkg.com/three@0.${REVISION}.x`;
const DRACO_LOADER = new DRACOLoader(MANAGER).setDecoderPath(`${THREE_PATH}/examples/js/libs/draco/gltf/`);
const KTX2_LOADER = new KTX2Loader(MANAGER).setTranscoderPath(`${THREE_PATH}/examples/js/libs/basis/`);
const MAP_NAMES = [
  'map',
  'aoMap',
  'emissiveMap',
  'glossinessMap',
  'metalnessMap',
  'normalMap',
  'roughnessMap',
  'specularMap',
];

const Preset = {ASSET_GENERATOR: 'assetgenerator'};

Cache.enabled = true;

@Component({
  selector: 'app-visualizer',
  templateUrl: './visualizer.component.html',
  styleUrls: ['./visualizer.component.scss'],
})
export class VisualizerComponent implements AfterViewInit, OnInit, OnDestroy {

  @Input() tenant: string;
  @Input() glbUrl: string;
  @Input() object: any;
  @Input() canvasHeight: number;
  @Input() objectType;
  @Input() textureUrl: string;
  @Input() code: string;
  @Input() configurations: any;
  @Input() attributes: Array<any>;
  @Input() objectId: string;
  @Input() refreshEvent: EventEmitter<any>;

  @Output() meshComponent = new EventEmitter();
  @Output() visualizerLoaded = new EventEmitter<boolean>();
  @ViewChild('objectViewer') objectViewer: ElementRef;
  bucketStorageName = environment.production ? 'rvr_smartobjects' : 'rvr_smartobjects_dev';
  cameraFov = 75;

  public el;
  public options;
  public lights;
  public content;
  public mixer: any;
  public clips;
  public gui;
  public state;
  mesh: any;
  target: any;
  scene: any;
  stats: any;
  prevTime: number;
  defaultCamera: any;
  activeCamera: any;
  renderer: any;
  pmremGenerator: any;
  controls: any;
  vignette: any;
  cameraCtrl = null;
  cameraFolder = null;
  animFolder = null;
  animCtrls = [];
  morphFolder = null;
  skeletonHelpers = [];
  gridHelper = null;
  axesHelper = null;
  axesCamera: any;
  axesRenderer: any;
  axesScene: any;
  axesDiv: any;
  axesCorner: any;
  lastId = [];
  selectedCode: any;
  textureUrlSelected: any;
  loading = true;
  defaultModel: any;

  constructor(private sceneHandler: SceneHandler) {

  }

  ngOnInit(): void {
    if (this.object?.userData.currentConfig) {

    //  this.configurationToSave = this.object.userData.currentConfig;
    }
    this.onRefreshEvent();
  }

  ngAfterViewInit(): void {
    this.init();
    if (this.object){
      this.loadWithObject();
    }else {
      this.load(this.glbUrl);
    }

  }

  ngOnDestroy() {
    this.sceneHandler.removeVideo(this.target);
    this.disposeScene();
  }

  applyConfigurationToModel(configuration) {
    if (configuration.config){
     this.previewTexture(configuration);
    } else if (configuration.meshes){
      const currentConfig = this.sceneHandler.configurationToSave.get(configuration.name);
      if (currentConfig && currentConfig.meshes){
        const meshesToRemove = [];
        currentConfig.meshes.forEach(mesh => {
          let meshIsFind = false;
          configuration?.meshes.forEach(confMesh => {
            if (confMesh === mesh) {
              meshIsFind = true;
            }
          })
          if (!meshIsFind) {
            meshesToRemove.push(mesh);
          }
        });
        this.resetObject(meshesToRemove);
      }
      this.customProcess(configuration.meshes);
    }

    const saveConfiguration = new ProductAdjustment();
    saveConfiguration.name = configuration.name;
    saveConfiguration.meshes = configuration.meshes;
    this.sceneHandler.configurationToSave.set(configuration.name , saveConfiguration);
  }

  customProcess(codes) {
    codes.forEach(code => {
      let targetObject = this.target.getObjectByName(code);
      if (targetObject) {
        targetObject.visible  = true;
      } else if (!targetObject) {
        this.iterateCustomProcess(code, this.target);
      }
      let object = this.object.getObjectByName(code);
      if (object) {
        object.visible  = true;
      } else if (!object) {
        this.iterateCustomProcess(code, this.object);
      }
    });
  }

  iterateCustomProcess(code, object) {
    if (object.children) {
      object.children.forEach(child => {
        let object = child.getObjectByName(code);
        if (object && !object.visible) {
          object.visible  = true;
        } else {
          this.iterateCustomProcess(code, child)
        }
      })
    }
  }

  initProcess(codes) {
    codes.forEach(code => {
      let object = this.target.getObjectByName(code);
      if (object) {
        object.visible = false;
      } else if (!object) {
        this.iterateInitProcess(code, this.target);
      }
      object = this.object.getObjectByName(code);
      if (object) {
        object.visible = false;
      } else if (!object) {
        this.iterateInitProcess(code, this.object);
      }
    });
  }

  iterateInitProcess(code, object) {
    if (object.children) {
      object.children.forEach(child => {
        let object = child.getObjectByName(code);
        if (object && object.visible) {
          object.visible  = false;
        } else {
          this.iterateInitProcess(code, child)
        }
      })
    }
  }

  resetObject(codes) {
    this.initProcess(codes);
    this.resetTexture(codes);
  }

   runChanges(configuration) {
    this.applyConfigurationToModel(configuration);
    this.object.userData.currentConfig = this.sceneHandler.configurationToSave;
  }

  init() {
    this.lights = [];
    this.content = null;
    this.mixer = null;
    this.clips = [];
    this.gui = null;

    this.state = {
      background: false,
      playbackSpeed: 1.0,
      actionStates: {},
      camera: DEFAULT_CAMERA,
      wireframe: false,
      skeleton: false,
      grid: false,

      // Lights
      addLights: true,
      exposure: 1.0,
      textureEncoding: 'sRGB',
      ambientIntensity: 0.3,
      ambientColor: 0xFFFFFF,
      directIntensity: 0.8 * Math.PI, // TODO(#116)
      directColor: 0xFFFFFF,
      bgColor1: '#ffffff',
      bgColor2: '#353535'
    };

    this.prevTime = 0;

    // this.stats = new Stats();
    // this.stats.dom.height = '48px';
    // [].forEach.call(this.stats.dom.children, (child) => (child.style.display = ''));

    this.scene = new Scene();

    this.defaultCamera = new PerspectiveCamera(this.cameraFov, 2, 0.001, 1000);
    this.activeCamera = this.defaultCamera;
    this.scene.add( this.defaultCamera );

    this.initRenderer();
    this.initPmremGenerator();
    this.initControls();

    // this.el.appendChild(this.renderer.domElement);
    this.addAxesHelper();
    // this.addGUI();
    // if (options.kiosk) this.gui.close();

    this.initAnimate();
    // window.addEventListener('resize', this.resize.bind(this), false);
  }

  initRenderer() {
    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.domElement.style.display = 'block';
    this.renderer.domElement.style.height = '100%';
    this.renderer.domElement.style.width = '100%';
    this.objectViewer.nativeElement.appendChild(this.renderer.domElement);
    this.renderer.physicallyCorrectLights = true;
    this.renderer.setClearColor( 0xcccccc );
    this.renderer.outputEncoding = sRGBEncoding;
  }

  initControls() {
    this.controls = new OrbitControls( this.defaultCamera, this.renderer.domElement );
    this.controls.autoRotate = false;
    this.controls.autoRotateSpeed = -10;
    this.controls.screenSpacePanning = true;
  }

  initPmremGenerator() {
    this.pmremGenerator = new PMREMGenerator( this.renderer );
    this.pmremGenerator.compileEquirectangularShader();
  }

  initAnimate() {
    this.animate = this.animate.bind(this);
    requestAnimationFrame( this.animate );
    this.resizeCanvasToDisplaySize();
  }

  resizeCanvasToDisplaySize() {
    const canvas = this.renderer.domElement;
    const width = document.getElementById('body')?.clientWidth;
    let height = document.getElementById('body')?.clientHeight;
    if (height === 0) {
      height =  window.innerHeight;
    }

    if (canvas && canvas.width !== width || canvas.height !== height) {
      this.renderer.setSize(width, height, false);
      (this.defaultCamera as any).aspect = width / height;
      (this.defaultCamera as any).updateProjectionMatrix();
    }
  }

  animate(time) {
    requestAnimationFrame(this.animate);
    this.resizeCanvasToDisplaySize();
    this.render();
    // this.prevTime = time;
  }

  render() {
    this.renderer.render( this.scene, this.activeCamera );
  }

  load(modelUrl: string) {
    const loader = new GLTFLoader(MANAGER).setCrossOrigin('anonymous').setDRACOLoader(DRACO_LOADER)
        .setKTX2Loader(KTX2_LOADER.detectSupport(this.renderer)).setMeshoptDecoder(MeshoptDecoder);

    const blobURLs = [];
    loader.load(modelUrl, (gltf) => {
      this.target = gltf.scene || gltf.scenes[0];
      this.sceneHandler.applyAttributes(this.attributes, this.target);
      const clips = gltf.animations || [];
      if (!this.target) {
        throw new Error(
            'This model contains no scene, and cannot be viewed here. However,'
            + ' it may contain individual 3D resources.'
        );
      }
      this.visualizerLoaded.emit(true);
      this.initCamera();
      blobURLs.forEach(URL.revokeObjectURL);
      this.loading = false;
    });
  }

   loadWithObject() {
     this.target = this.object.clone();


     this.target.position.set(0, 0, 0);
     const loader = new GLTFLoader(MANAGER).setCrossOrigin('anonymous').setDRACOLoader(DRACO_LOADER)
         .setKTX2Loader(KTX2_LOADER.detectSupport(this.renderer)).setMeshoptDecoder(MeshoptDecoder);

     const blobURLs = [];
     loader.load(this.glbUrl, (gltf) => {
       this.defaultModel = gltf.scene || gltf.scenes[0];
       if (!this.target) {
         throw new Error(
             'This model contains no scene, and cannot be viewed here. However,'
             + ' it may contain individual 3D resources.'
         );
       }else {
         this.initCamera();
         this.loading = false;
         this.visualizerLoaded.emit(true);
       }
     });
  }

   initCamera() {
    this.clear();

    const box = new Box3().setFromObject(this.target);
    const size = box.getSize(new Vector3()).length();
    const center = box.getCenter(new Vector3());

    // this.controls.reset();

    this.target.position.set(0, 0, 0);
    this.target.position.x += (this.target.position.x - center.x);
    this.target.position.y += (this.target.position.y - center.y);
    this.target.position.z += (this.target.position.z - center.z);

    const boxSize = box.getSize(new Vector3());
    const viewPortSize = 1.25 * Math.max(boxSize.x, boxSize.y, boxSize.z);
    const radFov = Math.PI / 180 * this.cameraFov;
    const distanceToObject = 0.5 * viewPortSize / Math.tan(radFov * 0.5);

    const xCameraRot = 10;
    const yCameraRotation = -25;

    const xCameraPos = distanceToObject * Math.sin(Math.PI / 180 * yCameraRotation);
    const yCameraPos = distanceToObject * Math.sin(Math.PI / 180 * xCameraRot);
    const zCameraPos = distanceToObject * Math.cos(Math.PI / 180 * yCameraRotation);

    this.defaultCamera.rotation.set(Math.PI / 180 * xCameraRot, Math.PI / 180 * yCameraRotation, 0);
    this.defaultCamera.position.set(xCameraPos, yCameraPos, zCameraPos);

    this.axesCamera.position.copy(this.defaultCamera.position);
    this.axesCamera.lookAt(this.axesScene.position);
    this.axesCamera.near = size / 100;
    this.axesCamera.far = size * 100;
    this.axesCamera.updateProjectionMatrix();
    this.axesCorner.scale.set(size, size, size);

    this.controls.saveState();
    this.controls.update();
    this.scene.add(this.target);
    this.content = this.target;

    this.state.addLights = true;

    this.content.traverse((node) => {
      if (node.isLight) {
        this.state.addLights = false;
      } else if (node.isMesh) {
        node.material.depthWrite = !node.material.transparent;
      }
    });

    // this.setClips(clips);

    this.updateLights();
    // this.updateGUI();
    this.updateEnvironment();
    this.updateTextureEncoding();
    this.updateDisplay();

    // window.content = this.content;
    this.printGraph(this.content);
  }

  printGraph(node) {
    node.children.forEach((child) => this.printGraph(child));
  }

  setCamera( name ) {
    if (name === DEFAULT_CAMERA) {
      this.controls.enabled = true;
      this.activeCamera = this.defaultCamera;
    } else {
      this.controls.enabled = false;
      this.content.traverse((node) => {
        if (node.isCamera && node.name === name) {
          this.activeCamera = node;
        }
      });
    }
  }

  updateTextureEncoding() {
    const encoding = this.state.textureEncoding === 'sRGB' ? sRGBEncoding : LinearEncoding;
    traverseMaterials(this.content, (material) => {
      if (material.map) { material.map.encoding = encoding; }
      if (material.emissiveMap) { material.emissiveMap.encoding = encoding; }
      if (material.map || material.emissiveMap) { material.needsUpdate = true; }
    });
  }

  updateLights() {
    const state = this.state;
    const lights = this.lights;

    if (state.addLights && !lights.length) {
      this.addLights();
    } else if (!state.addLights && lights.length) {
      this.removeLights();
    }

    this.renderer.toneMappingExposure = state.exposure;

    if (lights.length === 2) {
      lights[0].intensity = state.ambientIntensity;
      lights[0].color.setHex(state.ambientColor);
      lights[1].intensity = state.directIntensity;
      lights[1].color.setHex(state.directColor);
    }
  }

  addLights() {
    const state = this.state;

    if (this.options && this.options.preset === Preset.ASSET_GENERATOR) {
      const hemiLight = new HemisphereLight();
      hemiLight.name = 'hemi_light';
      this.scene.add(hemiLight);
      this.lights.push(hemiLight);
      return;
    }

    const light1  = new AmbientLight(state.ambientColor, state.ambientIntensity);
    light1.name = 'ambient_light';
    this.defaultCamera.add( light1 );

    const light2  = new DirectionalLight(state.directColor, state.directIntensity);
    light2.position.set(0.5, 0, 0.866); // ~60º
    light2.name = 'main_light';
    this.defaultCamera.add( light2 );
    this.lights.push(light1, light2);
  }

  removeLights() {
    this.lights.forEach((light) => light.parent.remove(light));
    this.lights.length = 0;
  }

  updateEnvironment() {
    // const environment = environments.filter((entry) => entry.name === this.state.environment)[0];
    const loader = new CubeTextureLoader();
    loader.setPath('/assets/envReflexionCubemap/');
    const textureCube = loader.load(['pic-right-flou.png', 'pic-left-flou.png', 'pic-up-flou.png', 'pic-down-flou.png', 'pic-forward-flou.png', 'pic-back-flou.png']);
    textureCube.encoding = sRGBEncoding;
    this.scene.environment = textureCube;
    // this.getCubeMapTexture( '/assets/envReflexionCubemap/Ciel.png' ).then(( { envMap } ) => {

    // if ((!envMap || !this.state.background) && this.activeCamera === this.defaultCamera) {
    //   this.scene.add(this.vignette);
    // } else {
    //   this.scene.remove(this.vignette);
    // }

    // this.scene.environment = envMap;
    // this.scene.background = this.state.background;
    this.scene.background = new CubeTextureLoader().setPath('/assets/envReflexionCubemap/')
        .load( [
          'gris1.jpg',
          'gris1.jpg',
          'gris1.jpg',
          'gris1.jpg',
          'gris1.jpg',
          'gris1.jpg'
        ] );
    // });
  }

  onRefreshEvent() {
    this.refreshEvent?.subscribe((attributes) => {
      this.attributes = attributes;
      this.sceneHandler.removeVideo(this.target);
      this.sceneHandler.applyAttributes(this.attributes, this.target);
    });
  }

  updateDisplay() {
    if (this.skeletonHelpers.length) {
      this.skeletonHelpers.forEach((helper) => this.scene.remove(helper));
    }

    traverseMaterials(this.content, (material) => {
      material.wireframe = this.state.wireframe;
    });

    this.content.traverse((node) => {
      if (node.isMesh && node.skeleton && this.state.skeleton) {
        const helper = new SkeletonHelper(node.skeleton.bones[0].parent);
        // @ts-ignore
        helper.material.linewidth = 3;
        this.scene.add(helper);
        this.skeletonHelpers.push(helper);
      }
    });

    if (this.state.grid !== Boolean(this.gridHelper)) {
      if (this.state.grid) {
        this.gridHelper = new GridHelper();
        this.axesHelper = new AxesHelper();
        this.axesHelper.renderOrder = 999;
        this.axesHelper.onBeforeRender = (renderer) => renderer.clearDepth();
        this.scene.add(this.gridHelper);
        this.scene.add(this.axesHelper);
      } else {
        this.scene.remove(this.gridHelper);
        this.scene.remove(this.axesHelper);
        this.gridHelper = null;
        this.axesHelper = null;
        this.axesRenderer.clear();
      }
    }
  }

  private disposeScene() {
    /*
    ** each iteration on the children array changes once you do a .remove()
    ** from the start and the indexing of that array changes.
    */

    for (let i = this.scene.children.length - 1; i >= 0; i--) {
      const obj = this.scene.children[i];

      if ((obj as any).material) {
        (obj as any).material.dispose();
      }
      if ((obj as any).geometry) {
        (obj as any).geometry.dispose();
      }
      this.scene.remove(obj);
    }
    this.renderer.dispose();
    this.scene.clear();
  }

  updateBackground() {
    this.vignette.style({colors: [this.state.bgColor1, this.state.bgColor2]});
  }

  fitTheObject(codes) {
    if (this.lastId.length){
      this.lastId.forEach(code => {
        this.target.getObjectByName(code).children = [] ;
      });
    }
    codes.forEach((obj) => {
      const objectById = this.target.getObjectByName(obj);
      const outlineMaterial = new MeshBasicMaterial({
        color: '#ff6400',
        name : 'outlineMaterial',
        wireframe: true
      });
      const outlineMesh = new Mesh(objectById.geometry, outlineMaterial);
      const add = this.target.getObjectByName(obj).add(outlineMesh);
    });
    this.lastId  = codes;

  }

  previewTexture(configuration){
    const configurations = configuration.config.config;
    if (configurations) {
      configurations.forEach(configurationItem => {
        const codes = configurationItem.meshes ;
        const textureUrl = configurationItem.texture.gcsPath;

        codes.forEach(code => {
          const objectById = this.target.getObjectByName(code);
          if (objectById){
            const texture = new TextureLoader().load( textureUrl);
            texture.flipY = false;
            texture.encoding = sRGBEncoding;
            texture.wrapS = texture.wrapT = RepeatWrapping;
            texture.anisotropy = 16;

            const originalMaterial = objectById.material;

            if (originalMaterial) {
              originalMaterial.map = texture;
              const parameters: MeshBasicMaterialParameters = originalMaterial;
              // @ts-ignore
              const material = new MeshBasicMaterial(
                  {
                    map: parameters.map,
                    transparent: parameters.transparent,
                    opacity: parameters.opacity,
                  });

              const TextureMesh = new Mesh(objectById.geometry , material);
              objectById.add(TextureMesh);
              objectById.children = objectById.children.splice(objectById.children.length - 1, 1);
            }
          }
        });
      });
    }
  }

  resetTexture(codes){
    codes.forEach(code => {
      const objectById = this.target.getObjectByName(code);
      const original = this.defaultModel.getObjectByName(code);
      if (objectById){
        if (original.material) {
          objectById.material.map = original.material.map;
          objectById.material.needsUpdate = true;
        }
        // objectById.children = [];
      }
    });
  }

  zoomCameraToSelection( camera, controls, object, fitOffset = 1.2 ) {
    const box = new Box3();

    box.expandByObject( object );

    const size = box.getSize( new Vector3() );
    const center = box.getCenter( new Vector3() );

    const maxSize = Math.max( size.x, size.y, size.z );
    const fitHeightDistance = maxSize / ( 2 * Math.atan( Math.PI * camera.fov / 360 ) );
    const fitWidthDistance = fitHeightDistance / camera.aspect;
    const distance = fitOffset * Math.max( fitHeightDistance, fitWidthDistance );

    const direction = controls.target.clone()
        .sub( camera.position )
        .normalize()
        .multiplyScalar( distance );

    controls.maxDistance = distance * 10;
    controls.target.copy( center );

    camera.near = distance / 100;
    camera.far = distance * 100;
    camera.updateProjectionMatrix();

    camera.position.copy( controls.target ).sub(direction);

    controls.update();

  }

  addAxesHelper() {
    this.axesDiv = document.createElement('div');
    this.objectViewer.nativeElement.appendChild(this.axesDiv);
    this.axesDiv.classList.add('axes');

    const {clientWidth, clientHeight} = this.axesDiv;

    this.axesScene = new Scene();
    this.axesCamera = new PerspectiveCamera( 50, clientWidth / clientHeight, 0.1, 10 );
    this.axesScene.add( this.axesCamera );

    this.axesRenderer = new WebGLRenderer( { alpha: true } );
    this.axesRenderer.setPixelRatio( window.devicePixelRatio );
    this.axesRenderer.setSize( this.axesDiv.clientWidth, this.axesDiv.clientHeight );

    this.axesCamera.up = this.defaultCamera.up;

    this.axesCorner = new AxesHelper(5);
    this.axesScene.add( this.axesCorner );
    this.axesDiv.appendChild(this.axesRenderer.domElement);
  }

  // addGUI () {
  //
  //   const gui = this.gui = new GUI({autoPlace: false, width: 260, hideable: true});
  //
  //   // Display controls.
  //   const dispFolder = gui.addFolder('Display');
  //   const envBackgroundCtrl = dispFolder.add(this.state, 'background');
  //   envBackgroundCtrl.onChange(() => this.updateEnvironment());
  //   const wireframeCtrl = dispFolder.add(this.state, 'wireframe');
  //   wireframeCtrl.onChange(() => this.updateDisplay());
  //   const skeletonCtrl = dispFolder.add(this.state, 'skeleton');
  //   skeletonCtrl.onChange(() => this.updateDisplay());
  //   const gridCtrl = dispFolder.add(this.state, 'grid');
  //   gridCtrl.onChange(() => this.updateDisplay());
  //   dispFolder.add(this.controls, 'autoRotate');
  //   dispFolder.add(this.controls, 'screenSpacePanning');
  //   const bgColor1Ctrl = dispFolder.addColor(this.state, 'bgColor1');
  //   const bgColor2Ctrl = dispFolder.addColor(this.state, 'bgColor2');
  //   bgColor1Ctrl.onChange(() => this.updateBackground());
  //   bgColor2Ctrl.onChange(() => this.updateBackground());
  //
  //   // Lighting controls.
  //   const lightFolder = gui.addFolder('Lighting');
  //   const encodingCtrl = lightFolder.add(this.state, 'textureEncoding', ['sRGB', 'Linear']);
  //   encodingCtrl.onChange(() => this.updateTextureEncoding());
  //   lightFolder.add(this.renderer, 'outputEncoding', {sRGB: sRGBEncoding, Linear: LinearEncoding})
  //     .onChange(() => {
  //       this.renderer.outputEncoding = Number(this.renderer.outputEncoding);
  //       traverseMaterials(this.content, (material) => {
  //         material.needsUpdate = true;
  //       });
  //     });
  //   const envMapCtrl = lightFolder.add(this.state, 'environment', environments.map((env) => env.name));
  //   envMapCtrl.onChange(() => this.updateEnvironment());
  //   [
  //     lightFolder.add(this.state, 'exposure', 0, 2),
  //     lightFolder.add(this.state, 'addLights').listen(),
  //     lightFolder.add(this.state, 'ambientIntensity', 0, 2),
  //     lightFolder.addColor(this.state, 'ambientColor'),
  //     lightFolder.add(this.state, 'directIntensity', 0, 4), // TODO(#116)
  //     lightFolder.addColor(this.state, 'directColor')
  //   ].forEach((ctrl) => ctrl.onChange(() => this.updateLights()));
  //
  //   // Animation controls.
  //   this.animFolder = gui.addFolder('Animation');
  //   this.animFolder.domElement.style.display = 'none';
  //   const playbackSpeedCtrl = this.animFolder.add(this.state, 'playbackSpeed', 0, 1);
  //   playbackSpeedCtrl.onChange((speed) => {
  //     if (this.mixer) this.mixer.timeScale = speed;
  //   });
  //   this.animFolder.add({playAll: () => this.playAllClips()}, 'playAll');
  //
  //   // Morph target controls.
  //   this.morphFolder = gui.addFolder('Morph Targets');
  //   this.morphFolder.domElement.style.display = 'none';
  //
  //   // Camera controls.
  //   this.cameraFolder = gui.addFolder('Cameras');
  //   this.cameraFolder.domElement.style.display = 'none';
  //
  //   // Stats.
  //   const perfFolder = gui.addFolder('Performance');
  //   const perfLi = document.createElement('li');
  //   this.stats.dom.style.position = 'static';
  //   perfLi.appendChild(this.stats.dom);
  //   perfLi.classList.add('gui-stats');
  //   perfFolder.__ul.appendChild( perfLi );
  //
  //   const guiWrap = document.createElement('div');
  //   this.el.appendChild( guiWrap );
  //   guiWrap.classList.add('gui-wrap');
  //   guiWrap.appendChild(gui.domElement);
  //   gui.open();
  //
  // }
  //
  // updateGUI () {
  //   this.cameraFolder.domElement.style.display = 'none';
  //
  //   this.morphCtrls.forEach((ctrl) => ctrl.remove());
  //   this.morphCtrls.length = 0;
  //   this.morphFolder.domElement.style.display = 'none';
  //
  //   this.animCtrls.forEach((ctrl) => ctrl.remove());
  //   this.animCtrls.length = 0;
  //   this.animFolder.domElement.style.display = 'none';
  //
  //   const cameraNames = [];
  //   const morphMeshes = [];
  //   this.content.traverse((node) => {
  //     if (node.isMesh && node.morphTargetInfluences) {
  //       morphMeshes.push(node);
  //     }
  //     if (node.isCamera) {
  //       node.name = node.name || `VIEWER__camera_${cameraNames.length + 1}`;
  //       cameraNames.push(node.name);
  //     }
  //   });
  //
  //   if (cameraNames.length) {
  //     this.cameraFolder.domElement.style.display = '';
  //     if (this.cameraCtrl) this.cameraCtrl.remove();
  //     const cameraOptions = [DEFAULT_CAMERA].concat(cameraNames);
  //     this.cameraCtrl = this.cameraFolder.add(this.state, 'camera', cameraOptions);
  //     this.cameraCtrl.onChange((name) => this.setCamera(name));
  //   }
  //
  //   if (morphMeshes.length) {
  //     this.morphFolder.domElement.style.display = '';
  //     morphMeshes.forEach((mesh) => {
  //       if (mesh.morphTargetInfluences.length) {
  //         const nameCtrl = this.morphFolder.add({name: mesh.name || 'Untitled'}, 'name');
  //         this.morphCtrls.push(nameCtrl);
  //       }
  //       for (let i = 0; i < mesh.morphTargetInfluences.length; i++) {
  //         const ctrl = this.morphFolder.add(mesh.morphTargetInfluences, i, 0, 1, 0.01).listen();
  //         Object.keys(mesh.morphTargetDictionary).forEach((key) => {
  //           if (key && mesh.morphTargetDictionary[key] === i) ctrl.name(key);
  //         });
  //         this.morphCtrls.push(ctrl);
  //       }
  //     });
  //   }
  //
  //   if (this.clips.length) {
  //     this.animFolder.domElement.style.display = '';
  //     const actionStates = this.state.actionStates = {};
  //     this.clips.forEach((clip, clipIndex) => {
  //       clip.name = `${clipIndex + 1}. ${clip.name}`;
  //
  //       // Autoplay the first clip.
  //       let action;
  //       if (clipIndex === 0) {
  //         actionStates[clip.name] = true;
  //         action = this.mixer.clipAction(clip);
  //         action.play();
  //       } else {
  //         actionStates[clip.name] = false;
  //       }
  //
  //       // Play other clips when enabled.
  //       const ctrl = this.animFolder.add(actionStates, clip.name).listen();
  //       ctrl.onChange((playAnimation) => {
  //         action = action || this.mixer.clipAction(clip);
  //         action.setEffectiveTimeScale(1);
  //         playAnimation ? action.play() : action.stop();
  //       });
  //       this.animCtrls.push(ctrl);
  //     });
  //   }
  // }

clear() {

    if ( !this.content ) { return; }

    this.scene.remove( this.content );

    // dispose geometry
    this.content.traverse((node) => {
      if ( !node.isMesh ) { return; }
      node.geometry.dispose();
    } );

    // dispose textures
    traverseMaterials( this.content, (material) => {
      MAP_NAMES.forEach( (map) => {
        if (material[ map ]) { material[ map ].dispose(); }
      } );
    } );
  }

}

function traverseMaterials(object, callback) {
  object.traverse((node) => {
    if (!node.isMesh) { return; }
    const materials = Array.isArray(node.material)
        ? node.material
        : [node.material];
    materials.forEach(callback);
  });
}

function isIOS() {
  return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
      ].includes(navigator.platform)
      // iPad on iOS 13 detection
      || (navigator.userAgent.includes('Mac') && 'ontouchend' in document);
}
