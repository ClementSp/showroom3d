const attributes = [
  {
    type: 'video',
    icon: 'videocam-outline'
  },
  {
    type: 'picture',
    icon: 'image-outline'
  }
];

export default attributes;
