import {Component, ComponentFactoryResolver, Input, OnInit, Type, ViewChild} from '@angular/core';
import attributes from './../attributes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalController} from '@ionic/angular';
import {DynamicFormDirective} from '../../../../../directives/dynamic-form.directive';
import {MatHorizontalStepper} from '@angular/material/stepper';
import {IAttributeForm} from '../IAttributeForm';

@Component({
  selector: 'app-attribute-form',
  templateUrl: './add-attribute-form.component.html',
  styleUrls: ['./add-attribute-form.component.scss'],
})
export class AddAttributeFormComponent implements OnInit {

  @Input() formByAttributeTypeMap: Map<string, Type<any>>;

  @ViewChild(DynamicFormDirective, {static: true}) formHost: DynamicFormDirective;
  @ViewChild('stepper') stepper: MatHorizontalStepper;

  readonly attributes = attributes;
  typeFormGroup: FormGroup;
  dataFormGroup: FormGroup;
  isEditable = false;
  isDataFormValid = false;

  constructor(private formBuilder: FormBuilder,
              private componentFactoryResolver: ComponentFactoryResolver,
              private modalController: ModalController) {}

  ngOnInit() {
    this.typeFormGroup = this.formBuilder.group({
      type: ['', Validators.required]
    });
    this.dataFormGroup = this.formBuilder.group({});
  }

  confirmFirstStep() {
    const type = this.typeFormGroup.controls.type.value;
    const formItem = this.formByAttributeTypeMap.get(type);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(formItem);

    const viewContainerRef = this.formHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<IAttributeForm>(componentFactory);
    componentRef.instance.formGroup = this.dataFormGroup;
    componentRef.instance.dataUpdated.subscribe(() => {
      this.isDataFormValid = this.dataFormGroup.valid;
    });

    const element: HTMLElement = componentRef.location.nativeElement as HTMLElement;
    element.style.width = '100%';

    this.stepper.next();
  }

  back() {
    this.stepper.reset();
  }

  close() {
    this.modalController.dismiss();
  }

  confirm() {
    this.modalController.dismiss({
      type: this.typeFormGroup.controls.type.value,
      data: this.dataFormGroup.getRawValue()
    });
  }
}
