import {Component, Input, OnInit, Output, ViewChild, EventEmitter, AfterViewInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IAttributeForm} from '../IAttributeForm';
import {BehaviorSubject} from 'rxjs';
import {IonSelect, Platform} from '@ionic/angular';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {MediasService} from '../../../../../services/medias.service';

@Component({
  selector: 'app-video-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.scss'],
})
export class VideoFormComponent implements OnInit, AfterViewInit, IAttributeForm {

  @Input() formGroup: FormGroup;
  @Input() videoId: string = '';
  @Input() isMuted: boolean = false;
  @Output() dataUpdated = new EventEmitter<any>();

  @ViewChild('ionSelect') ionSelect: IonSelect;

  videoIdFormControl: FormControl;
  isMutedFormControl: FormControl;

  isUploading = new BehaviorSubject<boolean>(false);
  isLoading = new BehaviorSubject<boolean>(true);
  uploader: FileUploader;
  videos: Array<any> = [];

  constructor(private platform: Platform, private mediasService: MediasService) {
    this.uploader = new FileUploader({});
  }

  ngOnInit() {
    this.videoIdFormControl = new FormControl(this.videoId, [Validators.required]);
    this.isMutedFormControl = new FormControl(this.isMuted);
    this.formGroup.addControl('videoId', this.videoIdFormControl);
    this.formGroup.addControl('isMuted', this.isMutedFormControl);
  }

  ngAfterViewInit(): void {
    this.mediasService.getMediasByType('video').subscribe((videos) => {
      this.videos = videos;
      this.videoIdFormControl.setValue(this.videoId);
      this.isLoading.next(false);
    });
  }

  upload() {
    const fileItem: FileItem = this.uploader.queue[0];

    this.isUploading.next(true);
    this.mediasService.postMedia('video', fileItem._file, fileItem.file.name).subscribe((media) => {
      this.videos.push(media);
      this.ionSelect.value = media.id;
      this.uploader.queue = [];
      this.dataUpdated.emit();
      this.isUploading.next(false);
    });
  }

  onUrlChanged($event: any) {
    const video = $event.detail.value;

    this.videoIdFormControl.setValue(video);
    this.dataUpdated.emit();
  }

  isMutedControlChanged(value: boolean) {
    this.dataUpdated.emit();
  }

  isMobile() {
    return this.platform.is('mobile');
  }
}
