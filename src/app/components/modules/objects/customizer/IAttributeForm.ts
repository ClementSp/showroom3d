import {EventEmitter} from '@angular/core';

export interface IAttributeForm {
    formGroup: any;
    dataUpdated: EventEmitter<any>;
}
