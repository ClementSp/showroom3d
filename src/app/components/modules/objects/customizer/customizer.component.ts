import {Component, OnDestroy, OnInit, EventEmitter, Input} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {EditorService} from '../../../../services/editor.service';
import * as THREE from 'three';
import {ModulesService} from '../../../../services/modules.service';
import {BehaviorSubject, Subject} from 'rxjs';
import attributes from './attributes';
import {TranslateService} from '@ngx-translate/core';
import {AddAttributeFormComponent} from './add-attribute-form/add-attribute-form.component';
import {FixturesManagerService} from '../../../../services/modules/fixtures-manager.service';
import {MediasService} from '../../../../services/medias.service';
import {FormBuilder} from '@angular/forms';
import {VideoFormComponent} from './video-form/video-form.component';
import {FixturesService} from '../../../../services/fixtures.service';
import {FixtureAction} from '../../../../common/FixtureAction';
import {PictureFormComponent} from './picture-form/picture-form.component';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {Showroom} from '../../../../models/showroom.model';
import {showroomEnv} from "../../../../../environments/showroom.env";
import {SceneHandler} from "../../../../DataHandlers/scene.handler";

@Component({
  selector: 'app-customizer',
  templateUrl: './customizer.component.html',
  styleUrls: ['./customizer.component.scss'],
})
export class CustomizerComponent implements OnInit, OnDestroy {

  fixtures = [];
  fixture: THREE.Group;
  attributes: Array<any>;
  refreshVisualizer = new EventEmitter();
  formByAttributeTypeMap = new Map();
  type = 'FIXTURE';
  isReady: boolean;
  glbUrl: string;

  readonly defaultAttributeIconName = 'information-outline';

  constructor(private modalController: ModalController,
              private formBuilder: FormBuilder,
              private showroomService: ShowroomsService,
              private fixturesManagerService: FixturesManagerService,
              private fixtureService: FixturesService,
              public translateService: TranslateService,
              private mediasService: MediasService,
              private modulesService: ModulesService,
              private editorService: EditorService,
              private sceneHandler: SceneHandler) {
    this.formByAttributeTypeMap.set('video', VideoFormComponent);
    this.formByAttributeTypeMap.set('picture', PictureFormComponent);
  }

  ngOnInit() {
    this.fixture = (this.editorService.attachedObject as THREE.Group);
    this.getFixtureAttributes();
    this.fixtureService.getFixture(this.fixture.userData.fixtureId).subscribe(fixture => {
      this.glbUrl = fixture.gltf;
      this.isReady = true;
    });
  }

  close() {
    this.sceneHandler.updateAttributeOfFixture({
      attributes: this.attributes,
      fixture: this.fixture
    });

    this.modalController.dismiss();
  }

  ngOnDestroy(): void {
  }

  async openAttributeFormModal() {
    const modal = await this.modalController.create({
      component: AddAttributeFormComponent,
      cssClass: 'fit-content-modal backdrop-05',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        formByAttributeTypeMap: this.formByAttributeTypeMap
      }
    });
    modal.onDidDismiss().then((response) => {
      if (response.data) {
        let attribute = response.data;
        const fixtureId = this.fixture.userData.id;

        this.fixturesManagerService.addAttribute(fixtureId, attribute).subscribe((response) => {
          attribute = response;
          this.attributes.push(attribute);
          this.refreshVisualizer.emit(this.attributes);
        });
      }
    });
    return await modal.present();
  }

  async openAttributeEditorModal(attribute: any) {
    const formGroup = this.formBuilder.group({});
    const fixtureId = this.fixture.userData.id;

    if (!this.formByAttributeTypeMap.get(attribute.type)) {
      return;
    }
    const form = this.formByAttributeTypeMap.get(attribute.type);
    const modal = await this.modalController.create({
      component: form,
      cssClass: 'fit-content-modal backdrop-05',
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        formGroup,
        ...attribute.data
      }
    });
    modal.onDidDismiss().then(() => {
      attribute.data = formGroup.getRawValue();
      this.fixturesManagerService.updateAttribute(fixtureId, attribute).subscribe(() => {
        this.refreshVisualizer.emit(this.attributes);
      });
    });
    return await modal.present();
  }

  getIconNameFromAttributeType(attributeType: string) {
    const attribute = attributes.find(a => a.type === attributeType);

    if (attribute) {
      return attribute.icon;
    }
    return this.defaultAttributeIconName;
  }

  deleteAttribute(attributeId: string) {
    const fixtureId = this.fixture.userData.id;

    this.fixturesManagerService.deleteAttribute(fixtureId, attributeId).subscribe(() => {
      this.attributes = this.attributes.filter(attr => attr.id !== attributeId);
      this.refreshVisualizer.emit(this.attributes);
    });
  }

  private getFixtureAttributes() {
    this.fixtures = showroomEnv.showroom.fixtures;
    const fixtureConfig = this.fixtures.find(m => m.id === this.fixture.userData.id);
    if (fixtureConfig) {
      this.attributes = fixtureConfig.attributes || [];
    } else {
      this.attributes = [];
    }
  }

  moveFixture() {
    return this.modalController.dismiss({action: FixtureAction.MOVE, portal: this.fixture});
  }

  removeFixture() {
    return this.modalController.dismiss({action: FixtureAction.REMOVE, portal: this.fixture});
  }
}
