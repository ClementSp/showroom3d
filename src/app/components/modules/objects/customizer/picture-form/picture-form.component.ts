import {Component, Input, OnInit, Output, ViewChild, EventEmitter, AfterViewInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IAttributeForm} from '../IAttributeForm';
import {BehaviorSubject} from 'rxjs';
import {IonSelect, Platform} from '@ionic/angular';
import {FileItem, FileUploader} from 'ng2-file-upload';
import {MediasService} from '../../../../../services/medias.service';

@Component({
  selector: 'app-picture-form',
  templateUrl: './picture-form.component.html',
  styleUrls: ['./picture-form.component.scss'],
})
export class PictureFormComponent implements OnInit, AfterViewInit, IAttributeForm {

  @Input() formGroup: FormGroup;
  @Input() pictureId: string = '';
  @Output() dataUpdated = new EventEmitter<any>();

  @ViewChild('ionSelect') ionSelect: IonSelect;

  pictureIdFormControl: FormControl;

  isUploading = new BehaviorSubject<boolean>(false);
  isLoading = new BehaviorSubject<boolean>(true);
  uploader: FileUploader;
  pictures: Array<any> = [];

  constructor(private platform: Platform, private mediasService: MediasService) {
    this.uploader = new FileUploader({});
  }

  ngOnInit() {
    this.pictureIdFormControl = new FormControl(this.pictureId, [Validators.required]);
    this.formGroup.addControl('pictureId', this.pictureIdFormControl);
  }

  ngAfterViewInit(): void {
    this.mediasService.getMediasByType('picture').subscribe((pictures) => {
      this.pictures = pictures;
      this.pictureIdFormControl.setValue(this.pictureId);
      this.isLoading.next(false);
    });
  }

  upload() {
    const fileItem: FileItem = this.uploader.queue[0];

    this.isUploading.next(true);
    this.mediasService.postMedia('picture', fileItem._file, fileItem.file.name).subscribe((media) => {
      this.pictures.push(media);
      this.ionSelect.value = media.id;
      this.uploader.queue = [];
      this.dataUpdated.emit();
      this.isUploading.next(false);
    });
  }

  onUrlChanged($event: any) {
    const picture = $event.detail.value;

    this.pictureIdFormControl.setValue(picture);
    this.dataUpdated.emit();
  }

  isMutedControlChanged(value: boolean) {
    this.dataUpdated.emit();
  }

  isMobile() {
    return this.platform.is('mobile');
  }
}
