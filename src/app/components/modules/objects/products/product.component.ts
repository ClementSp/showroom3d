import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChange,
    SimpleChanges
} from '@angular/core';
import {IProduct, IProductAssetData} from '../../../../models/product.model';
import * as THREE from 'three';
import {EditorMode} from '../../../../models/EditorMode';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {Subscription} from 'rxjs';
import {Showroom} from '../../../../models/showroom.model';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {ProductsManagerService} from '../../../../services/modules/products-manager.service';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {showroomEnv} from '../../../../../environments/showroom.env';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit, OnDestroy {
    @Input() object: any ;
    @Input() objectId: string;
    @Input() objectName: string;
    @Input() data: IProductAssetData ;
    @Input() showroom: Showroom ;
    @Output() onExitOrPlacement = new EventEmitter<void>();
    onProductPlaced: Subscription;

    constructor( private editorService: EditorService,
                 private toastService: ToastService,
                 private engineService: EngineService,
                 private currentNode: CurrentNodeHandler,
                 private productsManagerService: ProductsManagerService,
                 public sceneHandler: SceneHandler) { }

    ngOnInit() {
        this.initProductPlacement(this.object);
        this.onProductPlacement(this.objectId, this.data);
    }

    private async initProductPlacement(product: THREE.Group) {
        this.editorService.mode = EditorMode.PLACING_PRODUCT;
        this.editorService.currentlyPlacingProduct = product;
        // this.displayProductPlacementToast(product).then();
    }

    private onProductPlacement(id: string, productData: IProductAssetData) {
        const position = this.editorService.currentlyPlacingProduct.position;
        if (this.onProductPlaced) {
            this.onProductPlaced.unsubscribe();
        }
        this.onProductPlaced = this.editorService.onPlacedProduct.subscribe(() => {
            this.toastService.dismiss();
            this.pushProduct(id, position, productData).subscribe(
                ((product: IProduct) => {
                    showroomEnv.showroom.products.push(product);
                    this.onProductPlaced.unsubscribe();
                    this.editorService.currentlyPlacingProduct.userData.productId = product.productId;
                    this.editorService.currentlyPlacingProduct.userData.id = product.id;
                    this.editorService.currentlyPlacingProduct.traverse((child) => {
                        child.userData.parentId = product.id;
                        child.userData.ean = product.ean;
                    });
                    this.onExitOrPlacement.emit();
                })
            );
        });
    }

    private displayProductPlacementToast(product: THREE.Group) {
        return this.toastService.presentToast(ToastType.NORMAL, 'Click anywhere to add the product',
            0, true, () => this.onCancelProductPlacement(product));
    }

    private onCancelProductPlacement(product: THREE.Group) {
        this.object = undefined;
        this.objectId = '';
        this.objectName = '';
        this.data = undefined;
        this.editorService.target = undefined;
        this.editorService.currentlyPlacingProduct = undefined;
        this.sceneHandler.removeObjFromScene(product);
        this.editorService.currentMode.next(EditorMode.NONE);
        this.onExitOrPlacement.emit();
    }


    private pushProduct(id: string, position: { x: number; y: number; z: number }, productData: IProductAssetData) {
        return this.productsManagerService.addProduct({
            id,
            position: {x: position.x, y: position.y, z: position.z},
            scale: {x: 1, y: 1, z: 1},
            attributes: [],
            rotation: {x: 0, y: 0, z: 0},
            productId: productData.id,
            product_glbModelUrl: productData.product_glbModelUrl
        });
    }

    ngOnDestroy() {
        if (this.onProductPlaced) {
            this.onProductPlaced.unsubscribe();
        }
    }
}
