import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {EditorMode} from '../../../../models/EditorMode';
import * as THREE from 'three';
import {ModalController} from '@ionic/angular';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {showroomEnv} from '../../../../../environments/showroom.env';
import {Showroom} from '../../../../models/showroom.model';

@Component({
  selector: 'app-point-light',
  templateUrl: './point-light.component.html',
  styleUrls: ['./point-light.component.scss'],
})
export class PointLightComponent implements OnInit {
  @Input() object: any ;
  @Input() objectId: string;
  @Input() showroom: Showroom ;
  @Output() onExitOrPlacement = new EventEmitter<void>();
  onPointLightPlaced: Subscription;

  constructor( private editorService: EditorService,
               private toastService: ToastService,
               private engineService: EngineService,
               private currentNode: CurrentNodeHandler,
               private modalController: ModalController,
               private showroomService: ShowroomsService,
               public sceneHandler: SceneHandler) { }
  ngOnInit() {
    this.initPointLightPlacement(this.object);
    this.onPointLightPlacement(this.objectId);
  }

  private initPointLightPlacement(pointLight: any) {
    this.editorService.mode = EditorMode.ADD_POINT_LIGHT;
    this.editorService.currentlyPlacingPointLight = pointLight;
  }

  onPointLightPlacement(id: string) {
    if (this.onPointLightPlaced) {
      this.onPointLightPlaced.unsubscribe();
    }
    this.onPointLightPlaced = this.editorService.onPlacedPointLight.subscribe(() => {
      this.toastService.dismiss();
      this.pushPointLight(id).subscribe(pointLight => {
        showroomEnv.showroom.lights.push(this.editorService.currentlyPlacingPointLight);
        this.onPointLightPlaced.unsubscribe();
        this.onExitOrPlacement.emit();
      });
    });
  }

  private pushPointLight(id) {
    return this.showroomService.putLight({
      position: this.editorService.currentlyPlacingPointLight.position,
      tip: '',
      nodeId: undefined,
      type: 'POINT_LIGHT',
      id
    });
  }


close() {
    this.modalController.dismiss().then();
  }
}
