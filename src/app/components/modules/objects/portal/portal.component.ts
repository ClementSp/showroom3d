import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Subscription} from 'rxjs';
import {EditorService} from '../../../../services/editor.service';
import {ToastService, ToastType} from '../../../../services/toast.service';
import {EngineService} from '../../../../services/engine.service';
import {CurrentNodeHandler} from '../../../../DataHandlers/CurrentNode.handler';
import {EditorMode} from '../../../../models/EditorMode';
import * as THREE from 'three';
import {ModalController} from '@ionic/angular';
import {ShowroomsService} from '../../../../services/showrooms.service';
import {SceneHandler} from '../../../../DataHandlers/scene.handler';
import {showroomEnv} from '../../../../../environments/showroom.env';
import {Showroom} from '../../../../models/showroom.model';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss'],
})
export class PortalComponent implements OnInit {
  @Input() object: any ;
  @Input() objectId: string;
  @Input() showroom: Showroom ;
  @Output() onExitOrPlacement = new EventEmitter<void>();
  onPortalPlaced: Subscription;

  constructor( private editorService: EditorService,
               private toastService: ToastService,
               private engineService: EngineService,
               private currentNode: CurrentNodeHandler,
               private modalController: ModalController,
               private showroomService: ShowroomsService,
               public sceneHandler: SceneHandler) { }
  ngOnInit() {
    this.initPortalPlacement(this.object);
    this.onPortalPlacement(this.objectId);
  }

  private initPortalPlacement(portal: any) {
    this.editorService.mode = EditorMode.ADD_PORTAL_MARKERS;
    this.editorService.currentlyPlacingPortal = portal;
    // this.displayPortalPlacementToast(portal).then();
  }

  onPortalPlacement(id: string) {
    if (this.onPortalPlaced) {
      this.onPortalPlaced.unsubscribe();
    }
    this.onPortalPlaced = this.editorService.onPlacedPortal.subscribe(() => {
      this.toastService.dismiss();
      this.pushPortal(id).subscribe(portal => {
        showroomEnv.showroom.portals.push(portal);
        this.onPortalPlaced.unsubscribe();
        this.editorService.currentlyPlacingPortal.userData.id = portal.id;
        this.editorService.currentlyEditingPortal  = portal;
        this.editorService.currentlyPlacingPortal.traverse((child) => {
          child.userData.parentId = portal.id;
          child.userData.id = portal.id;
        });
        this.onExitOrPlacement.emit();
      });
    });
  }

  displayPortalPlacementToast(shapePortal: THREE.Group) {
      return this.toastService.presentToast(ToastType.NORMAL, 'Click anywhere to add the  Portal',
          0, true, () => this.onCancelPortalPlacement(shapePortal));
  }

  private onCancelPortalPlacement(shapePortal: THREE.Group) {
    this.object = undefined;
    this.objectId = '';
    this.editorService.target = undefined;
    this.editorService.currentlyPlacingFixture = undefined;
    this.sceneHandler.removeObjFromScene(shapePortal);
    this.editorService.mode = EditorMode.NONE;
    this.onExitOrPlacement.emit();
  }

  private pushPortal(id) {
    return this.showroomService.putPortal({
      position: this.editorService.currentlyPlacingPortal.position,
      scale: this.editorService.currentlyPlacingPortal.scale,
      rotation: {
        _x: this.editorService.currentlyPlacingPortal.rotation.x,
        _y: this.editorService.currentlyPlacingPortal.rotation.y,
        _z: this.editorService.currentlyPlacingPortal.rotation.z
      },
      tip: '',
      nodeId: undefined,
      type: undefined,
      id
    });
  }


close() {
    this.modalController.dismiss().then();
  }
}
