import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ObjectImporterComponent} from './object-importer.component';
import {IonicModule} from '@ionic/angular';
import {FixtureModalComponent} from './main-display/fixture-modal.component';
import {FixtureCardComponent} from './fixture-card/fixture-card.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {TranslateModule} from '@ngx-translate/core';
import {CustomizerComponent} from './customizer/customizer.component';
import {VisualizerComponent} from './visualizer/visualizer.component';
import {AddAttributeFormComponent} from './customizer/add-attribute-form/add-attribute-form.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {VideoFormComponent} from './customizer/video-form/video-form.component';
import {DynamicFormDirective} from '../../../directives/dynamic-form.directive';
import {SharedModule} from '../../../shared.module';
import {FileUploadModule} from 'ng2-file-upload';
import {PictureFormComponent} from './customizer/picture-form/picture-form.component';
import {ObjectSelectorComponent} from './object-selector/object-selector.component';
import {ProductModalComponent} from './product-modal/product-modal.component';
import {ProductCardComponent} from './product-card/product-card.component';
import {ProductComponent} from './products/product.component';
import {SmartObjectCardComponent} from './smart-object-card/smart-object-card.component';
import {SmartObjectsComponent} from './smart-objects/smart-objects.component';
import {FixturesComponent} from './fixtures/fixtures.component';
import {ProductEditModalComponent} from './product-edit-modal/product-edit-modal.component';
import {ShapePoiComponent} from './shapePio/shape-poi.component';
import {SmartObjectModalComponent} from './smart-object-modal/smart-object-modal.component';
import {SmartObjectEditModalComponent} from './smart-object-edit-modal/smart-object-edit-modal.component';
import {FolderCardComponent} from './folder-card/folder-card.component';
import {AddProductClientModeComponent} from './add-product-client-mode/add-product-client-mode.component';
import {PoiComponent} from './poi/poi.component';
import {PortalComponent} from './portal/portal.component';
import {LightSelectorComponent} from './light-selector/light-selector.component';
import {PointLightComponent} from './point-light/point-light.component';
import {SpotLightComponent} from './spot-light/spot-light.component';

@NgModule({
    declarations: [
        ObjectImporterComponent,
        FixtureModalComponent,
        ObjectSelectorComponent,
        ProductComponent,
        ProductModalComponent,
        SmartObjectCardComponent,
        PoiComponent,
        ShapePoiComponent,
        PortalComponent,
        SmartObjectsComponent,
        SmartObjectModalComponent,
        SmartObjectEditModalComponent,
        FixturesComponent,
        ProductEditModalComponent,
        FixtureCardComponent,
        ProductCardComponent,
        FolderCardComponent,
        CustomizerComponent,
        VisualizerComponent,
        AddAttributeFormComponent,
        VideoFormComponent,
        PictureFormComponent,
        DynamicFormDirective,
        LightSelectorComponent,
        PointLightComponent,
        SpotLightComponent,
        AddProductClientModeComponent
    ],
    exports: [ObjectImporterComponent, DynamicFormDirective, ShapePoiComponent, PoiComponent, VisualizerComponent, AddProductClientModeComponent],
    imports: [
        CommonModule,
        IonicModule,
        FlexLayoutModule,
        TranslateModule,
        MatStepperModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        SharedModule,
        FileUploadModule,
        FormsModule
    ]
})
export class ObjectImporterModule { }
