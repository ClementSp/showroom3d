import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {ModalController, PopoverController} from '@ionic/angular';
import {FixtureModalComponent} from './../objects/main-display/fixture-modal.component';
import {IFixture, IFixtureAssetData} from '../../../models/fixture.model';
import {BehaviorSubject, Subscription} from 'rxjs';
import {Showroom} from '../../../models/showroom.model';
import {ModulesService} from '../../../services/modules.service';
import {showroomEnv} from '../../../../environments/showroom.env';
import {uuid4} from '@capacitor/core/dist/esm/util';
import {ObjectSelectorComponent} from './object-selector/object-selector.component';
import {ProductModalComponent} from './product-modal/product-modal.component';
import {IProduct, IProductAssetData} from '../../../models/product.model';
import {SmartObjectModalComponent} from './smart-object-modal/smart-object-modal.component';
import {ISmartObject, ISmartObjectAssetData} from '../../../models/smartObject.model';
import {SceneHandler} from '../../../DataHandlers/scene.handler';
import {EditorMode} from '../../../models/EditorMode';
import {EditorService} from '../../../services/editor.service';
import {DropdownPoiTypeComponentComponent} from '../../../views/home/dropdown-poi-type-component/dropdown-poi-type-component.component';
import {LightSelectorComponent} from './light-selector/light-selector.component';

@Component({
  selector: 'app-object-importer',
  templateUrl: './object-importer.component.html',
  styleUrls: ['./object-importer.component.scss'],
})
export class ObjectImporterComponent implements OnInit, OnDestroy, OnChanges {
  static moduleName = 'fixturesManager';

  @Input() readonly engineIsLoading$ = new BehaviorSubject<boolean>(false);
  @Input() readonly shellIsValid$ = new BehaviorSubject<boolean>(false);
  @Input() hasToCloseObjectImporter: boolean;
  @Input() showroom: Showroom;
  @Output() onObjectPlacement = new EventEmitter<boolean>();

  enabled = false;
  onFixturePlaced: Subscription;
  objectName: any;
  object: any;
  objectId: string;
  data: any;
  isReady: boolean;

  constructor(private modalController: ModalController,
              private popoverController: PopoverController,
              private modulesService: ModulesService,
              private editorService: EditorService,
              public sceneHandler: SceneHandler) { }

  get showroomEnv() {
    return showroomEnv;
  }

  private static getDefaultProduct(productData: IProductAssetData): IProduct {
    return {
      ean: productData.product_ean,
      product_glbModelUrl: productData.product_glbModelUrl,
      poiId: '',
      id: '',
      position: {x: 0, y: 0, z: 0},
      scale: {x: 1, y: 1, z: 1},
      rotation: {x: 0, y: 0, z: 0},
      productId: productData.id,
      components: productData.components,
      template: ''
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasToCloseObjectImporter) {
      this.closeObjectImporter();
    }
  }

  private static getDefaultSmartObject(smartObjectData: ISmartObjectAssetData): ISmartObject {
    return {
      smartObjectId: smartObjectData.id,
      smart_objects_glbModelUrl: smartObjectData.smart_objects_glbModelUrl,
      id: '',
      position: {x: 0, y: 0, z: 0},
      scale: {x: 1, y: 1, z: 1},
      rotation: {x: 0, y: 0, z: 0}
    };
  }

  private static getDefaultFixture(fixtureData: any) {
    return {
      id: '',
      gltf: fixtureData.gltf,
      position: {x: 0, y: 0, z: 0},
      scale: {x: 1, y: 1, z: 1},
      attributes: [],
      rotation: {x: 0, y: 0, z: 0},
      fixtureId: fixtureData.id
    };
  }

  ngOnInit() {
    this.modulesService.getModules().then(modules => {
      modules.forEach(mod => {
        if (mod.name === ObjectImporterComponent.moduleName) {
          this.enabled = mod.enabled;
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (this.onFixturePlaced) {
      this.onFixturePlaced.unsubscribe();
    }
  }

  async showPoiSubMenu(myEvent) {
    const popover = await this.popoverController.create({
      component: DropdownPoiTypeComponentComponent,
      translucent: true,
      event: myEvent
    });
    popover.onWillDismiss().then((result) => {
      if (result && result.data) {
        this.toggleFeature(result.data);
        this.isReady = true;
      }
    });

    return await popover.present();
  }

  async addPortal() {
    this.toggleFeature('PORTAL');
    this.isReady = true;
  }

  async addPortalMarkers() {
    const id = uuid4();
    this.objectName = 'Portal';
    this.sceneHandler.loadPortalInScene(id).then((returnedPortal) => {
      this.isReady = false;
      this.object = returnedPortal;
      this.objectId = id;
      this.isReady = true;
    });
  }

  async addPointLight() {
    const id = uuid4();
    this.objectName = 'PointLight';
    this.sceneHandler.loadPointLightInScene(id).then((returnedPointLight) => {
      this.isReady = false;
      this.object = returnedPointLight;
      this.objectId = id;
      this.isReady = true;
    });
  }

  async addSpotLight() {
    const id = uuid4();
    this.objectName = 'SpotLight';
    this.sceneHandler.loadSpotLightInScene(id).then((returnedSpotLight) => {
      this.isReady = false;
      this.object = returnedSpotLight;
      this.objectId = id;
      this.isReady = true;
    });
  }

  async openSelectObject(myEvent) {
    const popover = await this.popoverController.create({
      component: ObjectSelectorComponent,
      translucent: true,
      event: myEvent
    });
    popover.onWillDismiss().then((result) => {
      if (result && result.data) {
        this.objectName = result.data;
        this.toggleFeature(result.data);
      }
    });

    return await popover.present();
  }

  async openSelectLight(myEvent) {
    const popover = await this.popoverController.create({
      component: LightSelectorComponent,
      translucent: true,
      event: myEvent
    });
    popover.onWillDismiss().then((result) => {
      if (result && result.data) {
        this.objectName = result.data;
        this.toggleFeature(result.data);
      }
    });

    return await popover.present();
  }

  async openProductModal() {
    const modal = await this.modalController.create({
      component: ProductModalComponent,
      showBackdrop: true,
      cssClass: 'fullscreen-modal',
      backdropDismiss: true,
      componentProps: {
        multipleSelect: false
      }
    });
    modal.onDidDismiss().then(async (response) => {
      if (!response.data) {
        this.isReady = false;
        return;
      }
      const productData: IProductAssetData = response.data;
      const product: IProduct = ObjectImporterComponent.getDefaultProduct(productData);
      if (productData && product) {
        const id = uuid4();
        const returnProduct = await this.sceneHandler.placeGltfInScene(id, productData.product_glbModelUrl, product);
        this.object = returnProduct;
        this.objectId = id;
        this.data = productData;
        this.isReady = true;
      }
    });
    return await modal.present().then();
  }

  async openFixtureModal() {
    const modal = await this.modalController.create({
      component: FixtureModalComponent,
      showBackdrop: true,
      cssClass: 'fullscreen-modal',
      backdropDismiss: true,
      componentProps: {
        multipleSelect: false
      }
    });
    modal.onDidDismiss().then(async (response) => {
      if (!response.data) {
        this.isReady = false;
        return;
      }
      const fixtureData: IFixtureAssetData = response.data;
      const fixture: IFixture = ObjectImporterComponent.getDefaultFixture(fixtureData);
      if (fixtureData) {
        const id = uuid4();
        const returnFixture = await this.sceneHandler.placeGltfInScene(id, fixtureData.gltf, fixture);
        this.isReady = false;
        this.object = returnFixture;
        this.objectId = id;
        this.data = fixtureData;
        this.isReady = true;
        // this.initFixturePlacement(returnFixture);
        // this.onFixturePlacement(id, fixtureData);
      }
    });
    return await modal.present().then();
  }

  async openSmartObjectModal() {
    const modal = await this.modalController.create({
      component: SmartObjectModalComponent,
      showBackdrop: true,
      cssClass: 'fullscreen-modal',
      backdropDismiss: true,
      componentProps: {
        multipleSelect: false
      }
    });
    modal.onDidDismiss().then(async (response) => {
      if (!response.data) {
        this.isReady = false;
        return;
      }
      const smartObjectData: ISmartObjectAssetData = response.data;
      const product: ISmartObject = ObjectImporterComponent.getDefaultSmartObject(smartObjectData);
      if (smartObjectData) {
        const id = uuid4();
        const returnSmartObject = await this.sceneHandler.placeGltfInScene(id, smartObjectData.smart_objects_glbModelUrl, product);
        this.object = returnSmartObject;
        this.objectId = id;
        this.data = smartObjectData;
        this.isReady = true;
      }
    });
    return await modal.present().then();
  }

   toggleFeature(data: any) {
    this.onObjectPlacement.emit(true);
    switch (data) {
      case 'FIXTURE':
        this.openFixtureModal().then();
        break;
      case 'PRODUCT':
        this.openProductModal().then();
        break;
      case 'SMART_OBJECT':
        this.openSmartObjectModal().then();
        break;
      case 'poi':
        this.addPoiMarkers().then();
        this.editorService.currentlyPlacingPoi = this.object;
        break;
      case 'shape-poi':
        this.addShapePoiMarkers().then();
        this.editorService.currentlyPlacingShapePoi = this.object;
        break;
      case 'PORTAL':
        this.addPortalMarkers().then();
        this.editorService.currentlyPlacingPortal = this.object;
        break;
      case 'POINT_LIGHT':
        this.addPointLight().then();
        this.editorService.currentlyPlacingPointLight = this.object;
        break;
      case 'SPOT_LIGHT':
        this.addSpotLight().then();
        this.editorService.currentlyPlacingSpotLight = this.object;
        break;
    }
  }

  async addPoiMarkers() {
    const id = uuid4();
    this.objectName = 'Poi';
    this.sceneHandler.loadPoiInScene(id).then((returnedPoi) => {
      this.isReady = false;
      this.object = returnedPoi;
      this.objectId = id;
      this.isReady = true;
    });
  }

  async addShapePoiMarkers() {
    const id = uuid4();
    this.objectName = 'ShapePoi';
    this.sceneHandler.loadShapePioInScene(id).then((returnedPoi) => {
      this.isReady = false;
      this.object = returnedPoi;
      this.objectId = id;
      this.isReady = true;
    });
  }

  closeObjectImporter() {
    this.sceneHandler.removeObjFromScene(this.object);
    this.object = undefined;
    this.objectId = '';
    this.objectName = '';
    this.data = undefined;
    this.editorService.target = undefined;
    this.editorService.currentlyPlacingFixture = undefined;
    this.editorService.mode = EditorMode.NONE;
    this.onObjectPlacement.emit(false);
  }

  close() {}
}
