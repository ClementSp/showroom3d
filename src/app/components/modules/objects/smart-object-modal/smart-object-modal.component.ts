import { Component, OnInit } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ModalController} from '@ionic/angular';
import {IProductAssetData} from '../../../../models/product.model';
import {SmartObjectsService} from '../../../../services/smart-object.service';
import {ISmartObjectAssetData} from '../../../../models/smartObject.model';

@Component({
  selector: 'app-smart-object-modal',
  templateUrl: './smart-object-modal.component.html',
  styleUrls: ['./smart-object-modal.component.scss'],
})
export class SmartObjectModalComponent implements OnInit {

  smartObject = new BehaviorSubject<Array<IProductAssetData>>([]);
  isLoading: boolean;

  constructor(private smartObjectService: SmartObjectsService,
              public translateService: TranslateService,
              private modalController: ModalController) {
     this.smartObjectService.getAllSmartObjects().then(smartObjects => {
       this.isLoading = true ;
       this.smartObject.next([...smartObjects]);
       this.isLoading = false ;
     });
  }

  ngOnInit() {
  }

  close() {
    this.modalController.dismiss().then();
  }

  smartObjectPlacement(smartObjectAssetData: ISmartObjectAssetData) {
    this.modalController.dismiss(smartObjectAssetData).then();
  }

}
