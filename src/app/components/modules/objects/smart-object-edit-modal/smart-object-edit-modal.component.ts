import {Component, OnDestroy, OnInit, EventEmitter, Input} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {EditorService} from '../../../../services/editor.service';
import * as THREE from 'three';
import {BehaviorSubject, Subject} from 'rxjs';
import {FormBuilder} from '@angular/forms';
import {ProductAction} from '../../../../common/ProductAction';
import {SmartObjectsService} from '../../../../services/smart-object.service';

@Component({
  selector: 'app-smart-object-edit',
  templateUrl: './smart-object-edit-modal.component.html',
  styleUrls: ['./smart-object-edit-modal.component.scss'],
})
export class SmartObjectEditModalComponent implements OnInit, OnDestroy {

  products = [];
  smartObject: THREE.Group;
  visualizerIsLoaded = new BehaviorSubject<boolean>(false);
  type = 'SMART_OBJECT';
  glbUrl: any;
  isReady: any;

  constructor(private modalController: ModalController,
              private formBuilder: FormBuilder,
              private smartObjectService: SmartObjectsService,
              private editorService: EditorService) {}

  ngOnInit() {
    this.smartObject = (this.editorService.attachedObject as THREE.Group);
    this.smartObjectService.getSmartObjectById(this.smartObject.userData.smartObjectId).then(smartObject => {
      this.glbUrl = smartObject.smart_objects_glbModelUrl;
      this.isReady = true;
    });
  }

  close() {
    this.modalController.dismiss();
  }

  ngOnDestroy(): void {
  }

  moveFixture() {
    return this.modalController.dismiss({action: ProductAction.MOVE, portal: this.smartObject});
  }

  removeFixture() {
    return this.modalController.dismiss({action: ProductAction.REMOVE, portal: this.smartObject});
  }
}
