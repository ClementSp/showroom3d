import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FixturesService} from '../../../../services/fixtures.service';
import {BehaviorSubject} from 'rxjs';
import {IFixtureAssetData} from '../../../../models/fixture.model';
import {ModalController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import ToolbarFeature from "../../../../models/ToolbarFeature";
import {EditorMode} from "../../../../models/EditorMode";
import {ToolbarComponent} from "../../../toolbar/toolbar.component";

@Component({
    selector: 'app-fixture-modal',
    templateUrl: './fixture-modal.component.html',
    styleUrls: ['./fixture-modal.component.scss'],
})
export class FixtureModalComponent implements OnInit, AfterViewInit {

    fixtures = new BehaviorSubject<Array<IFixtureAssetData>>([]);

    constructor(private fixtureService: FixturesService,
                public translateService: TranslateService,
                private modalController: ModalController) {
        this.fixtureService.getAllFixtures().subscribe((fixtures) => {
            this.fixtures.next([...fixtures]);
        });
    }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
    }

    close() {
        this.modalController.dismiss();
    }

    fixturePlacement(fixtureData: IFixtureAssetData) {
        this.modalController.dismiss(fixtureData);
    }
}
