import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JitsiB2BComponent } from './jitsi-b2-b.component';

describe('JitsiB2BComponent', () => {
  let component: JitsiB2BComponent;
  let fixture: ComponentFixture<JitsiB2BComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JitsiB2BComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JitsiB2BComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
