import {Component, Input} from '@angular/core';
import {JitsiParticipant, JitsiService} from '../../../../services/jitsi.service';

@Component({
  selector: 'app-video-call',
  templateUrl: './video-call.component.html',
  styleUrls: ['./video-call.component.scss'],
})
export class VideoCallComponent {
  @Input() participant: JitsiParticipant;

  constructor(public jitsiService: JitsiService) { }

  isVideoMuted() {
    return this.participant.tracks.video.isMuted();
  }

  isAudioMuted() {
    return this.participant.tracks.audio.isMuted();
  }

}
