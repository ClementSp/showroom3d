import { Component } from '@angular/core';
import {JitsiService} from '../../../../services/jitsi.service';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-settings-call',
  templateUrl: './settings-call.component.html',
  styleUrls: ['./settings-call.component.scss'],
})
export class SettingsCallComponent {
  constructor(public jitsiService: JitsiService,
              private modalController: ModalController) { }

  changeAudioInput(event) {
    this.jitsiService.setCurrentAudioInputDevice(event.detail.value);
  }

  changeVideoInput(event) {
    this.jitsiService.setCurrentVideoInputDevice(event.detail.value);
  }

  changeAudioOutput(event) {
    this.jitsiService.setCurrentAudioOutputDevice(event.detail.value);
  }

  close() {
    return this.modalController.dismiss();
  }
}
