import {Component, ViewChild} from '@angular/core';
import {ModulesService} from '../../../services/modules.service';
import {JitsiCallStatus, JitsiParticipant, JitsiService} from '../../../services/jitsi.service';
import {userEnv} from '../../../../environments/user.env';

@Component({
  selector: 'app-jitsi-b2-b',
  templateUrl: './jitsi-b2-b.component.html',
  styleUrls: ['./jitsi-b2-b.component.scss'],
})
export class JitsiB2BComponent {
  @ViewChild('callWindow') callWindow: any;
  moduleName = 'JitsiB2B';
  enabled = false;
  opened = false;
  callId = null;
  hasPermission = null;
  settingsOpened = false;
  position = {x: 200, y: 200};
  participants = new Array<JitsiParticipant>();

  constructor(private modulesService: ModulesService,
              public jitsiService: JitsiService) {
    if (userEnv.callId) {
      this.callId = userEnv.callId;

      this.modulesService.getModules().then(modules => {
        modules.forEach(mod => {
          if (mod.name === this.moduleName) {
            this.enabled = mod.enabled;
            if (this.callId && this.enabled) {
              this.startCall();
            } else {
              // TODO: ask for room name
              this.enabled = false;
            }
          }
        });
      });

      this.jitsiService.participantsListUpdate.subscribe(() => {
        if (this.jitsiService.status === JitsiCallStatus.NOT_CONNECTED) {
          return;
        }

        this.participants = this.jitsiService.participants;
      });

      this.jitsiService.hasPermission.subscribe((hasPerm) => {
        this.hasPermission = hasPerm;
        if (hasPerm) {
          this.jitsiService.updateDevicesLists();
        }
      });
    }
  }

  startCall() {
    this.opened = true;
    const boundaries = document.getElementsByTagName('body')[0].getBoundingClientRect();
    this.position = {x: boundaries.right - 315, y: boundaries.bottom - 265};
    if (this.jitsiService.status === JitsiCallStatus.NOT_CONNECTED && this.callId) {
      this.jitsiService.init(this.callId);
    }
  }

  stopCall() {
    this.jitsiService.unload().finally(() => {
      this.opened = false;
    });
  }

  openSettings() {
    this.settingsOpened = !this.settingsOpened;
  }

}
