import {Component, EventEmitter, Output} from '@angular/core';
import {JitsiService} from '../../../../services/jitsi.service';
import {ModalController} from '@ionic/angular';
import {SettingsCallComponent} from '../settings-call/settings-call.component';

@Component({
  selector: 'app-call-overlay',
  templateUrl: './call-overlay.component.html',
  styleUrls: ['./call-overlay.component.scss'],
})
export class CallOverlayComponent {
  @Output() endCall = new EventEmitter();

  constructor(public jitsiService: JitsiService,
              private modalController: ModalController) { }

  muteCam() {
    this.jitsiService.muteLocalVideo();
  }

  muteMic() {
    this.jitsiService.muteLocalMic();
  }

  callButtonPressed() {
    this.endCall.emit();
  }

  async openSettings() {
    const modal = await this.modalController.create({
      component: SettingsCallComponent,
    });
    return await modal.present();
  }
}
