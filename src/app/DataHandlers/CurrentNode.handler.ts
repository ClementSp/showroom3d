import { Injectable, EventEmitter } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Showroom} from '../models/showroom.model';
import {MatterportNode} from '../models/MatterportNode';

export interface ChangeCurrentNodeData {
    id: string;
    setCamera: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class CurrentNodeHandler {
    private source = new BehaviorSubject<MatterportNode>(null);
    public source$: Observable<MatterportNode> = this.source.asObservable();
    public changeCurrentNode = new EventEmitter<ChangeCurrentNodeData>();

    constructor() {
    }

    initialize(showroom: Showroom, nodeList: Array<MatterportNode>, entranceId?: string) {
        if (!entranceId) {
            this.source.next(nodeList[0]);
        } else {
            const found = nodeList.find(node => node.id === entranceId);
            if (found) {
                this.source.next(found);
            } else {
                this.source.next(nodeList[0]);
            }
        }
        // showroom.settings.entrance.sweepId = this.firstIndex;
    }

    set value(value: MatterportNode) {
        this.source.next(value);
    }

    get value() {
        return this.source?.value;
    }

    get firstIndex() {
        return this.source?.value?.id;
    }

    get id() {
        return this.source?.value?.id;
    }

    get neighbors() {
        return this.source?.value?.neighbors;
    }

    get position() {
        return this.source?.value?.position;
    }

    get floorPosition() {
        return this.source?.value?.floorPosition;
    }

    get cameraPosition() {
        return this.source?.value?.cameraPosition;
    }

    set cameraPosition(rotation: { x: number; y: number; z: number }) {
        this.source.value.cameraPosition = rotation;
    }
}
