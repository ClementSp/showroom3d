import {Injectable} from '@angular/core';
import * as THREE from "three";

@Injectable({
  providedIn: 'root'
})
export class CameraHandler {

  public camera: any;

  constructor() {}

  setCamera(camera) {
    this.camera = camera;
    this.camera.near = 0.0001;

    // Set audio listener
    const listener = new THREE.AudioListener();
    this.camera.add(listener);
  }

  getCamera() {
    return this.camera;
  }
}
