import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ProductsService} from '../services/products.service';
import CustomProduct from '../models/CustomProduct';
import {CustomerService} from '../services/customer.service';
import {customerEnv} from '../../environments/customer.env';
import {userEnv} from '../../environments/user.env';
import {ShowroomsService} from '../services/showrooms.service';

export interface IWishListProduct {
    ean: string;
    price: number;
    quantity: number;
}

@Injectable({
    providedIn: 'root'
})
export class WishlistHandler {

    _products = new BehaviorSubject<Array<IWishListProduct>>([]);
    products$ = this._products.asObservable();

    constructor(private showroomService: ShowroomsService,
                private customerService: CustomerService,
                private productsService: ProductsService) {
        const wishlist = JSON.parse(localStorage.getItem('wishlist')) || {};

        if (wishlist[userEnv.showroomId]) {
            this.products = wishlist[userEnv.showroomId];
        }
    }

    addProduct(product: IWishListProduct) {
        const products  = [...this._products.value];
        const productInList = products.find((item => item.ean === product.ean));

        if (!product.price) {
            product.price = 0;
        }
        if (productInList) {
            productInList.quantity = product.quantity;
            productInList.price = product.price;
        } else {
            products.push(product);
        }
        this.products = products;
    }

    resetWishList() {
        this.products = [];
    }

    removeProduct(ean: string) {
        let products = [...this._products.value];

        products = products.filter(item => item.ean !== ean);
        this.products = products;
    }

    incrementProductQuantity(ean: string) {
        const products = this._products.value;
        const productFromList = products.find(item => item.ean === ean);

        if (productFromList) {
            productFromList.quantity++;
            this.products = products;
        }
    }

    decrementProductQuantity(ean: string) {
        const products = this._products.value;
        const productFromList = products.find(item => item.ean === ean);

        if (productFromList && productFromList.quantity > 0) {
            productFromList.quantity--;
            this.products = products;
        }
    }

    getProductByEan(ean: string): Observable<any> {
        return this._products.pipe(
            map((products) => products.find(item => item.ean === ean))
        );
    }

    sendWishList() {
        const promises = [];

        this._products.value.forEach((wishItem) => {
            promises.push(this.productsService.getProductByEan(wishItem.ean).toPromise().then((product: CustomProduct) => {
                let firstImageMediaId = '';
                let pdfId = '';
                for (const media of product.medias) {
                    if (media.type === 'picture') {
                        firstImageMediaId = media.id;
                    }
                    if (media.type === 'pdf') {
                        pdfId = media.id;
                    }
                }
                if (firstImageMediaId && pdfId) {
                    return this.productsService.getMediaUrl(product.product_ean, firstImageMediaId).toPromise()
                        .then((url) => {
                            return this.productsService.getMediaUrl(product.product_ean, pdfId).toPromise()
                                .then((pdfUrl) => {
                                    return  {
                                        name: product.code,
                                        productId: product.id,
                                        quantity: wishItem.quantity,
                                        thumbnailUrl: url,
                                        pdfUrl: pdfUrl,
                                        price: product.sale_price ?? 0
                                    };
                                })
                        });
                } else if (firstImageMediaId) {
                    return this.productsService.getMediaUrl(product.product_ean, firstImageMediaId).toPromise()
                        .then((url) => {
                            return  {
                                name: product.code,
                                productId: product.id,
                                quantity: wishItem.quantity,
                                thumbnailUrl: url,
                                pdfUrl: '',
                                price: product.sale_price ?? 0
                            };
                        });
                } else if (pdfId) {
                    return this.productsService.getMediaUrl(product.product_ean, pdfId).toPromise()
                        .then((pdfUrl) => {
                            return  {
                                name: product.code,
                                productId: product.id,
                                quantity: wishItem.quantity,
                                thumbnailUrl: '',
                                pdfUrl: pdfUrl,
                                price: product.sale_price ?? 0
                            };
                        })
                } else {
                    return  {
                        name: product.code,
                        productId: product.id,
                        quantity: wishItem.quantity,
                        thumbnailUrl: '',
                        pdfUrl: '',
                        price: product.sale_price ?? 0
                    };
                }

            }));
        });
        return Promise.all(promises).then((wishList) => {
            return this.showroomService.sendWishList(wishList);
        });
    }

    set products(products: Array<IWishListProduct>) {
        const wishList = JSON.parse(localStorage.getItem('wishlist')) || {};

        wishList[userEnv.showroomId] = products;
        localStorage.setItem('wishlist', JSON.stringify(wishList));
        this._products.next(products);
    }

    get length() {
        return this._products.value.length;
    }

    get total() {
        let total = 0;

        this._products.value.forEach((product: IWishListProduct) => {
            total += product.price * product.quantity;
        });
        return total;
    }
}
