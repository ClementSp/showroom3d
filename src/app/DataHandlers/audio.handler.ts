import {Injectable} from '@angular/core';
import * as THREE from 'three';
import {Platform} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AudioHandler {
  public audioListener = new THREE.AudioListener();
  public volume = 10;
  private audioItems = [];
  private isGlobalMuted = false;
  private defaultVolume = 100;
  private timeTillRetry = 200;

  constructor(private platform: Platform) {}

  public async addAudioToPlay(audio) {
    this.audioItems.push({audio, isAlwaysMuted: !!audio.muted});

    const tryToPlay = setInterval(() => {
      if (this.audioListener.context.state !== 'running') {
        this.audioListener.context.resume();
      }

      audio.muted = this.isGlobalMuted;
      audio.volume = this.getDefaultVolume() / 100;

      audio.play()
        .then(() => clearInterval(tryToPlay))
        .catch(() => {});
    }, this.timeTillRetry);
  }

  public toggleGlobalMute() {
    this.isGlobalMuted = !this.isGlobalMuted;

    this.audioItems.forEach(item => {
      if (item.isAlwaysMuted) {
        return;
      }
      item.audio.muted = this.isGlobalMuted;
    });
  }

  public setVolume(value: number) {
    this.volume = value;

    this.audioItems.forEach(item => {

      if (item.isAlwaysMuted) {
        return;
      }
      item.audio.volume = value / 100;

    });
  }

  public getDefaultVolume() {
    return this.defaultVolume;
  }

  public isMuted() {
    return this.isGlobalMuted;
  }
}
