import {Injectable} from '@angular/core';
import * as THREE from 'three';
import {showroomEnv} from '../../environments/showroom.env';
import {defaultPoiShapeParameters, Poi, ShapePoi, Showroom} from '../models/showroom.model';
import {ShowroomsService} from '../services/showrooms.service';
import Utils from '../models/Utils';
import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader';
import {CurrentNodeHandler} from './CurrentNode.handler';
import {GLTF, GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
import {IFixture, IFixtureAssetData, IFixtureAttribute} from '../models/fixture.model';
import MovementMarker, {MovementMarkerState} from '../models/MovementMarker';
import ActionMarker from '../models/ActionMarker';
import PortalMarker, {Portal, ShowroomPortal, SweepPortal} from '../models/PortalMarker';
import {MediasService} from '../services/medias.service';
import {AudioHandler} from './audio.handler';
import {BehaviorSubject} from 'rxjs';
import {FixturesService} from '../services/fixtures.service';
import {MeshoptDecoder} from 'three/examples/jsm/libs/meshopt_decoder.module';
import {DRACOLoader} from 'three/examples/jsm/loaders/DRACOLoader';
import {KTX2Loader} from 'three/examples/jsm/loaders/KTX2Loader';
import {WebGLRenderer} from 'three/src/Three';
import {Color, sRGBEncoding} from 'three';

@Injectable({
  providedIn: 'root'
})
export class SceneHandler {
  public sceneReady = new BehaviorSubject<any>(false);

  public renderer: WebGLRenderer;
  public raycaster = new THREE.Raycaster();
  public scene: THREE.Scene;
  public mesh: any;
  public camera: THREE.PerspectiveCamera;
  public mouse: THREE.Vector3;
  public controls: any;
  public clock = new THREE.Clock(true);
  public cameraFov = 75;
  public showroom: Showroom;
  public objLoader = new OBJLoader();
  public gltfLoader;
  public meshIsLoading = 0;
  public objectsAreLoading = 0;
  public nbObjectsToLoad = 0;
  public positionToMove;
  public rotationToMove;
  public cameraMoveCoef = 0.05;
  public cameraRotateCoef = 0.05;
  public isTransformObject = false;

  public texturesMap = new Map<string, THREE.Texture>();
  public videos = new Array<HTMLVideoElement>();
  public loadedGltfMap = new Map<string, THREE.Group>([]);
  public initObjectsMap = new Map<string, string[]>([]);
  public objectNameToHandlersMap = new Map();
  public configurationToSave = new Map();

  readonly isSoundMuted = new BehaviorSubject<boolean>(false);
  readonly mainSceneName = 'main';
  readonly fixtureName = 'fixture';
  readonly productName = 'product';
  readonly smartObjectName = 'smartObject';
  readonly meshName = 'shell';
  readonly meshComponentName = 'shellComponent';

  constructor(public showroomService: ShowroomsService,
              public fixtureService: FixturesService,
              public mediasService: MediasService,
              public audioHandler: AudioHandler,
              public currentNode: CurrentNodeHandler) {
    this.objectNameToHandlersMap.set('video', this.screenHandler.bind(this));
    this.objectNameToHandlersMap.set('picture', this.pictureHandler.bind(this));
    this.showroom = showroomEnv.showroom;
  }

  initRenderer(viewerContainer: HTMLElement) {
    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.domElement.style.display = 'block';
    this.renderer.domElement.style.height = '100%';
    this.renderer.domElement.style.width = '100%';
    this.renderer.physicallyCorrectLights = true;
    this.renderer.setClearColor( 0xcccccc );
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.setPixelRatio(window.devicePixelRatio * 4);
    viewerContainer.appendChild(this.renderer.domElement);
  }

  initCamera(viewerContainer: HTMLElement) {
    this.camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 1000 );
    const listener = new THREE.AudioListener();
    this.camera.add( listener );
    this.scene.add( this.camera );
    this.camera.position.set(0, 1.7, 0);
    this.camera.aspect = viewerContainer.clientWidth / viewerContainer.clientHeight;
    this.camera.zoom = 1;
  }

  zoom(value) {
    this.camera.zoom += value * 0.05;
    // this.updateCameraMatrix();
  }

  initControls() {
    this.controls = new CameraControl(this);
  }

  initAnimate() {
    this.animate = this.animate.bind(this);
    requestAnimationFrame( this.animate );
  }

  moveCameraToPosition() {
    if (this.positionToMove) {
      const directionX = this.camera.position.x < this.positionToMove.x ? 1 : -1;
      // const directionY = this.camera.position.y < this.positionToMove.y ? 1 : -1;
      const directionZ = this.camera.position.z < this.positionToMove.z ? 1 : -1;
      const cameraMoveCoefX = Math.abs(this.positionToMove.x - this.camera.position.x);
      // const cameraMoveCoefY = Math.abs(this.positionToMove.y - this.camera.position.y);
      const cameraMoveCoefZ = Math.abs(this.positionToMove.z - this.camera.position.z);

      const moveX = this.camera.position.x + directionX * cameraMoveCoefX * this.cameraMoveCoef;
      // const moveY = this.camera.position.y + directionY * cameraMoveCoefY * this.cameraMoveCoef;
      const moveZ = this.camera.position.z + directionZ * cameraMoveCoefZ * this.cameraMoveCoef;

      this.camera.position.set(moveX, 1.7, moveZ);

      if (Math.abs(this.positionToMove.x - this.camera.position.x) < this.cameraMoveCoef &&
          // Math.abs(this.positionToMove.y - this.camera.position.y) < this.cameraMoveCoef &&
          Math.abs(this.positionToMove.z - this.camera.position.z) < this.cameraMoveCoef) {
        this.positionToMove = undefined;
      }
    }
  }

  moveCameraToRotation() {
    if (this.rotationToMove) {
      const directionX = this.camera.rotation.x < this.rotationToMove.x ? 1 : -1;
      const directionY = this.camera.rotation.y < this.rotationToMove.y ? 1 : -1;
      const directionZ = this.camera.rotation.z < this.rotationToMove.z ? 1 : -1;
      const cameraMoveCoefX = Math.abs(this.rotationToMove.x - this.camera.rotation.x);
      const cameraMoveCoefY = Math.abs(this.rotationToMove.y - this.camera.rotation.y);
      const cameraMoveCoefZ = Math.abs(this.rotationToMove.z - this.camera.rotation.z);

      const moveX = this.camera.rotation.x + directionX * cameraMoveCoefX * this.cameraRotateCoef;
      const moveY = this.camera.rotation.y + directionY * cameraMoveCoefY * this.cameraRotateCoef;
      const moveZ = this.camera.rotation.z + directionZ * cameraMoveCoefZ * this.cameraRotateCoef;

      this.camera.rotation.set(moveX, moveY, moveZ);

      if (Math.abs(this.rotationToMove.x - this.camera.rotation.x) < this.cameraRotateCoef &&
          Math.abs(this.rotationToMove.y - this.camera.rotation.y) < this.cameraRotateCoef &&
          Math.abs(this.rotationToMove.z - this.camera.rotation.z) < this.cameraRotateCoef) {
        this.rotationToMove = undefined;
      }
    }
  }

  resizeCanvasToDisplaySize() {
    const canvas = this.renderer.domElement;
    const width = document.getElementById('body')?.clientWidth;
    let height = document.getElementById('body')?.clientHeight;
    if (height === 0) {
      height =  window.innerHeight;
    }

    if (canvas && canvas.width !== width || canvas.height !== height) {
      this.renderer.setSize(width, height, false);
      (this.camera as any).aspect = width / height;
      (this.camera as any).updateProjectionMatrix();
    }
  }

  animate(time) {
    requestAnimationFrame(this.animate);
    this.render();
    // this.prevTime = time;
  }

  render() {
    // this.camera.lookAt(this.mouse);
    //this.updateCameraMatrix();
    this.moveCameraToPosition();
    this.moveCameraToRotation();
    this.renderer.render( this.scene, this.camera );
    //this.controls.update(this.clock.getDelta());
  }

  updateCameraMatrix() {
    if (this.camera) {
      this.camera.updateProjectionMatrix();
    }
  }

  initScene(viewerContainer: HTMLElement) {
    this.scene = new THREE.Scene();
    this.initRenderer(viewerContainer);
    this.initCamera(viewerContainer);
    this.initControls();

    const MANAGER = new THREE.LoadingManager();
    const THREE_PATH = `https://unpkg.com/three@0.${THREE.REVISION}.x`;
    const DRACO_LOADER = new DRACOLoader(MANAGER).setDecoderPath(`${THREE_PATH}/examples/js/libs/draco/gltf/`);
    const KTX2_LOADER = new KTX2Loader(MANAGER).setTranscoderPath(`${THREE_PATH}/examples/js/libs/basis/`);
    this.gltfLoader = new GLTFLoader(MANAGER).setCrossOrigin('anonymous').setDRACOLoader(DRACO_LOADER)
        .setKTX2Loader(KTX2_LOADER.detectSupport(this.renderer)).setMeshoptDecoder(MeshoptDecoder);

    this.initLights();
    this.initSceneObject();
  }

  setScene(scene) {
    this.scene = scene;
  }

  getScene() {
    return this.scene;
  }

  setSceneObjectVisibility(value: boolean) {
    if (this.scene && this.scene.children) {
      this.scene.children.forEach((child) => {
        if (child instanceof MovementMarker
            || child instanceof ActionMarker
            || child instanceof PortalMarker
            || child.name === this.fixtureName
            || child.name === this.productName
            || child.name === this.productName
            || child.name === this.smartObjectName) {
          child.visible = value;
        }
      });
    }
  }

  /* ------------ Light ------------ */

  initLights() {
    this.scene.name = this.mainSceneName;
    console.log(this.showroom.lights);
    this.showroom.lights?.forEach(light => {
      if (light.type === 'POINT_LIGHT') {
        this.loadPointLightInScene(light.id , light);
      } else if (light.type === 'SPOT_LIGHT') {
        this.loadSpotLightInScene(light.id , light);
      }
    });
    //this.createDirectionalLight({x: 0, y: 50, z: 0});
    //this.createDirectionalLight({x: 0, y: 0, z: 50});
    //this.createDirectionalLight({x: -43, y: 0, z: -25});
    //this.createDirectionalLight({x: 43, y: 0, z: -25});
    this.createAmbiantlLight();
  }

  private createDirectionalLight(position) {
    const dirLight = new THREE.DirectionalLight( 0xffffff );
    dirLight.position.set( position.x, position.y, position.z);
    dirLight.intensity = 0.8;
    dirLight.castShadow = true;
    dirLight.shadow.camera.top = 2;
    dirLight.shadow.camera.bottom = - 2;
    dirLight.shadow.camera.left = - 2;
    dirLight.shadow.camera.right = 2;
    dirLight.shadow.camera.near = 0.1;
    dirLight.shadow.camera.far = 40;
    this.scene.add(dirLight);
  }

  private createAmbiantlLight() {
    const dirLight = new THREE.AmbientLight( 0xffffff );
    dirLight.intensity = 0.3;
    this.scene.add(dirLight);
  }

  /* ------------ Mesh ------------ */

  initSceneObject() {
    this.showroomService.getAsset().subscribe((assetUrl) => {
     if (assetUrl) {
      this.gltfLoader.load(assetUrl , (mesh: GLTF) => {
          if (mesh) {
            this.mesh = mesh.scene || mesh.scenes[0];
            this.mesh.name = this.meshName;
            this.mesh.children.forEach(child => {
              this.setMeshName(child);
            });
            this.scene.add(this.mesh);
            this.initObjects();
            this.initPois();
            this.initShapePois();
            this.initPortals();
            this.setSceneObjectVisibility(true);
            this.meshIsLoading = 1;
            this.initAnimate();
          }
          this.sceneReady.next(true);
        }, (onProgess) => {
          this.meshIsLoading = onProgess.loaded / onProgess.total;
        });
     } else {
       this.initAnimate();
       this.meshIsLoading = 1;
       this.objectsAreLoading = 1;
     }
    });
  }

  setMeshName(object) {
    object.renderOrder = 0;
    object.name = this.meshComponentName;

    if (object.children) {
      object.children.forEach(child => {
        this.setMeshName(child);
      });
    }
  }

  setMeshMaterial(value) {
    // this.mesh.children?.forEach(child => {
    //   child.material.colorWrite = value;
    // });
  }

  /* ------------ Objects ------------ */

  initObjects() {
    const gltfUrls = new Map<string, any>();
    this.showroom.fixtures.forEach(fixtureInfo => {
      this.nbObjectsToLoad ++;
      this.fixtureService.getFixture(fixtureInfo.fixtureId).subscribe((fixture: IFixtureAssetData) => {
          this.loadGltfInScene(fixtureInfo.id, fixture.gltf, fixtureInfo);
      });
    });

    this.showroom.products.forEach(productInfo => {
      this.nbObjectsToLoad ++;
      if (!gltfUrls.get(productInfo.product_glbModelUrl)) {
        gltfUrls.set(productInfo.product_glbModelUrl, [productInfo]);
      } else {
        gltfUrls.get(productInfo.product_glbModelUrl).push(productInfo);
      }
    });
    this.showroom.smartObjects.forEach(smartObjectInfo => {
      this.nbObjectsToLoad ++;
      if (!gltfUrls.get(smartObjectInfo.smart_objects_glbModelUrl)) {
        gltfUrls.set(smartObjectInfo.smart_objects_glbModelUrl, [smartObjectInfo]);
      } else {
        gltfUrls.get(smartObjectInfo.smart_objects_glbModelUrl).push(smartObjectInfo);
      }
    });

    Array.from(gltfUrls.keys()).forEach(gltfUrl => {
      if (gltfUrl) {
        this.loadSyncGltf(gltfUrl).then(() => {
          gltfUrls.get(gltfUrl).forEach(info => {
            if (info.gltf) {
              this.loadGltfInScene(info.id, info.gltf, info);
            } else if (info.product_glbModelUrl) {
              this.loadGltfInScene(info.id, info.product_glbModelUrl, info);
            } else if (info.smart_objects_glbModelUrl) {
              this.loadGltfInScene(info.id, info.smart_objects_glbModelUrl, info);
            }
          });
        });
      }
    });

    if (!this.nbObjectsToLoad) {
      this.objectsAreLoading = 1;
    }
  }

  initShapePois() {
    this.showroom.shapePois?.forEach(shape => {
      this.loadShapePioInScene(shape.id , shape);
    });
  }

  initPois() {
    this.showroom.pois?.forEach(poi => {
      this.loadPoiInScene(poi.id , poi);
    });
  }

  async loadPoiInScene(id , poi?: Poi, spritePath?: string): Promise<any> {
    const geometry = new THREE.PlaneGeometry(0.2, 0.2, 1, 1);

    let normalTexture;
    if (poi && poi.sprite) {
      normalTexture = this.texturesMap.get(poi.sprite.path);
      if (poi.sprite?.data?.color) {
        normalTexture = this.texturesMap.get(poi.sprite?.data?.color);
      }
    } else {
      normalTexture = this.texturesMap.get(spritePath);

      if (!normalTexture) {
        normalTexture = this.texturesMap.get('productPoi');
      }
    }

    const material = new THREE.MeshBasicMaterial({
      transparent: true,
      color: 0xffffff
    });
    material.map = normalTexture;

    const cube = new THREE.Mesh( geometry, material );
    cube.name = 'poi';
    cube.userData.type = 'POI';
    cube.userData.id = id;
    if (poi && poi.position && poi.rotation && poi.scale){
      cube.userData.poi = poi;
      cube.position.set(poi.position.x , poi.position.y, poi.position.z);
      cube.rotation.set(poi.rotation._x , poi.rotation._y, poi.rotation._z);
      cube.scale.set(poi.scale.x , poi.scale.y, poi.scale.z);
      cube.userData.parentId = id;
    } else {
      cube.userData.parentId = id;
    }

    this.scene.add(cube);
    return cube;
  }

  async loadShapePioInScene(id , shapePoi?: ShapePoi): Promise<any> {
    const geometry = new THREE.BoxGeometry( 0.5, 0.5, 0.5);
    const color =  defaultPoiShapeParameters.color;
    const material = new THREE.MeshBasicMaterial( {color} );
    material.transparent = true;
    material.opacity = .3;
    const cube = new THREE.Mesh( geometry, material );
    cube.name = 'shapePoi';
    cube.userData.type = 'SHAPE_POI';
    cube.userData.id = id;
    if (shapePoi){
      cube.userData.poi = shapePoi;
      cube.position.set(shapePoi.position.x , shapePoi.position.y, shapePoi.position.z);
      cube.rotation.set(shapePoi.rotation._x , shapePoi.rotation._y, shapePoi.rotation._z);
      cube.scale.set(shapePoi.scale.x , shapePoi.scale.y, shapePoi.scale.z);
      cube.userData.parentId = id;
    } else {
      cube.userData.parentId = id;
      cube.userData.poi = {
        isShapePoi: true
      };
      cube.position.set(0 , 0, 0);
      cube.rotation.set(0, 0, 0);
      cube.scale.set(1, 1, 1);
    }

    this.scene.add(cube);
    return cube;
  }

  initPortals() {
    this.showroom.portals?.forEach(portal => {
      if (portal.link) {
        this.showroomService.getShowroomImageWithResolution(portal.link.target).then((url) => {
          this.loadPortalInScene(portal.id , portal, url);
        });
      } else {
        this.loadPortalInScene(portal.id , portal);
      }
    });
  }

  async loadPortalInScene(id , portal?: any, spritePath?: string): Promise<any> {
    const geometry = new THREE.PlaneGeometry(0.3, 0.3, 1, 1);
    let normalTexture;
    if (spritePath) {
      normalTexture = spritePath;
    } else {
      normalTexture = this.texturesMap.get('sweepLink');
    }

    const material = new THREE.MeshBasicMaterial({
      transparent: true,
      color: 0xffffff
    });
    material.map = normalTexture;

    const cube = new THREE.Mesh( geometry, material );
    cube.name = 'portal';
    cube.userData.type = 'PORTAL';
    cube.userData.id = id;
    if (portal){
      cube.userData.portal = portal;
      cube.position.set(portal.position.x , portal.position.y, portal.position.z);
      cube.rotation.set(portal.rotation._x , portal.rotation._y, portal.rotation._z);
      cube.scale.set(portal.scale.x , portal.scale.y, portal.scale.z);
      cube.userData.parentId = id;
    } else {
      cube.userData.parentId = id;
    }

    this.scene.add(cube);
    return cube;
  }

  async loadPointLightInScene(id , lightInfo?: any): Promise<any> {
    const geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2);
    const color =  defaultPoiShapeParameters.color;
    const material = new THREE.MeshBasicMaterial( {color} );
    material.transparent = true;
    material.opacity = 0;
    const cube = new THREE.Mesh( geometry, material );
    cube.name = 'pointLight';
    cube.userData.type = 'POINT_LIGHT';
    cube.userData.id = id;
    cube.userData.parentId = id;

    const pointLight = new THREE.PointLight( 0xffffff );
    pointLight.userData.id = id;
    pointLight.userData.parentId = id;
    pointLight.userData.type = 'POINT_LIGHT';
    pointLight.name = 'pointLight';
    pointLight.intensity = 0.8;
    pointLight.castShadow = true;
    pointLight.visible = true;

    if (lightInfo) {
      cube.position.set(lightInfo.position.x, lightInfo.position.y, lightInfo.position.z);
      console.log(lightInfo);
      if (lightInfo.intensity) {
        pointLight.intensity = lightInfo.intensity;
      }
      if (lightInfo.color) {
        pointLight.color = new Color(lightInfo.color);
      }
    }

    cube.add(pointLight);
    this.scene.add(cube);
    return cube;
  }

  async loadSpotLightInScene(id , lightInfo?: any): Promise<any> {
    const geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2);
    const color =  defaultPoiShapeParameters.color;
    const material = new THREE.MeshBasicMaterial( {color} );
    material.transparent = true;
    material.opacity = 0;
    const cube = new THREE.Mesh( geometry, material );
    cube.name = 'spotLight';
    cube.userData.type = 'SPOT_LIGHT';
    cube.userData.id = id;
    cube.userData.parentId = id;

    const spotLight = new THREE.SpotLight( 0xffffff );
    spotLight.userData.id = id;
    spotLight.name = 'spotLight';
    spotLight.intensity = 0.8;
    spotLight.castShadow = true;
    spotLight.visible = true;
    spotLight.rotation.set(lightInfo.rotation.x, lightInfo.rotation.y, lightInfo.rotation.z);

    if (lightInfo) {
      cube.position.set(lightInfo.position.x, lightInfo.position.y, lightInfo.position.z);
      if (lightInfo.intensity) {
        spotLight.intensity = lightInfo.intensity;
      }
      if (lightInfo.color) {
        spotLight.color = new Color(lightInfo.color);
      }
    }
    cube.add(spotLight);
    this.scene.add(cube);
    return cube;
  }

  /* ------------ Object interactions ------------ */
  applyInitialConfig(saveConfigurations, target) {
    if (saveConfigurations && saveConfigurations.length > 0) {
      saveConfigurations.forEach(configuration => {
        this.applyProductConfiguration(configuration, target);
      });
    }
  }

  applyProductConfiguration(configuration, target) {
    if (configuration.meshes) {
      configuration.meshes.forEach(mesh => {
        const object = target.getObjectByName(mesh);
        if (object) {
          object.visible = true;
        }
      });
    }

    if (configuration.config) {
      const configs = configuration.config?.config;
      if (configs) {
        configs.forEach(config => {
          if (config.meshes) {
            config.meshes.forEach(mesh => {
              const object = target.getObjectByName(mesh);
              if (object) {
                object.visible = true;
                if (config.texture) {
                  const texture = new THREE.TextureLoader().load(config.texture.gcsPath);
                  texture.flipY = false;
                  object.material = new THREE.MeshBasicMaterial({map: texture});
                }
              }
            });
          }
        });
      }
    }
  }

  applyAttributes(attributes, target) {
    if (attributes && attributes.length > 0) {
      attributes.forEach((attribute: IFixtureAttribute) => {
        this.applyAttributeOnFixture(target, attribute);
      });
    }
  }

  applyAttributeOnFixture(object: any, attribute: IFixtureAttribute) {
    object.children.forEach((child) => {
      if (child.children) {
        this.applyAttributeOnFixture(child, attribute);
      }
      if (this.objectNameToHandlersMap.get(attribute.type) && child.name === attribute.target) {
        this.objectNameToHandlersMap.get(attribute.type)(child, attribute);
      }
    });
  }

  /* ------------ Loaders ------------ */

  public async loadSyncGltf(gltfUrl: string): Promise<GLTF> {
    return new Promise((resolve) =>  {
      this.gltfLoader.load(gltfUrl,
          (gltf: GLTF) => {
            this.loadedGltfMap.set(gltfUrl, gltf.scene);
            resolve(gltf);
          });
    });
  }

  async loadGltfInScene(id: string, gltfUrl: string, gltfInfos?: any ) {
    if (gltfUrl) {
      const mesh = this.getMesh(id, gltfUrl, gltfInfos);

      if (!mesh) {
        this.loadGltf(id, gltfUrl, gltfInfos);
      } else {
        this.onObjectReady(id, mesh, gltfInfos);
      }
    }
  }

  async placeGltfInScene(id: string, gltfUrl: string, gltfInfos?: any ) {
    let mesh = this.getMesh(id, gltfUrl, gltfInfos);

    if (!mesh) {
      const gltf = await this.loadSyncGltf(gltfUrl);
      mesh = this.copyObject(gltf.scene);
    }

    return this.onObjectReady(id, mesh, gltfInfos);
  }

  getMesh(id: string, gltfUrl: string, gltfInfos) {
    if (gltfUrl && gltfUrl.length > 0) {
      const url = gltfUrl.split('?')[0];
      return this.copyObject(this.loadedGltfMap.get(url));
    }
  }

  copyObject(object) {
    if (object) {
      const newObject = object.clone();
      if (object.children) {
        object.children.forEach((child, index) => {
          if (newObject.children[index].material) {
            newObject.children[index].material = child.material.clone();
          }
        });
      }
      return newObject;
    }
  }

  iterateCopy() {

  }

  async loadGltf(id: string, gltfUrl: string, gltfInfos) {
    this.gltfLoader.loadAsync(gltfUrl).then((gltf: GLTF) => {
      const mesh = gltf.scene;
      this.loadedGltfMap.set(gltfUrl, mesh);
      this.onObjectReady(id, this.copyObject(mesh), gltfInfos);
    });
  }

  async onObjectReady(id, mesh, gltfInfos) {
    if (mesh) {
      if (gltfInfos) {
        if (gltfInfos.rotation) {
          mesh.rotateX(gltfInfos.rotation.x);
          mesh.rotateY(gltfInfos.rotation.y);
          mesh.rotateZ(gltfInfos.rotation.z);
        }
        if (gltfInfos.scale) {
          mesh.scale.set(
              gltfInfos.scale.x,
              gltfInfos.scale.y,
              gltfInfos.scale.z);
        }
        if (gltfInfos.position) {
          mesh.position.set(
              gltfInfos.position.x,
              gltfInfos.position.y,
              gltfInfos.position.z
          );
        }
      }
      if (gltfInfos.attributes && gltfInfos.attributes.length > 0) {
        mesh.renderOrder = 3;
      } else {
        mesh.renderOrder = 2;
      }

      mesh.userData.id = id;

      if (gltfInfos.fixtureId) {
        mesh.userData.fixtureId = gltfInfos.fixtureId;
        mesh.userData.type = 'FIXTURE';
        mesh.name = this.fixtureName;
      }
      if (gltfInfos.productId) {
        if (gltfInfos.components) {
         const codesToHide =   gltfInfos.components.filter(component => component.visible === false ).map(elem => elem.code);
         if (codesToHide.length) {
           this.initObjectVisibility(codesToHide, mesh , gltfInfos.product_glbModelUrl);
         }
        }
        mesh.userData.productId = gltfInfos.productId;
        mesh.userData.template = gltfInfos.template;
        mesh.userData.ean = gltfInfos.ean;
        mesh.userData.type = 'PRODUCT';
        mesh.name = this.productName;
      }
      if (gltfInfos.smartObjectId) {
        mesh.userData.smartObjectId = gltfInfos.smartObjectId;
        mesh.userData.type = 'SMART_OBJECT';
        mesh.name = this.smartObjectName;
      }

      mesh.traverse((child) => {
        child.userData.parentId = id;
      });

      this.applyAttributes(gltfInfos.attributes, mesh);
      this.applyInitialConfig(gltfInfos.saveConfigurations, mesh);
      this.scene.add(mesh);

      this.objectsAreLoading += 1 / this.nbObjectsToLoad;
      if (this.objectsAreLoading > 0.9) {
        this.objectsAreLoading = 1;
      }
      return mesh;
    }
  }


  /* ------------ Handler ------------ */

  createCanvas(imgUrl: string, cb: any) {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const color = getComputedStyle(document.documentElement).getPropertyValue('--ion-color-primary') ?? '#fdfdfd';
    canvas.width = 256;
    canvas.height = 256;
    const centerX = canvas.width / 2;
    const centerY = canvas.height / 2;
    const radius =  (canvas.width / 2) - ((canvas.width / 2) * 0.05);

    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    ctx.lineWidth = 10;
    ctx.strokeStyle = color;
    ctx.stroke();
    ctx.clip();

    const image = new Image();
    if (imgUrl && !imgUrl.startsWith('/assets/')) {
      image.crossOrigin = 'Anonymous';
    }
    image.onload = () => {
      ctx.drawImage(image, 0, 0, image.width, image.height,
          0, 0, canvas.width, canvas.height);
      return cb(canvas);
    };

    image.src = imgUrl;

    return canvas;
  }

  pictureHandler(screen: THREE.Mesh, attribute: IFixtureAttribute) {
    if (!attribute?.data?.url && (!attribute?.data?.pictureId || attribute?.data?.pictureId.lenght === 0)) {
      return;
    }

    if (attribute.data?.url) {
      const texture = new THREE.TextureLoader().load(attribute.data.url);
      texture.flipY = false;
      (screen as any).material = new THREE.MeshBasicMaterial( { map: texture } );
    } else {
      this.mediasService.getMediasById(attribute.data.pictureId).subscribe((media) => {
        const texture = new THREE.TextureLoader().load(media.url);
        texture.flipY = false;
        (screen as any).material = new THREE.MeshBasicMaterial( { map: texture } );
      });
    }
  }

  screenHandler(screen: THREE.Mesh, attribute: IFixtureAttribute) {
    console.log('screen handler');
    if (!attribute?.data?.url && (!attribute?.data?.videoId || attribute?.data?.videoId.lenght === 0)) {
      return;
    }
    const video = (document.createElement('video') as HTMLVideoElement);
    const source = document.createElement('source');
    if (attribute.data.url) {
      source.src = attribute.data.url;
      source.type = 'video/mp4';
      video.loop = true;
      video.setAttribute( 'webkit-playsinline', 'webkit-playsinline' );
      video.muted = true;
      video.id = screen.id.toString();

      // Positional audio
      const camera = this.getObjectByType('PerspectiveCamera') as THREE.PerspectiveCamera;
      const listener = camera.children.find(c => c.type === 'AudioListener') as THREE.AudioListener;

      const sound = new THREE.PositionalAudio( listener );
      sound.setMediaElementSource(video);
      sound.setDistanceModel('exponential');
      sound.setRolloffFactor(20);
      sound.setRefDistance(3);
      screen.add(sound);

      video.crossOrigin = 'anonymous';
      video.style.display = 'none';
      video.appendChild(source);
      const texture = new THREE.VideoTexture(video);
      const parameters = {map: texture};
      this.videos.push(video);
      texture.format = THREE.RGBFormat;
      video.setAttribute('playsinline', 'playsinline');
      video.setAttribute('webkit-playsinline.', 'webkit-playsinline.');
      texture.flipY = false;
      (screen as any).material = new THREE.MeshLambertMaterial(parameters);
      video.play().then(() => {
        video.muted = this.isSoundMuted.value;
      });
    } else {
      console.log(attribute);
      this.mediasService.getMediasById(attribute.data.videoId).subscribe((media) => {
        console.log(media);
        source.src = media.url;
        source.type = 'video/mp4';
        video.loop = true;
        video.setAttribute( 'webkit-playsinline', 'webkit-playsinline' );
        video.muted = true;
        video.id = screen.id.toString();

        // Positional audio
        const listener = this.camera.children.find(c => c.type === 'AudioListener') as THREE.AudioListener;

        const sound = new THREE.PositionalAudio( listener );
        sound.setMediaElementSource(video);
        sound.setDistanceModel('exponential');
        sound.setRolloffFactor(20);
        sound.setRefDistance(3);
        screen.add(sound);

        video.crossOrigin = 'anonymous';
        video.style.display = 'none';
        video.appendChild(source);
        const texture = new THREE.VideoTexture(video);
        const parameters = {map: texture};
        this.videos.push(video);
        texture.format = THREE.RGBFormat;
        video.setAttribute('playsinline', 'playsinline');
        video.setAttribute('webkit-playsinline.', 'webkit-playsinline.');
        texture.flipY = false;
        (screen as any).material = new THREE.MeshLambertMaterial(parameters);
        video.play().then(() => {
          video.muted = this.isSoundMuted.value;
        });
      });
    }
  }

  removeVideo(object) {
    const lastVideo = this.videos.find(video => video.id === object.id.toString());
    if (lastVideo) {
      object.children = [];
      lastVideo.src = null;
    } else if (object.children) {
      object.children.forEach((child) => {
        this.removeVideo(child);
      });
    }

      // screen.children = [];
  }

  getObjectByType(type: string): THREE.Object3D {
    if (this.scene) {
      return this.scene.getObjectByProperty('type', type);
    }
  }


  /* ------------ Action ------------ */
  toggleMute() {
    this.isSoundMuted.next(!this.isSoundMuted.value);
    this.audioHandler.toggleGlobalMute();
    this.videos.forEach((video) => {
      video.muted = this.isSoundMuted.value;
    });
  }

  removeObjFromScene(obj: THREE.Object3D) {
    if (this.scene && this.scene.children) {
      for (let i = this.scene.children.length - 1; i >= 0; i--) {
        const objIter = this.scene.children[i];
        if (objIter === obj) {
          this.removeVideo(objIter);
          if ((objIter as any).material) {
            (objIter as any).material.dispose();
          }
          if ((objIter as any).geometry) {
            (objIter as any).geometry.dispose();
          }
          this.scene.remove(obj);
        }
      }
    }
  }

  public updateAttributeOfFixture(data) {
    const fixture = data.fixture;
    const fixtureId = data.fixture.id;
    const gcsFixtureId = fixture.userData.fixtureId;
    const attributes = data.attributes;

    this.fixtureService.getAllFixtures().subscribe((fixtures) => {
      const fixtureInfo: IFixture = fixtures.find(f => f.id === gcsFixtureId);

      this.fixtureService.getFixture(gcsFixtureId).toPromise()
          .then((fixtureData: IFixtureAssetData) => {
            fixtureInfo.attributes = attributes;
            fixtureInfo.rotation = fixture.rotation;
            fixtureInfo.scale = fixture.scale;
            fixtureInfo.position = fixture.position;
            fixtureInfo.fixtureId = fixture.userData.fixtureId;
            this.removeObjFromScene(fixture);
            return this.loadGltfInScene(fixtureId, fixtureData.gltf, fixtureInfo);
          });
    });
  }

   initObjectVisibility(codesToHide , mesh , url) {
    codesToHide?.forEach(code => {
      mesh.getObjectByName(code).visible = false;
    });
    this.initObjectsMap.set(url, codesToHide);
  }

  setCamera(camera) {
    this.camera.position.set(camera.position.x, camera.position.y, camera.position.z);
    this.camera.rotation.order = 'YXZ';
    this.camera.rotation.set(camera.rotation.x, camera.rotation.y, camera.rotation.z);
  }

  moveCamera(position) {
    this.positionToMove = position;
  }

  rotateCamera(rotation) {
    this.rotationToMove = rotation;
  }

  setTransformObject(isTransform) {
    this.isTransformObject = isTransform;
  }
}

class CameraControl {
  public zoomMode = false;
  public press = false;
  public sensitivity = 0.06;
  public previousTouch = {
    x: 0,
    y: 0
  };

  constructor(sceneHandler: SceneHandler){
    sceneHandler.renderer.domElement.addEventListener('mousemove', event => {
      if (!this.press || sceneHandler.isTransformObject){ return; }

      sceneHandler.rotationToMove = undefined;
      sceneHandler.camera.rotation.order = 'YXZ';
      if (event.button === 0 || event.button === 2){
        const lon = event.movementY * this.sensitivity;
        const lat = event.movementX * this.sensitivity;
        const vector3 =  Utils.lonLatToVector3(lon, lat);
        sceneHandler.camera.rotation.x += vector3.x;
        sceneHandler.camera.rotation.y += vector3.y;
        sceneHandler.camera.rotation.z = 0;
      }
    });

    sceneHandler.renderer.domElement.addEventListener('touchstart', event => {
      sceneHandler.rotationToMove = undefined;
      const touch = event.touches[0];
      this.previousTouch = {
        x: touch.pageX,
        y: touch.pageY,
      };
    });


    sceneHandler.renderer.domElement.addEventListener('touchmove', event => {
      const touch = event.touches[0];

      sceneHandler.camera.rotation.order = 'YXZ';

      const movementX = touch.pageX - this.previousTouch.x;
      const movementY = touch.pageY - this.previousTouch.y;

      const lon = movementY * this.sensitivity * 2;
      const lat = movementX * this.sensitivity * 2;
      const vector3 =  Utils.lonLatToVector3(lon, lat);
      sceneHandler.camera.rotation.x += vector3.x;
      sceneHandler.camera.rotation.y += vector3.y;
      sceneHandler.camera.rotation.z = 0;

      this.previousTouch = {
        x: touch.pageX,
        y: touch.pageY,
      };
    });

    sceneHandler.renderer.domElement.addEventListener('mousedown', () => { this.press = true; });
    sceneHandler.renderer.domElement.addEventListener('mouseup', () => { this.press = false; });
    sceneHandler.renderer.domElement.addEventListener('mouseleave', () => { this.press = false; });
  }
}
