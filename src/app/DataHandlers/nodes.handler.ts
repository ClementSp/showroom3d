import {Injectable} from '@angular/core';

export enum NodeType {
  POI = 'poi',
  SHAPE_POI = 'shapePoi',
  PORTAL = 'portal',
  SPOT_LIGHT = 'spotLight',
  POINT_LIGHT = 'pointLight',
  FIXTURE = 'fixture',
  PRODUCT = 'product',
  SMARTOBJECT = 'smartObject',
  GREEN_SCREEN = 'greenScreen',
  SWEEP = 'sweep'
}

export interface ShowroomNode {
  node: any;
  data: any;
  type: string;
}

@Injectable({
  providedIn: 'root'
})
export class NodesHandler {
  private showroomNodes = new Array<ShowroomNode>();

  constructor() {
  }

  public addNode(node: ShowroomNode) {
    this.showroomNodes.push(node);
  }

  public getNodes() {
    return this.showroomNodes;
  }

  public getNodesByType(type: NodeType) {
    return this.showroomNodes.filter(sn => sn.type === type);
  }

  public removeNode(showroomNode: ShowroomNode) {
    this.showroomNodes = this.showroomNodes.filter(n => n.node.obj3D.uuid !== showroomNode.node.obj3D.uuid);
    showroomNode.node.stop();
  }

  public removeNodeById(id: string) {
    const idx = this.showroomNodes.findIndex(n => n.data.id === id);

    if (idx <= -1) {
      return;
    }

    this.showroomNodes[idx].node.stop();
    this.showroomNodes.splice(idx, 1);
  }

  public removeNodesByType(type: NodeType) {
    const nodes = this.showroomNodes.filter(sn => sn.type === type);

    nodes.forEach((node) => {
      node.node.stop();
    });
    this.showroomNodes = this.showroomNodes.filter(sn => sn.type !== type);
  }

  public removeAllNodes() {
    this.showroomNodes.forEach(node => {
      node.node.stop();
      delete node.node;
    });
    this.showroomNodes = [];
  }

  public getNodeById(nodeId: string) {
    return this.showroomNodes.find(n => n.data?.id === nodeId);
  }

  public updateNodeInput(nodeId: string, inputField: string, value: any) {
    const node = this.getNodeById(nodeId);

    if (!node) {
      return console.warn('[WARNING]: Tried to modify input of non existing node:', nodeId);
    }

    node.node.components[0].instance.inputs[inputField] = value;
  }

  public updateNodeDataById(nodeId: string, data: any) {
    const node = this.getNodeById(nodeId);

    if (!node) {
      return console.warn('[WARNING]: Tried to update data of non existing node:', nodeId);
    }

    this.updateNodeDataByNode(node, data);
  }

  public updateNodeDataByNode(showroomNode: ShowroomNode, newData: any) {
    const index = this.showroomNodes.findIndex(n => n.node.obj3D.uuid === showroomNode.node.obj3D.uuid);

    if (index >= 0) {
      this.showroomNodes[index].data = newData;
    }
  }
  public getNodeByName(nodeName: string) {
    return this.showroomNodes.find(n => n.data?.name === nodeName);
  }

}
