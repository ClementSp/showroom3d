import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ShowroomEntranceSettings} from '../models/ShowroomSettings.model';

@Injectable({
    providedIn: 'root'
})
export class ShowroomHandler {

    entrance$ = new BehaviorSubject<ShowroomEntranceSettings>(undefined);

    constructor() {
    }

    set entrance(data: ShowroomEntranceSettings) {
        this.entrance$.next(data);
    }

    get entrance() {
        return this.entrance$.value;
    }
}
