export const defaultSprites = new Map([
  ['blue', '/assets/info2.png'],
  ['red', '/assets/info2_red.png'],
  ['yellow', '/assets/info2_yellow.png'],
  ['green', '/assets/info2_green.png'],
  ['orange', '/assets/info2_orange.png'],
  ['grey', '/assets/info2_grey.png'],
  ['black', '/assets/info2_black.png'],
  ['purple', '/assets/info2_purple.png'],
]);
