import {IProduct} from '../models/product.model';

export enum ProductAction {
  ADD = 'add',
  REMOVE = 'remove',
  EDIT = 'edit',
  MOVE = 'move',
  CANCEL = 'cancel'
}

export interface ProductActionData {
  action: ProductAction;
  product: IProduct;
}
