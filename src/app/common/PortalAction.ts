import {Portal} from '../models/PortalMarker';

export enum PortalAction {
  ADD = 'add',
  REMOVE = 'remove',
  EDIT = 'edit',
  MOVE = 'move',
  CANCEL = 'cancel'
}

export interface PortalActionData {
  action: PortalAction;
  portal: Portal;
}
