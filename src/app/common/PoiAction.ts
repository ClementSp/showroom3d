import {Poi} from '../models/showroom.model';

export enum PoiAction {
  ADD = 'add',
  REMOVE = 'remove',
  EDIT = 'edit',
  MOVE = 'move',
  CANCEL = 'cancel'
}

export interface PoiActionData {
  action: PoiAction;
  poi: Poi;
}
