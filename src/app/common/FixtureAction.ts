import {IFixture} from '../models/fixture.model';

export enum FixtureAction {
  ADD = 'add',
  REMOVE = 'remove',
  EDIT = 'edit',
  MOVE = 'move',
  CANCEL = 'cancel'
}

export interface FixtureActionData {
  action: FixtureAction;
  fixture: IFixture;
}
