import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Camera, HiddenSweep, MenuJumpLink, MenuLink, Poi, ShapePoi, Showroom} from '../models/showroom.model';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {Portal} from '../models/PortalMarker';
import {IFixture} from '../models/fixture.model';
import {userEnv} from '../../environments/user.env';
import {HttpClientService} from './http-client.service';
import {ToastService} from './toast.service';
import {AbScenariosService} from './ab-scenarios.service';
import {ShowroomSettings} from '../models/ShowroomSettings.model';
import {customerEnv} from '../../environments/customer.env';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';
import {Deserialize, Serialize} from 'cerialize';
import {PointLight} from 'three';

export enum LogType {
    CONNECTION = 'store_connection',
    SWEEP_MOVE = 'sweep_move',
    CLUSTER_SLIDE_NEXT = 'cluster_slide_next',
    CLUSTER_SLIDE_PREV = 'cluster_slide_prev',
    CLICK_PRODUCT = 'click_product',
    CLICK_VIDEO = 'click_video',
    CLICK_CLUSTER = 'click_cluster'
}

export enum ShowroomImage {
    LOW = 'low',
    HIGH = 'high',
    TWO_K = '2k',
    FOUR_K = '4k'
}

@Injectable({
    providedIn: 'root'
})
export class ShowroomsService extends HttpClientService{

    constructor(public httpClient: HttpClient,
                private abScenarioService: AbScenariosService,
                public toastService: ToastService) {
        super(httpClient, toastService);
    }
    createShowroomCopy(id: string, showroomName: string, sameConf: boolean, isTemplate: boolean): Observable<any> {
        return this.put(`${environment.apiUrl2}${userEnv.tenant}/showRooms/createCopy/${id}`, {
            name: showroomName,
            conf: sameConf,
            isTemplate
        }).pipe(
            map((response => {
                return response.result;
            }))
        );
    }
    getShowroom(): Promise<Showroom> {
        return this.get(this.baseUrl())
            .pipe( map(resp => resp.result) )
            .toPromise()
            .then(((showroom: Showroom) => {
                if (showroom?.name) {
                    return new Showroom(showroom);
                } else {
                    return this.abScenarioService.getAbScenario().then((abScenario) => {
                        return new Showroom({
                            id: abScenario.id,
                            name: abScenario.name,
                            showroomId: abScenario.id,
                            hideWishlist: true,
                            settings: {
                                components: {
                                    wishlist: {
                                        enabled: false
                                    }
                                }
                            }
                        });
                    });
                }
            }));
    }

    getShowroomById(showroomId: string): Promise<Showroom> {
        return this.get(`${environment.apiUrl2}${userEnv.tenant}/showRooms/${showroomId}/`)
            .pipe(
                map((response: any) => {
                    return response.result;
                })
            ).toPromise();
    }

    putHiddenSweep(sweep: HiddenSweep) {
        return this.put(this.baseUrl() + 'hiddenSweep', sweep);
    }

    putMenuItem(menuLink: MenuLink) {
        return this.put(this.baseUrl() + 'menuItem', menuLink);
    }

    putMenuLink(menuLink: MenuJumpLink) {
        return this.put(this.baseUrl() + 'menuLinkItem', menuLink);
    }

    putCamera(camera: Camera) {
        return this.put(this.baseUrl() + 'camera', camera);
    }

    updateShowroom(showroom: Showroom) {
        return this.put(`${environment.apiUrl2}${userEnv.tenant}/showRooms/update`, {showroom});
    }
    updateWithTenant(showroom: Showroom): Observable<any> {
        return this.put(`${environment.apiUrl2}${userEnv.tenant}//showRooms/update`, showroom).pipe(map(
            result => {
                return result.result;
            }));
    }
    updateFixtures(fixtures: Array<IFixture>) {
        return this.post(this.baseUrl() + 'fixtures', {fixtures});
    }

    putPoi(poi: Poi) {
        return this.put(this.baseUrl() + 'poi', poi);
    }

    deletePoi(id: string): Observable<any>  {
        return this.delete(this.baseUrl() + 'poi/' + id);
    }

    putLight(light: any) {
        return this.put(this.baseUrl() + 'light', light);
    }

    deleteLight(id: string): Observable<any>  {
        return this.delete(this.baseUrl() + 'light/' + id);
    }

    putShapePoi(poi: Poi | ShapePoi){
        return this.put(this.showroomBaseUrl() + 'shapePoi', poi).pipe(
            map((response: any) => {
                return response.result;
            }));
    }

    deleteShapePoi(id: string): Observable<any> {
        return this.delete(this.showroomBaseUrl() + 'shapePoi/' + id);
    }

    deleteMenuItem(id: string) {
        return this.delete(this.baseUrl() + 'menuItem/' + id);
    }

    deleteLinkMenuItem(id: string) {
        return this.delete(this.baseUrl() + 'menuLinkItem/' + id);
    }

    deleteCamera(id: string) {
        return this.delete(this.baseUrl() + 'camera/' + id);
    }

    updateMenu(menu: Array<MenuLink>) {
        return this.put(this.baseUrl() + 'menu', { menu });
    }

    updateLinkMenu(linkMenu: Array<MenuJumpLink>) {
        return this.put(this.baseUrl() + 'linkMenu', { linkMenu });
    }

    updateSettings(settings: ShowroomSettings) {
        return this.put(this.baseUrl() + 'settings', settings);
    }

    sendWishList(wishlist: Array<any>) {
        return this.post(this.baseUrl() + 'sendWishList', {
            wishlist,
            customer: {
                email: customerEnv.email,
                name: customerEnv.name,
                country: customerEnv.country
            }
        }).pipe(map((response: any) => response.result)).toPromise();
    }

    putPortal(portal: Portal) {
        return this.put(this.baseUrl() + 'portal', portal);
    }

    deletePortal(portalId: string): Observable<any> {
        return this.delete(this.baseUrl() + 'portal/' + portalId);
    }

    getAsset() {
        return this.get(this.baseUrl() + 'asset').pipe(map(response => {
            return response.result;
        }));
    }
    saveShowroomCategory(body: any): Observable<any> {
        return this.put(`${environment.apiUrl2}${userEnv.tenant}/categories/showrooms`, body).pipe(
            map(response => {return response.result;
            })
        );
    }
    getShowroomImageWithResolution(showroomId = userEnv.showroomId, resolution: ShowroomImage = ShowroomImage.HIGH) {
        return this.get(`${environment.apiUrl2}${userEnv.tenant}/showRooms/${showroomId}/showroomImage/${resolution}`)
            .pipe(map((response: any) => response.result)).toPromise();
    }

    me(): Observable<User> {
        return this.get(`${environment.apiUrl2}auth/me`).pipe(
            map((response => {
                if (response.result) {
                    const user = Deserialize(response.result, User);
                    user.id = response.result.id;
                    return user;
                } else {
                    return null;
                }
            }))
        );
    }
    private baseUrl() {
        return `${environment.apiUrl2}${userEnv.tenant}/showRooms/${userEnv.showroomId}/`;
    }

    private showroomBaseUrl() {
        return `${environment.apiUrl2}${userEnv.tenant}/showroomMp/${userEnv.showroomId}/`;
    }
}
