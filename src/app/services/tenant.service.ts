import {Injectable} from '@angular/core';
import {HttpClientService} from './http-client.service';
import {environment} from '../../environments/environment';
import {userEnv} from '../../environments/user.env';
import {map} from 'rxjs/operators';
import {Showroom} from '../models/showroom.model';

export interface ShowroomBasicInfo {
    name: string;
    showroom_id: string;
    id: string;
    isMatterport: boolean;
    customers: Array<any>;
}

@Injectable({
  providedIn: 'root'
})
export class TenantService {
  constructor(private httpClientService: HttpClientService)
  {}

  getTenantShowroomsList(throwError = false): Promise<Array<ShowroomBasicInfo>> {
    return this.httpClientService.get(`${environment.apiUrl2}${userEnv.tenant}/showRooms/list`, throwError)
        .pipe(
            map((response: any) => {
              return response.result;
            })
        ).toPromise();
  }
}

