import { Injectable } from '@angular/core';
import {HttpClientService} from './http-client.service';
import {HttpClient} from '@angular/common/http';
import {ToastService} from './toast.service';
import {environment} from '../../environments/environment';
import {userEnv} from '../../environments/user.env';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AbScenariosService extends HttpClientService{

  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
    super(httpClient, toastService);
  }

  getAbScenario(): Promise<any> {
    return this.get(this.baseUrl())
        .pipe(map(resp => resp.result))
        .toPromise();
  }

  private baseUrl() {
    return `${environment.apiUrl2}${userEnv.tenant}/abScenarios/getById/${userEnv.showroomId}`;
  }
}
