import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToastService} from '../toast.service';
import {Observable} from 'rxjs';
import {IProduct} from '../../models/product.model';
import {map, tap} from 'rxjs/operators';
import {Euler, Vector3} from '../../models/Utils';
import {environment} from '../../../environments/environment';
import {userEnv} from '../../../environments/user.env';
import {HttpClientService} from '../http-client.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsManagerService extends HttpClientService {

  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
    super(httpClient, toastService);
  }

  addProduct(data: any): Observable<IProduct> {
    return this.httpClient.put<any>(this.baseUrl() + 'add', data, { headers: this.blobHeader() })
        .pipe(map(response => response.result));
  }

  deleteProduct(id: string): Observable<any> {
    return this.httpClient.delete<any>(this.baseUrl() + id, { headers: this.blobHeader() });
  }

  translateProduct(productId: string, position: Vector3) {
    return this.httpClient.post<any>(this.baseUrl() + productId + '/translate'  , {...position}, { headers: this.blobHeader() });
  }

  rotateProduct(productId: string, rotation: Euler) {
    return this.httpClient.post<any>(this.baseUrl() + productId + '/rotation'  , {
      x: rotation._x,
      y: rotation._y,
      z: rotation._z
    }, { headers: this.blobHeader() });
  }

  scaleProduct(productId: string, scale: Vector3) {
    return this.httpClient.post<any>(this.baseUrl() + productId + '/scale'  , {...scale}, { headers: this.blobHeader() });
  }

  private blobHeader() {
    return {
      Authorization: `Bearer ${userEnv.jwt}`
    };
  }
  private baseUrl() {
    return `${environment.apiUrl2}${userEnv.tenant}/showRooms/${userEnv.showroomId}/products/`;
  }
}
