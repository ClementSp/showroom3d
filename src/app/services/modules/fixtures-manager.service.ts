import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToastService} from '../toast.service';
import {Observable} from 'rxjs';
import {IFixture} from '../../models/fixture.model';
import {map, tap} from 'rxjs/operators';
import {Euler, Vector3} from '../../models/Utils';
import {environment} from '../../../environments/environment';
import {userEnv} from '../../../environments/user.env';
import {HttpClientService} from '../http-client.service';

@Injectable({
  providedIn: 'root'
})
export class FixturesManagerService extends HttpClientService {

  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
    super(httpClient, toastService);
  }

  addFixture(data: any): Observable<IFixture> {
    return this.httpClient.put<any>(this.baseUrl() + 'add', data, { headers: this.blobHeader() })
        .pipe(map(response => response.result));
  }

  deleteFixture(id: string): Observable<any> {
    return this.httpClient.delete<any>(this.baseUrl() + id, { headers: this.blobHeader() });
  }

  translateFixture(fixtureId: string, position: Vector3) {
    return this.httpClient.post<any>(this.baseUrl() + 'translate/' + fixtureId, {...position}, { headers: this.blobHeader() });
  }

  rotateFixture(fixtureId: string, rotation: Euler) {
    return this.httpClient.post<any>(this.baseUrl() + 'rotate/' + fixtureId, {
      x: rotation._x,
      y: rotation._y,
      z: rotation._z
    }, { headers: this.blobHeader() });
  }

  scaleFixture(fixtureId: string, scale: Vector3) {
    return this.httpClient.post<any>(this.baseUrl() + 'scale/' + fixtureId, {...scale}, { headers: this.blobHeader() });
  }

  addAttribute(fixtureId: string, data: any) {
    return this.httpClient.post<any>(this.baseUrl() + fixtureId + '/attribute', data, { headers: this.blobHeader() })
        .pipe(map(response => response.result));
  }

  deleteAttribute(fixtureId: string, attributeId: any) {
    return this.httpClient.delete<any>(this.baseUrl() + fixtureId + '/attribute/' + attributeId, { headers: this.blobHeader() })
        .pipe(map(response => response.result));
  }

  updateAttribute(fixtureId: string, attribute: any) {
    return this.httpClient.put<any>(this.baseUrl() + fixtureId + '/attribute/' + attribute.id, attribute, { headers: this.blobHeader() })
        .pipe(map(response => response.result));
  }

  private blobHeader() {
    return {
      Authorization: `Bearer ${userEnv.jwt}`
    };
  }

  private baseUrl() {
    return `${environment.apiUrl2}${userEnv.tenant}/showRooms/${userEnv.showroomId}/fixturesManager/`;
  }
}
