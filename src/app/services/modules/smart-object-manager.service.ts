import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToastService} from '../toast.service';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {Euler, Vector3} from '../../models/Utils';
import {environment} from '../../../environments/environment';
import {userEnv} from '../../../environments/user.env';
import {HttpClientService} from '../http-client.service';
import {ISmartObject} from '../../models/smartObject.model';

@Injectable({
  providedIn: 'root'
})
export class SmartObjectManagerService extends HttpClientService {

  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
    super(httpClient, toastService);
  }

  addSmartObject(data: any): Observable<ISmartObject> {
    return this.httpClient.put<any>(this.baseUrl() + 'add', data, { headers: this.blobHeader() })
        .pipe(map(response => response.result));
  }

  deleteSmartObject(id: string): Observable<any> {
    return this.httpClient.delete<any>(this.baseUrl() + id, { headers: this.blobHeader() });
  }

  translateSmartObject(smartObjectId: string, position: Vector3) {
    return this.httpClient.post<any>(this.baseUrl() + smartObjectId + '/translate', {...position}, { headers: this.blobHeader() });
  }

  rotateSmartObject(smartObjectId: string, rotation: Euler) {
    return this.httpClient.post<any>(this.baseUrl() + smartObjectId + '/rotate', {
      x: rotation._x,
      y: rotation._y,
      z: rotation._z
    }, { headers: this.blobHeader() });
  }

  scaleSmartObject(smartObjectId: string, scale: Vector3) {
    return this.httpClient.post<any>(this.baseUrl() + smartObjectId + '/scale', {...scale}, { headers: this.blobHeader() });
  }



  private blobHeader() {
    return {
      Authorization: `Bearer ${userEnv.jwt}`
    };
  }
  private baseUrl() {
    return `${environment.apiUrl2}${userEnv.tenant}/showRooms/${userEnv.showroomId}/smartObjects/`;
  }

}
