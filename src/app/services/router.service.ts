import { Injectable } from '@angular/core';
import {Router, UrlTree} from '@angular/router';
import {userEnv} from "../../environments/user.env";

@Injectable({
  providedIn: 'root'
})
export class RouterService {
  constructor(private router: Router) {}

  navigateByUrl(url: string) {
    return this.router.navigateByUrl(this.keepCurrentParams(url));
  }

  navigateToError(message?: string) {
    if (message) {
      this.router.navigateByUrl('error', {state: {message}});
    } else {
      this.router.navigateByUrl('error');
    }
  }

  parseUrl(url: string): UrlTree {
    return this.router.parseUrl(url + window.location.search);
  }

  getCurrentNavigation() {
    return this.router.getCurrentNavigation();
  }

  navigate(urlParams) {
    let route = this.parseUrl(this.router.url).toString();
    route = route + '?' + 'callId=' + urlParams.get('callId') + '&' +
    'customer=' + urlParams.get('customer') + '&' +
    'tenant=' + urlParams.get('tenant') + '&' +
    'name=' + urlParams.get('name') + '&' +
    'toolbar=' + urlParams.get('toolbar');
    this.router.navigateByUrl(route);
  }

  get url() {
    return this.router.url;
  }

  private keepCurrentParams(url: string): string {
    url = url + '?';
    const queryParams = this.router.parseUrl(window.location.search).queryParams;
    if (queryParams.tenant) {
      url = url + 'tenant=' + queryParams.tenant;
    }
    if (queryParams.name) {
      url = url + '&name=' + queryParams.name;
    }
    if (queryParams.toolbar) {
      url = url + '&toolbar=' + queryParams.toolbar;
    }
    if (queryParams.customer) {
      url = url + '&customer=' + queryParams.customer;
    }
    if (queryParams.callId) {
      url = url + '&callId=' + queryParams.callId;
    }
    return url;
  }
}
