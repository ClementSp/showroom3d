import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
declare var JitsiMeetJS: any;
declare var $: any;

export interface JitsiTracks {
  audio: any;
  video: any;
}

export interface JitsiParticipant {
  id: string;
  tracks: JitsiTracks;
}

export enum JitsiCallStatus {
  NOT_CONNECTED,
  CONNECTED
}

@Injectable({
  providedIn: 'root'
})
export class JitsiService {
  initOptions = {
    disableAudioLevels: true
  };
  options = {
    hosts: {
      domain: 'meet.jit.si',
      muc: 'conference.meet.jit.si'
    },
    bosh: 'https://meet.jit.si/http-bind?room=',
  };
  confOptions = {
    openBridgeChannel: true
  };

  connection = null;
  room = null;
  localTracks = [];
  participants = new Array<JitsiParticipant>();
  participantsListUpdate = new Subject();
  roomName = null;
  status = JitsiCallStatus.NOT_CONNECTED;
  micEnabled = true;
  camEnabled = true;
  hasPermission = new BehaviorSubject(null);
  audioOutputDevices = new BehaviorSubject([]);
  audioInputDevices = new BehaviorSubject([]);
  videoInputDevices = new BehaviorSubject([]);
  audioInputDevice = new BehaviorSubject('');
  audioOutputDevice = new BehaviorSubject('');
  videoInputDevice = new BehaviorSubject('');

  constructor() {}

  init(roomName: string) {
    this.options.bosh = 'https://meet.jit.si/http-bind?room=';
    this.room = null;
    this.connection = null;
    this.roomName = null;
    this.localTracks = [];
    this.participants = [];
    this.participantsListUpdate.next(true);
    this.status = JitsiCallStatus.NOT_CONNECTED;
    if (!roomName) {
      throw new Error('You must provide a room name');
    }
    this.roomName = roomName;
    this.initConnection();
    this.updateDevicesLists()
        .then(() => {
          this.initLocalTracks();
          this.audioOutputDevice.next(JitsiMeetJS.mediaDevices.getAudioOutputDevice());
        });
  }

  initConnection() {
    JitsiMeetJS.setLogLevel(JitsiMeetJS.logLevels.ERROR);
    JitsiMeetJS.init(this.initOptions);
    this.options.bosh += this.roomName;
    this.connection = new JitsiMeetJS.JitsiConnection(null, null, this.options);
    this.addConnectionListeners();
    this.connection.connect();
  }

  initLocalTracks() {
    JitsiMeetJS.createLocalTracks({ devices: [ 'audio', 'video' ] })
        .then(tracks => this.onLocalTracks(tracks)).catch(err => {
      this.hasPermission.next(false);
      this.unload();
    });
  }

  updateDevicesLists() {
    const promises = [
      this.updateVideoInputDevicesList(),
      this.updateAudioInputDevicesList(),
      this.updateAudioOutputDevicesList()
    ];
    return Promise.all(promises);
  }

  addConnectionListeners() {
    if (!this.connection) {
      return;
    }
    this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED, () => this.connectionSuccess());
    this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_FAILED, () => this.connectionFailed());
    this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED, () => this.disconnect());
  }

  addTrackListeners(track) {
    track.addEventListener(JitsiMeetJS.events.track.TRACK_AUDIO_LEVEL_CHANGED, level => this.trackAudioLevelChanged(level));
    track.addEventListener(JitsiMeetJS.events.track.TRACK_MUTE_CHANGED, () => this.trackMuted(track));
    track.addEventListener(JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED, () => this.trackStopped(track));
    track.addEventListener(JitsiMeetJS.events.track.TRACK_AUDIO_OUTPUT_CHANGED, deviceId => this.trackAudioOutputChanged(deviceId, track));
  }

  getCurrentOutputDevice() {
    return JitsiMeetJS.mediaDevices.getAudioOutputDevice();
  }

  getCurrentInputDevice() {
    return this.audioInputDevice;
  }

  setCurrentAudioOutputDevice(deviceId) {
    this.audioOutputDevice.next(deviceId);
    JitsiMeetJS.mediaDevices.setAudioOutputDevice(deviceId);
  }

  setCurrentVideoInputDevice(deviceId) {
    JitsiMeetJS.createLocalTracks({
      devices: [ 'audio', 'video' ],
      cameraDeviceId: deviceId
    }).then(tracks => this.onLocalTracks(tracks)).catch(err => {
      this.hasPermission.next(false);
    });
  }

  setCurrentAudioInputDevice(deviceId) {
    JitsiMeetJS.createLocalTracks({
      devices: [ 'audio', 'video' ],
      micDeviceId: deviceId
    }).then(tracks => this.onLocalTracks(tracks)).catch(err => {
      this.hasPermission.next(false);
    });

  }

  updateAudioOutputDevicesList() {
    if (JitsiMeetJS.mediaDevices.isDeviceChangeAvailable('output')) {
      return navigator.mediaDevices.enumerateDevices()
          .then(devices => {
            const audioOutputDevices = devices.filter(d => d.kind === 'audiooutput');
            this.audioOutputDevices.next(audioOutputDevices);
          });
    }
    return new Promise(r => r([]));
  }

  updateVideoInputDevicesList() {
    if (JitsiMeetJS.mediaDevices.isDeviceChangeAvailable('input')) {
      return navigator.mediaDevices.enumerateDevices()
          .then(devices => {
            const videoInputDevices = devices.filter(d => d.kind === 'videoinput');
            this.videoInputDevices.next(videoInputDevices);
          });
    }
    return new Promise(r => r([]));
  }

  updateAudioInputDevicesList() {
    if (JitsiMeetJS.mediaDevices.isDeviceChangeAvailable('input')) {
      return navigator.mediaDevices.enumerateDevices()
          .then(devices => {
            const audioInputDevices = devices.filter(d => d.kind === 'audioinput');
            this.audioInputDevices.next(audioInputDevices);
          });
    }
    return new Promise(r => r([]));
  }

  muteLocalMic() {
    const localAudio = this.localTracks.find(track => track.getType() === 'audio');

    if (localAudio) {
      if (localAudio.isMuted()) {
        localAudio.unmute();
        this.micEnabled = true;
      } else {
        localAudio.mute();
        this.micEnabled = false;
      }
    }
  }

  muteLocalVideo() {
    const localVideo = this.localTracks.find(track => track.getType() === 'video');

    if (localVideo) {
      if (localVideo.isMuted()) {
        localVideo.unmute();
        this.camEnabled = true;
      } else {
        localVideo.mute();
        this.camEnabled = false;
      }
    }
  }

  disposeLocalTracks() {
    const promises = [];
    this.localTracks.forEach(track => {
      promises.push(track.dispose());
    });
    return Promise.all(promises);
  }

  unload() {
    return this.disposeLocalTracks().then(() => {
      if (this.room) {
        return this.room.leave();
      } else {
        return new Promise(r => r(true));
      }
    }).then(() => {
      this.connection.disconnect();
      this.status = JitsiCallStatus.NOT_CONNECTED;
    });
  }

  addLocalTracks(tracks) {
    this.localTracks = tracks;
    this.localTracks.forEach(track => {
      this.addTrackListeners(track);
      if (track.getType() === 'audio') {
        this.audioInputDevice.next(track.deviceId);
        if (!this.micEnabled) {
          track.mute();
        }
      } else {
        this.videoInputDevice.next(track.deviceId);
        if (!this.camEnabled) {
          track.mute();
        }
      }
      if (this.room) {
        this.room.addTrack(track);
      }
      this.hasPermission.next(true);
    });
  }

  /*
    Events handlers
   */
  onLocalTracks(tracks) {
    this.disposeLocalTracks()
        .then(() => {
          this.addLocalTracks(tracks);
        });
  }

  trackAudioLevelChanged(audioLevel) {
    console.log(`Local audio level changed to: ${audioLevel}`);
  }

  roomTrackAudioLevelChanged(userId, audioLevel) {
    console.log(`Audio level for user: ${userId} is now ${audioLevel}`);
  }

  trackMuted(track) {
    console.log('Track muted:', track);
  }

  trackStopped(track) {
    console.log('Track stopped:', track);
  }

  trackAudioOutputChanged(deviceId, track) {
    console.log(`track audio output device was changed to ${deviceId} for track`, track);
  }

  trackRemoved(track) {
    console.log('Track removed:', track);
  }

  connectionSuccess() {
    this.status = JitsiCallStatus.CONNECTED;
    this.room = this.connection.initJitsiConference(this.roomName, this.confOptions);
    this.room.setReceiverVideoConstraint(720);
    this.room.on(JitsiMeetJS.events.conference.TRACK_ADDED, track => this.trackAdded(track));
    this.room.on(JitsiMeetJS.events.conference.TRACK_REMOVED, track => this.trackRemoved(track));
    this.room.on(JitsiMeetJS.events.conference.CONFERENCE_JOINED, () => this.conferenceJoined());
    this.room.on(JitsiMeetJS.events.conference.USER_JOINED, id => this.userJoined(id));
    this.room.on(JitsiMeetJS.events.conference.USER_LEFT, id => this.userLeft(id));
    this.room.on(JitsiMeetJS.events.conference.TRACK_MUTE_CHANGED, track => this.trackMuted(track));
    this.room.on(JitsiMeetJS.events.conference.DISPLAY_NAME_CHANGED, (id, displayName) => this.displayNameChanged(id, displayName));
    this.room.on(JitsiMeetJS.events.conference.TRACK_AUDIO_LEVEL_CHANGED, (id, level) => this.roomTrackAudioLevelChanged(id, level));
    this.room.join();
  }

  displayNameChanged(userId, displayName) {
    console.log(`User ${userId} has changed its name to ${displayName}`);
  }

  userLeft(userId) {
    const participantIndex = this.participants.findIndex(p => p.id === userId);

    if (participantIndex <= -1) {
      return;
    }

    this.participantsListUpdate.next(true);
    this.participants.splice(participantIndex, 1);
  }

  userJoined(userId) {
    if (this.participants.length >= 1) {
      this.room.kickParticipant(userId);
      return;
    }
    const index = this.participants.findIndex(p => p.id === userId);

    if (index > -1) {
      return;
    }

    const participant = {
      id: userId,
      tracks: {
        video: null,
        audio: null
      }
    };
    this.participants.push(participant);
    this.participantsListUpdate.next(true);
  }

  conferenceJoined() {
    this.localTracks.forEach(track => {
      this.room.addTrack(track);
    });
  }

  trackAdded(track) {
    if (track.isLocal()) {
      return;
    }
    const participantId = track.getParticipantId();
    const participant = this.participants.find(m => m.id === participantId);

    if (!participant) {
      return;
    }
    this.addTrackListeners(track);
    participant.tracks[track.getType()] = track;
    this.participantsListUpdate.next(true);
  }

  disconnect() {
    this.connection.removeEventListener(JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED, () => this.connectionSuccess());
    this.connection.removeEventListener(JitsiMeetJS.events.connection.CONNECTION_FAILED, () => this.connectionFailed());
    this.connection.removeEventListener(JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED, () => this.disconnect());
  }

  connectionFailed() {
    console.error('Connection Failed!');
  }
}
