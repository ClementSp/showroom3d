import { Injectable } from '@angular/core';
import {HttpClientService} from './http-client.service';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {ToastService} from './toast.service';
import {userEnv} from '../../environments/user.env';
import {EditorService} from './editor.service';
import {Observable} from 'rxjs';

interface LoginData {
  jwt: string;
  lng: string;
  expiresAt: number;
  from: 'platform' | 'credentials';
}

export interface Credentials {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService extends HttpClientService {
  readonly baseUrl = `users/_me`;
  readonly handleLng = ['en', 'fr', 'es'];

  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
    super(httpClient, toastService);
    this.initEventHandler();
    // userEnv.jwt = localStorage.getItem('jwt');
  }

  getUser(): Promise<void> {
    return this.getMe().then((me: any) => {
      userEnv.authorization = userEnv.loginType === 'platform';
    });
  }

  getMe(): Promise<any> {
    return this.get(environment.apiUrl2 + 'auth/me').pipe(
        map((response) => {
          if(response.result) {
            const user = response.result;
            user.id = response.result.id;
            return user;
          } else {
            return null;
          }
        })
    ).toPromise();
  }

  getUsers() {
    return this.post(environment.apiUrl2 + userEnv.tenant + '/users/all', {}).pipe(
        map((response: any) => {
          return response.result;
        }));
  }

  login(credentials: Credentials): Promise<any> {
    return this.httpClient.post<any>(`${environment.apiUrl2}auth/login`, credentials)
        .pipe(
            map(resp => {
              const result = resp.result;

              this.completeLogin({
                jwt: result.jwt,
                lng: userEnv.currentLng,
                expiresAt: result.expiresAt,
                from: 'credentials'
              });
              return result;
            })
        ).toPromise();
  }

  register(data: any): Promise<string> {
    return this.httpClient.post<any>(`${environment.apiUrl2}users/register`, data)
        .pipe(map(resp => resp.result))
        .toPromise();
  }

  updateUser(user: any): Observable<any> {
    return this.put(environment.apiUrl2 + 'users', user).pipe(
        map((response: any) => {
          return response.result;
        }));
  }

  // PRIVATE FUNCTIONS

  private initEventHandler() {
    const eventMethod = window.addEventListener ? 'addEventListener'  : 'attachEvent';
    const eventHandler = window[eventMethod];
    const messageEvent = (eventMethod === 'attachEvent') ? 'onmessage' : 'message';

    eventHandler(messageEvent, (event) => {
      if (event.data.data && event.data.message) {
        switch (event.data.message) {
          case 'authState':
            event.data.data.from = 'platform';
            return this.autoLoginFromPlatform(event.data.data);
          default: return;
        }
      }
    });
    this.postMessageToParent('init', undefined);
  }

  private autoLoginFromPlatform(loginData: any) {
    if (loginData.logged) {
      this.completeLogin({
        jwt: loginData.jwt,
        expiresAt: loginData.expiresAt,
        lng: userEnv.currentLng,
        from: 'platform'
      });
    }
  }

  private completeLogin(data: LoginData) {
    localStorage.setItem('authState', JSON.stringify(data));
    userEnv.loginType = data.from;
    userEnv.jwt = data.jwt;
    userEnv.expiresAt = data.expiresAt;
    userEnv.authorization = true;
    userEnv.initialized.next(true);
    if (data.lng && this.handleLng.includes(data.lng)) {
      userEnv.currentLng = data.lng;
    }
  }

  private postMessageToParent(msg, data) {
    parent.window.postMessage({
      message: msg,
      data
    }, environment.parentUrl);
  }
}
