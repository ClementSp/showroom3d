import {Injectable} from '@angular/core';
import {from, Subject, Subscription} from 'rxjs';
import { concatMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PreloaderService {

  speed = 200;
  task = new Subject();
  subscription: Subscription;
  alreadyLoaded = new Array<string>();

  constructor() {
  }

  addToQueue(urls: Array<string>) {
    if (this.preloadSupported()) {
      const filteredUrls = urls.filter(url => this.alreadyLoaded.indexOf(url) === -1);
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.subscription = this.task.pipe(
          concatMap((url: string) => from(this.preloadUrl(url)))
      ).subscribe();
      filteredUrls.forEach(url => this.task.next(url));
    }
  }

  private preloadUrl(url: string) {
    return new Promise((resolve) => {
      let linkTag: HTMLElementTagNameMap['link'];

      try {
        linkTag = document.createElement('link');
        linkTag.href = url;
        linkTag.as = 'image';
        linkTag.rel = 'preload';
        linkTag.onload = () => {
          this.alreadyLoaded.push(url);
          setTimeout(() => resolve(true), this.speed);
        };
        document.head.appendChild(linkTag);
      } catch (e) {
        setTimeout(() => resolve(true), this.speed);
      }
    });
  }

  private preloadSupported(): boolean {
    const link = document.createElement('link');
    const relList = link.relList;
    if (!relList || !relList.supports) {
      return false;
    }
    return relList.supports('preload');
  }
}
