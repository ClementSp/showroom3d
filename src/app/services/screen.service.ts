import { Injectable } from '@angular/core';
import {Platform} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ScreenService {

  constructor(private platform: Platform) { }

  screenIsXs() {
    return this.platform.width() <= 576;
  }

  screenIsSm() {
    return this.platform.width() > 576 && this.platform.width() <= 768;
  }

  screenIsMd() {
    return this.platform.width() > 768 && this.platform.width() <= 992;
  }

  screenIsLg() {
    return this.platform.width() > 992 && this.platform.width() <= 1200;
  }

  screenIsXl() {
    return this.platform.width() > 1200;
  }
}
