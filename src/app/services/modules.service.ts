import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToastService} from './toast.service';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {userEnv} from '../../environments/user.env';

@Injectable({
    providedIn: 'root'
})
export class ModulesService {
    constructor(public httpClient: HttpClient,
                public toastService: ToastService) {}

    getModules(): Promise<any> {
        return this.httpClient.get<any>(this.baseUrl(), {})
            .pipe( map(resp => resp.result))
            .toPromise();
    }

    getConfig(moduleName: string): Promise<any> {
        return this.httpClient.get<any>(`${this.configBaseUrl()}${userEnv.showroomId}/${moduleName}`)
            .pipe( map(resp => {
                const result = resp.result;

                if (result.config) {
                    return result.config;
                }
            }))
            .toPromise();
    }

    private baseUrl() {
        return `${environment.apiUrl2}${userEnv.tenant}/modules/showroom360/`;
    }

    private configBaseUrl() {
        return `${environment.apiUrl2}${userEnv.tenant}/modules/config/showroom360/`;
    }
}
