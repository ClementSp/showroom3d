import { Injectable } from '@angular/core';
import {HttpClientService} from './http-client.service';
import {HttpClient} from '@angular/common/http';
import {ToastService} from './toast.service';
import {environment} from '../../environments/environment';
import {userEnv} from '../../environments/user.env';
import {showroomEnv} from '../../environments/showroom.env';

@Injectable({
  providedIn: 'root'
})
export class EmailService extends HttpClientService {

  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
    super(httpClient, toastService);
  }

  showroomContact(body: any) {
    return this.post(`${this.baseUrl()}send/showroomContact/${userEnv.showroomId}`, body).toPromise();
  }

  private baseUrl() {
    return `${environment.apiUrl2}${userEnv.tenant}/email/`;
  }
}
