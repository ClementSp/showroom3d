import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClientService} from './http-client.service';
import {userEnv} from '../../environments/user.env';
import {ToastService} from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class FixturesService extends HttpClientService {

  constructor(public httpClientService: HttpClient,
              public toastService: ToastService) {
    super(httpClientService, toastService);
  }

  getFixture(fixtureId: string) {
    return this.get(this.baseUrl() + fixtureId).pipe(
        map((response) => response.result)
    );
  }

  getAllFixtures() {
    return this.get( this.baseUrl() +  'all').pipe(
        map((response) => response.result)
    );
  }

  private baseUrl() {
    return `${environment.apiUrl2}${userEnv.tenant}/fixtures/`;
  }
}
