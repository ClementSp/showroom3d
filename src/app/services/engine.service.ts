import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import * as THREE from 'three';
import MovementMarker from '../models/MovementMarker';
import {Poi, Showroom} from '../models/showroom.model';
import ActionMarker from '../models/ActionMarker';
import {CarouselComponent} from '../components/carousel/carousel.component';
import {ModalController, Platform} from '@ionic/angular';
import {UrlContentComponent} from '../components/url-content/url-content.component';
import {LogType, ShowroomsService} from './showrooms.service';
import {CustomProductPageV2Component} from '../components/custom-product-page-v2/custom-product-page-v2.component';
import PortalMarker, {Portal, PortalType, ShowroomPortal, SweepPortal} from '../models/PortalMarker';
import {PdfViewerComponent} from '../components/pdf-viewer/pdf-viewer.component';
import {Template1Component} from '../components/custom-product-page-v2/templates/template1/template-1.component';
import {Template2Component} from '../components/custom-product-page-v2/templates/template2/template-2.component';
import {Template3Component} from '../components/custom-product-page-v2/templates/template3/template-3.component';
import {CustomerService} from './customer.service';
import {MediasService} from './medias.service';
import {UserService} from './user.service';
import {userEnv} from '../../environments/user.env';
import {TranslateService} from '@ngx-translate/core';
import {AnalyticService} from './analytic.service';
import {FirstStepComponent} from '../components/custom-product-page-v2/templates/cci/first-step/first-step.component';
import {FirstPremiumStepComponent} from '../components/custom-product-page-v2/templates/cci/first-premium-step/first-premium-step.component';
import {showroomEnv} from '../../environments/showroom.env';
import {WishlistHandler} from '../DataHandlers/Wishlist.handler';
import {ConfirmationModalComponent} from '../components/confirmation-modal/confirmation-modal.component';
import {AudioHandler} from '../DataHandlers/audio.handler';
import {ImageViewerModalComponent} from '../components/image-viewer-modal/image-viewer-modal.component';
import {TemplateFullPicturesComponent} from '../components/custom-product-page-v2/templates/templateFullPicture/template-full-pictures.component';
import {PreloaderService} from './preloader.service';
import {ShowroomHandler} from '../DataHandlers/Showroom.handler';
import {SceneHandler} from '../DataHandlers/scene.handler';
import {EditorService} from './editor.service';

interface Annotation {
  description: string;
  top: string;
  left: string;
}

@Injectable({
  providedIn: 'root'
})
export class EngineService {
  public dismissModalEvent = new EventEmitter<void>();
  readonly isGyroscropeOn = new BehaviorSubject<boolean>(true);
  public annotation: Annotation;
  readonly mainSceneName = 'main';
  readonly fixtureName = 'fixture';
  readonly productName = 'product';
  readonly shapePoiName = 'shapePoi';
  readonly smartObjectName = 'smartObject';
  private selectedProduct = undefined;
  private material: THREE.MeshBasicMaterial;
  public showroom: Showroom;
  public editorView: any;
  public editorMeshView: any;
  public textureLoader = new THREE.TextureLoader();
  public defaultSprites: any;

  readonly latitudeBoundaryBeforeUpswing = -0.5;
  readonly upswingValue = -0.3;

  // SPHERE_RADIUS In PSV lib to 100

  constructor(private modalController: ModalController,
              private userService: UserService,
              private platform: Platform,
              private mediasService: MediasService,
              private translateService: TranslateService,
              private showroomService: ShowroomsService,
              private analyticService: AnalyticService,
              public customerService: CustomerService,
              public wishListHandler: WishlistHandler,
              private preloaderService: PreloaderService,
              private audioHandler: AudioHandler,
              private sceneHandler: SceneHandler) {
    this.loadDefaultTextures();
  }

  init(viewerContainer: HTMLElement, entranceFromUrl) {
    this.showroom = showroomEnv.showroom;

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    this.sceneHandler.initScene(viewerContainer);

    this.loadVideoSpriteTextures().then(() => {
    this.loadProductSpriteTextures().then(() => {
    this.loadPdfSpriteTextures().then(() => {
    this.loadImageSpriteTextures().then(() => {
    this.loadChatSpriteTextures().then(() => {
    this.loadPoisTextures()})})})})});
    this.loadSweepLinkSpriteTextures();

    this.launchPresentation();

    if (!userEnv.authorization) {
      this.analyticService.putLog(LogType.CONNECTION);
    }

    this.setCameraFromEntrance();
    this.onDismissModalEvent();
    this.zoom(1);
    this.initShowroom(entranceFromUrl);
  }

  private launchPresentation() {
    if (!userEnv.authorization) {
      if (this.showroom.settings.presentationVideoAutoStart
          && this.showroom.settings.presentationVideoUrl) {
        this.displayVideoModal(this.showroom.settings.presentationVideoUrl, );
      } else if (this.showroom.settings.presentation.data) {
        this.displayVideoModal(this.showroom.settings.presentation.data.url);
      }
    }
  }

  initShowroom(entranceFromUrl) {
    this.initAudio();
  }

  private initAudio() {
    if (this.showroom.settings.audio?.url) {
      const audio = new Audio();
      audio.src = this.showroom.settings.audio?.url;
      audio.loop = true;
      this.audioHandler.addAudioToPlay(audio);
    }
  }

  toggleMute() {
    this.sceneHandler.toggleMute();
  }

  toggleGyroscope() {
    this.isGyroscropeOn.next(!this.isGyroscropeOn.value);
  }

  zoom(value) {
  }

  getZoomLevel() {
  }

  getSceneChildren(sceneName: string) {
    if (this.scene) {
      return this.scene.children;
    }
  }

  getDirection(v1: THREE.Vector3, v2: THREE.Vector3) {
    const dir = new THREE.Vector3();

    return dir.subVectors(v2, v1);
  }

  getCameraPositionLonLat() {
  }

  setCameraPositionFromLonLat(rotation) {
  }

  setCameraFromEntrance() {
    this.sceneHandler.setCamera(this.showroom.settings.entrance);
  }

  raycastFromCamera(x: number, y: number) {
    const mouse = new THREE.Vector2(x, y);

    if (!this.sceneHandler.camera) {
      return;
    }

    this.sceneHandler.raycaster.setFromCamera(mouse, this.sceneHandler.camera);
  }

  getIntersectObjects(recursive = false) {
    if (!this.scene) {
      return [];
    }
    return this.sceneHandler.raycaster.intersectObjects(this.scene.children, recursive);
  }

  get camera() {
    return this.sceneHandler.camera;
  }

  get scene() {
    return this.sceneHandler.scene;
  }

  async triggerObjectHover(intersects: THREE.Intersection[]) {
    const actionsMarkers = intersects.map(i => i.object).filter(i => !(i instanceof ActionMarker));

    if (actionsMarkers.length === 0) {
      this.annotation = undefined;
    }

    intersects.forEach((intersection: THREE.Intersection) => {
      if (intersection.object instanceof ActionMarker) {
        if (this.annotation) {
          this.annotation = undefined;
        }
        this.displayTooltip(intersection);
      } else if (intersection.object instanceof PortalMarker) {
        if (this.annotation) {
          this.annotation = undefined;
        }
        this.displayPortalTooltip(intersection);
      }
    });
  }

  displayTooltip(intersection: THREE.Intersection) {
    const poi: Poi = (intersection.object as any).poi;
    const positionOnDom = this.convertTo2DPositionOnScreen(intersection.point);

    this.annotation = {
      description: poi.tip,
      top: `${positionOnDom.y}px`,
      left: `${positionOnDom.x}px`
    };
  }

  displayPortalTooltip(intersection: THREE.Intersection) {
    const portal: Portal = (intersection.object as any).portal;
    const positionOnDom = this.convertTo2DPositionOnScreen(intersection.point);

    this.annotation = {
      description: portal.tip,
      top: `${positionOnDom.y}px`,
      left: `${positionOnDom.x}px`
    };
  }

  async handlePortalClick(portal: ShowroomPortal | SweepPortal) {
    if (portal.type === PortalType.SWEEP_LINK) {
      //this.currentNodeHandler.changeCurrentNode.emit({id: portal.sweepId, setCamera: true});
    } else if (portal.type === PortalType.SHOWROOM_LINK) {
      if (showroomEnv.sendEndExperienceWishlist && ( this.wishListHandler.length > 0)) {
        const modal = await this.modalController.create({
          component: ConfirmationModalComponent,
          cssClass: 'fit-content-modal',
          componentProps: {
            message: `Do you want to received your favorite list by mail ?`
          }
        });
        modal.onDidDismiss().then((resp) => {
          if (resp.data) {
            this.wishListHandler.sendWishList().then((result) => {
              this.goToShowroom(portal);
            });
          } else {
            this.goToShowroom(portal);
          }
        });
        return modal.present();
      } else {
        this.goToShowroom(portal);
      }
    }
  }

  goToShowroom(portal) {
    let nextLoc = location.search.replace(/name=[^&$]*/i, '');
    nextLoc = nextLoc.replace(/entranceSweep=[^&$]*/i, '');
    nextLoc = nextLoc.replace(/isGyroscropeOn=[^&$]*/i, '');
    if (nextLoc.indexOf('?') > -1) {
      nextLoc = nextLoc + '&name=' + portal.link.target;
    } else {
      nextLoc = nextLoc + '?name=' + portal.link.target;
    }
    nextLoc = nextLoc + '&entranceSweep=' + portal.link.sweepId;
    nextLoc = nextLoc + '&isGyroscropeOn=' + this.isGyroscropeOn.value;
    location.search = nextLoc;
  }

  async triggerObjectOnClick(intersects: THREE.Intersection[], isEditing) {
    let isIntersecting = false;

    if (intersects && intersects.length > 0) {
      intersects.forEach((intersection: THREE.Intersection) => {
        if (!isIntersecting) {
          if (intersection.object instanceof PortalMarker) {
            this.handlePortalClick(intersection.object.portal);
            isIntersecting = true;
            return;
          }

          if (intersection.object.userData.type === 'POI' || intersection.object.userData.type === 'SHAPE_POI') {
            const poi = intersection.object.userData.poi;
            switch (poi.type) {
              case 'product':
                if (poi.data?.products.length > 1) {
                  this.displayCarousel(poi);
                } else {
                  this.displayCustomProduct(poi);
                }
                isIntersecting = true;
                return;
              case 'productUrl':
                if (poi.data?.products.length > 1) {
                  this.displayCarousel(poi);
                } else {
                  this.displayProductModal(poi);
                }
                isIntersecting = true;
                return;
              case 'video':
                const url = this.getVideoUrl(poi);
                this.displayVideoModal(url, poi.data.videos[0].openInNewTab, poi.id);
                isIntersecting = true;
                return;
              case 'pdf':
                this.displayPdf(poi);
                isIntersecting = true;
                return;
              case 'chat':
                this.displayChat(poi);
                isIntersecting = true;
                return;
              case 'image':
                if (poi.data.images.length > 1) {
                  this.displayCarousel(poi);
                } else {
                  this.displayImageModal(poi);
                }
                isIntersecting = true;
                return;
            }
          } else {
            const objectPoi = this.getPoiFrom3dObject(intersection.object);
            if (objectPoi && objectPoi.userData) {
              switch (objectPoi.userData.type) {
                case 'PRODUCT':
                  const product = this.scene.children.find(i => i.userData.id === objectPoi.userData.id);

                  if (product && product.userData.type === 'PRODUCT') {
                    this.selectedProduct = product;
                  }
                  this.displayCustomProduct(product);
                  isIntersecting = true;
                  return;
                case 'SMART_OBJECT':
                  isIntersecting = true;
                  return;
              }
            }
          }
        }
      });
    }
    if (!isEditing && !isIntersecting) {
      const meshIntersect = intersects.find(intersect => intersect.object.name === 'shellComponent');
      if (meshIntersect) {
        this.sceneHandler.moveCamera(meshIntersect.point);
      }
      // todo camera move
    }
  }

  getPoiFrom3dObject(object) {
    if (object && object.userData) {
      const type = object.userData.type;
      if (type === 'POI' || type === 'SHAPE_POI' || type === 'PRODUCT' || type === 'SMART_OBJECT') {
        return object;
      } else if (object.parent) {
        return this.getPoiFrom3dObject(object.parent);
      } else {
        return false;
      }
    } else if (object.parent) {
      return this.getPoiFrom3dObject(object.parent);
    }
    return false;
  }

  selectCustomProductClass(poi: any) {
    let template = poi?.data?.template;
    if (this.selectedProduct === poi){
      template = this.selectedProduct.userData.template
    }
    if (!template) {
      return CustomProductPageV2Component;
    }
    switch (template.toString()) {
      case '0':
        return CustomProductPageV2Component;
        break;
      case '1':
        return Template1Component;
        break;
      case '2':
        return Template2Component;
        break;
      case '3':
        return Template3Component;
        break;
      case '4':
        return FirstStepComponent;
        break;
      case '5':
        return FirstPremiumStepComponent;
        break;
      case '6':
        return TemplateFullPicturesComponent;
        break;
      default:
        return CustomProductPageV2Component;
        break;
    }
  }

  selectCustomProductCssClass(poi: any) {
    if (this.selectedProduct === poi){
      return 'backdrop-05 template1';
    }
    if (!poi.data.template) {
      return 'backdrop-05 template1';
    }
    switch (poi.data.template.toString()) {
      case '0':
        return 'default backdrop-05';
      case '1':
        return 'backdrop-05 template1';
      case '2':
        return 'backdrop-05 template2';
      case '3':
        return 'backdrop-05 template3';
      case '4':
        return 'backdrop-05 two-steps-template';
      case '5':
        return 'backdrop-05 two-steps-premium-template';
      case '6':
        return 'backdrop-05 fullPictures';
      default:
        return 'default backdrop-05';
    }
  }

  async displayCustomProduct(poi: any) {
    const productClass = this.selectCustomProductClass(poi);
    const productCSS = this.selectCustomProductCssClass(poi);
    const modal = await this.modalController.create({
      component: productClass,
      cssClass: productCSS,
      componentProps: {
        data: {
          ean: (poi === this.selectedProduct) ? this.selectedProduct.userData.ean : poi.data?.products[0],
          productId: (poi === this.selectedProduct) ? this.selectedProduct.userData.productId : undefined ,
          object : poi
        }
      }
    });
    return await modal.present()
        .then(() => {
          if (!userEnv.authorization) {
            this.analyticService.putLog(LogType.CLICK_PRODUCT, poi.data?.products[0]);
          }
        });
  }

  getPdfUrl(poi) {
    let url = '';

    if (poi.data.pdfs) {
      let foundLanguage = false;
      for (let i = 0; i < poi.data.pdfs.length; i++) {
        if (poi.data.pdfs[i].language === this.translateService.currentLang) {
          foundLanguage = true;
          url = poi.data.pdfs[i].url;
        }
      }
      if (!foundLanguage) {
        url = poi.data.pdfs[0].url;
      }
    } else if (poi.data.pdf) {
      url = poi.data.pdf;
    }

    return url;
  }

  async displayPdf(poi: any) {
    const modal = await this.modalController.create({
      component: PdfViewerComponent,
      cssClass: 'backdrop-05 pdf-page',
      componentProps: {
        data: {
          pdf: this.getPdfUrl(poi)
        }
      }
    });
    return await modal.present();
  }

  async displayChat(poi: any) {
  }

  private loadScript() {
  }

  openChat() {
  }

  triggerClosingChat() {
  }

  async displayCarousel(poi: any) {
    const modal = await this.modalController.create({
      component: CarouselComponent,
      cssClass: 'fullscreen-modal backdrop-05',
      componentProps: {
        poi
      }
    });
    return await modal.present()
        .then(() => {
          if (!userEnv.authorization) {
            this.analyticService.putLog(LogType.CLICK_CLUSTER, poi.id);
          }
        });
  }

  async displayImageModal(poi: Poi) {
    let cssClass = 'product-modal backdrop-05 ';

    if (!this.platform.is('mobile')) {
      cssClass += ' lg-product-page';
    }
    if (poi.data.images[0].openInNewTab) {
      window.open(poi.data.images[0].url, '_blank');
    } else {
      this.mediasService.getMediasById(poi.data.images[0].id).subscribe(async (media) => {
        const modal = await this.modalController.create({
          component: ImageViewerModalComponent,
          cssClass: 'edit-marker-component backdrop-05',
          showBackdrop: true,
          backdropDismiss: true,
          componentProps: {
            src: media.url,
            title: 'Preview'
          }
        });
        return await modal.present();
      });

    }
  }

  async displayProductModal(poi: Poi) {
    let cssClass = 'product-modal backdrop-05 ';

    if (!this.platform.is('mobile')) {
      cssClass += ' lg-product-page';
    }
    if (poi.data.products[0].openInNewTab) {
      let tabOrWindow = window.open(poi.data.products[0].productUrl, '_blank');
      if (!tabOrWindow) {
        tabOrWindow = parent.window.open(poi.data.products[0].productUrl, '_blank');
      }
      if (tabOrWindow) {
        tabOrWindow.focus();
        if (!userEnv.authorization) {
          this.analyticService.putLog(LogType.CLICK_PRODUCT, poi.data.products[0].ean);
        }
      }
    } else {
      const modal = await this.modalController.create({
        component: UrlContentComponent,
        cssClass,
        componentProps: {
          data : {
            url: poi.data.products[0].productUrl,
            dismissEvent: this.dismissModalEvent,
            scroll: false
          }
        }
      });
      return await modal.present()
          .then(() => {
            if (!userEnv.authorization) {
              this.analyticService.putLog(LogType.CLICK_PRODUCT, poi.data.products[0].ean);
            }
          });
    }
  }

  getVideoUrl(poi) {
    let url: string;

    if (poi.data.videos) {
      let foundLanguage = false;
      for (let i = 0; i < poi.data.videos.length; i++) {
        if (poi.data.videos[i].language === this.translateService.currentLang) {
          foundLanguage = true;
          url = poi.data.videos[i].url;
        }
      }
      if (!foundLanguage) {
        url = poi.data.videos[0].url;
      }
    } else if (poi.data.url) {
      url = poi.data.url;
    }

    return url;
  }

  async displayVideoModal(url: string, openInNewTab: boolean = false, origin?: string) {
    if (url) {
      const urlObj = new URL(url);

      urlObj.searchParams.set('autoplay', 'true');
      urlObj.searchParams.set('mute', '1');
      if (openInNewTab) {
        let tabOrWindow = window.open(urlObj.href, '_blank');
        if (!tabOrWindow) {
          tabOrWindow = parent.window.open(urlObj.href, '_blank');
        }
        if (tabOrWindow) {
          if (!userEnv.authorization) {
            this.analyticService.putLog(LogType.CLICK_VIDEO, url, undefined, origin);
          }
          tabOrWindow.focus();
        }
      } else {
        const modal = await this.modalController.create({
          component: UrlContentComponent,
          cssClass: 'video-modal backdrop-05',
          componentProps: {
            data: {
              url: urlObj.href,
              dismissEvent: this.dismissModalEvent,
              scroll: false
            }
          }
        });
        return await modal.present().then(() => {
          if (!userEnv.authorization) {
            this.analyticService.putLog(LogType.CLICK_VIDEO, url, undefined, origin);
          }
        });
      }
    }
  }


// OBJECT HANDLERS


// PRIVATE FUNCTIONS


  private onDismissModalEvent() {
    this.dismissModalEvent.subscribe(() => {
      this.modalController.getTop().then((top: HTMLIonModalElement) => {
        if (top) {
          this.modalController.dismiss();
        }
      });
    });
  }

  private convertTo2DPositionOnScreen(vector: THREE.Vector3) {
    const camera = this.camera;
    const canvas = this.sceneHandler.renderer.domElement; // `renderer` is a THREE.WebGLRenderer

    vector.project(camera); // `camera` is a THREE.PerspectiveCamera

    vector.x = Math.round((0.5 + vector.x / 2) * (canvas.width / window.devicePixelRatio));
    vector.y = Math.round((0.5 - vector.y / 2) * (canvas.height / window.devicePixelRatio));

    return { x: vector.x, y: vector.y };
  }

  getSpritePath(showroom: Showroom, poi: Poi) {
    let spritePath;
    if (poi) {
      switch (poi.type) {
        case 'product':
        case 'productUrl':
          spritePath = showroom.settings.sprites.productPoi;
          break;
        case 'video':
          spritePath = showroom.settings.sprites.videoPoi;
          break;
        case 'pdf':
          spritePath = showroom.settings.sprites.pdfPoi;
          break;
        case 'chat':
          spritePath = showroom.settings.sprites.chatPoi;
          break;
        default:
          spritePath = showroom.settings.sprites.productPoi;
          break;
      }
    }
    if (!spritePath) {
      spritePath = '/assets/ui/info2.png';
    }
    return spritePath;
  }


  /* --------------- Action ---------------*/
  isEditorView() {
    return this.editorView;
  }

  setEditorView(value) {
    return this.editorView = value;
  }

  isEditorMeshView() {
    return this.editorMeshView;
  }

  setEditorMeshView(value) {
    this.sceneHandler.setMeshMaterial(value);
    return this.editorMeshView = value;
  }

  /* ------------ Texture ------------ */
  public loadDefaultTextures() {
    return fetch('/assets/ui/sprites.json').then((resp) => resp.json()).then((result) => {
      this.defaultSprites = result.defaultSprites;
      this.defaultSprites.forEach((sprite) => {
        const texture = this.textureLoader.load(sprite.path);
        if(texture && sprite.path) {
          this.sceneHandler.texturesMap.set(sprite.name, texture);
        }
      });
    });
  }

  async loadVideoSpriteTextures() {
    return this.addTexture(this.showroom.settings.sprites.videoPoi);
  }
  async loadProductSpriteTextures() {
    return this.addTexture(this.showroom.settings.sprites.productPoi);
  }
  async loadPdfSpriteTextures() {
    return this.addTexture(this.showroom.settings.sprites.pdfPoi);
  }
  async loadImageSpriteTextures() {
    return this.addTexture(this.showroom.settings.sprites.imagePoi);
  }
  async loadChatSpriteTextures() {
    return this.addTexture(this.showroom.settings.sprites.chatPoi);
  }
  async loadSweepLinkSpriteTextures() {
    return this.addTexture(this.showroom.settings.sprites.sweepLink);
  }
  async loadSweepSpriteTextures() {
    return this.addTexture(this.showroom.settings.sprites.sweep);
  }

  async loadPoisTextures() {
    const promises = [];
    this.showroom.pois.forEach((poi) => {
      if (poi.sprite) {
        promises.push(this.addTexture(poi.sprite.path));
      }
    });
    return Promise.all(promises).then(() => {});
  }

  async addTexture(path: string) {
    const texture = this.textureLoader.load(path);
    if (texture && path) {
      this.sceneHandler.texturesMap.set(path, texture);
    }
  }

  confirmTextureOrGetDefault(spritePath: string, key: string): string {
    const texture = this.sceneHandler.texturesMap.get(spritePath);

    if (texture) {
      return spritePath;
    } else {
      const indexOfSpritePath = this.defaultSprites.map(item => item.name).indexOf(key);
      return this.defaultSprites[indexOfSpritePath].path;
    }
  }
}
