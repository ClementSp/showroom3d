import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {userEnv} from '../../environments/user.env';
import {HttpClientService} from './http-client.service';

@Injectable({
  providedIn: 'root'
})
export class SmartObjectsService {

  constructor(private httpClient: HttpClientService) { }

  getAllSmartObjects(throwError = false) {
    return this.httpClient.post(this.baseUrlSmartObjects() + 'all', {}, throwError).pipe(
        map((response: any) => response.result)
    ).toPromise();
  }

  getSmartObjectById(id: string, throwError = false): Promise<any> {
    const url = this.baseUrlSmartObjects() + id;

    return this.httpClient.post(url, {}, throwError).pipe(
        map((response: any) => response.result)
    ).toPromise();
  }

  private baseUrlSmartObjects() {
    return environment.apiUrl2 + userEnv.tenant + '/smartObjects/';
  }
}
