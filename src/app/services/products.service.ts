import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import CustomProduct, {ACustomProduct, ProductMedia} from '../models/CustomProduct';
import {environment} from '../../environments/environment';
import {userEnv} from '../../environments/user.env';
import {showroomEnv} from '../../environments/showroom.env';
import {QueryDSL} from './queryDSL.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpClient: HttpClient) { }

  putProduct(product: any) {
    return this.httpClient.put<any>(this.baseUrlProducts() + 'update', product, { headers: this.header() });
  }

  getAllProducts() {
    return this.httpClient.post(this.baseUrlProducts() + 'all_simple', {}, {
      headers: this.header()
    }).pipe(
        map((response: any) => response.result)
    );
  }

  getProducts(term?: string, page?: number, language?: string) {
    term = term || '';
    page = page || 0;
    language = language || '';
    return this.httpClient.post<any>(this.baseUrlProducts() + 'find', {term, page, language}, {
      headers: this.header()
    }).pipe(
        map((response) => response.result)
    );
  }

  getProductById(id: string): Promise<any> {
    const url = this.baseUrlProducts() + id;
    return this.httpClient.get<any>(url).pipe(
        map((response) => {
          if (response.result) {
            return response.result;
          } else {
            return null;
          }
        })
    ).toPromise();
  }

  getProductByEan(ean: string, customerId?: string, language?: string): Observable<CustomProduct> {
    const url = this.baseUrlProducts() + 'showroom/' + ean + '/' + customerId + '/' + language;

    return this.httpClient.get<any>(url).pipe(
        map((response) => {
          if (response.result) {
            return new CustomProduct(response.result as ACustomProduct);
          } else {
            return null;
          }
        })
    );
  }

  getMediaUrl(ean: string, mediaId: string) {
    const url = this.baseUrlProducts() + ean + '/medias/' + mediaId;

    return this.httpClient.get<any>(url).pipe(
        map(response => {
          if (!response) {
            return;
          }
          if (typeof response.result === 'string' && response.result.indexOf('?') !== -1) {
            response.result = response.result.substring(0, response.result.indexOf('?'));
          }
          return response.result;
        })
    );
  }

  putMedia(ean: string, media: ProductMedia, file: any) {
    const formData = new FormData();

    formData.append('file', file);
    // tslint:disable-next-line:forin
    for (const key in media) {
      formData.append(key, media[key]);
    }

    return this.httpClient.post<any>( this.baseUrlProducts() + ean + '/medias',
        formData, { headers: this.blobHeader() })
        .pipe(map((response: any) => response.result));
  }

  deleteMedia(ean: string, mediaId: string) {
    return this.httpClient.delete<any>(this.baseUrlProducts() + ean + '/medias/' + mediaId, { headers: this.header() });
  }

  rotateMedia(ean: string, mediaId: string, rotationDeg: number) {
    return this.httpClient.post<any>(this.baseUrlProducts() + ean + '/medias/' + mediaId + '/rotate',
        {rotation: rotationDeg}, { headers: this.header() }).pipe(map(res => res.result));
  }

  private header() {
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${userEnv.jwt}`
    };
  }

  private blobHeader() {
    return {
      Authorization: `Bearer ${userEnv.jwt}`
    };
  }

  private baseUrlProducts() {
    return environment.apiUrl2 + userEnv.tenant + '/products/';
  }

  private baseUrl() {
    return environment.apiUrl2 + userEnv.tenant + '/categories/products/';
  }


}
