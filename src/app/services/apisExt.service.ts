import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {userEnv} from '../../environments/user.env';
import {environment} from '../../environments/environment';
import {HttpClientService} from './http-client.service';
import {HttpClient} from '@angular/common/http';
import {ToastService} from './toast.service';
import {showroomEnv} from '../../environments/showroom.env';

@Injectable({
  providedIn: 'root'
})
export class ApisExtService  extends HttpClientService{

    constructor(public httpClient: HttpClient,
                public toastService: ToastService) {
        super(httpClient, toastService);
    }

    getApiToken(): Observable<any> {
      return this.get(this.baseUrl() + `${userEnv.showroomId}/token`)
          .pipe(tap(resp => console.log(resp)), map(response => response.result?.access_token));
    }

    getApiCollections(): Observable<any> {
        return this.get(this.baseUrl() + `${showroomEnv.api}/collections/` + showroomEnv.apiToken)
            .pipe(map((response) => response.result));
    }

    getApiCollectionProducts(collectionCode): Observable<any> {
        return this.get(this.baseUrl() + `${showroomEnv.api}/productsByCollection/` + showroomEnv.apiToken + '/' + collectionCode)
            .pipe(map((response) => response.result));
    }

    getApiSalesCatalogs(): Observable<any> {
        return this.get(this.baseUrl() + `${showroomEnv.api}/salesCatalogs/` + showroomEnv.apiToken)
            .pipe(map((response) => response.result));
    }

    getApiSalesCatalogProducts(salesCatalogCode): Observable<any> {
        return this.get(this.baseUrl() + `${showroomEnv.api}/productsBySalesCatalog/` + showroomEnv.apiToken + '/' + salesCatalogCode)
            .pipe(map((response) => response.result));
    }

    getApiProducts(term?: string): Observable<any> {
        term = term || '';
        return this.httpClient.post<any>(this.baseUrl() + `${showroomEnv.api}/products/` + showroomEnv.apiToken, {term}, {
            headers: this.header()}).pipe(map((response) => response.result));
    }

    private header() {
        return {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${userEnv.jwt}`
        };
    }

    private baseUrl() {
        return `${environment.apiUrl2}${userEnv.tenant}/apiExt/`;
    }
}
