import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {userEnv} from '../../environments/user.env';

export enum MediaType {
    PICTURE= 'picture',
    VIDEO = 'video',
    PDF = 'pdf'
}

@Injectable({
    providedIn: 'root'
})
export class MediasService {

    constructor(private httpClient: HttpClient) {
    }

    updatePoiPdf(poiId: string, file: any, language: string): Observable<any> {
        const formData = new FormData();

        formData.append('file', file);
        return this.httpClient.post<any>( this.baseUrlPdf() + poiId + '/updatePdf/' + language,
            formData, { headers: this.blobHeader() })
            .pipe(map((response: any) => response.result));
    }

    deletePoiPdf(poiId: string, language: string): Observable<any> {
        return this.httpClient.delete<any>(this.baseUrlPdf() + `${poiId}/${language}`, { headers: this.blobHeader() })
            .pipe(map((response: any) => response.result));
    }

    postMedia(type: string, file: any, fileName: string) {
        const formData = new FormData();
        const lastIndex = fileName.lastIndexOf('.');

        if (lastIndex > -1) {
            fileName = fileName.substring(0, lastIndex);
        }
        formData.append('file', file);
        return this.httpClient.post<any>(`${this.baseUrlMedias()}${type}/${fileName}`,
            formData, { headers: this.blobHeader() })
            .pipe(map((response: any) => response.result));
    }

    getMediasByType(type: string) {
        return this.httpClient.get<any>(`${this.baseUrlMedias()}type/${type}`)
            .pipe(map((response: any) => response.result));
    }

    getMediasById(id: string) {
        return this.httpClient.get<any>(`${this.baseUrlMedias()}id/${id}`)
            .pipe(map((response: any) => response.result));
    }

    deleteMediaById(id: string) {
        return this.httpClient.delete<any>(`${this.baseUrlMedias()}id/${id}`)
            .pipe(
                map((response: any) => {
                    return response.result;
                })
            );
    }

    private blobHeader() {
        return {
            Authorization: `Bearer ${userEnv.jwt}`
        };
    }

    private baseUrlPdf() {
        return environment.apiUrl2 + userEnv.tenant + '/pdf/';
    }

    private baseUrlMedias() {
        return environment.apiUrl2 + userEnv.tenant + '/medias/';
    }
}
