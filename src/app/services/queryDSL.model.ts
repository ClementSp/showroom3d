import {autoserialize} from 'cerialize';

export class QueryDSL {

  @autoserialize public query: any;
  @autoserialize public aggregations: any;
  @autoserialize public filter: any[];
  @autoserialize public sort: any[];
  @autoserialize public size: number;
  @autoserialize public from: number;
  @autoserialize public level: any[];
  @autoserialize public info: any;

  static addQueryStringInBoolMustArray(fields: string[], query, must: Array<any>) {
    query = QueryDSL.split(query);
    must.push({
      query_string: {
        query,
        fields
      }
    });
  }

  static addQueryStringInBoolShouldArray(fields: string[], query, should: Array<any>) {
    query = QueryDSL.split(query);
    should.push({
      query_string: {
        query,
        fields
      }
    });
  }

  static addTermInBoolMustArray(key: string, value, must: Array<any>) {
    const term = {};
    term[key] = value;
    must.push({term});
  }

  static split(query: string): string {
    query = query.split(' ').join('\\ ');
    query = query.split('+').join('\\+');
    query = query.split('.').join('\\.');
    query = query.split('-').join('\\-');
    query = query.split('/').join('\\/');
    query = query.split('!').join('\\!');
    query = query.split('?').join('\\?');
    query = query.split('(').join('\\(');
    query = query.split(')').join('\\)');
    query = query.split('[').join('\\[');
    query = query.split(']').join('\\]');
    query = query.split('$').join('\\$');
    query = query.split('^').join('\\^');
    return query;
  }

  addBoolMustArray(): Array<any> {
    if (!this.query) {
      this.query = {};
    }
    this.query.bool = {must: []};
    return this.query.bool.must;
  }

  addBoolShouldArray(): Array<any> {
    if (!this.query) {
      this.query = {};
    }
    this.query.bool = {should: []};
    return this.query.bool.should;
  }

  addQueryString(fields: string[], query) {
    if (!this.query) {
      this.query = {
        query_string: {}
      };
    }
    query = QueryDSL.split(query);
    this.query.query_string = {
      query,
      fields
    };
    return this;
  }

  addMultiMatch(fields: string[], query) {
    if (!this.query) {
      this.query = {
        multi_match: {}
      };
    }
    this.query.multi_match = {
      query,
      fields
    };
    return this;
  }

  addTerm(key: string, value) {
    if (!this.query) {
      this.query = {
        term: {}
      };
    }
    this.query.term[key] = value;
    return this;
  }

  addAggregations(querySize, field, fieldVersion, contains) {
    if (!this.aggregations) {
      this.aggregations = {};
    }
    if (!this.aggregations.aggs) {
      this.aggregations.aggs = {};
    }
    if (!this.aggregations.aggs.terms) {
      this.aggregations.aggs.terms = {};
    }
    if (field) {
      this.aggregations.aggs.terms['field'] = field;
    }
    if (querySize) {
      this.aggregations.aggs.terms['size'] = querySize;
    }
    if (!this.aggregations.aggs.terms.include && contains) {
      this.aggregations.aggs.terms.include = contains;
    }
    if (!this.aggregations.aggs.aggs) {
      this.aggregations.aggs.aggs = {};
    }
    if (!this.aggregations.aggs.aggs.products) {
      this.aggregations.aggs.aggs.products = {};
    }
    if (!this.aggregations.aggs.aggs.products.top_hits) {
      this.aggregations.aggs.aggs.products.top_hits = {};
    }
    this.aggregations.aggs.aggs.products.top_hits['size'] = 1;
    if (!this.aggregations.aggs.aggs.products.top_hits.sort) {
      this.aggregations.aggs.aggs.products.top_hits.sort = [];
    }
    this.aggregations.aggs.aggs.products.top_hits.sort.push(field);
    const sortVersion = {};
    sortVersion[fieldVersion] = {order: 'desc'};
    this.aggregations.aggs.aggs.products.top_hits.sort.push(sortVersion);
    return this;
  }

  addMatch(key: string, value) {
    if (!this.query) {
      this.query = {
        match: {}
      };
    }
    this.query.match[key] = value;
    return this;
  }

  addSize(value: number) {
    this.size = value;
    return this;
  }
}
