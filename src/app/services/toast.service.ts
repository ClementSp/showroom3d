import { Injectable } from '@angular/core';
import { ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';

export enum ToastType {
  ERROR,
  WARNING,
  NORMAL
}

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  lastToast: HTMLIonToastElement;

  constructor(public toastController: ToastController,
              private translateService: TranslateService) {

  }

  dismiss() {
    this.toastController.dismiss();
  }

  async presentToast(type: ToastType, message: string, duration: number = 2000, closable: boolean = false, closeCallback?: Function) {
    let cssClass;
    const buttons = [];

    if (this.lastToast) {
      await this.lastToast.dismiss();
    }
    if (closable) {
      buttons.push({
        text: 'Close',
        role: 'cancel',
        handler: () => closeCallback()
      });
    }

    switch (type) {
      case ToastType.ERROR: cssClass = 'error-toast';
                            break;
      case ToastType.NORMAL: cssClass = 'toast';
                             break;
      case ToastType.WARNING: cssClass = 'warning-toast';
                              break;
    }

    const translated: string = this.translateService.instant(message) || message
    const toast = await this.toastController.create({
      message: translated,
      position: 'top',
      duration,
      buttons,
      cssClass
    });
    this.lastToast = toast;
    await toast.present();
  }
}
