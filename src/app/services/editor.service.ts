import {EventEmitter, Injectable} from '@angular/core';
import {EditorMode} from '../models/EditorMode';
import {BehaviorSubject} from 'rxjs';
import {ModalController} from '@ionic/angular';
import {Poi} from '../models/showroom.model';
import {EngineService} from './engine.service';
import * as THREE from 'three';
import {MatterportNode} from '../models/MatterportNode';
import {EditMarkerComponent} from '../components/edit-marker/edit-marker.component';
import {ShowroomsService} from './showrooms.service';
import {EffectComposer} from 'three/examples/jsm/postprocessing/EffectComposer';
import {OutlinePass} from 'three/examples/jsm/postprocessing/OutlinePass';
import {filter} from 'rxjs/operators';
import {Portal} from '../models/PortalMarker';
import {FixturesManagerService} from './modules/fixtures-manager.service';
import {userEnv} from '../../environments/user.env';
import {PortalEditionModalComponent} from '../components/portal-edition-modal/portal-edition-modal.component';
import {CurrentNodeHandler} from '../DataHandlers/CurrentNode.handler';
import {SceneHandler} from '../DataHandlers/scene.handler';
import {ProductsManagerService} from './modules/products-manager.service';
import {SmartObjectManagerService} from './modules/smart-object-manager.service';
import EditorTransformControls from '../components/editor-transform-controls/editor-transform-controls';
import {CustomizerComponent} from '../components/modules/objects/customizer/customizer.component';
import {SmartObjectEditModalComponent} from '../components/modules/objects/smart-object-edit-modal/smart-object-edit-modal.component';
import ToolbarFeature from '../models/ToolbarFeature';
import {ToolbarComponent} from '../components/toolbar/toolbar.component';
import {showroomEnv} from '../../environments/showroom.env';
import {CustomProductPageV2Component} from '../components/custom-product-page-v2/custom-product-page-v2.component';
import {Color} from 'three';

export interface EditorSettings {
    displayShell: boolean;
}

enum EditorModalType {
    NONE = -1,
    CAMERA,
    FIXTURE,
    SHAPE_POI,
    POI,
    PORTAL,
    LIGHT
}

@Injectable({
    providedIn: 'root'
})
export class EditorService {

    editorView = new BehaviorSubject(false);
    editorMeshView = new BehaviorSubject(false);
    currentMode = new BehaviorSubject<EditorMode>(EditorMode.NONE);
    settings: BehaviorSubject<EditorSettings>;

    currentlyEditingPoi: Poi;
    currentlyEditingPortal: Portal;
    currentlyEditingShapePoi: any;
    currentlyEditingPointLight: any;
    currentlyEditingSpotLight: any;
    composer: EffectComposer;
    outlinePass: OutlinePass;

    onPlacedPoi = new EventEmitter<void>();
    onPlacedFixture = new EventEmitter<void>();
    onPlacedSmartObject = new EventEmitter<void>();
    onPlacedProduct = new EventEmitter<void>();
    onPlacedShapePoi = new EventEmitter<void>();
    onPlacedPortal = new EventEmitter<void>();
    onPlacedPointLight = new EventEmitter<void>();
    onPlacedSpotLight = new EventEmitter<void>();

    currentlyPlacingPoi: THREE.Object3D = undefined;
    currentlyPlacingFixture: THREE.Object3D = undefined;
    currentlyPlacingSmartObject: THREE.Object3D = undefined;
    currentlyPlacingProduct: THREE.Object3D = undefined;
    currentlyPlacingShapePoi: THREE.Object3D = undefined;
    currentlyPlacingPortal: THREE.Object3D = undefined;
    currentlyPlacingPointLight: THREE.Object3D = undefined;
    currentlyPlacingSpotLight: THREE.Object3D = undefined;

    isEditorActivated = this.editorView.asObservable();
    objectType: string;
    toolBarModal: HTMLIonModalElement;
    _transformControls: EditorTransformControls;
    target;

    private readonly _noActiveModeClickPriorities =
        ['POI', 'SHAPE_POI', 'PORTAL', 'PRODUCT', 'SMART_OBJECT', 'FIXTURE', 'POINT_LIGHT', 'SPOT_LIGHT'];

    private readonly _noActiveModeHandlersMap = new Map([
        ['POI', this.handleClickOnPoiWhileNoActiveMode.bind(this)],
        ['SHAPE_POI', this.handleClickOnShapePoiWhileNoActiveMode.bind(this)],
        ['PORTAL', this.handleClickOnPortalWhileNoActiveMode.bind(this)],
        ['PRODUCT', this.handleClickOnProductWhileNoActiveMode.bind(this)],
        ['SMART_OBJECT', this.handleClickOnSmartObjectWhileNoActiveMode.bind(this)],
        ['FIXTURE', this.handleClickOnFixtureWhileNoActiveMode.bind(this)],
        ['POINT_LIGHT', this.handleClickOnPointLightWhileNoActiveMode.bind(this)],
        ['SPOT_LIGHT', this.handleClickOnSpotLightWhileNoActiveMode.bind(this)],
    ]);

    private readonly modeFunctionMap = new Map([
        [EditorMode.NONE, this.noActiveMode.bind(this)],
        [EditorMode.ADD_POI_MARKERS, this.addPoi.bind(this)],
        [EditorMode.ADD_SHAPE_POI_MARKERS, this.addShapePio.bind(this)],
        [EditorMode.ADD_PORTAL_MARKERS, this.addPortal.bind(this)],
        [EditorMode.ADD_POINT_LIGHT, this.addPointLight.bind(this)],
        [EditorMode.ADD_SPOT_LIGHT, this.addSpotLight.bind(this)],
        [EditorMode.PLACING_FIXTURE, this.onPlacingFixture.bind(this)],
        [EditorMode.PLACING_PRODUCT, this.onPlacingProduct.bind(this)],
        [EditorMode.PLACING_SMART_OBJECT, this.onPlacingSmartObject.bind(this)]
    ]);

    constructor(private engineService: EngineService,
                private fixturesManagerService: FixturesManagerService,
                private productsManagerService: ProductsManagerService,
                private smartObjectsManagerService: SmartObjectManagerService,
                private showroomService: ShowroomsService,
                public modalController: ModalController,
                private currentNode: CurrentNodeHandler,
                private sceneHandler: SceneHandler) {
        this.setEditorModeAfterLogin();
        this.sceneHandler.sceneReady.subscribe((sceneReady) => {
            if (sceneReady) {
                this.initControls();
                this.updateTransformModeOnEditorModeChange();
                this.engineService.scene.add(this._transformControls);
                this.initializeComposer();
            }
        });
        this.settings = new BehaviorSubject<EditorSettings>({
            displayShell: false,
        });
    }

    // getters & setters

    get isActive(): boolean {
        return this.editorView.value;
    }

    get isMeshActive(): boolean {
        return this.editorMeshView.value;
    }

    get mode(): EditorMode {
        return this.currentMode.value;
    }

    get attachedObject(): THREE.Object3D {
        return this._transformControls.attachedObject.value;
    }

    set mode(mode: EditorMode) {
        this.currentMode.next(mode);
    }

    closeModal() {
        this.modalController.dismiss();
    }

    initSettingsEvents() {
        const shell = this.sceneHandler.mesh;

        this.settings.subscribe((value: EditorSettings) => {
            shell.traverse((child: THREE.Object3D) => {
                if (child instanceof THREE.Mesh) {
                    (child.material as THREE.Material).colorWrite = value.displayShell;
                }
            });
        });
    }

    switchView(event) {
        const settings = this.settings.value;
        if (event === '0') {
            this.editorView.next(false);
            this.engineService.setEditorView(false);
            this.engineService.setEditorMeshView(false);
            this.editorMeshView.next(false);
            this.detachTransformControls();
            this.toolBarModal?.dismiss();
            settings.displayShell = false;
            this.settings.next(settings);
        } else if (event === '1') {
            this.editorView.next(true);
            this.engineService.setEditorView(true);
            this.engineService.setEditorMeshView(false);
            this.editorMeshView.next(false);
            settings.displayShell = false;
            this.settings.next(settings);
        } else {
            this.editorView.next(true);
            this.engineService.setEditorView(true);
            this.engineService.setEditorMeshView(true);
            this.editorMeshView.next(true);
            settings.displayShell = true;
            this.settings.next(settings);
        }
    }

    initControls() {
        this._transformControls = new EditorTransformControls(this.fixturesManagerService, this.productsManagerService,
            this.smartObjectsManagerService, this.showroomService, this.engineService.camera,
            this.sceneHandler.renderer.domElement, this.sceneHandler);
    }

    detachTransformControls() {
        this._transformControls.detach();
        this.currentlyPlacingPoi = undefined;
        this.currentlyPlacingProduct = undefined;
        this.currentlyPlacingFixture = undefined;
        this.currentlyPlacingShapePoi = undefined;
        this.currentlyPlacingSmartObject = undefined;
        this.currentMode.next(EditorMode.NONE);
    }

    attachTransformControls(target) {
        this._transformControls.attachTo(target);
    }

    setModeControls(mode) {
        this._transformControls.setMode(mode);
    }

    handleClick(intersects: Array<THREE.Intersection>, currentNode: MatterportNode) {
        console.log(this.currentMode.value.valueOf());
        if (this.modeFunctionMap.has(this.currentMode.value.valueOf())) {
            this.modeFunctionMap.get(this.currentMode.value.valueOf())(intersects, currentNode);
        }
    }

    private setEditorModeAfterLogin() {
        userEnv.initialized.pipe(
            filter(isInitialized => isInitialized)
        ).subscribe(() => {
            this.editorView.next(userEnv.loginType === 'platform');
        });
    }

    isEditing() {
        return this.currentMode.value === EditorMode.ADD_PORTAL_MARKERS ||
            this.currentMode.value === EditorMode.ADD_SHAPE_POI_MARKERS ||
            this.currentMode.value === EditorMode.ADD_POI_MARKERS ||
            this.currentMode.value === EditorMode.ADD_POINT_LIGHT ||
            this.currentMode.value === EditorMode.ADD_SPOT_LIGHT;
    }

    private onPlacing(intersects: Array<THREE.Intersection>, objectType, currentPlacingObject, eventEmitter) {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);

        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            currentPlacingObject.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(currentPlacingObject, objectType);
            eventEmitter.emit();
        }
    }

    private onPlacingFixture(intersects: Array<THREE.Intersection>) {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);

        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingFixture.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingFixture, 'FIXTURE');
            this.onPlacedFixture.emit();
        }
    }

    private onPlacingSmartObject(intersects: Array<THREE.Intersection>) {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);

        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingSmartObject.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingSmartObject, 'SMART_OBJECT');
            this.onPlacedSmartObject.emit();
        }
    }

    private onPlacingProduct(intersects: Array<THREE.Intersection>) {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);

        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingProduct.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingProduct, 'PRODUCT');
            this.onPlacedProduct.emit();
        }
    }

    private addShapePio(intersects: Array<THREE.Intersection>): void {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingShapePoi.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingShapePoi, 'SHAPE_POI');
            this.onPlacedShapePoi.emit();
        }
    }

    private addPoi(intersects: Array<THREE.Intersection>): void {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingPoi.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingPoi, 'POI');
            this.onPlacedPoi.emit();
        }
    }

    private addPortal(intersects: Array<THREE.Intersection>): void {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingPortal.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingPortal, 'PORTAL');
            this.onPlacedPortal.emit();
        }
    }

    private addPointLight(intersects: Array<THREE.Intersection>): void {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingPointLight.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingPointLight, 'POINT_LIGHT');
            this.onPlacedPointLight.emit();
        }
    }

    private addSpotLight(intersects: Array<THREE.Intersection>): void {
        let position;
        const intersectShellComponents = intersects.filter(o => o.object.name === 'shellComponent').map(i => i.point);
        if (intersectShellComponents && intersectShellComponents.length > 0) {
            position = intersectShellComponents[0];
            this.currentlyPlacingSpotLight.position.set(
                position.x,
                position.y,
                position.z
            );
            this.setSelect(this.currentlyPlacingSpotLight, 'SPOT_LIGHT');
            this.onPlacedSpotLight.emit();
        }
    }

    private noActiveMode(intersects: Array<THREE.Intersection>, currentNode: MatterportNode) {
        let handled = false;
        this.objectType = undefined;
        for (const entity of this._noActiveModeClickPriorities) {
            if (this._noActiveModeHandlersMap.has(entity)) {
                handled = this._noActiveModeHandlersMap.get(entity)( intersects, currentNode);
            }
            if (handled && this.getObjectSelected(entity)) {
                return;
            }
        }
    }

    private initializeComposer() {
        this.composer = new EffectComposer(this.sceneHandler.renderer);
        this.outlinePass = new OutlinePass(
            new THREE.Vector2(window.innerWidth, window.innerHeight),
            this.sceneHandler.scene,
            this.engineService.camera
        );
        this.composer.addPass(this.outlinePass);
    }

    private updateTransformModeOnEditorModeChange() {
        this.currentMode.subscribe(value => {
            if (value === EditorMode.TRANSLATE ||
                value === EditorMode.SCALE ||
                value === EditorMode.ROTATE) {
                switch (EditorMode[value].toLowerCase()) {
                    case 'translate':
                        this.setModeControls('translate');
                        break;
                    case 'rotate':
                        this.setModeControls('rotate');
                        break;
                    case 'scale':
                        this.setModeControls('scale');
                        break;
                }
            }
        });
    }

    private handleClickOnPointLightWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let lightsId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        lightsId = [...new Set(lightsId)];

        if (lightsId[0] && lightsId[0].length > 0 ) {
            const light = this.sceneHandler.scene.children.filter(i => i.userData.id === lightsId[0]);
            if (light && light.length > 0 && light[0].userData.type === 'POINT_LIGHT') {
                this.setSelect(light[0], 'POINT_LIGHT');
            }
            return true;
        }
        return false;
    }

    private handleClickOnSpotLightWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let lightsId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        lightsId = [...new Set(lightsId)];

        if (lightsId[0] && lightsId[0].length > 0 ) {
            const light = this.sceneHandler.scene.children.filter(i => i.userData.id === lightsId[0]);
            if (light && light.length > 0 && light[0].userData.type === 'SPOT_LIGHT') {
                this.setSelect(light[0], 'SPOT_LIGHT');
            }
            return true;
        }
        return false;
    }

    private handleClickOnFixtureWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let fixturesId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        fixturesId = [...new Set(fixturesId)];

        if (fixturesId[0] && fixturesId[0].length > 0 ) {
            const fixture = this.sceneHandler.scene.children.filter(i => i.userData.id === fixturesId[0]);
            if (fixture && fixture.length > 0 && fixture[0].userData.type === 'FIXTURE') {
                this.setSelect(fixture[0], 'FIXTURE');
            }
            return true;
        }
        return false;
    }

    private handleClickOnSmartObjectWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let smartObjectsId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        smartObjectsId = [...new Set(smartObjectsId)];

        if (smartObjectsId[0] && smartObjectsId[0].length > 0 ) {
            const smartObjects = this.sceneHandler.scene.children.filter(i => i.userData.id === smartObjectsId[0]);
            if (smartObjects && smartObjects.length > 0 && smartObjects[0].userData.type === 'SMART_OBJECT' ) {
                this.setSelect(smartObjects[0], 'SMART_OBJECT');
            }
            return true;
        }
        return false;
    }

    private handleClickOnProductWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let productsId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        productsId = [...new Set(productsId)];

        if (productsId[0] && productsId[0].length > 0) {
            const product = this.sceneHandler.scene.children.filter(i => i.userData.id === productsId[0]);

            if (product && product.length > 0 && product[0].userData.type === 'PRODUCT') {
                this.setSelect(product[0], 'PRODUCT');
            }
            return true;
        }
        return false;
    }

    private handleClickOnShapePoiWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let shapeId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        shapeId = [...new Set(shapeId)];

        if (shapeId[0] && shapeId[0].length > 0) {
            const shapePoi = this.sceneHandler.scene.children.filter(i => i.userData.id === shapeId[0]);
            if (shapePoi && shapePoi.length > 0 && shapePoi[0].userData.type === 'SHAPE_POI') {
                this.currentlyEditingShapePoi = shapePoi[0].userData.poi;
                this.setSelect(shapePoi[0], 'SHAPE_POI');
            }
            return true;
        }
        return false;
    }

    private handleClickOnPoiWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let poiId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        poiId = [...new Set(poiId)];

        if (poiId[0] && poiId[0].length > 0) {
            const poi = this.sceneHandler.scene.children.filter(i => i.userData.id === poiId[0]);
            if (poi && poi.length > 0 && poi[0].userData.type === 'POI') {
                this.currentlyEditingPoi = poi[0].userData.poi;
                this.setSelect(poi[0], 'POI');
            }
            return true;
        }
        return false;
    }

    private handleClickOnPortalWhileNoActiveMode(intersects: Array<THREE.Intersection>): boolean {
        let portalId = intersects
            .filter(i => i.object.userData.parentId !== undefined)
            .map(i => i.object.userData.parentId);
        portalId = [...new Set(portalId)];

        if (portalId[0] && portalId[0].length > 0) {
            const portal = this.sceneHandler.scene.children.filter(i => i.userData.id === portalId[0]);
            if (portal && portal.length > 0 && portal[0].userData.type === 'PORTAL') {
                this.currentlyEditingPortal = portal[0].userData.portalId;
                this.setSelect(portal[0], 'PORTAL');
            }
            return true;
        }
        return false;
    }

    deleteProduct() {
        const obj = this.attachedObject;

        if (obj) {
            const productId = obj.userData.id;

            this.productsManagerService.deleteProduct(productId).subscribe((response) => {
                if (response.status === 200) {
                    const productToRemove = showroomEnv.showroom.products.find(product => product.id === productId);
                    showroomEnv.showroom.products.splice(showroomEnv.showroom.products.indexOf(productToRemove), 1);
                    this.detachTransformControls();
                    this.closeModal();
                    this.sceneHandler.removeObjFromScene(obj);
                }
            });
        }
    }

    deleteFixture() {
        const obj = this.attachedObject;

        if (obj) {
            const fixtureId = obj.userData.id;

            this.fixturesManagerService.deleteFixture(fixtureId).subscribe((response) => {
                if (response.status === 200) {
                    const fixtureToRemove = showroomEnv.showroom.fixtures.find(fixture => fixture.id === fixtureId);
                    showroomEnv.showroom.fixtures.splice(showroomEnv.showroom.fixtures.indexOf(fixtureToRemove), 1);
                    this.detachTransformControls();
                    this.closeModal();
                    this.sceneHandler.removeObjFromScene(obj);
                }
            });
        }
    }

    deleteSmartObject() {
        const obj = this.attachedObject;

        if (obj) {
            const smartObjectId = obj.userData.id;

            this.smartObjectsManagerService.deleteSmartObject(smartObjectId).subscribe((response) => {
                if (response.status === 200) {
                    const smartObjectToRemove = showroomEnv.showroom.smartObjects.find(smartObject => smartObject.id === smartObjectId);
                    showroomEnv.showroom.smartObjects.splice(showroomEnv.showroom.smartObjects.indexOf(smartObjectToRemove), 1);
                    this.detachTransformControls();
                    this.closeModal();
                    this.sceneHandler.removeObjFromScene(obj);
                }
            });
        }
    }

    public removeShapePoiMarkers(): void {
        const obj = this.attachedObject;

        if (obj) {
            const shapePoiId = obj.userData.id;

            this.showroomService.deleteShapePoi(shapePoiId).subscribe((response) => {
                if (response.status === 200) {
                    this.detachTransformControls();
                    this.closeModal();
                    showroomEnv.showroom.pois = showroomEnv.showroom.shapePois.filter(poi => poi.id !== shapePoiId);
                    this.sceneHandler.removeObjFromScene(obj);
                }
            });
        }
    }


    public removePoiMarkers(): void {
        const obj = this.attachedObject;

        if (obj) {
            const poiId = obj.userData.id;

            this.showroomService.deletePoi(poiId).subscribe((response) => {
                if (response.status === 200) {
                    this.detachTransformControls();
                    this.closeModal();
                    showroomEnv.showroom.pois = showroomEnv.showroom.pois.filter(poi => poi.id !== poiId);
                    this.sceneHandler.removeObjFromScene(obj);
                }
            });
        }
    }

    public removePortal(): void {
        const obj = this.attachedObject;
        if (obj) {
            const portalId = obj.userData.id;
            this.showroomService.deletePortal(portalId).subscribe((response) => {
                if (response.status === 200) {
                    this.detachTransformControls();
                    this.closeModal();
                    showroomEnv.showroom.portals = showroomEnv.showroom.portals.filter(portal => portal.id !== portalId);
                    this.sceneHandler.removeObjFromScene(obj);
                }
            });
        }
    }

    public removeLight(): void {
        const obj = this.attachedObject;
        if (obj) {
            const lightId = obj.userData.id;
            this.showroomService.deleteLight(lightId).subscribe((response) => {
                if (response.status === 200) {
                    this.detachTransformControls();
                    this.closeModal();
                    showroomEnv.showroom.lights = showroomEnv.showroom.lights.filter(light => light.id !== lightId);
                    this.sceneHandler.removeObjFromScene(obj);
                }
            });
        }
    }

    setSelect(target: any, objectType) {
        this.objectType = objectType;
        this.mode = EditorMode.TRANSLATE;
        this.target = target;
        this.attachTransformControls(this.target);
        if (objectType === 'SPOT_LIGHT' || objectType === 'POINT_LIGHT') {
            this.openTransformOptionsForLight();
        } else {
            this.openTransformOptionsForObject();
        }
    }

     getObjectSelected(entity: string) {
       if (entity === 'FIXTURE' || entity === 'PRODUCT' ||
           entity === 'SHAPE_POI' || entity === 'POI' ||
           entity === 'SMART_OBJECT' || entity === 'PORTAL' ||
           entity === 'SPOT_LIGHT' || entity === 'POINT_LIGHT'){
           return entity === this.objectType;
       }else{
           return true;
       }
    }

    async openModal() {
        if (this.objectType === 'PRODUCT') {
            this.openCustomizeProductModal();
        } else if (this.objectType === 'SMART_OBJECT') {
            this.openCustomizeSmartObjectModal();
        } else if (this.objectType === 'FIXTURE') {
            this.openCustomizeFixtureModal();
        } else if (this.objectType === 'SHAPE_POI') {
            this.openCustomizeShapePoiModal();
        } else if (this.objectType === 'POI') {
            this.openCustomizePoiModal();
        } else if (this.objectType === 'PORTAL') {
            this.openCustomizePortalModal();
        } else if (this.objectType === 'POINT_LIGHT' ||
            this.objectType === 'SPOT_LIGHT') {
            this.currentMode.next(EditorMode.EDIT);
            // this.setModeControls(undefined);
        }
    }

    async deleteObject() {
        if (this.objectType === 'PRODUCT') {
            this.deleteProduct();
        } else if (this.objectType === 'SMART_OBJECT') {
            this.deleteSmartObject();
        } else if (this.objectType === 'FIXTURE') {
            this.deleteFixture();
        } else if (this.objectType === 'SHAPE_POI') {
            this.removeShapePoiMarkers();
        } else if (this.objectType === 'POI') {
            this.removePoiMarkers();
        } else if (this.objectType === 'PORTAL') {
            this.removePortal();
        } else if (this.objectType === 'POINT_LIGHT') {
            this.removeLight();
        } else if (this.objectType === 'SPOT_LIGHT') {
            this.removeLight();
        }
    }

    async openCustomizeFixtureModal() {
        const modal = await this.modalController.create({
            component: CustomizerComponent,
            cssClass: 'fullscreen-modal',
            showBackdrop: true,
            backdropDismiss: true
        });
        await modal.present();
    }

    async openCustomizeProductModal() {
        const modal = await this.modalController.create({
            component: CustomProductPageV2Component,
            cssClass: 'fullscreen-modal',
            showBackdrop: false,
            backdropDismiss: true,
            componentProps: {
                data : {
                    productId:  this.attachedObject.userData.productId,
                    object : this.attachedObject,
                    isEditor: true
                }
            }
        });
        return await modal.present()
            .then(() => {
                if (!userEnv.authorization) {
                  //  this.analyticService.putLog(LogType.CLICK_PRODUCT, poi.data?.products[0]);
                }
            });
    }

    async openCustomizeSmartObjectModal() {
        const modal = await this.modalController.create({
            component: SmartObjectEditModalComponent,
            cssClass: 'fullscreen-modal',
            showBackdrop: true,
            backdropDismiss: true,
        });
        await modal.present();
    }

    async openCustomizeShapePoiModal() {
        const poi =  this.currentlyEditingShapePoi;
        poi.isShapePoi = true;
        const modal = await this.modalController.create({
            component: EditMarkerComponent,
            cssClass: 'edit-marker-component backdrop-05',
            componentProps: {
                poi,
            }
        });
        return await modal.present();
    }

    async openCustomizePoiModal() {
        const poi =  this.currentlyEditingPoi;
        const modal = await this.modalController.create({
            component: EditMarkerComponent,
            cssClass: 'edit-marker-component backdrop-05',
            componentProps: {
                poi,
            }
        });
        return await modal.present();
    }

    async openCustomizePortalModal() {
        const portal =  this.currentlyEditingPortal;
        const modal = await this.modalController.create({
            component: PortalEditionModalComponent,
            cssClass: 'edit-marker-component backdrop-05',
            componentProps: {
                portal,
                menu: showroomEnv.showroom.menu,
                entrance: showroomEnv.showroom.settings.entrance
            }
        });
        return await modal.present();
    }

    async updateValueFromPrecision() {
        switch (this.target.userData.type) {
            case 'POINT_LIGHT':
            case 'SPOT_LIGHT':
                const light = showroomEnv.showroom.lights.find(showroomLight => showroomLight.id === this.target.userData.id);
                console.log(this.target);
                console.log(this.target.children[0].color);
                light.intensity = this.target.children[0].intensity;
                this.target.children[0].color.a = 0;
                light.color = this.target.children[0].color;
                this.showroomService.putLight(light).subscribe();
                break;
        }
    }

    async openTransformOptions() {
        await this.openToolbar(EditorModalType.FIXTURE, [
            { iconName: 'move-outline', editorMode: EditorMode.TRANSLATE, description: 'Change position' },
            { iconName: 'refresh-outline', editorMode: EditorMode.ROTATE, description: 'Rotate' },
            { iconName: 'resize-outline', editorMode: EditorMode.SCALE, description: 'Rescale' }
        ] as Array<ToolbarFeature>, EditorMode.TRANSLATE, () => this.detachTransformControls());
    }

    async openTransformOptionsForObject() {
        await this.openToolbar(EditorModalType.FIXTURE, [
            { iconName: 'move-outline', editorMode: EditorMode.TRANSLATE, description: 'Change position' },
            { iconName: 'refresh-outline', editorMode: EditorMode.ROTATE, description: 'Rotate' },
            { iconName: 'resize-outline', editorMode: EditorMode.SCALE, description: 'Rescale' },
            { iconName: 'pencil-outline', editorMode: EditorMode.EDIT, description: 'Edit' },
            { iconName: 'trash-outline', editorMode: EditorMode.DELETE, description: 'Delete' }
        ] as Array<ToolbarFeature>, EditorMode.TRANSLATE, () => this.detachTransformControls());
    }

    async openTransformOptionsForLight() {
        await this.openToolbar(EditorModalType.LIGHT, [
            { iconName: 'move-outline', editorMode: EditorMode.TRANSLATE, description: 'Change position' },
            { iconName: 'refresh-outline', editorMode: EditorMode.ROTATE, description: 'Rotate' },
            { iconName: 'pencil-outline', editorMode: EditorMode.EDIT, description: 'Edit' },
            { iconName: 'trash-outline', editorMode: EditorMode.DELETE, description: 'Delete' }
        ] as Array<ToolbarFeature>, EditorMode.TRANSLATE, () => this.detachTransformControls());
    }

    async openToolbar(modalType: EditorModalType, features: Array<ToolbarFeature>, defaultMode: EditorMode, closeCallback?: Function) {
        this.currentMode.next(defaultMode);
        this.toolBarModal = await this.modalController.create({
            component: ToolbarComponent,
            cssClass: 'tools-modal',
            backdropDismiss: false,
            componentProps: {
                modeChanges: this.currentMode,
                features
            },
            showBackdrop: false
        });
        this.toolBarModal.onDidDismiss().then(() => {
            if (closeCallback) {
                closeCallback();
            }
        });
        await this.toolBarModal.present();
    }
}
