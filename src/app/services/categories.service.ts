import {Injectable} from '@angular/core';
import {Deserialize} from 'cerialize';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {QueryDSL} from './queryDSL.model';
import {HttpClient} from "@angular/common/http";
import {userEnv} from "../../environments/user.env";
import CustomProduct, {ACustomProduct} from "../models/CustomProduct";
import {ProductTreeNode} from "../models/productTreeNode.model";
import {TextureTreeNode} from "../models/textureTreeNode.model";
import {Texture} from "../models/texture.model";
import {FixtureTreeNode} from "../models/fixtureTreeNode.model";
import {IFixtureAssetData} from "../models/fixture.model";
import {environment} from "../../environments/environment";
import {SmartObjectTreeNode} from "../models/smartObjectTreeNode.model";
import {ISmartObjectAssetData} from "../models/smartObject.model";
@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private httpClient: HttpClient) { }

  /*getCategories(filterDate: boolean): Observable<Array<TreeNode>> {
    const queryDSL = new QueryDSL();
    return this.httpClient.post(`environment.apiUrl + /plugin/kuzzle-app/${userEnv.tenant }/categories/all`, queryDSL).pipe(
      map(((response: any) => {
        if (response) {
          const now = new Date();
          const categories = [];
          response.result.forEach(category => {
            if (filterDate || (!category.validAt || category.validAt > now.getDate()) &&
              (!category.validendAt || category.validendAt < now.getDate())) {
              categories.push(Deserialize(category, fromModels.TreeNode));
            }
          });
          return categories;
        }
        return [];
      }))
    );
  }*/

  getProductCategories(folderId): Observable<Array<ProductTreeNode>> {
    const query = {
      folderId: undefined
    };
    if (folderId) {
      query.folderId = folderId;
    }

    return this.httpClient.post(environment.apiUrl2 + `${userEnv.tenant }/categories/products/all`, query).pipe(
      map(((response: any) => {
        if(response && response.result) {
          const items = [];
          response.result.forEach(item => {
            if (item.product_ean) {
              items.push(new CustomProduct(item as ACustomProduct));
            } else {
              items.push(Deserialize(item, ProductTreeNode));
            }
          });
          return items;
        }
        return undefined;
      }))
    );
  }

  getTextureCategories(folderId): Observable<Array<TextureTreeNode>> {
    const query = {
      folderId: undefined
    };
    if (folderId) {
      query.folderId = folderId;
    }

    return this.httpClient.post(environment.apiUrl2 + `${userEnv.tenant }/categories/textures/all`, query).pipe(
      map(((response: any) => {

        if(response && response.result) {
          const items = [];
          response.result.forEach(item => {
            let textures;
            if ( item.name) {
              if (item.textures){
                textures = Deserialize(item, TextureTreeNode);
              }else{
                textures = Deserialize(item, Texture);
              }

              textures.id = item.id;

              items.push(textures);
            } else {
              items.push(Deserialize(item, TextureTreeNode));
            }
          });
          return items;
        }
        return [];
      }))
    );
  }

  getFixturesCategories(folderId): Observable<Array<FixtureTreeNode>> {
    const query = {
      folderId: undefined
    };
    if (folderId) {
      query.folderId = folderId;
    }
    return this.httpClient.post(environment.apiUrl2 + `${userEnv.tenant }/categories/fixtures/all`, query).pipe(
      map(((response: any) => {
        if(response && response.result) {
          const items = [];
          response.result.forEach(item => {
            if (item.gcsPath || item.windows) {
              const fixtures = Deserialize(item, IFixtureAssetData);
              fixtures.id = item.id;
              items.push(fixtures);
            } else {
              items.push(Deserialize(item, FixtureTreeNode));
            }
          });
          return items;
        }
        return [];
      }))
    );
  }

  getSmartObjectCategories(folderId): Observable<Array<SmartObjectTreeNode>> {
    const query = {
      folderId: undefined
    };
    if (folderId) {
      query.folderId = folderId;
    }

    return this.httpClient.post(environment.apiUrl2 + `${userEnv.tenant }/categories/smartObjects/all`, query).pipe(
      map(((response: any) => {
        if(response && response.result) {
          const items = [];
          response.result.forEach(item => {
            if (item.gcsPath || item.windows) {
              const smartObjects = Deserialize(item, ISmartObjectAssetData);
              smartObjects.id = item.id;
              items.push(smartObjects);
            } else {
              items.push(Deserialize(item, SmartObjectTreeNode));
            }
          });
          return items;
        }
        return undefined;
      }))
    );
  }

  getCategoryPicture (id , categoryType):Observable<any> {
    return this.httpClient.get(environment.apiUrl2 + `${userEnv.tenant }/${categoryType}/picture/${id}`).pipe(map(
        (picture: any) => {
        return picture.result;
      }));

  }
}
