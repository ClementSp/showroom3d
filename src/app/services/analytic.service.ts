import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {userEnv} from '../../environments/user.env';
import {tap} from 'rxjs/operators';
import {LogType} from './showrooms.service';
import {HttpClientService} from './http-client.service';
import {ToastService} from './toast.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AnalyticService extends HttpClientService{

  constructor(public httpService: HttpClient,
              public toastService: ToastService) {
    super(httpService, toastService);
  }

  putLog(type: LogType, target: string = null, value: any = null, origin: string = null) {
    return this.httpService.put<any>(this.baseUrl(), {
      shopId: userEnv.showroomId,
      customer: userEnv.customer,
      sessionId: userEnv.sessionId,
      target,
      type,
      value,
      origin
    }).pipe(tap((resp) => userEnv.sessionId = resp.result.sessionId )).toPromise();
  }

  private baseUrl() {
    return `${environment.apiUrl2}analytics/logs/showroom`;
  }
}
