import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {userEnv} from '../../environments/user.env';
import {ToastService} from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
  }

  get(url: string, customOptions?: any): Observable<any> {
    const options = (customOptions) ? Object.assign({}, this.headers, customOptions) : this.headers;
    return this.httpClient.get<any>(url, options);
  }

  put(url: string, body: any, customOptions?: any): Observable<any> {
    const options = (customOptions) ? Object.assign({}, this.headers, customOptions) : this.headers;
    return this.httpClient.put<any>(url, body, options);
  }

  post(url: string, body: any, customOptions?: any) {
    const options = (customOptions) ? Object.assign({}, this.headers, customOptions) : this.headers;
    return this.httpClient.post(url, body, options);
  }

  delete(url: string, customOptions?: any) {
    const options = (customOptions) ? Object.assign({}, this.headers, customOptions) : this.headers;
    return this.httpClient.delete(url, options);
  }

  postMedia(url: string, formData: FormData, customOptions?: any) {
    const options = (customOptions) ? Object.assign({}, this.blobHeaders, customOptions) : this.blobHeaders;
    return this.httpClient.post(url, formData, options);
  }

  putMedia(url: string, formData: FormData, customOptions?: any) {
    const options = (customOptions) ? Object.assign({}, this.blobHeaders, customOptions) : this.blobHeaders;
    return this.httpClient.put(url, formData, options);
  }


  get headers() {
    if (userEnv.jwt) {
      return {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userEnv.jwt}`
        }
      };
    }
    return {
      headers: {
        'Content-Type': 'application/json'
      }
    };
  }

  get blobHeaders() {
    if (userEnv.jwt) {
      return {
        headers: {
          Authorization: `Bearer ${userEnv.jwt}`
        }
      };
    }
    return {};
  }
}
