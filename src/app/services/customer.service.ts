import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {userEnv} from '../../environments/user.env';
import {HttpClientService} from './http-client.service';
import {ToastService} from './toast.service';
import {customerEnv} from '../../environments/customer.env';

export interface ShowroomRight {
  showroomId: string;
  validendAt: string;
  showPrice?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends HttpClientService {
  readonly handledCurrencies = ['EUR', 'DOL'];

  constructor(public httpClient: HttpClient,
              public toastService: ToastService) {
    super(httpClient, toastService);
  }

  getCustomer(): Promise<any> {
    return this.get(this.baseUrl() + 'getById/' + userEnv.customer).pipe(
        map((response) => response.result),
        map((customer) => {
          if (customer) {
            customerEnv.showroomRights = customer.showroomRights || [];
            customerEnv.country = customer.country || 'France';
            customerEnv.currency = customer.currency || 'EUR';
            customerEnv.email = customer.email;
            customerEnv.name = customer.name;

            const customerRight = customerEnv.showroomRights.find(right => right.showroomId === userEnv.showroomId);

            customerEnv.showPrice = customerRight?.showPrice ?? false;
          }
        })
    ).toPromise();
  }

  private baseUrl() {
    return `${environment.apiUrl2}${userEnv.tenant}/customers/`;
  }
}
