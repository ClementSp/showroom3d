import {Component, ElementRef, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {RouterService} from './services/router.service';
import {ShowroomsService} from './services/showrooms.service';
import {CustomerService} from './services/customer.service';
import {UserService} from './services/user.service';
import {Showroom} from './models/showroom.model';
import {userEnv} from '../environments/user.env';
import {customerEnv} from '../environments/customer.env';
import {ShowroomHandler} from './DataHandlers/Showroom.handler';
import {AbScenariosService} from './services/ab-scenarios.service';
import {showroomEnv} from '../environments/showroom.env';
import {ModulesService} from './services/modules.service';
import {ApisExtService} from './services/apisExt.service';
import {WishlistHandler} from "./DataHandlers/Wishlist.handler";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
      private platform: Platform,
      private splashScreen: SplashScreen,
      private statusBar: StatusBar,
      private userService: UserService,
      private showroomService: ShowroomsService,
      private modulesService: ModulesService,
      private customerService: CustomerService,
      private abScenarioService: AbScenariosService,
      private apiExtService: ApisExtService,
      private router: RouterService,
      private translateService: TranslateService,
      private wishlistHandler: WishlistHandler,
      private elementRef: ElementRef
  ) {
    const urlParams = this.router.parseUrl(window.location.search);
    userEnv.callId = urlParams.queryParams.callId;
    userEnv.customer = urlParams.queryParams.customer;
    userEnv.tenant = urlParams.queryParams.tenant;
    userEnv.showroomId = urlParams.queryParams.name;
    userEnv.toolbar = urlParams.queryParams.toolbar;

    userEnv.shopifyc = urlParams.queryParams.shopifyc;
    if (userEnv.shopifyc) {
      const cartObject = atob(userEnv.shopifyc);
      if (cartObject) {
        const cart = JSON.parse(cartObject);
        if (cart.items) {
          cart.items.forEach(item => {
            this.wishlistHandler.addProduct({
              ean: item.id,
              price: item.price,
              quantity: item.quantity
            });
          })
        }
      }
    }
    this.translateService.addLangs(['en', 'fr', 'es']);

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translateService.setDefaultLang(userEnv.defaultLanguage);

    this.userService.getMe().then((response) => {
      if (response) {
        if (userEnv.loginType === 'platform') {
          userEnv.roles = response.profileIds;
        }
        if (response.profileIds[0] !== 'anonymous') {
          userEnv.authorization = true;
        }
      }
    });
  }

  ngOnInit(): void {
    this.initPlatform()
        .then(() => this.initCustomer())
        .then(() => this.initLanguage())
        .then(() => this.initShowroomEnv())
        .then((showroom: Showroom) => this.initStyle(showroom))
        .catch(() => this.router.navigateToError());
  }

  initPlatform() {
    return this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  initShowroomEnv(): Promise<Showroom> {
    return this.showroomService.getShowroom().then((showroom: Showroom) => {
        showroomEnv.showroom = showroom
        showroomEnv.hideWishlist = showroom.settings?.components?.wishlist?.enabled === false;
        showroomEnv.sendEndExperienceWishlist = showroom.settings?.components?.endWishlist?.enabled;
        showroomEnv.private = showroom.settings?.privacy?.private === true;
        showroomEnv.initialized.next(true);
        this.modulesService.getModules().then(modules => {
          modules.forEach(mod => {
            if (mod.name === 'fixturesManager') {
              showroomEnv.fixturesManager = mod.enabled;
            }
            if (mod.name === 'api' && mod.enabled) {
              this.getApiConfig();
            }
          });
        });
        return showroom;
      }
    );
  }

  getApiConfig() {
    this.modulesService.getConfig('api').then(config => {
      if (config?.enabled) {
        showroomEnv.api = config.api.code;
        this.apiExtService.getApiToken().subscribe(token => {
          showroomEnv.apiToken = token;
        });
      }
    });
  }

  initUser() {
    return this.userService.getUser();
  }

  initCustomer() {
    return this.customerService.getCustomer();
  }

  initLanguage() {
    if (userEnv.customer) {
      if (customerEnv?.country) {
        this.translateService.use(customerEnv.country.substring(0, 2).toLowerCase());
      } else {
        this.translateService.use(userEnv.defaultLanguage);
      }
    } else {
      this.translateService.use(userEnv.defaultLanguage);
    }
  }

  initStyle(showroom: Showroom) {
    if (showroom.style) {
      if (showroom.style.primaryColor) {
        const primaryParse = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(showroom.style.primaryColor);
        const primaryRGB = {
          r: parseInt(primaryParse[1], 16),
          g: parseInt(primaryParse[2], 16),
          b: parseInt(primaryParse[3], 16)
        };
        this.elementRef.nativeElement.style.setProperty('--ion-color-primary', showroom.style.primaryColor);
        this.elementRef.nativeElement.style.setProperty('--ion-color-primary-rgb', [primaryRGB.r, primaryRGB.g, primaryRGB.b]);
        this.elementRef.nativeElement.style.setProperty('--ion-color-primary-shade', this.rgdHexToRgbaHex(showroom.style.primaryColor));
        this.elementRef.nativeElement.style.setProperty('--ion-color-primary-tint', showroom.style.primaryColor);
      }
      if (showroom.style.secondaryColor) {
        const secondaryParse = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(showroom.style.secondaryColor);
        const secondaryRGB = {
          r: parseInt(secondaryParse[1], 16),
          g: parseInt(secondaryParse[2], 16),
          b: parseInt(secondaryParse[3], 16)
        };
        this.elementRef.nativeElement.style.setProperty('--ion-color-secondary', showroom.style.secondaryColor);
        this.elementRef.nativeElement.style.setProperty('--ion-color-secondary-rgb', [secondaryRGB.r, secondaryRGB.g, secondaryRGB.b]);
        this.elementRef.nativeElement.style.setProperty('--ion-color-secondary-shade', this.rgdHexToRgbaHex(showroom.style.secondaryColor));
        this.elementRef.nativeElement.style.setProperty('--ion-color-secondary-tint', showroom.style.secondaryColor);
      }
      if (showroom.style.tertiaryColor) {
        const tertiaryParse = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(showroom.style.tertiaryColor);
        const tertiaryRGB = {
          r: parseInt(tertiaryParse[1], 16),
          g: parseInt(tertiaryParse[2], 16),
          b: parseInt(tertiaryParse[3], 16)
        };
        this.elementRef.nativeElement.style.setProperty('--ion-color-tertiary', showroom.style.tertiaryColor);
        this.elementRef.nativeElement.style.setProperty('--ion-color-primary-rgb', [tertiaryRGB.r, tertiaryRGB.g, tertiaryRGB.b]);
        this.elementRef.nativeElement.style.setProperty('--ion-color-tertiary-shade',  this.rgdHexToRgbaHex(showroom.style.tertiaryColor));
        this.elementRef.nativeElement.style.setProperty('--ion-color-tertiary-tint', showroom.style.tertiaryColor);
      }
      if (showroom.style.sidenavColor) {
        this.elementRef.nativeElement.style.setProperty('--ion-toolbar-background', this.rgdHexToRgbaHex(showroom.style.sidenavColor));
        const sidenavParse = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(showroom.style.sidenavColor);
        if ((parseInt(sidenavParse[1], 16) + parseInt(sidenavParse[2], 16) + parseInt(sidenavParse[3], 16)) > 512 ) {
          this.elementRef.nativeElement.style.setProperty('--ion-color-primary-contrast', '#000000');
        } else {
          this.elementRef.nativeElement.style.setProperty('--ion-color-primary-contrast', '#ffffff');
        }
      }
      if (showroom.style.homePageColor) {
        this.elementRef.nativeElement.style.setProperty('--ion-home-page-color', showroom.style.homePageColor);
      }
      if (showroom.style.menuColor) {
        this.elementRef.nativeElement.style.setProperty('--ion-menu-background', this.rgdHexToRgbaHex(showroom.style.menuColor));
      }
    }
  }

  rgdHexToRgbaHex(color) {
    const newColor = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
    const newColorRGB = {
      r: parseInt(newColor[1], 16),
      g: parseInt(newColor[2], 16),
      b: parseInt(newColor[3], 16)
    };
    const newColorRGBShade = {
      r : newColorRGB.r,
      g : newColorRGB.g,
      b : newColorRGB.b,
      a : .80
    };
    return this.rgbaToHex(newColorRGBShade);
  }

  rgbaToHex(rgba) {
    const outParts = [
      rgba.r.toString(16),
      rgba.g.toString(16),
      rgba.b.toString(16),
      Math.round(rgba.a * 255).toString(16).substring(0, 2)
    ];

    // Pad single-digit output values
    outParts.forEach((part, i) => {
      if (part.length === 1) {
        outParts[i] = '0' + part;
      }
    });

    return ('#' + outParts.join(''));
  }
}
