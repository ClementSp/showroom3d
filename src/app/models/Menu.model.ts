export interface ExtendedMenuLink {
    id: string;
    nodeId: string;
    title: string;
    cameraPosition?: { x: number, y: number, z: number };
    level2: boolean;
    hidden: boolean;
    expanded: boolean;
}
