import {Vector3} from './Utils';

export interface ISmartObject {
    id: string;
    smartObjectId: string;
    position: Vector3;
    rotation: Vector3;
    smart_objects_glbModelUrl: string;
    scale: Vector3;
}

export class ISmartObjectAssetData {
    android: string;
    createdAt: number;
    folderIDOnBS: string;
    gcsPath: string;
    smart_objects_glbModelUrl: string;
    id: string;
    ios: string;
    name: string;
    product_version: number;
    smart_objects_thumbnailUrl: string;
    webgl: string;
    windows: string;
}
