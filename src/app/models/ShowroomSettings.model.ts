export interface ShowroomCameraSettings {
    maxY: number;
    minY: number;
}

export interface ShowroomMapSettings {
    url: string;
}

export interface ShowroomEntranceSettings {
    position: {x: number, y: number, z: number};
    rotation: {x: number, y: number, z: number};
}

export interface ShowroomPresentationSettings {
    type: ShowroomPresentationSettingsType;
    data: any;
}

export enum ShowroomPresentationSettingsType {
    URL_VIDEO = 'url-video',
    URL_IMAGE = 'url-image',
    VIDEO = 'custom-video',
    IMAGE = 'custom-image',
    MAP = 'map'
}

export interface ShowroomComponentsSettings {
    welcomePage: {
        enabled: boolean;
        backgroundUrl: string;
        secondText: boolean;
    };
    wishlist: {
        enabled: boolean;
        email: {
            enabled: boolean;
            toClient: boolean;
            toOwner: boolean;
            others: Array<string>;
        }
    };
    endWishlist: {
        enabled: boolean;
    };
    inventory: {
        enabled: boolean;
        nbItemForWarningMessage: any;
    };
    zoom: {
        hide: boolean;
    };
}

export interface ShowroomSpritesSettings {
    size: number;
    videoPoi: string;
    productPoi: string;
    pdfPoi: string;
    imagePoi: string;
    chatPoi: string;
    sweepLink: string;
    sweep: string;
    logo: ShowroomSpritesLogoSettings;
    showroomImage: string;
}

export interface ShowroomSpritesLogoSettings {
    light: string;
    dark: string;
}

export interface ShowroomStyleSettings {
    menuColor: string;
    primaryColor: string;
    homePageColor: string;
    tertiaryColor: string;
    secondaryColor: string;
    sidenavColor: string;
}

export interface Privacy {
    private: boolean;
}

export interface ShowroomAudioSettings {
    url: string;
}

export interface ShowroomSettings {
    presentation?: ShowroomPresentationSettings; // heavy changes
    style?: ShowroomStyleSettings; // heavy changes
    camera?: ShowroomCameraSettings;
    entrance?: ShowroomEntranceSettings;
    components?: ShowroomComponentsSettings;
    map?: ShowroomMapSettings;
    privacy?: Privacy;
    menuTitle?: string;
    sprites?: ShowroomSpritesSettings; // heavy changes
    presentationVideoUrl: string;
    presentationVideoAutoStart: boolean;
    hideMenu: boolean;
    audio?: ShowroomAudioSettings;
    editable: boolean;
    model3dCustomizable: boolean;
    logoUrl: string;
}
