import 'firebase/storage';
import {Portal, ShowroomPortal} from './PortalMarker';
import {IFixture} from './fixture.model';
import {Euler, Vector3} from './Utils';
import mergeDeep from '../Utils/mergeDeep.helper';
import {ShowroomSettings} from './ShowroomSettings.model';
import * as THREE from 'three';
import {IProduct} from './product.model';
import {ISmartObject} from "./smartObject.model";

interface ShowroomUrl {
    customerUrl: string;
    customer: string;
}

export interface MenuLink {
    id: string;
    title: string;
    cameraPosition: { x: number, y: number, z: number };
    cameraRotation: { x: number, y: number, z: number };
    level2: boolean;
}

export interface MenuJumpLink {
    name: string;
    id: string;
    showroomPortal: ShowroomPortal;
}

export interface Camera {
    position: {x: number, y: number, z: number};
    rotation: {x: number, y: number, z: number};
    id: string;
}

export interface SpritePath {
    path: string;
    type: string;
    data?: any;
}

export interface Poi {
    data: any;
    nodeId?: string;
    position: Vector3;
    scale: Vector3;
    rotation: Euler;
    tip: string;
    id: string;
    type: string;
    sprite?: SpritePath;
    size: number;
    spriteColor: string;
}

export const defaultPoiShapeParameters = {
    highlightOnHover: true,
    opacity: .15,
    opacityHover: .3,
    color: '#ffffff'
};
export interface ShapePoi extends Poi {
    isShapePoi: true;
}
export interface HiddenSweep {
    hiddenFrom: Array<string>;
    removed: boolean;
    id: string;
}

export interface ShowroomStyle {
    primaryColor: string;
    secondaryColor: string;
    tertiaryColor: string;
    homePageColor: string;
    sidenavColor: string;
    menuColor: string;
}

export class Showroom {

    // tslint:disable-next-line:variable-name
    name: string;
    id: string;
    showroomId: string;
    urls: Array<ShowroomUrl> = [];
    settings: ShowroomSettings;
    menu: Array<MenuLink> = [];
    menuLink: Array<MenuJumpLink> = [];
    cameras: Array<Camera> = [];
    fixtures: Array<IFixture> = [];
    products: Array<IProduct> = [];
    smartObjects: Array<ISmartObject> = [];
    pois: Array<Poi> = [];
    shapePois: Array<ShapePoi>;
    hiddenSweeps: Array<HiddenSweep> = [];
    nodeList: Array<any> = [];
    portals: Array<any> = [];
    lights: Array<any> = [];
    style: ShowroomStyle;
    rawData: string;


    constructor(data: any) {
        if (!data || !data.name || !data.id) {
            throw new Error('Fail to create new showroom: missing mandatory property.');
        }
        if (data._kuzzle_info) {
            delete data._kuzzle_info;
        }
        this.initializeSettings();
        this.copyData(data);
        this.initializeNodeList();
    }

    getCurrentPositionVector3(): Vector3 {
        return this.settings.entrance.position;
    }

    getNodeById(id: string) {
        return this.nodeList.find((node) => node.id === id);
    }

    private initializeNodeList() {
        this.nodeList.forEach((node) => {
            node.position = new THREE.Vector3(
                node.position.x,
                node.position.y,
                node.position.z
            );
            node.floorPosition = new THREE.Vector3(
                node.floorPosition.x,
                node.floorPosition.y,
                node.floorPosition.z
            );
        });
    }

    private copyData(data: any) {
        Object.assign(this, mergeDeep(Object.assign({}, this), data));
    }

    private initializeSettings() {
        this.settings = {
            camera: {
                minY: 0,
                maxY: Math.PI,
            },
            components: {
                welcomePage: {
                    enabled: true,
                    backgroundUrl: undefined,
                    secondText: false
                },
                wishlist: {
                    enabled: true,
                    email : {
                        enabled: false,
                        toClient: false,
                        toOwner: false,
                        others: []
                    }
                },
                endWishlist: {
                    enabled: true,
                },
                inventory: {
                    enabled: false,
                    nbItemForWarningMessage: 0
                },
                zoom: {
                    hide: false
                }
            },
            entrance: {
                position: {
                    x: undefined,
                    y: undefined,
                    z: undefined
                },
                rotation: {
                    x: undefined,
                    y: undefined,
                    z: undefined
                }
            },
            map: {
                url: undefined
            },
            sprites: {
                size: 0.1,
                videoPoi: null,
                productPoi: null,
                pdfPoi: null,
                imagePoi: null,
                sweepLink: null,
                chatPoi: null,
                sweep: null,
                logo: null,
                showroomImage: null,
            },
            presentationVideoAutoStart: false,
            presentationVideoUrl: '',
            presentation: {
                type: null,
                data: null
            },
            menuTitle: 'Sections',
            privacy: {
                private: false
            },
            hideMenu: false,
            editable: false,
            model3dCustomizable: false,
            logoUrl: null
        };
    }
}
