import {autoserialize, autoserializeAs, serializeAs, deserialize} from 'cerialize';


// tslint:disable-next-line:class-name
interface info {
  size: any;
  extension: any;
  isRGB: any;
  height: any;
  width: any;
}

export class Texture {

  @autoserializeAs('id') public id: string;
  @autoserializeAs('name') public name: string;
  @autoserializeAs('gcsPath') public gcsPath: string;
  @autoserializeAs('type') public type: string;
  @autoserializeAs('description') public description: string;
  @autoserializeAs('texture_categoryId') public categoryId: string;
  @autoserializeAs('info') public info: info;

  [key: string]: any

  public static OnDeserialized(instance: Texture, json: any): void {
    instance.id = json._id;
  }
}
