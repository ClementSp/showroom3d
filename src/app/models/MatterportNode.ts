import * as THREE from 'three';

export interface MatterportNode {
    id: string;
    imgPath: string;
    position: THREE.Vector3;
    cameraPosition: { x: number; y: number; z: number };
    floorPosition: THREE.Vector3;
    neighbors: string[];
}
