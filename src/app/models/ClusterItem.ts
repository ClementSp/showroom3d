interface IData {
    product: any;
    viewData: any;
    cssClass: string;
}

export interface IClusterItem {
    name: string;
    picture1: string;
    picture2: string;
    ean: string;
}

export default class ClusterItem implements IClusterItem {
    name: string;
    ean: string;
    picture1: string;
    picture2: string;

    imagesUrls = new Array<string>();

    viewData: any;
    cssClass: string;

    constructor() {
    }
}
