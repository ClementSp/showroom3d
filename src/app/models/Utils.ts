import * as THREE from 'three';
import {AbstractControl, ValidatorFn} from '@angular/forms';
import {ElementRef} from '@angular/core';

export interface Vector3 {
    x: number;
    y: number;
    z: number;
}

export interface Vector2 {
    x: number;
    y: number;
}

export interface Euler {
    _x: number;
    _y: number;
    _z: number;
}

export interface SphericalCoords {
    lon: number;
    lat: number;
}

export default class Utils {
    static vector3toLonLat(vector3: THREE.Vector3) {
        vector3.normalize();

        let lng = Math.atan2(-vector3.z, -vector3.x) - Math.PI / 2;

        if ( lng < - Math.PI ) {
            lng += Math.PI * 2;
        }

        let lat = Math.asin(vector3.y);

        lng = 180 * lng / Math.PI;
        lat = 180 * lat / Math.PI;

        lng = Math.round(lng * 100) / 100;
        lat = Math.round(lat * 100) / 100;
        return [ lng, lat ];
    }

    static lonLatToVector3(lng, lat) {
        const out = new THREE.Vector3();

        lng = lng / 180 * Math.PI;
        lat = lat / 180 * Math.PI;


        out.set(
            Math.cos( lat ) * Math.sin( lng ),
            Math.sin( lat ),
            - Math.cos( lat ) * Math.cos( lng )
        );
        return out;
    }

    static transformToYoutubeEmbedUrl(url) {
        if (url.includes('youtube') || url.includes('youtu.be') && !url.includes('/embed/')) {
            // tslint:disable-next-line:no-shadowed-variable
            const getIdFromYoutubeUr = (url: string) => {
                const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
                const match = url.match(regExp);

                return (match && match[2].length === 11)
                    ? match[2]
                    : null;
            };
            return 'https://www.youtube.com/embed/' + getIdFromYoutubeUr(url);
        }
        return url;
    }

    static urlValidator: ValidatorFn = (control: AbstractControl) => {
        let validUrl = false;
        const reg = new RegExp(/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i);

        try {
            if (reg.test(control.value)) {
                const url = new URL(control.value);
                validUrl = true;
            }
        } catch {
            validUrl = false;
        }

        return validUrl ? null : { invalidUrl: true };
    }

    static getSphericalCoordsFromVector3(point: THREE.Vector3): SphericalCoords {
        const coords = this.vector3toLonLat(point);
        let lon = coords[0];
        const lat = coords[1];

        /*
         *   Longitude of THREE.JS is: 0 to 360 degrees around z axe but
         *   on click, vector3toLonLat() return longitude: 0 -> 180 / -180 -> 0.
         */

        if (lon < 0) {
            lon = 180 + (180 - Math.abs(lon));
        }
        lon = (lon - 180) % 180;
        return {lon, lat};
    }

    static degToRad = (degrees) => degrees * (Math.PI / 180);

    static generateThumbnailFromVideo(url: string, el: ElementRef): Promise<string> {
        const canvas = el.nativeElement;
        let status;

        if (!url) {
            throw new Error('No video url');
        }

        return fetch(url).then((response) => {
            status = response.status;
            return response.blob();
        }).then((blob) => {
            const video: HTMLVideoElement = document.createElement('video');
            const context: CanvasRenderingContext2D = canvas.getContext('2d');

            return new Promise<string>((resolve, reject) => {
                if (status !== 200) {
                    reject();
                }
                canvas.addEventListener('error',  reject);
                video.addEventListener('error',  reject);
                video.addEventListener('loadeddata', () => {
                    video.currentTime = 2;
                });
                video.addEventListener('canplay', () => {
                    canvas.width = video.videoWidth;
                    canvas.height = video.videoHeight;
                    context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
                    resolve(canvas.toDataURL());
                });
                if (blob && blob.type) {
                    video.setAttribute('type', blob.type);
                }
                video.preload = 'auto';
                video.src = window.URL.createObjectURL(blob);
                video.load();
            });
        });
    }
}
