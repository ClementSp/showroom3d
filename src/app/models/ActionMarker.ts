
import {Poi} from './showroom.model';
import * as THREE from 'three';
import Utils from './Utils';

enum ActionMarkerState {
    NORMAL
}

export default class ActionMarker extends THREE.Sprite {
    public poi: Poi;
    private states = new Map<ActionMarkerState, THREE.Texture>();
    private currentState = ActionMarkerState.NORMAL;

    constructor(poi: Poi, scale: number, normalTexture: THREE.Texture) {
        super(new THREE.SpriteMaterial({
            transparent: true,
            color: 0xffffff
        }));

        // const position = Utils.lonLatToVector3(poi.lon, poi.lat);
        const position = poi.position;
        if (!scale) {
            scale = 0.1;
        }

        this.currentState = ActionMarkerState.NORMAL;
        this.states.set(ActionMarkerState.NORMAL, normalTexture);
        this.updateTexture();
        this.poi = poi;
        this.position.set(-position.x, position.y, -position.z);
        // this.position.set(-position.x * 100, position.y * 100, -position.z * 100);
        this.scale.set(scale, scale, scale);
        (this.material as any).depthFunc = THREE.AlwaysDepth;
        this.material.depthWrite = true;
        this.material.depthTest = true;
        this.name = 'ActionMarker';
        this.visible = true;
    }

    set state(state: ActionMarkerState) {
        this.currentState = state;
        this.updateTexture();
    }

    private updateTexture() {
        (this.material as any).map = this.states.get(this.currentState);
    }
}
