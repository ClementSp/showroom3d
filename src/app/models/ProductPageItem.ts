import {Type} from '@angular/core';

export default class ProductPageItem {
    constructor(public component: Type<any>, public data: any) {
    }
}
