import {autoserialize, deserializeAs} from 'cerialize';
import {ACustomProduct} from "./CustomProduct";

export class ProductTreeNode {
  @autoserialize public name: string;
  @autoserialize public id: string;
  @deserializeAs(ProductTreeNode) public children: Array<ProductTreeNode>;
  @autoserialize public parentId: string;
  @autoserialize public products: Array<ACustomProduct>;
  @autoserialize public validAt: Date;
  @autoserialize public validendAt: Date;
  @autoserialize public picture: string;
  @autoserialize public file: any;
  @autoserialize public createdAt: Date;
}
