import {merge, Observable, Subject, Subscription} from 'rxjs';

export default class EngineEvent {

    private proxy$ = new Subject<any>();
    private subscription: Subscription;
    private triggers = new Array<Observable<any>>();

    constructor(eventFn: any) {
        this.proxy$.asObservable().subscribe((data) => {
            eventFn(data);
        });
    }

    addTrigger(trigger: Observable<any>) {
        this.triggers.push(trigger);
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        this.subscription = merge(...this.triggers).subscribe((values) => {
            this.proxy$.next(values);
        });
    }
}
