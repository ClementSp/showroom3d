import {EditorMode} from './EditorMode';
import {Subject} from 'rxjs';

export default interface ToolbarFeature {
    iconName: string;
    editorMode: EditorMode;
    description: string;
    event?: Subject<any>;
}
