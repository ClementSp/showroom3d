import {Poi} from './showroom.model';

export interface IProduct {
    name: string;
    ean: number;
    productUrl: string;
    imgUrl1: string;
    imgUrl2: string;
    openInNewTab: boolean;
}

export interface IVideo {
    url: string;
    language: string;
    openInNewTab: boolean;
}

export default class ClusterData  {
    name = '';
    ean = '';
    products: Array<IProduct> = [];

    constructor(poi: Poi) {
        if (poi && poi.data) {
            if (poi.data.name) {
                this.name = poi.data.name;
            }
            if (poi.data.ean) {
                this.ean = poi.data.ean;
            }
            if (poi.data?.products) {
                poi.data?.products.forEach((item) => {
                    this.products.push(item as IProduct);
                });
            }
        }
    }

    pushProduct(productUrl: string, imgUrl1: string, imgUrl2: string, name: string, ean: number, openInNewTab: boolean) {
        const product = {
            productUrl,
            imgUrl1,
            imgUrl2,
            name,
            ean,
            openInNewTab
        };
        this.products.push(product);
        return product;
    }
}
