import {Vector3} from './Utils';

export interface IFixtureAttribute {
    data: any;
    type: string;
    target: string;
}

export interface IFixture {
    id: string;
    fixtureId: string;
    position: Vector3;
    rotation: Vector3;
    gltf: string;
    scale: Vector3;
    attributes: Array<IFixtureAttribute>;
}

export class IFixtureAssetData {
    android: string;
    createdAt: number;
    folderIDOnBS: string;
    gcsPath: string;
    gltf: string;
    id: string;
    ios: string;
    name: string;
    product_version: number;
    thumbnailUrl: string;
    webgl: string;
    windows: string;
}
