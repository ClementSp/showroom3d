import {Texture} from './texture.model';
import {uuid4} from '@capacitor/core/dist/esm/util';

export class ProductAdjustment {
    id = uuid4();
    meshes: string[] = [];
    config: Configuration = undefined;
    name = '';
    option: Option;
}
export class Selection {
    id: string;
    type: string;
    options: Option [] = [];
    name: string;
}

export class Option {
    id = uuid4();
    name = '';
    components: string [] = [];
    configurations: Configuration[] = [];
}

export class Configuration {
    name = '';
    config: TextureConfig[] = [];
}

export class TextureConfig {
    texture: Texture;
    meshes: string[] = [];
}
export class Mesh {
    code: string;
    name: string;
    editable: boolean;
    visible: boolean;
}


export interface ImageMediaInfos {
    blob: any;
    media: ProductMedia;
    url: string;
}

export interface VideoMediaInfos {
    blob: any;
    media: ProductMedia;
    url: string;
    name: string;
}

export interface PdfMediaInfos {
    blob: any;
    media: ProductMedia;
    url: string;
}

export interface ProductMedia {
    type: string;
    ext: string;
    id: string;
}

export interface IBaseProduct {
    product_ean: string;
    name: string;
    code: string;
    ean: string;
    id?: string;
    label: string;
    description?: string;
    language?: string;
    medias: Array<ProductMedia>;
    product_glbModelUrl: string;
    sketchfabUrl: string;
    productSheetUrl: string;
    product_size: string;
    product_color: string;
}

export abstract class ACustomProduct implements IBaseProduct{
    id = '';
    code = '';
    ean = '';
    name = '';
    label = '';
    product_ean = '';
    product_price: number;
    sale_price: number;
    language = '';
    description = '';
    details = '';
    medias = [];
    twitterUrl = '';
    facebookUrl = '';
    instagramUrl = '';
    sketchfabUrl = '';
    website = '';
    linkedinUrl = '';
    youtubeUrl = '';
    contact = '';
    qrCode = '';
    productSheetUrl = '';
    currency = '';
    product_size: string;
    product_color: string;
    subLabel: string;
    product_glbModelUrl: string;
    ownerId: string;
    product_thumbnailUrl: string;
    inventory_quantity: string;
    poiId: any;
    selections: Selection [];
    picture: string;
}

export default class CustomProduct extends ACustomProduct  {
    constructor(productData?: ACustomProduct) {
        super();
        if (productData) {
            Object.keys(productData).forEach((key: string) => {
                this[key] = productData[key];
            });
        }
    }
}
