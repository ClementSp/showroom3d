import * as THREE from 'three';

export enum MovementMarkerState {
    UNKNOWN = -1,
    NORMAL,
    REMOVED,
    HIDDEN
}

export default class MovementMarker extends THREE.Mesh {
    public callback: any;
    private currentState: MovementMarkerState;
    private states = new Map<MovementMarkerState, THREE.Texture>();
    public panoramaId: string;

    constructor(offset: THREE.Vector3,
                forward: THREE.Vector3,
                scale: number,
                normalTexture: THREE.Texture,
                removedTexture: THREE.Texture,
                hiddenTexture: THREE.Texture,
                panoramaId: string) {
        super(new THREE.PlaneGeometry(1, 1, 1, 1), new THREE.MeshBasicMaterial({
            transparent: true,
            color: 0xffffff
        }));
        this.currentState = MovementMarkerState.NORMAL;
        this.states.set(MovementMarkerState.NORMAL, normalTexture);
        this.states.set(MovementMarkerState.REMOVED, removedTexture);
        this.states.set(MovementMarkerState.HIDDEN, hiddenTexture);
        this.updateTexture();
        this.position.set(-offset.x, offset.y + 0.05, -offset.z);
        this.lookAt(forward);
        this.rotateOnAxis(new THREE.Vector3(1, 0, 0), -1.57);
        this.scale.set(scale, scale, scale);
        this.name = 'MovementMarker';
        this.panoramaId = panoramaId;
        this.visible = true;
        // (this.material as any).depthFunc = THREE.AlwaysDepth;
    }

    set state(state: MovementMarkerState) {
        this.currentState = state;
        this.updateTexture();
    }

    private updateTexture() {
        (this.material as any).map = this.states.get(this.currentState);
    }
}
