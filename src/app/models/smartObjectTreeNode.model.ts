import {autoserialize, deserializeAs} from 'cerialize';
import {ISmartObjectAssetData} from './smartObject.model';

export class SmartObjectTreeNode {
  @autoserialize public name: string;
  @autoserialize public id: string;
  @deserializeAs(SmartObjectTreeNode) public children: Array<SmartObjectTreeNode>;
  @autoserialize public parentId: string;
  @autoserialize public smartObjects: Array<ISmartObjectAssetData>;
  @autoserialize public validAt: Date;
  @autoserialize public validendAt: Date;
  @autoserialize public picture: string;
  @autoserialize public createdAt: Date;
}
