import {Vector3} from './Utils';

export interface IProductAttribute {
    data: any;
    type: string;
    target: string;
}

export interface IProduct {
    id: string;
    productId: string;
    ean: string;
    position: Vector3;
    rotation: Vector3;
    product_glbModelUrl: string;
    scale: Vector3;
    configurations?: any [];
    saveConfigurations?: any [];
    components?: any [];
    poiId: string;
    template: string;
}

export interface IProductAssetData {
    android: string;
    createdAt: number;
    folderIDOnBS: string;
    gcsPath: string;
    product_glbModelUrl: string;
    inventory_quantity: string;
    id: string;
    ios: string;
    product_ean: string;
    name: string;
    code: string;
    product_version: number;
    product_thumbnailUrl: string;
    webgl: string;
    windows: string;
    medias: any[];
    components: any[];
}
