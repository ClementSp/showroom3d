import {autoserializeAs} from 'cerialize';

export class Link {
    public id: string;
    @autoserializeAs('url')  public url: string;
    @autoserializeAs('host')  public host: string;
    @autoserializeAs('parameters')  public parameters: string[];
    @autoserializeAs('appId')  public appId: string;
    @autoserializeAs('appType')  public appType: string;
    @autoserializeAs('validity')  public validity: Date;
    @autoserializeAs('customerId')  public customerId: string;

    public static OnDeserialized(instance: Link, json: any): void {
        instance.id = json._id;
    }

    constructor(data?: any) {
        Object.assign(this, data);
    }
}
