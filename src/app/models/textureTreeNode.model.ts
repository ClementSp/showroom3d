import { autoserialize, deserializeAs } from 'cerialize';
import {Texture} from './texture.model';
export class TextureTreeNode {
  @autoserialize public name: string;
  @autoserialize public id: string;
  @deserializeAs(TextureTreeNode) public children: Array<TextureTreeNode>;
  @autoserialize public parentId: string;
  @autoserialize public textures: Array<Texture>;
  @autoserialize public validAt: Date;
  @autoserialize public validendAt: Date;
  @autoserialize public picture: string;
  @autoserialize public createdAt: Date;
}
