import { autoserialize, deserializeAs } from 'cerialize';
import {IFixture} from "./fixture.model";

export class FixtureTreeNode {
  @autoserialize public name: string;
  @autoserialize public id: string;
  @deserializeAs(FixtureTreeNode) public children: Array<FixtureTreeNode>;
  @autoserialize public parentId: string;
  @autoserialize public fixtures: Array<IFixture>;
  @autoserialize public validAt: Date;
  @autoserialize public validendAt: Date;
  @autoserialize public picture: string;
  @autoserialize public createdAt: Date;
}
