import * as THREE from 'three';
import Utils, {Vector3, Vector2, Euler} from './Utils';

export enum PortalType {
    SHOWROOM_LINK = 'showroom_link',
    SWEEP_LINK = 'sweep_link'
}

export interface SweepPortal extends Portal {
    sweepId: string;
    type: PortalType.SWEEP_LINK;
}

export interface ShowroomPortal extends Portal {
    link: {
        target: string;
        sweepId: string;
    };
    type: PortalType.SHOWROOM_LINK;
}

export interface Portal {
    type: PortalType;
    position: Vector3;
    scale: Vector3;
    rotation: Euler;
    tip: string;
    id: string;
    nodeId: string;
}

export enum PortalMarkerState {
    UNKNOWN = -1,
    NORMAL
}

export default class PortalMarker extends THREE.Sprite {
    public portal: SweepPortal | ShowroomPortal;
    private states = new Map<PortalMarkerState, THREE.Texture>();
    private currentState: PortalMarkerState;

    constructor(portal: SweepPortal | ShowroomPortal, scale: number, normalTexture: THREE.Texture) {
        super(new THREE.SpriteMaterial({
            transparent: true,
            color: 0xffffff
        }));

        this.portal = portal;
        this.currentState = PortalMarkerState.NORMAL;
        this.states.set(PortalMarkerState.NORMAL, normalTexture);
        this.updateTexture();
        this.scale.set(scale, scale, scale);
        (this.material as any).depthFunc = THREE.AlwaysDepth;
        this.material.depthWrite = true;
        this.material.depthTest = true;
        this.name = 'PortalMarker';
        this.material.rotation = this.deg2rad(-portal.rotation);
        this.visible = true;
    }

    set matRotation(deg: number) {
        this.material.rotation = this.deg2rad(-deg);
    }

    private deg2rad(degrees)
    {
        return degrees * (Math.PI / 180);
    }

    private updateTexture() {
        (this.material as any).map = this.states.get(this.currentState);
    }
}
