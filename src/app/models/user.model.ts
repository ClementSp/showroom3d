import {autoserialize, autoserializeAs} from 'cerialize';
import * as _ from 'lodash';

export class User {
  @autoserializeAs('_id') public id: string;
  @autoserialize public fullname: string;
  @autoserialize public tenant: string;
  @autoserialize public firstname: string;
  @autoserialize public lastname: string;
  @autoserialize public language: string;
  @autoserialize public country: string;
  @autoserialize public usernameForSearch: string;
  @autoserialize public email: string;
  @autoserialize public phone: string;
  @autoserialize public pictureUrl: string;
  @autoserialize public validated: boolean;
  @autoserialize public authorization: boolean;
  @autoserialize public lastLogin: Date;
  @autoserialize public createdAt: number;
  @autoserialize public profileIds: string[];
  @autoserialize public resetPasswordToken: string;
  @autoserialize public importSmartObjectConfig: string[];
  @autoserialize public importProductConfig: string[];
  @autoserialize public importShowroomConfig: string[];
  @autoserialize public importPriceConfig: string[];
  @autoserialize public importDescriptionConfig: string[];
  @autoserialize public importShopConfig: string[];
  @autoserialize public defaultShowroom: string;
  @autoserialize public strongAuthentication: boolean;
  @autoserialize public toValidateCode: boolean;
  @autoserialize public paymentToken: string;
  public userData;
  [key: string]: any

  static toString(user: User, defaultValue: string = ''): string {
    if (!user) {
      return defaultValue;
    }
    const sortedRepresentative: string[] = [user.fullname, user.email, user.id];
    return _.find(sortedRepresentative, (value) => !!value);
  }

  static OnDeserialized(instance: User, json: any): void {
    instance.fullname = json.fullname ? json.fullname : `${json.firstname} ${json.lastname}`;
  }

}
