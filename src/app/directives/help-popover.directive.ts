import {Directive, Input} from '@angular/core';
import {GetHoverTimeDirective, HoverTimeDirectiveData} from './get-hover-time.directive';
import {PopoverController, Platform} from '@ionic/angular';
import {HelpPopoverComponent} from '../components/help-popover/help-popover.component';
import {userEnv} from '../../environments/user.env';

@Directive({
  selector: '[appHelpPopover]'
})
export class HelpPopoverDirective extends GetHoverTimeDirective {

  @Input() helpMessage: string;

  constructor(public popoverController: PopoverController,
              private platform: Platform) {
    super();

    this.hoverTime.subscribe((data: HoverTimeDirectiveData) => {
      const progress = data.value;
      if (progress > 100) {
        this.presentPopover(data.event);
        this.onExit();
      }
    });

    this.cancel.subscribe(() => {
      this.popoverController.getTop().then((top: HTMLIonPopoverElement) => {
        if (top) {
          this.popoverController.dismiss();
        }
      });
    });
  }

  async presentPopover(ev: any) {
    if (!this.platform.is('mobile')) {
      const popover = await this.popoverController.create({
        component: HelpPopoverComponent,
        componentProps: {
          helpMessage: this.helpMessage,
        },
        cssClass: 'help-popover',
        showBackdrop: false,
        event: ev,
        mode: 'ios'
      });
      return await popover.present();
    }
  }
}
