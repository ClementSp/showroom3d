import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appComponentHolder]'
})
export class ComponentHolderDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }
}
