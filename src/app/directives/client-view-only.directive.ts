import {Directive, ElementRef, TemplateRef, ViewContainerRef} from '@angular/core';
import {EditorService} from '../services/editor.service';
import {userEnv} from '../../environments/user.env';

@Directive({
  selector: '[appClientViewOnly]'
})
export class ClientViewOnlyDirective {

  constructor(private element: ElementRef,
              private templateRef: TemplateRef<any>,
              public editorService: EditorService,
              private viewContainer: ViewContainerRef) {
    this.editorService.isEditorActivated.subscribe((isEditorActivated: boolean) => {
      if (!userEnv.authorization || !isEditorActivated) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    });
  }
}
