import {Directive, EventEmitter, HostListener, Output} from '@angular/core';
import {interval, Observable, Subject} from 'rxjs';
import {filter, takeUntil, tap} from 'rxjs/operators';
import {Event} from '@angular/router';

export interface HoverTimeDirectiveData {
    value: number;
    event: Event;
}

@Directive({
    selector: '[getHoverTime]'
})
export class GetHoverTimeDirective {

    @Output() hoverTime = new EventEmitter<HoverTimeDirectiveData>();

    state = new Subject<'cancel' | 'start'>();

    cancel: Observable<string>;

    constructor() {
        this.cancel = this.state.pipe(
            filter(v => v === 'cancel'),
            tap(v => {
                this.hoverTime.emit({
                    value: 0,
                    event: undefined
                });
            })
        );
    }

    @HostListener('mouseleave', ['$event'])
    onExit() {
        this.state.next('cancel');
    }

    @HostListener('mouseenter', ['$event'])
    onHover(event: any) {
        this.state.next('start');

        const n = 100;

        interval(n).pipe(
            takeUntil(this.cancel),
            tap(v => {
                this.hoverTime.emit({
                    value: v * n,
                    event: event
                });
            })
        ).subscribe();
    }
}
