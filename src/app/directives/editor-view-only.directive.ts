import {Directive, ElementRef, TemplateRef, ViewContainerRef} from '@angular/core';
import {EditorService} from '../services/editor.service';
import {userEnv} from '../../environments/user.env';

@Directive({
  selector: '[appEditorViewOnly]'
})
export class EditorViewOnlyDirective {

  constructor(private element: ElementRef,
              private templateRef: TemplateRef<any>,
              public editorService: EditorService,
              private viewContainer: ViewContainerRef) {
    this.editorService.isEditorActivated.subscribe((value) => {
      if (!value || userEnv.loginType !== 'platform' || this.isPersonalShopper()) {
        this.viewContainer.clear();
      } else if (viewContainer.length === 0){
        this.viewContainer.createEmbeddedView(this.templateRef);
      }
    });
  }

  private isPersonalShopper() {
    const roles = userEnv.roles ?? [];

    return roles.find(role => role === userEnv.tenant + '-profile-personal-shopper');
  }
}
