import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {AbstractControl, FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Directive({
  selector: '[appInputError]'
})
export class InputErrorDirective implements OnInit {

  @Input() label: string;
  @Input() control: AbstractControl;

  @Input() set errors(errors: any) {
    this.setText();
  }

  constructor(private element: ElementRef,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.setText();
  }

  private setText() {
    if (this.control.hasError('required')) {
      this.getTranslation('{0} {1}', [this.label, 'required']);
      this.setRed();
    } else if (this.control.hasError('pattern')) {
      this.getTranslation('{0}: {1}', [this.label, 'invalid characters']);
      this.setRed();
    } else if (this.control.hasError('invalidUrl')) {
      this.getTranslation('{0} {1}', [this.label, 'wrong format']);
      this.setRed();
    } else if (this.control.hasError('minlength')) {
      this.getTranslation('{0} {1} ' + this.control.errors.minlength.requiredLength, [this.label, 'minimum length is']);
      this.setRed();
    } else if (this.control.hasError('maxlength')) {
      this.getTranslation('{0} {1} ' + this.control.errors.minlength.requiredLength, [this.label, 'maximum length is']);
      this.setRed();
    } else {
      this.getTranslation('{0}', [this.label]);
      this.setDefault();
    }
  }

  private getTranslation(context: string, toTranslates: Array<string>) {
    toTranslates.forEach((toTranslate, index) => {
      this.translateService.get(toTranslate).subscribe((translated) => {
        context = context.replace('{' + index + '}', translated);
        this.element.nativeElement.innerText = context;
      });
    });
  }

  private setRed() {
    this.element.nativeElement.style.color = '#f04141';
  }

  private setDefault() {
    this.element.nativeElement.style.color = 'black';
  }
}
