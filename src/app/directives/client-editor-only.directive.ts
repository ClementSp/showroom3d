import {Directive, ElementRef, TemplateRef, ViewContainerRef} from '@angular/core';
import {EditorService} from '../services/editor.service';
import {userEnv} from '../../environments/user.env';
import {ShowroomsService} from '../services/showrooms.service';

@Directive({
  selector: '[appClientEditorOnly]'
})

export class ClientEditorOnlyDirective {
  private isEditor: boolean;

  constructor(private element: ElementRef,
              private templateRef: TemplateRef<any>,
              public editorService: EditorService,
              public showroomService: ShowroomsService,
              private viewContainer: ViewContainerRef) {
    this.editorService.isEditorActivated.subscribe((value) => {
      this.showroomService.getShowroom().then(showroom => {
        if (showroom.settings.editable && !value && userEnv.jwt ) {
          this.isEditor = true;
          this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.isEditor = false;
          this.viewContainer.clear();
        }
      });
    });
  }
   isClientEditor() {
    return this.isEditor;
  }
}
