### Run the application

ionic serve

#### Link to the official ionic documentation

https://ionicframework.com/docs/cli/commands/serve

### Deployment

Both of these commands must be executed at the root of the project. 

#### Development

./deploy.sh dev

#### Production

./deploy.sh prod

#### Manually

1) ionic build --configuration=<production | development>
2) firebase deploy --only hosting:<prod | dev>

(1) will compiles web assets and prepares them for deployment in the www folder<br />
(2) using the firebase.json, it will use the previously generated folder www as the public folder and deploy it on the hosting firebase service
